@extends('layouts.master')

@section('title', trans('menu.ms'))

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">{{ trans('menu.ms') }}</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="{{ route('dashboard') }}">{{ trans('menu.dashboard') }}</a>
                            </li>
                            <li>
                                <a>{{ trans('menu.produksi') }}</a>
                            </li>
                            <li class="active">
                                {{ trans('menu.ms') }}
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            @if(Session::has('successMessage'))
            <div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-check-all"></i>
                <strong>{{ Session::get('successMessage') }}.</strong>
            </div>
            @endif

            @if(Session::has('errorMessage'))
            <div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-block-helper"></i>
                <strong>{{ Session::get('errorMessage') }}.</strong>
            </div>
            @endif
            
            <div class="row">
                <div class=" col-md-12">
                    <div class="panel panel-border panel-success">
                        <div class="panel-heading">
                             <div class="col-md-12">
                                <div class="col-md-2">
                                    <h3 class="panel-title">{{ trans('fields.tally_no') }}</h3>
                                </div>
                                <div class="col-md-3">
                                    <h3 class="panel-title">: {{ $data->tally_no }}</h3>
                                </div>
                                <div class="col-md-2">
                                    <h3 class="panel-title">{{ trans('fields.rendemen') }}</h3>
                                </div>
                                <div class="col-md-5">
                                    <h3 class="panel-title">: {{ $data->volumes }}</h3>
                                </div>
                            </div><br>
                        </div>
                        
                        

                        <div class="panel-body">
                            <div class="row">
                                <form class="form-horizontal" role="form" method="post" action="{{ route('ms-save') }}">
                                    {{ csrf_field() }}
                                    <div class="col-sm-12 data-table-toolbar text-right">
                                        <div class="form-group">
                                            <button type="button" class="btn btn-primary waves-effect waves-light btn-sm" data-toggle="modal" data-target="#dtmutasi"><i class="fa fa-plus-circle"></i> {{ trans('fields.add-msss') }} </button>
                                             <a id="clear-details" href="{{ url('produksi/wip') }}" class="btn btn-sm btn-danger">
                                                <i class="fa fa-reply"></i> {{ trans('fields.back') }} 
                                            </a>


                                                @can('access', [$resource, 'add'])
                                                <a href="{{ route('afkir-kering-add', ['id' => $model->tally_id]) }}" data-toggle="tooltip" class="btn btn-primary waves-effect waves-light btn-sm" >
                                                    <i class="fa fa-plus"></i> {{ trans('fields.inputafkir') }}
                                                </a>
                                                @endcan
                                        </div>
                                        <div class="row">
                                            <table class="table table-bordered table-hover table-striped m-0">
                                                <thead>
                                                    <tr>
                                                        <th width="5%" class="text-center">{{ trans('fields.num') }}</th>
                                                        <th width="15%" class="text-center">{{ trans('fields.wood-type') }}</th>
                                                        <th width="10%" class="text-center">{{ trans('fields.t') }}</th>
                                                        <th width="10%" class="text-center">{{ trans('fields.l') }}</th>
                                                        <th width="10%" class="text-center">{{ trans('fields.p') }}</th>
                                                        <th width="10%" class="text-center">{{ trans('fields.pcs') }}</th>
                                                        <th width="10%" class="text-center">{{ trans('fields.m3') }}</th>
                                                        <th width="15%" class="text-center">{{ trans('fields.action') }}</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $no = ($models->currentPage() - 1) * $models->perPage() + 1; ?>
                                                    @foreach($models as $model)
                                                    <tr>
                                                        <th scope="row" style="text-align: center;">{{ $no++ }}</th>
                                                       
                                                        <td>{{ $model->jenis_kayu }}</td>
                                                        <td>{{ $model->tinggi }}</td>
                                                        <td>{{ $model->lebar }}</td>
                                                        <td>{{ $model->panjang }}</td>
                                                        <td>{{ $model->pcs }}</td>
                                                        <td>{{ $model->volume }}</td>
                                                        <td class="text-center">
                                                        
                                                             @can('access', [$resource, 'update'])
                                                            <a data-target="#dtmutasi{{ $model->isi_ms_id }}" data-toggle="modal"  class="btn btn-xs btn-warning" data-original-title="Edit">
                                                                <i class="fa fa-pencil"></i>
                                                            </a>
                                                            @endcan

                                                             @can('access', [$resource, 'update'])
                                                                <a  data-toggle="modal" data-target="#deletems{{ $model->isi_ms_id }}" class="btn btn-xs btn-danger" >
                                                                    <i class="fa fa-remove"></i> 
                                                                </a>
                                                                <div id="deletems{{ $model->isi_ms_id }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                                <h4 class="modal-title">{{ trans('fields.deletenotally') }}</h4>
                                                                            </div>
                                                                            <div class="modal-body">       
                                                                                <h5>Apakah anda yakin ingin menghapus Tally Ini ??</h5>
                                                                            </div>

                                                                            <div class="modal-footer">
                                                                                <input type="hidden" name="id" class="form-control" value="{{ $model->isi_ms_id }}">
                                                                                <a href="{{ route('ms-destroy', ['id' => $model->isi_ms_id]) }}" data-toggle="tooltip" class="btn btn-danger waves-effect" >Yes</a>
                                                                                <button type="button" class="btn btn-default waves-effect waves-light" data-dismiss="modal">No</button>
                                                                            </div> 
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endcan


                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                                
                                            </table>
                                            
                                        </div>
                                    </div>
                                </form>
                            </div> 
                        </div>

                          @foreach($models as $model)
                        @can('access', [$resource, 'update'])

                          <div id="dtmutasi{{ $model->isi_ms_id }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form role="form" method="post" action="{{ route('ms-saveisi') }}">
                                        {{ csrf_field() }}
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">{{ trans('fields.add-ms') }}</h4>
                                        </div>
                                        <div class="modal-body">
                                      
                                            <input type="hidden" name="tally_id" class="form-control" value="{{ $model->tally_id }}">
                                                 <input type="hidden" name="isi_ms_id" class="form-control" value="{{ count($errors) > 0 ? old('isi_ms_id') : $model->isi_ms_id }}">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="field-1" class="control-label">{{ trans('fields.wood-type') }}</label>
                                                        <select class="form-control" id="jenis_kayu_id" name="jenis_kayu_id">
                                                            <?php $kayuId = count($errors) > 0 ? old('jenis_kayu_id') : $model->jenis_kayu_id; ?>
                                                            <option value="">- {{ trans('fields.change') }} {{ trans('fields.wood-type') }} -</option>
                                                            @foreach($woodOptions as $wood)
                                                            <option value="{{ $wood->jenis_kayu_id }}" {{ $wood->jenis_kayu_id == $kayuId ? 'selected' : '' }}>{{ $wood->jenis_kayu }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="tinggi" class="control-label">{{ trans('fields.high') }}</label>
                                                        <input type="number" class="form-control" id="tinggi" name="tinggi" value="{{ count($errors) > 0 ? old('tinggi') : $model->tinggi }}">
                                                    @if($errors->has('tinggi'))
                                                        <span class="help-block">{{ $errors->first('tinggi') }}</span>
                                                    @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="lebar" class="control-label">{{ trans('fields.width') }}</label>
                                                        <input type="number" class="form-control" id="lebar" name="lebar" value="{{ count($errors) > 0 ? old('lebar') : $model->lebar }}">
                                                    @if($errors->has('lebar'))
                                                        <span class="help-block">{{ $errors->first('lebar') }}</span>
                                                    @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="panjang" class="control-label">{{ trans('fields.length') }}</label>
                                                        <input type="number" class="form-control" id="panjang" name="panjang" value="{{ count($errors) > 0 ? old('panjang') : $model->panjang }}">
                                                    @if($errors->has('panjang'))
                                                        <span class="help-block">{{ $errors->first('panjang') }}</span>
                                                    @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="pcs" class="control-label">{{ trans('fields.pcs') }}</label>
                                                        <input type="number" class="form-control" id="pcs" name="pcs" value="{{ count($errors) > 0 ? old('pcs') : $model->pcs }}">
                                                    @if($errors->has('pcs'))
                                                        <span class="help-block">{{ $errors->first('pcs') }}</span>
                                                    @endif
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-info waves-effect waves-light">Save changes</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div><!-- /.modal -->

                        @endcan
                        @endforeach

                        <div id="dtmutasi" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form role="form" method="post" action="{{ route('ms-saveisi') }}">
                                        {{ csrf_field() }}
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">{{ trans('fields.add-ms') }}</h4>
                                        </div>
                                        <div class="modal-body">
                                      
                                            <input type="hidden" name="tally_id" class="form-control" value="{{ $model->tally_id }}">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="field-1" class="control-label">{{ trans('fields.wood-type') }}</label>
                                                        <select class="form-control" id="jenis_kayu_id" name="jenis_kayu_id">
                                                            <?php $kayuId = count($errors) > 0 ? old('jenis_kayu_id') : $model->jenis_kayu_id; ?>
                                                            <option value="">- {{ trans('fields.change') }} {{ trans('fields.wood-type') }} -</option>
                                                            @foreach($woodOptions as $wood)
                                                            <option value="{{ $wood->jenis_kayu_id }}" {{ $wood->jenis_kayu_id == $kayuId ? 'selected' : '' }}>{{ $wood->jenis_kayu }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="tinggi" class="control-label">{{ trans('fields.high') }}</label>
                                                        <input type="number" class="form-control" id="tinggi" name="tinggi" >
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="lebar" class="control-label">{{ trans('fields.width') }}</label>
                                                        <input type="number" class="form-control" id="lebar" name="lebar" >
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="panjang" class="control-label">{{ trans('fields.length') }}</label>
                                                        <input type="number" class="form-control" id="panjang" name="panjang">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="pcs" class="control-label">{{ trans('fields.pcs') }}</label>
                                                        <input type="number" class="form-control" id="pcs" name="pcs">
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div><!-- /.modal -->
                         
                    </div>


                </div>
            </div>
        </div>
        @endsection