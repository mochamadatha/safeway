<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHdRombelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hd_rombel', function (Blueprint $table) {
            $table->increments('hd_rombel_id');
            $table->integer('kelas_id');
            $table->integer('guru_id');
            $table->integer('tahun_ajaran_id');
            $table->string('batas_masuk', 10);
            $table->string('batas_pulang', 10);
            $table->boolean('status')->default(true);
            $table->integer('created_by');
            $table->dateTime('created_date');
            $table->integer('modified_by')->nullable();
            $table->dateTime('modified_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hd_rombel');
    }
}
