<?php

namespace App\Service\DivisionContainer;

class AgentService
{
    public static function getActiveAgent()
    {
        return \DB::table('mst_agent')
            ->where('status', '=', true)
            ->orderBy('nama_agent')
            ->get();
    }
}
