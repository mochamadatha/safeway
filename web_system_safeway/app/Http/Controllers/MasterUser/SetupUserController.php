<?php

namespace App\Http\Controllers\MasterUser;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\Master\Role;
use App\Service\Master\RoleService;
use App\Service\Master\StatusService;
use DB;

class SetupUserController extends Controller
{
    const URL       = 'master-user/setup-user';
    const RESOURCE  = 'setup-user';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        if ($request->isMethod('post')) {
            $request->session()->put('filters', $request->all());
            return redirect(self::URL.'?page=1');
        } elseif (empty($request->get('page'))) {
            $request->session()->forget('filters');
        }
        $filters = $request->session()->get('filters');
        $query = DB::table('users')
                    ->select('users.*', 'mst_role.nama_role', 'mst_user_status.nama_status')
                    ->join('mst_role', 'mst_role.role_id', '=', 'users.role_id')
                    ->join('mst_user_status', 'mst_user_status.status_id', '=', 'users.status_id')
                    ->orderBy('name');

        if (!empty($filters['status']) || !$request->session()->has('filters')) {
            $query->where('users.status', '=', true);
        } else {
            $query->where('users.status', '=', false);
        }

        if (!empty($filters['name'])) {
            $query->where('users.name', 'like', '%'.$filters['name'].'%');
        }

        if (!empty($filters['username'])) {
            $query->where('users.username', 'like', '%'.$filters['username'].'%');
        }

        if (!empty($filters['email'])) {
            $query->where('users.email', 'like', '%'.$filters['email'].'%');
        }

        if (!empty($filters['role'])) {
            $query->where('users.role_id', 'like', '%'.$filters['role'].'%');
        }

        return view('master-user/setup-user.index',[
            "models"        => $query->paginate(10),
            "filters"       => $filters,
            "roleOptions"   => RoleService::getActiveRole(),
            "statusOptions" => StatusService::getActiveStatus(),
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function add(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        } 

        $model           = new User();
        $model->status   = true;
        
        return view('master-user/setup-user.add',[
            "model"         => $model,
            "roleOptions"   => RoleService::getActiveRole(),
            "statusOptions" => StatusService::getActiveStatus(),
        ]);

    }

    public function edit(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }  

        $model = User::find($id);

        if ($model===null) {
            abort('404');
        }

        return view('master-user/setup-user.add',[
            "model"         => $model,
            "roleOptions"   => RoleService::getActiveRole(),
            "statusOptions" => StatusService::getActiveStatus(),
        ]);
    }

    public function save(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add']) && !\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        } 

        $id     = intval($request->get('id'));
        
        $this->validate($request, [
            'name'            => 'required|string|max:255',
            'username'        => 'required|max:255|unique:users,username,'.$id.',id',
            'role'            => 'required',
            'status_user'     => 'required',
        ]);

        DB::beginTransaction();
        try {

            if(empty($id)){
                $this->validate($request, [
                    'password' => 'required|string|min:6',
                ]);
                $model                  = new User();
                $model->username        = $request->get('username');
            }else{
                $model                  = User::find($id);
            }

            $model->name        = $request->get('name');
            $model->email       = $request->get('email');
            $model->role_id     = $request->get('role');
            $model->status      = !empty($request->get('status')) ? true : false;
            $model->status_id   = $request->get('status_user');
            
            if(!empty($request->get('password'))){
                $model->password    = bcrypt($request->get('password'));
            }

            $now = new \DateTime();

            $foto = $request->file('foto');
            if ($foto !== null) {
                $fotoName   = md5($now->format('YmdHis')) . "." . $foto->guessExtension();
                $foto->move(\Config::get('app.paths.foto-user'), $fotoName);
                $model->foto = $fotoName;
            }

            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('menu.setup-user').' '.$request->get('name')])
                );

        } catch (\Exception $e) {
             DB::rollback();
             $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('menu.setup-user').' '.$request->get('name')])
                );
        }

        return redirect(route('setup-user-index'));
    }
}
