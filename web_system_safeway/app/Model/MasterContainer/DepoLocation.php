<?php

namespace App\Model\MasterContainer;

use Illuminate\Database\Eloquent\Model;

class DepoLocation extends Model
{
    protected $table        = 'mst_depolocation';
    protected $primaryKey   = 'depolocation_id';
    public $timestamps      = false;
}
