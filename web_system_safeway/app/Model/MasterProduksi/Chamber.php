<?php

namespace App\Model\MasterProduksi;

use Illuminate\Database\Eloquent\Model;

class Chamber extends Model
{
    protected $table        = 'mst_chamber';
    protected $primaryKey   = 'chamber_id';
    public $timestamps      = false;
}
