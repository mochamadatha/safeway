<?php 
// use App\Service\Master\SupplierService; 
// use App\Model\MasterProduksi\Supplier;
// use App\Model\Produksi\HdLpb;


list($titlefooter, $date) = explode("#", $title);
?>

<br/><br/>
<table id="header" width="100%" height="100%" border="1px">
	<tr>
		<td rowspan="2" width="20%" >
			
		</td>
		<th rowspan="2" width="46%"  align="center " style="font-size: 12px; font-weight: bold;" >LAPORAN PENERIMAAN BAHAN SAWN TIMBER</th>
		<td width="34%" height="15" style="font-size: 9px; font-weight: bold;" >
			<table>
				<tr>
					<td width="30%" align-text="center"> NO. LPB</td>
					<td width="5%"> :</td>
					<td width="65%"> {{ $model->nomor_lpb }} </td>
				</tr>
			</table>
		</td>
	</tr>

	<tr>
		<td width="34%"   style="font-size: 9px; font-weight: bold;" >
			<table>
				<tr>
					<td width="30%"> TANGGAL</td>
					<td width="5%"> :</td>
					<td width="65%"> {{ date('d-m-Y', strtotime($model->tanggal_kedatangan)) }} </td>
				</tr>
			</table>
		</td>
	</tr>
    <tr>
		<td width="66%" colspan="2" rowspan="2"  style="font-size: 9px; font-weight: bold;" >
			<table>
				<tr>
					<td width="30%"> SUPPLIER</td>
					<td width="5%"> :</td>
					<td width="65%"> {{ $model->pic }} - {{ $model->plat_mobil }} </td>
				</tr>
			</table>
		</td>
		<td width="34%"  height="15" style="font-size: 9px; font-weight: bold;" >
			<table>
				<tr>
					<td width="30%"> NO. PO</td>
					<td width="5%"> :</td>
					<td width="65%">{{ $model->nomor_po }} </td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
	
		<td  width="34%" height="15" style="font-size: 9px; font-weight: bold;" >
			<table>
				<tr>
					<td width="30%"> NO. SJ</td>
					<td width="5%"> :</td>
					<td width="65%"> GRS </td>
				</tr>
			</table>
		</td>
	</tr>
	<tr  bgcolor="#808B96" style="color: white; font-weight: bold; font-size: 10px;" align="center">
	
		<td rowspan="2" width="4%" height="16">No</td>
		<td rowspan="2" width="16%" height="16">jenis kayu</td>
		<td rowspan="2" width="10%" height="16">No. Tally Sheet</td>
		<td colspan="3" width="18%" height="16">Ukuran</td>
		<td colspan="2" width="18%" height="16">Jumlah Diterima</td>
		<td rowspan="2" width="17%" height="16">Harga Satuan</td>
		<td rowspan="2" width="17%" height="16">Total Harga</td>
		
	</tr>
	<tr bgcolor="#808B96" style="color: white; font-weight: bold; font-size: 10px;" align="center">
		<td height="15">T</td>
		<td height="15">L</td>
		<td height="15">P</td>
		<td height="15">PCS</td>
		<td height="15">m3</td>
	</tr>
   
</table>
<hr/>