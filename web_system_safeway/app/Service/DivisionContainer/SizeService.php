<?php

namespace App\Service\DivisionContainer;

class SizeService
{
    public static function getActiveSize()
    {
        return \DB::table('mst_size')
            ->where('status', '=', true)
            ->orderBy('size')
            ->get();
    }
}
