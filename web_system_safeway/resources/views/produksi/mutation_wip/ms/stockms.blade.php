@extends('layouts.master')

@section('title', trans('menu.ms'))

@section('header')
        <!-- <link href="{{ asset('plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/> -->
        <!-- <link href="{{ asset('plugins/datatables/buttons.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/> -->
        <!-- <link href="{{ asset('plugins/datatables/fixedHeader.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/> -->
        <!-- <link href="{{ asset('plugins/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/> -->
        <!-- <link href="{{ asset('plugins/datatables/scroller.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/> -->
        <!-- <link href="{{ asset('plugins/datatables/dataTables.colVis.css') }}" rel="stylesheet" type="text/css"/> -->
        <link href="{{ asset('plugins/datatables/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
        <!-- <link href="{{ asset('plugins/datatables/fixedColumns.dataTables.min.css') }}" rel="stylesheet" type="text/css"/> -->
@endsection

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">{{ trans('menu.stock-ms') }}</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="{{ route('dashboard') }}">{{ trans('menu.dashboard') }}</a>
                            </li>
                            <li>
                                <a>{{ trans('menu.produksi') }}</a>
                            </li>
                            <li class="active">
                                {{ trans('menu.stock-ms') }}
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            @if(Session::has('successMessage'))
            <div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-check-all"></i>
                <strong>{{ Session::get('successMessage') }}.</strong>
            </div>
            @endif

            @if(Session::has('errorMessage'))
            <div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-block-helper"></i>
                <strong>{{ Session::get('errorMessage') }}.</strong>
            </div>
            @endif
            
            <div class="row">
                <div class=" col-md-12">
                    <div class="panel panel-border panel-success">
                        <div class="panel-heading">
                            </div>
                                 <div class="panel-body">
                                    <div class="row">
                                <form class="form-horizontal" role="form" method="post" action="">
                                    {{ csrf_field() }}

                 
                                    <div class="col-md-6">
                                        

                                         <div class="form-group {{ $errors->has('role') ? 'has-error' : '' }}">
                                            <label class="col-md-4 control-label">{{ trans('fields.wood-type') }}</label>
                                            <div class="col-md-8">
                                                <select class="form-control" id="role" name="role">
                                                    <option value="">{{ trans('fields.all') }}</option>
                                                @foreach($woodOptions as $role)
                                                    <option value="{{ $role->jenis_kayu_id }}" {{ !empty($filters['role']) && $filters['role'] == $role->jenis_kayu_id ? 'selected' : ''}} >{{ $role->jenis_kayu }}</option>
                                                @endforeach
                                                </select>
                                                @if($errors->has('role'))
                                                    <span class="help-block">{{ $errors->first('role') }}</span>
                                                @endif
                                            </div>
                                        </div>


                                        <div class="form-group {{ $errors->has('tinggi') ? 'has-error' : '' }}">
                                            <label class="col-md-4 control-label">{{ trans('fields.high') }}</label>
                                            <div class="col-md-8">
                                                <input type="text" name="tinggi" class="form-control" value="{{ !empty($filters['tinggi']) ? $filters['tinggi'] : '' }}">
                                                @if($errors->has('tinggi'))
                                                    <span class="help-block">{{ $errors->first('tinggi') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group {{ $errors->has('lebar') ? 'has-error' : '' }}">
                                            <label class="col-md-4 control-label">{{ trans('fields.width') }}</label>
                                            <div class="col-md-8">
                                                <input type="text" name="lebar" class="form-control" value="{{ !empty($filters['lebar']) ? $filters['lebar'] : '' }}">
                                                @if($errors->has('lebar'))
                                                    <span class="help-block">{{ $errors->first('lebar') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group {{ $errors->has('panjang') ? 'has-error' : '' }}">
                                            <label class="col-md-4 control-label">{{ trans('fields.length') }}</label>
                                            <div class="col-md-8">
                                                <input type="text" name="panjang" class="form-control" value="{{ !empty($filters['panjang']) ? $filters['panjang'] : '' }}">
                                                @if($errors->has('panjang'))
                                                    <span class="help-block">{{ $errors->first('panjang') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        
                                       
                                    </div>
                                    <div class="col-md-12">
                                        <p class="text-muted font-14 m-b-20 pull-right">
                                          
                                            <button type="submit" class="btn btn-info waves-effect w-md waves-light"><i class="fa fa-search"></i> {{ trans('fields.search') }}</button>
                                        </p>
                                    </div>
                                </form>
                            </div>
                        
                        

                        <div class="panel-body">
                            <div class="row">
                                <form class="form-horizontal" role="form" method="post" action="{{ route('ms-save') }}">
                                    {{ csrf_field() }}
                                    <div class="col-sm-12 data-table-toolbar text-right">
                                        

                              

                                        <div class="form-group">

                                             @can('access', [$resource, 'add'])
                                            <button type="button" class="btn btn-primary waves-effect w-md waves-light" data-toggle="modal" data-target="#dtms"><i class="fa fa-plus-circle"></i> {{ trans('fields.add-data-ms') }} </button>
                                            @endcan

                                            @can('access', [$resource, 'add'])
                                            <a href="{{ route('reproses-ms-index') }}" class="btn btn-primary waves-effect w-md waves-light"><i class="fa fa-plus"></i> {{ trans('fields.stock-reproses-ms') }}</a>
                                             @endcan

                                            @can('access', [$resource, 'add'])
                                            <a href="{{ route('proses-ms-index') }}" class="btn btn-primary waves-effect w-md waves-light"><i class="fa fa-plus"></i> {{ trans('fields.history-proses-ms') }}</a>
                                             @endcan
                                           

                                           
                                             @can('access', [$resource, 'add'])
                                            <a data-target="#infoms" data-toggle="modal"   class="btn btn-primary waves-effect w-md  waves-light" data-original-title="fa-plus">
                                            <i class="fa fa-info"></i> {{ trans('fields.info') }}</a>
                                            </a>
                                            @endcan

                                             @can('access', [$resource, 'excel'])
                                            <a type="button" class="btn btn-primary waves-effect w-md waves-light" data-toggle="modal" data-target="#dtexcel" data-original-title="Edit"  method="post" enctype="multipart/form-data"></i> {{ trans('fields.import-data-ms') }}</a>
                                            @endcan

                                            @can('access', [$resource, 'add'])
                                            <a href="{{ route('ms-export-excel') }}" class="btn btn-success waves-effect w-md waves-light"> {{ trans('fields.export-excel') }}</a>
                                            @endcan
                                            
                                        </div>
                                        <div class="row">
                                            <table class="table table-bordered table-hover table-striped m-0">
                                                <thead>
                                                    <tr>
                                                        <th width="5%" class="text-center">{{ trans('fields.num') }}</th>
                                                        <th width="15%" class="text-center">{{ trans('fields.wood-type') }}</th>
                                                        <th width="15%" class="text-center">{{ trans('fields.modified_date') }}</th>
                                                        <th width="5%" class="text-center">{{ trans('fields.t') }}</th>
                                                        <th width="5%" class="text-center">{{ trans('fields.l') }}</th>
                                                        <th width="5%" class="text-center">{{ trans('fields.p') }}</th>
                                                        <th width="5%" class="text-center">{{ trans('fields.pcs') }}</th>
                                                        <th width="10%" class="text-center">{{ trans('fields.m3') }}</th>
                                                        <th width="20%" class="text-center">{{ trans('fields.action') }}</th>
                                                        
                                                    </tr>
                                                </thead>
                                               <tbody>
                                                    <?php $no = ($models->currentPage() - 1) * $models->perPage() + 1; ?>
                                                    @foreach($models as $model)
                                                    <tr>
                                                        <th width="5%" class="text-center" scope="row" style="text-align: center;">{{ $no++ }}</th>
                                                       
                                                        <td width="15%" class="text-center">{{ $model->jenis_kayu }}</td>
                                                        <td width="15%" class="text-center">{{ $model->modified_date }}</td>
                                                        <td width="5%" class="text-center">{{ $model->tinggi }}</td>
                                                        <td width="5%" class="text-center">{{ $model->lebar }}</td>
                                                        <td width="5%" class="text-center">{{ $model->panjang }}</td>
                                                        <td width="5%" class="text-center">{{ $model->pcs }}</td>
                                                        <td width="5%" class="text-center">{{ $model->volume }}</td>
                                                        <td width="20%" class="text-center">
                                                            

                                                              @can('access', [$resource, 'add'])
                                                            <a  data-id="{{ $model->jumlah_ms_id }}" data-label="{{ $model->jenis_kayu }}" data-toggle="modal" data-target="#modal-plus" class="btn btn-xs btn-primary cancel-action" data-original-title="Edit">
                                                                {{ trans('fields.plus') }}</i>
                                                            </a>
                                                            {{-- <a  data-toggle="modal" data-target="#dtstockms{{ $model->jumlah_ms_id }}" class="btn btn-xs btn-warning" data-original-title="Edit">
                                                                <i class="fa fa-plus"></i>
                                                            </a> --}}
                                                            <!-- modal -->
                                                            
                                                            <!-- /.modal -->
                                                            @endcan


                                                               @can('access', [$resource, 'add'])
                                                            <a  data-id="{{ $model->jumlah_ms_id }}" data-label="{{ $model->jenis_kayu }}" data-toggle="modal" data-target="#modal-min" class="btn btn-xs btn-danger waves-light cancel-action" data-original-title="Edit">
                                                              {{ trans('fields.min') }} </i>
                                                            </a>
                                                            {{-- <a  data-toggle="modal" data-target="#dtstockms{{ $model->jumlah_ms_id }}" class="btn btn-xs btn-warning" data-original-title="Edit">
                                                                <i class="fa fa-plus"></i>
                                                            </a> --}}
                                                            <!-- modal -->
                                                            
                                                            <!-- /.modal -->
                                                            @endcan

                                                            @can('access', [$resource, 'add'])
                                                            <a  data-id="{{ $model->jumlah_ms_id }}" data-label="{{ $model->jenis_kayu }}" data-toggle="modal" data-target="#modal-cancel" class="btn btn-xs btn-warning cancel-action" data-original-title="Edit">
                                                                 {{ trans('fields.reproses-data') }}</i>
                                                            </a>
                                                            {{-- <a  data-toggle="modal" data-target="#dtstockms{{ $model->jumlah_ms_id }}" class="btn btn-xs btn-warning" data-original-title="Edit">
                                                                <i class="fa fa-plus"></i>
                                                            </a> --}}
                                                            <!-- modal -->
                                                            
                                                            <!-- /.modal -->
                                                            @endcan

                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                                
                                            </table>
                                            <div class="data-table-toolbar">
                                                {!! $models->render() !!}
                                            </div>
                                            
                                        </div>
                                    </div>
                                </form>
                            </div> 
                        </div>
                </div>
            </div>
        </div>





        <div id="modal-cancel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form id="form-cancel" role="form" method="post" action="{{ route('ms-savetakems') }}">
                        {{ csrf_field() }}

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title" id="cancel-text">{{ trans('fields.taken-ms') }}</h4>
                        </div>



                        <div class="modal-body">

                            <div class="row">
                                  <div class="col-md-6">
                                    <label>{{ trans('fields.min-stock-ms') }}</label>
                                        <div>
                                            <div class="form-group">
                                                 <input type="number" class="form-control" name="pcs">
                                                 <input type="hidden" id="jumlah_ms_id" name="jumlah_ms_id" value="">
                                            </div>
                                        </div>
                                    </div>
                            </div>

                            
                        </div>
                       
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close
                            </button>
                            <button id="btn-cancel-po" type="submit" class="btn btn-info waves-effect waves-light">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


         <div id="modal-plus" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form id="form-cancel" role="form" method="post" action="{{ route('ms-saveplusms') }}">
                        {{ csrf_field() }}

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title" id="cancel-text">{{ trans('fields.plus-ms') }}</h4>
                        </div>



                        <div class="modal-body">

                            <div class="row">
                                <div class="col-md-6">
                                                    <label>{{ trans('fields.date-plus-ms') }}</label>
                                                        <div>
                                                            <div class="form-group">
                                                                <input type="date" class="form-control" placeholder="mm/dd/yyyy" id="datepicker-autoclose" name="date_min_ms">
                            
                                                            </div><!-- input-group -->
                                                        </div>
                                                    </div>

                                  <div class="col-md-6">
                                    <label>{{ trans('fields.min-stock-ms') }}</label>
                                        <div>

                                            <div class="form-group">
                                                 <input type="number" class="form-control" name="pcs">
                                                 <input type="hidden" id="jumlah_ms_plus" name="jumlah_ms_id" value="">
                                            </div>
                                        </div>
                                    </div>
                            </div>

                            
                        </div>
                       
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close
                            </button>
                            <button id="btn-cancel-po" type="submit" class="btn btn-info waves-effect waves-light">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>



        <div id="modal-min" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form id="form-cancel" role="form" method="post" action="{{ route('ms-saveminms') }}">
                        {{ csrf_field() }}

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title" id="cancel-text">{{ trans('fields.min-ms') }}</h4>
                        </div>



                        <div class="modal-body">

                            <div class="row">
                                <div class="col-md-6">
                                                    <label>{{ trans('fields.date-min-ms') }}</label>
                                                        <div>
                                                            <div class="form-group">
                                                                <input type="date" class="form-control" placeholder="mm/dd/yyyy" id="datepicker-autoclose" name="date_min_ms">
                            
                                                            </div><!-- input-group -->
                                                        </div>
                                                    </div>

                                  <div class="col-md-6">
                                    <label>{{ trans('fields.min-stock-ms') }}</label>
                                        <div>

                                            <div class="form-group">
                                                 <input type="number" class="form-control" name="pcs">
                                                 <input type="hidden" id="jumlah_ms_min" name="jumlah_ms_id" value="">
                                            </div>
                                        </div>
                                    </div>
                            </div>

                            
                        </div>
                       
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close
                            </button>
                            <button id="btn-cancel-po" type="submit" class="btn btn-info waves-effect waves-light">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>







       

         <!-- <div id="modal-min" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form id="form-cancel" role="form" method="post" action="{{ route('ms-saveminms') }}">
                        {{ csrf_field() }}

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title" id="cancel-text">{{ trans('fields.min-ms') }}</h4>
                        </div>



                        <div class="modal-body">

                            <div class="row">
                                  
                                    <div class="col-md-6">
                                    <label>{{ trans('fields.taken-stock-ms') }}</label>
                                        <div>
                                            <div class="form-group">
                                                 <input type="number" class="form-control" name="pcs">
                                                 <input type="hidden" id="jumlah_ms_id" name="jumlah_ms_id" value="">
                                            </div>
                                        </div>
                                    </div>
                            </div>

                            
                        </div>
                       
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close
                            </button>
                            <button id="btn-cancel-po" type="submit" class="btn btn-info waves-effect waves-light">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
 -->
        <div id="dtexcel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form role="form" method="post" action="{{ route('ms-import-excel') }}" method="post" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">{{ trans('fields.add-tally') }}</h4>
                                        </div>
                                 
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label for="file" class="col-md-3 control-label">Import</label>
                                                <div class="col-md-6">
                                                    <input type="file" id="file" name="file" class="form-control" autorequired>
                                                    <span class="help-block with-error"></span>
                                                </div>
                                            </div>
                                           
                                        </div><br>


                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                        </div>
                                      
                                    </form>
                                </div>
                            </div>
                        </div>



         <div id="infoms" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <!-- <form id="form-cancel" role="form" method="post" action="{{ route('raft-result-savematerialms') }}">
                                        {{ csrf_field() }} -->

                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title" id="cancel-text">{{ trans('fields.info') }}</h4>
                                        </div>



                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="card-box table-responsive">
                                                        <table id="datatable" class="table table-striped table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th>{{ trans('fields.num') }}</th>
                                                                <th>{{ trans('fields.wood-type') }}</th>
                                                                <th>{{ trans('fields.high') }}</th>
                                                                <th>{{ trans('fields.width') }}</th>
                                                                <th>{{ trans('fields.length') }}</th>
                                                                <th>{{ trans('fields.pcs') }}</th>
                                                                <th>{{ trans('fields.m3') }}</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php $no = 1;//($models->currentPage() - 1) * $models->perPage() + 1; ?>
                                                            @foreach($info as $in)
                                                            <tr>
                                                                <td>{{ $no++ }}</td>
                                                                <td>{{ $in->jenis_kayu }}</td>
                                                                <td>{{ $in->tinggi }}</td>
                                                                <td>{{ $in->lebar }}</td>
                                                                <td>{{ $in->panjang }}</td>
                                                                <td>{{ $in->pcs }}</td>
                                                                <td>{{ $in->volume }}</td>
                                                            </tr>
                                                            @endforeach
                                                            </tbody>
                                                        </table>

                                                         <table>
                                                          <?php
                            
                                                                $total_pcs = 0; 
                                                                $total_volume = 0; 
                                                                $total_tally = 0 ;
                                                            
                                                            ?>
                                                            @foreach($models as $view)
                                                            <?php 
                                                                $total_pcs += $view->pcs;
                                                                $total_volume += $view->volume;
                                            
                                                                
                                                            ?>
                                                            @endforeach


                                                            <tr>
                                                                <td></td>
                                                                <td>Total Pcs</td>
                                                                <td> : </td>
                                                                <td>{{ $total_pcs }}</td>
                                                            </tr>
                                                             <tr>
                                                                <td></td>
                                                                <td>Total Volume</td>
                                                                <td> : </td>
                                                                <td>{{ $total_volume }}</td>
                                                            </tr>  
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>  
                                        </div>

                                </div>
                            </div>
                        </div>


                          <div id="dtms" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form role="form" method="post" action="{{ route('ms-save') }}">
                                        {{ csrf_field() }}
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">{{ trans('fields.add-tally-sheet') }}</h4>
                                        </div>
                                        <div class="modal-body">
                                      
                                           
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="field-1" class="control-label">{{ trans('fields.wood-type') }}</label>
                                                        <select class="form-control" id="jenis_kayu_id" name="jenis_kayu_id">
                                                            <option value="">- {{ trans('fields.change') }} {{ trans('fields.wood-type') }} -</option>
                                                            @foreach($woodOptions as $wood)
                                                            <option value="{{ $wood->jenis_kayu_id }}" {{ !empty($filters['wood']) && $filters['wood'] == $wood->jenis_kayu_id ? 'selected' : ''}} >{{ $wood->jenis_kayu }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                               
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="tinggi" class="control-label">{{ trans('fields.high') }}</label>
                                                        <input type="number" class="form-control" id="tinggi" name="tinggi">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="lebar" class="control-label">{{ trans('fields.width') }}</label>
                                                        <input type="number" class="form-control" id="lebar" name="lebar">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="panjang" class="control-label">{{ trans('fields.length') }}</label>
                                                        <input type="number" class="form-control" id="panjang" name="panjang">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="pcs" class="control-label">{{ trans('fields.pcs') }}</label>
                                                        <input type="number" class="form-control" id="pcs" name="pcs">
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div><!-- /.modal -->
                     
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

        @section('script')
        @parent
        <script type="text/javascript">
            $(document).on('ready', function(){

                $('.cancel-action').on('click', function() {
                    $("#jumlah_ms_id").val($(this).data('id'));
                    // $("#cancel-text").html('kdasmaskdma sdklmasd alsdmkasldkmasdkm ' + $(this).data('label') + '?');
                    $("#cancel-text").html('{{ trans('fields.taken-ms') }} ' + $(this).data('label') + '?');
                    clearFormCancel()
                });

                $('#btn-cancel-po').on('click', function(event) {
                    event.preventDefault();
                    if ($('textarea[name="reason"]').val() == '') {
                        $(this).parent().parent().parent().addClass('has-error');
                        $(this).parent().parent().parent().find('span.help-block').html('Reason is required');
                        return
                    } else {
                        clearFormCancel()
                    }

                    $('#form-cancel').trigger('submit');
                });
            });

            var clearFormCancel = function() {
                $('#form-cancel').removeClass('has-error');
                $('#form-cancel').find('span.help-block').html('');
            };
        </script>
        @endsection

          @section('script')
        @parent
        <script type="text/javascript">
            $(document).on('ready', function(){

                $('.cancel-action').on('click', function() {
                    $("#jumlah_ms_plus").val($(this).data('id'));
                    // $("#cancel-text").html('kdasmaskdma sdklmasd alsdmkasldkmasdkm ' + $(this).data('label') + '?');
                    $("#cancel-text").html('{{ trans('fields.taken-ms') }} ' + $(this).data('label') + '?');
                    clearFormCancel()
                });

                $('#btn-cancel-po').on('click', function(event) {
                    event.preventDefault();
                    if ($('textarea[name="reason"]').val() == '') {
                        $(this).parent().parent().parent().addClass('has-error');
                        $(this).parent().parent().parent().find('span.help-block').html('Reason is required');
                        return
                    } else {
                        clearFormCancel()
                    }

                    $('#form-cancel').trigger('submit');
                });
            });

            var clearFormCancel = function() {
                $('#form-cancel').removeClass('has-error');
                $('#form-cancel').find('span.help-block').html('');
            };
        </script>
        @endsection


         @section('script')
        @parent
        <script type="text/javascript">
            $(document).on('ready', function(){

                $('.cancel-action').on('click', function() {
                    $("#jumlah_ms_min").val($(this).data('id'));
                    // $("#cancel-text").html('kdasmaskdma sdklmasd alsdmkasldkmasdkm ' + $(this).data('label') + '?');
                    $("#cancel-text").html('{{ trans('fields.taken-ms') }} ' + $(this).data('label') + '?');
                    clearFormCancel()
                });

                $('#btn-cancel-po').on('click', function(event) {
                    event.preventDefault();
                    if ($('textarea[name="reason"]').val() == '') {
                        $(this).parent().parent().parent().addClass('has-error');
                        $(this).parent().parent().parent().find('span.help-block').html('Reason is required');
                        return
                    } else {
                        clearFormCancel()
                    }

                    $('#form-cancel').trigger('submit');
                });
            });

            var clearFormCancel = function() {
                $('#form-cancel').removeClass('has-error');
                $('#form-cancel').find('span.help-block').html('');
            };
        </script>
        @endsection




        @section('script')
    <!-- jQuery  -->
        <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables/dataTables.bootstrap.js') }}"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#datatable').dataTable();
            });
        </script>
        @endsection