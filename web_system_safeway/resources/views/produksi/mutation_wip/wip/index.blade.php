@extends('layouts.master')

@section('title', trans('menu.wip'))

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">{{ trans('menu.wip') }}</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="{{ route('dashboard') }}">{{ trans('menu.dashboard') }}</a>
                            </li>
                            <li>
                                <a>{{ trans('menu.produksi') }}</a>
                            </li>
                            <li class="active">
                                {{ trans('menu.wip') }}
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            @if(Session::has('successMessage'))
            <div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-check-all"></i>
                <strong>{{ Session::get('successMessage') }}.</strong>
            </div>
            @endif

            @if(Session::has('errorMessage'))
            <div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-block-helper"></i>
                <strong>{{ Session::get('errorMessage') }}.</strong>
            </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-border panel-success">
                        <div class="panel-heading">
                        </div>

                        <div class="panel-body">
                            <div class="row">
                                <form class="form-horizontal" role="form" method="post" action="">
                                    {{ csrf_field() }}
                                    <div class="col-md-12">
                                        <p class="text-muted font-14 m-b-20 pull-right">
                                            @can('access', [$resource, 'add'])
                                            <a data-toggle="modal" data-target="#dtwip" class="btn btn-primary waves-effect w-md waves-light"><i class="fa fa-plus"></i> {{ trans('fields.add-wip') }}</a>
                                            @endcan

                                            @can('access', [$resource, 'add'])
                                            <a href="{{ route('wip-stockafkir') }}"  class="btn btn-primary waves-effect w-md waves-light"><i class="fa fa-plus"></i> {{ trans('fields.stock-afkir') }}</a>
                                            @endcan

                                            @can('access', [$resource, 'add'])
                                            <a href="{{ route('wip-exportexcel') }}"  class="btn btn-success waves-effect w-md waves-light"><i class="fa fa-plus"></i> {{ trans('fields.export-excel') }}</a>
                                            @endcan

                                      

                                            <div class="col-md-2">
                                                <input type="text" name="tally_no" class="form-control" value="{{ !empty($filters['tally_no']) ? $filters['tally_no'] : '' }}">
                                                @if($errors->has('tally_no'))
                                                <span class="help-block">{{ $errors->first('tally_no') }}</span>
                                                @endif
                                            </div>

                                            <div class="col-md-4">
                                                <button type="submit" class="btn btn-info waves-effect w-xs waves-light"><i class="fa fa-search"></i> </button>
                                            </div>
                                        </p>
                                    </div>
                                </form>
                            </div>


                            <div class="row">
                                <table class="table table-bordered table-hover table-striped m-0">
                                    <thead>
                                        <tr>
                                            <th width="10%" class="text-center">{{ trans('fields.num') }}</th>
                                            <th width="30%" class="text-center">{{ trans('fields.notally') }}</th>
                                            <th width="20%" class="text-center">{{ trans('fields.date-wip') }}</th> 
                                            <th width="10%" class="text-center">{{ trans('fields.rendemen') }}</th>
                                            <th width="30%" class="text-center">{{ trans('fields.action') }}</th>
                                               
                                        </tr>
                                    </thead>
                                    
                                    <tbody>
                                    <?php $no = 1;//($models->currentPage() - 1) * $models->perPage() + 1; ?>
                                    @foreach($models as $isi)
                                    <tr>
                                        <th scope="row" style="text-align: center;">{{ $no++ }}</th>
                                        <td>{{ $isi->tally_no }}</td>
                                        <td>{{ date('d-m-Y', strtotime($isi->date_mutation_wip)) }}</td> 
                                    <td>
                                    @if($isi->volumes <> null)
                                        {{ $isi->volumes }}
                                    @else
                                        {{ 'Belum Ada' }}
                                    @endif
                                    </td>
                                        </td> 
                                        <td class="text-center">
                                               @can('access', [$resource, 'add'])
                                                <a href="{{ route('ms-add', ['id' => $isi->tally_id]) }}" data-toggle="tooltip" class="btn btn-xs btn-primary" >
                                                    <i class="fa fa-plus"></i> {{ trans('fields.input-ms') }}
                                                </a>
                                                @endcan

                                                <!-- @can('access', [$resource, 'add'])
                                                <a href="{{ route('afkir-kering-add', ['id' => $isi->tally_id]) }}" data-toggle="tooltip" class="btn btn-xs btn-primary" >
                                                    <i class="fa fa-plus"></i> {{ trans('fields.inputafkir') }}
                                                </a>
                                                @endcan -->

                                        </td>


                                    </tr>
                                    @endforeach
                                    </tbody>

                                </table>
                                <div class="data-table-toolbar">
                                    {!! $models->render() !!}
                                </div>
                            </div>


                        <div id="dtwip" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form role="form" method="post" action="{{ route('wip-save') }}">
                                        {{ csrf_field() }}

                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">{{ trans('fields.add-wip') }}</h4>
                                        </div>



                                        <div class="modal-body">

                                            <div class="row">
                                                  <div class="col-md-6">
                                                    <label>{{ trans('fields.date-wip') }}</label>
                                                        <div>
                                                            <div class="form-group">
                                                                <input type="date" class="form-control" placeholder="mm/dd/yyyy" id="datepicker-autoclose" name="date_mutation_wip">
                            
                                                            </div><!-- input-group -->
                                                        </div>
                                                    </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                     <div class="form-group">
                                                        <label for="label" class="control-label">{{ trans('fields.notally') }}</label>
                                                        <textarea class="form-control" rows="7" name="tally_no"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                       
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close
                                            </button>
                                            <button type="submit" class="btn btn-info waves-effect waves-light">Save changes</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div><!-- /.modal -->
                     
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

