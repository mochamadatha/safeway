<?php

namespace App\Service\Master;

use DB;

class PalletService
{
    public static function getActivePallet()
    {
        return \DB::table('mst_pallet_name')
            ->select('mst_pallet_name.pallet_name_id', DB::raw("CONCAT(pallet_name,' ', tinggi,' ', lebar,' ', panjang) as pallet"))
            ->where('status', '=', true)
            ->orderBy('pallet_name')
            ->get();
    }


    // ->select('mst_jenis_kayu.jenis_kayu', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 'dt_isi_tally.panjang', 'mst_harga.harga',DB::raw('sum(dt_isi_tally.pcs) as pcs, sum(dt_isi_tally.volume) as volume')) //

    //$comp = Component::select(DB::raw("CONCAT('name','id') AS ID"))->get()
    //  public static function getActivePallet()
    // {
    //     return \DB::table('mst_pallet_name')
    //             ->where('status', '=', true)
    //             ->orderBy('pallet_name')
    //             ->get();
    // }
}
