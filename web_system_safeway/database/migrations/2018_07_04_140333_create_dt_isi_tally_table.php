<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDtIsiTallyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dt_isi_tally', function (Blueprint $table) {
            $table->increments('isi_tally_id');
            $table->integer('tally_id');
            $table->integer('jenis_kayu_id');
            $table->integer('deskripsi_id');
            $table->integer('tinggi');
            $table->integer('lebar');
            $table->integer('panjang');
            $table->integer('pcs');
            $table->double('volume');
            $table->boolean('status')->default(true);
            $table->integer('created_by');
            $table->date('created_date');
            $table->integer('modified_by')->nullable();
            $table->dateTime('modified_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dt_isi_tally');
    }
}
