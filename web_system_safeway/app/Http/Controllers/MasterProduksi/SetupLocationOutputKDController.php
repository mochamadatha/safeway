<?php

namespace App\Http\Controllers\MasterProduksi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\MasterProduksi\LokasiOutputKD;
use DB;

class SetupLocationOutputKDController extends Controller
{
    const URL       = 'master-produksi/setup-location-outputkd';
    const RESOURCE  = 'setup-location-outputkd';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        if ($request->isMethod('post')) {
            $request->session()->put('filters', $request->all());
            return redirect(self::URL . '?page=1');
        } elseif (empty($request->get('page'))) {
            $request->session()->forget('filters');
        }
        $filters = $request->session()->get('filters');
        $query = \DB::table('mst_lokasi_outputkd')
            ->orderBy('mst_lokasi_outputkd.lokasi_outputkd');

        if (!empty($filters['status']) || !$request->session()->has('filters')) {
            $query->where('mst_lokasi_outputkd.status', '=', true);
        } else {
            $query->where('mst_lokasi_outputkd.status', '=', false);
        }

        if (!empty($filters['lokasi_outputkd'])) {
            $query->where('mst_lokasi_outputkd.lokasi_outputkd', 'like', '%' . $filters['lokasi_outputkd'] . '%');
        }

        return view('master-produksi/location-outputkd.index', [
            "models"        => $query->paginate(10),
            "filters"       => $filters,
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function add(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        $model           = new LokasiOutputKD();
        $model->status   = true;

        return view('master-produksi/location-outputkd.add', [
            "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }

    public function edit(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $model = LokasiOutputKD::find($id);

        if ($model === null) {
            abort('404');
        }

        return view('master-produksi/location-outputkd.add', [
            "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }



    public function save(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add']) && !\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $id     = intval($request->get('id', 0));

        $this->validate($request, [
            'lokasi_outputkd' => 'required|max:255|unique:mst_lokasi_outputkd,lokasi_outputkd,' . $id . ',lokasi_outputkd_id',
        ]);

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            if (empty($id)) {
                $model  = new LokasiOutputKD();
                $model->created_by      = \Auth::user()->id;
                $model->created_date    = $now;
            } else {
                $model  = LokasiOutputKD::find($id);
                $model->modified_by   = \Auth::user()->id;
                $model->modified_date = $now;
            }

            $model->lokasi_outputkd   = $request->get('lokasi_outputkd');
            $model->status   = !empty($request->get('status')) ? true : false;

            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('menu.wood-type') . ' ' . $request->get('lokasi_outputkd')])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('menu.wood-type') . ' ' . $request->get('lokasi_outputkd')])
            );
        }

        return redirect(route('setup-location-outputkd-index'));
    }
}
