<?php 
// use App\Service\Master\SupplierService; 
// use App\Model\MasterProduksi\Supplier;
// use App\Model\Produksi\HdLpb;


list($titlefooter, $date) = explode("#", $title);
?>

<br/><br/>
<table id="header" width="100%" height="100%" border="1px">
	<tr>
		<!-- <td rowspan="2" width="20%" >
			 <img src="C:\xampp\htdocs\safeway\public_safeway\assets\images\lpb_pdf_logo.png" align="center">
		</td> -->
		<th  width="100%"  align="center " style="font-size: 12px; font-weight: bold;" >LAPORAN MUTASI</th>
		<!-- <td width="34%" height="15" style="font-size: 9px; font-weight: bold;" >
			<table>
				<tr>
					<td width="30%" align-text="center"> NO. Mutasi</td>
					<td width="5%"> :</td>
					<td width="65%"> {{ $model->nomor_mutasi}} </td>
				</tr>
			</table>
		</td> -->
	</tr>

	<tr>
		<td width="34%"   style="font-size: 9px; font-weight: bold;" >
			<table>
				<<!-- tr>
					<td width="30%"> TANGGAL</td>
					<td width="5%"> :</td>
					<td width="65%"> {{ $date }} </td>
				</tr> -->
			</table>
		</td>
	</tr>
    <tr>
		<td width="66%" colspan="2" rowspan="2"  style="font-size: 9px; font-weight: bold;" >
			<table>
				<tr>
					<td width="30%"> Supir + Plat Nomor</td>
					<td width="5%"> :</td>
					<td width="65%"> {{ $model->supir_plat}} </td>
				</tr>
			</table>
			<table>
				<tr>
					<td width="30%"> Lokasi Mutasi</td>
					<td width="5%"> :</td>
					<td width="65%"> {{ $model->lokasi_mutasi}} </td>
				</tr>
			</table>
			<table>
				<tr>
					<td width="30%"> Note</td>
					<td width="5%"> :</td>
					<td width="65%"><font size="7"> {{ $model->catatan}}</font> </td>
				</tr>
			</table>
		</td>

		<td width="34%"  height="15" style="font-size: 9px; font-weight: bold;" >
			<table>
				<tr>
					<td width="50%"> Nomor Mutasi</td>
					<td width="5%"> :</td>
					<td width="45%"> {{ $model->nomor_mutasi}}   </td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
	
		<td  width="34%" height="15" style="font-size: 9px; font-weight: bold;" >
			<table>
				<tr>
					<td width="50%"> Tanggal Mutasi</td>
					<td width="5%"> :</td>
					<td width="45%"> {{ date('d-m-Y', strtotime($model->tanggal_mutasi)) }} </td>
				</tr>
			</table>
		</td>
	</tr>
	<tr  bgcolor="#008d95" style="color: white; font-weight: bold; font-size: 10px;" align="center">
	
		<td rowspan="2" width="4%" height="22">No</td>
			<td rowspan="2" width="15%" height="22">No. Tally Sheet</td>
			<td rowspan="2" width="15%" height="22">Jenis Kayu</td>
			<td rowspan="2" width="7%" height="22">Status</td>
			<td colspan="3" width="24%" height="22">Ukuran</td>
			<td colspan="2" width="20%" height="22">Jumlah Diterima</td>
			<td rowspan="2" width="15%" height="22">Tanggal</td>
		
	</tr>
	<tr bgcolor="#008d95" style="color: white; font-weight: bold; font-size: 10px;" align="center">
		<td height="19">T</td>
		<td height="19">L</td>
		<td height="19">P</td>
		<td height="19">PCS</td>
		<td height="19">m3</td>
	</tr>
   
</table>
