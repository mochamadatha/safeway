<?php

namespace App\Model\Produksi;

use Illuminate\Database\Eloquent\Model;

class HasilReprosesMs extends Model
{
    protected $table        = 'dt_hasil_reproses_ms';
    protected $primaryKey   = 'hasil_reproses_ms_id';
    public $timestamps      = false;
}
