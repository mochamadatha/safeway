<?php

namespace App\Http\Controllers\Produksi\Mutation_Wip;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
// use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Controllers\Controller;
use App\Model\Produksi\LPB;
use App\Model\Produksi\HdMutasi;
use App\Model\Produksi\DtMutasi;
use App\Service\Master\TallyService;
use App\Model\MasterProduksi\Supplier;
use App\Service\Master\SupplierService;
use App\Service\Master\WoodService;
use App\Model\Produksi\HdLpb;
use Elibyy\TCPDF\Facades\TCPDF;
use App\Model\Produksi\IsiBandsaw;
use App\Model\Produksi\IsiAfkir;
use App\Service\Master\ChamberService;
use App\Model\MasterProduksi\LokasiMutasi;
use App\Service\Master\MutationLocationService;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class MutationController extends Controller
{
    const URL       = 'produksi/mutation';
    const RESOURCE  = 'mutation';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        if ($request->isMethod('post')) {
            $request->session()->put('filters', $request->all());
            return redirect(self::URL . '?page=1');
        } elseif (empty($request->get('page'))) {
            $request->session()->forget('filters');
        }
        $filters = $request->session()->get('filters');
        $query = \DB::table('hd_mutasi')
            ->select('hd_mutasi.*', 'mst_lokasi_mutasi.lokasi_mutasi') //, 'tally_table.tally_no'

            ->join('mst_lokasi_mutasi', 'mst_lokasi_mutasi.lokasi_mutasi_id', '=', 'hd_mutasi.lokasi_mutasi_id')
            //   ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_mutasi_id.supplier_id')
            ->orderBy('hd_mutasi.created_by');

        if (!empty($filters['status']) || !$request->session()->has('filters')) {
            $query->where('hd_mutasi.status', '=', true);
        } else {
            $query->where('hd_mutasi.status', '=', false);
        }
        $filters = $request->session()->get('filters');
        $query = \DB::table('hd_mutasi')
            ->select('hd_mutasi.*', 'mst_lokasi_mutasi.lokasi_mutasi') //, 'tally_table.tally_no'
            ->join('mst_lokasi_mutasi', 'mst_lokasi_mutasi.lokasi_mutasi_id', '=', 'hd_mutasi.lokasi_mutasi_id')
            //   ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_mutasi_id.supplier_id')
            ->orderBy('hd_mutasi.created_date', 'DESC');


        if (!empty($filters['nomor_mutasi'])) {
            $query->where('hd_mutasi_id.nomor_mutasi', 'like', '%' . $filters['nomor_mutasi'] . '%');
        }

        return view('produksi/mutation_wip/mutation.index', [
            "models"        => $query->paginate(10),
            "filters"       => $filters,
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function add(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        $model           = new HdMutasi();
        $model->status   = true;

        return view('produksi/mutation_wip/mutation.add', [
            "model"         => $model,
            "MutationLocationOptions"   => MutationLocationService::getActiveMutationLocation(),
        ]);
    }

    public function addtally(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        if ($request->isMethod('post')) {
            $request->session()->put('filters', $request->all());
            return redirect(self::URL . '?page=1');
        } elseif (empty($request->get('page'))) {
            $request->session()->forget('filters');
        }

        $filters = $request->session()->get('filters');
        $model = \DB::table('hd_mutasi')
            ->select('hd_mutasi.hd_mutasi_id', 'mst_lokasi_mutasi.lokasi_mutasi', 'hd_mutasi.nomor_mutasi', 'hd_mutasi.tanggal_mutasi', 'hd_mutasi.supir_plat', 'hd_mutasi.catatan', 'hd_mutasi.created_date')
            ->where('hd_mutasi.status', '=', true)
            ->where('hd_mutasi.hd_mutasi_id', '=', $id)
            ->join('mst_lokasi_mutasi', 'mst_lokasi_mutasi.lokasi_mutasi_id', '=', 'hd_mutasi.lokasi_mutasi_id')
            ->orderBy('hd_mutasi.created_date', 'DESC')->first();

        // dd($model);
        if ($model === null) {
            abort('404');
        }

        if (!empty($filters['tally_no'])) {
            // $query->where('tally_table.tally_no', 'like', '%'.$filters['tally_no'].'%');
        }

        $page = Input::get('page', 1);
        $paginate = 10;

        $query = DB::table('hd_lpb')
            ->select('tally_table.tally_no', 'mst_jenis_kayu.jenis_kayu', 'mst_deskripsi_tallysheet.nama_deskripsi', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 'dt_isi_tally.panjang', 'dt_isi_tally.pcs', 'dt_isi_tally.volume', 'dt_isi_tally.created_date')
            ->where('hd_lpb.status', '=', true)


            ->where('tally_table.status_mutation_wip', '=', 0)
            ->where('hd_mutasi.hd_mutasi_id', '=', $id)
            ->whereNotIn('dt_lpb.tally_id',  DB::table('dt_isi_bandsaw')
                ->select('tally_id')
                ->groupBy('tally_id'))
            ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', 'hd_lpb.lokasi_kedatangan_id')
            ->join('dt_lpb', 'dt_lpb.hd_lpb_id', '=', 'hd_lpb.hd_lpb_id')
            ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
            ->join('dt_mutasi', 'dt_mutasi.tally_id', '=', 'tally_table.tally_id')
            ->join('hd_mutasi', 'hd_mutasi.hd_mutasi_id', '=', 'dt_mutasi.hd_mutasi_id')

            ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_tally.deskripsi_id')

            ->orderBy('tally_table.tally_no');

        //dd ($query);

        $query2 = DB::table('dt_isi_bandsaw')
            ->select('tally_table.tally_no', 'mst_jenis_kayu.jenis_kayu', 'mst_deskripsi_tallysheet.nama_deskripsi', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'dt_isi_bandsaw.pcs', 'dt_isi_bandsaw.volume', 'dt_isi_bandsaw.created_date')
            ->where('dt_isi_bandsaw.status', '=', true)

            ->whereIn('dt_isi_bandsaw.tally_id', DB::table('tally_table')
                ->select('tally_table.tally_id')

                ->where('tally_table.status_mutation_wip', '=', 0)
                ->where('hd_mutasi.hd_mutasi_id', '=', $id)
                ->join('dt_isi_bandsaw', 'dt_isi_bandsaw.tally_id', '=', 'tally_table.tally_id')
                ->groupBy('tally_id'))
            ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'dt_isi_bandsaw.tally_id')
            ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
            ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=', 'hd_lpb.lokasi_kedatangan_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')

            ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')
            ->join('dt_mutasi', 'dt_mutasi.tally_id', '=', 'tally_table.tally_id')
            ->join('hd_mutasi', 'hd_mutasi.hd_mutasi_id', '=', 'dt_mutasi.hd_mutasi_id')
            ->groupBy('tally_table.tally_no',  'mst_jenis_kayu.jenis_kayu', 'mst_deskripsi_tallysheet.nama_deskripsi', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'dt_isi_bandsaw.pcs', 'dt_isi_bandsaw.volume',  'dt_isi_bandsaw.created_date')
            ->union($query)
            ->orderBy('tally_no', 'DESC')->get();





        if (!empty($filters['status']) || !$request->session()->has('filters')) {
            // $query->where('hd_lpb.status', '=', true);
        } else {
            // $query->where('hd_lpb.status', '=', false);
        }

        $slice = array_slice($query2->toArray(), $paginate * ($page - 1), $paginate);
        $result = new LengthAwarePaginator($slice, count($query2), $paginate);
        // return View::make('produksi/mutation_wip/mutation.addtally');
        // dd(url()->current());
        $result = $result->setPath(url()->current());


        return view('produksi/mutation_wip/mutation.addtally', [

            // "models"         => $query->paginate(10),
            "model"         => $model,
            "models"         => $result,
            "ChamberOptions"   => ChamberService::getActiveChamber(),
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function pdf(Request $request, $id) //bagian dispacth
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        $model = \DB::table('hd_mutasi')
            ->select('hd_mutasi.hd_mutasi_id', 'mst_lokasi_mutasi.lokasi_mutasi', 'hd_mutasi.nomor_mutasi', 'hd_mutasi.tanggal_mutasi', 'hd_mutasi.supir_plat', 'hd_mutasi.catatan')
            ->where('hd_mutasi.status', '=', true)
            ->where('hd_mutasi.hd_mutasi_id', '=', $id)
            ->join('mst_lokasi_mutasi', 'mst_lokasi_mutasi.lokasi_mutasi_id', '=', 'hd_mutasi.lokasi_mutasi_id')
            ->orderBy('hd_mutasi.hd_mutasi_id')->first();





        // $query = DB::table('hd_lpb')
        // ->select('mst_jenis_kayu.jenis_kayu', DB::raw('sum(dt_isi_tally.pcs)as pcs, sum(dt_isi_tally.volume)as volume'))
        //     ->where('hd_lpb.status', '=', true)
        //     ->where('tally_table.date_mutation_wip', '!=', NULL)
        //     ->where('tally_table.status_mutation_wip', '=', 0)
        //     ->where('hd_mutasi.hd_mutasi_id', '=', $id)
        //     ->whereNotIn('dt_lpb.tally_id',  DB::table('dt_isi_bandsaw')
        //             ->select('tally_id')
        //             ->groupBy('tally_id')) 
        //     ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', 'hd_lpb.lokasi_kedatangan_id')
        //     ->join('dt_lpb', 'dt_lpb.hd_lpb_id', '=', 'hd_lpb.hd_lpb_id')
        //     ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
        //     ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
        //     ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
        //     ->join('dt_mutasi', 'dt_mutasi.tally_id', '=', 'tally_table.tally_id')
        //     ->join('hd_mutasi', 'hd_mutasi.hd_mutasi_id', '=', 'dt_mutasi.hd_mutasi_id')

        //     ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_tally.deskripsi_id')
        //     ->join('mst_lokasi_outputkd', 'mst_lokasi_outputkd.lokasi_outputkd_id','=', 'tally_table.lokasi_outputkd_id')
        //      ->groupBy('mst_jenis_kayu.jenis_kayu');

        //     //dd ($query);

        // $query2 = DB::table('dt_isi_bandsaw')
        // ->select('mst_jenis_kayu.jenis_kayu',   DB::raw('sum(dt_isi_bandsaw.pcs)as pcs, sum(dt_isi_bandsaw.volume)as volume'))
        //     ->where('dt_isi_bandsaw.status', '=', true)
        //     ->where('hd_lpb.status', '=', true)
        //     // ->where('tally_table.date_in_kd', '=', null)
        //     // ->where('dt_isi_bandsaw.tally_id', '=', $id)
        //     ->whereIn('dt_isi_bandsaw.tally_id', DB::table('tally_table')
        //             ->select('tally_table.tally_id')

        //              ->where('tally_table.date_mutation_wip', '!=', NULL)
        //              ->where('tally_table.status_mutation_wip', '=', 0)
        //             ->where('hd_mutasi.hd_mutasi_id', '=', $id)
        //             ->join('dt_isi_bandsaw','dt_isi_bandsaw.tally_id', '=', 'tally_table.tally_id')
        //             ->groupBy('tally_id'))
        //     ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'dt_isi_bandsaw.tally_id')
        //     ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
        //     ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
        //     ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=' ,'hd_lpb.lokasi_kedatangan_id')
        //     ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')

        //     ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')

        //      ->join('mst_lokasi_outputkd', 'mst_lokasi_outputkd.lokasi_outputkd_id','=', 'tally_table.lokasi_outputkd_id')
        //      ->join('dt_mutasi', 'dt_mutasi.tally_id', '=', 'tally_table.tally_id')
        //     ->join('hd_mutasi', 'hd_mutasi.hd_mutasi_id', '=', 'dt_mutasi.hd_mutasi_id')
        //     ->groupBy('mst_jenis_kayu.jenis_kayu')
        //     ->union($query)
        //     ->orderBy('jenis_kayu')->get();

        $query = DB::table('hd_lpb')
            ->select(
                'mst_jenis_kayu.jenis_kayu',
                DB::raw('SUM(dt_isi_tally.pcs) as pcs'),
                DB::raw('SUM(dt_isi_tally.volume) as volume'),
                DB::raw('"Non Bandsaw" status_data')
            )
            ->where('hd_lpb.status', '=', true)
            ->where('tally_table.date_mutation_wip', '!=', NULL)
            ->where('tally_table.status_mutation_wip', '=', 0)
            ->where('hd_mutasi.hd_mutasi_id', '=', $id)
            ->whereNotIn('dt_lpb.tally_id',  DB::table('dt_isi_bandsaw')
                ->select('tally_id')
                ->groupBy('tally_id'))
            // ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', 'hd_lpb.lokasi_kedatangan_id')
            ->join('dt_lpb', 'dt_lpb.hd_lpb_id', '=', 'hd_lpb.hd_lpb_id')
            ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
            ->join('dt_mutasi', 'dt_mutasi.tally_id', '=', 'tally_table.tally_id')
            ->join('hd_mutasi', 'hd_mutasi.hd_mutasi_id', '=', 'dt_mutasi.hd_mutasi_id')
            ->groupBy('mst_jenis_kayu.jenis_kayu', 'status_data');



        $query2 = DB::table('dt_isi_bandsaw')
            ->select('mst_jenis_kayu.jenis_kayu', DB::raw('SUM(dt_isi_bandsaw.pcs) as pcs'),  DB::raw('SUM(dt_isi_bandsaw.volume) as volume'), DB::raw('"Bandsaw" status_data'))
            ->where('dt_isi_bandsaw.status', '=', true)
            // ->where('tally_table.date_in_kd', '=', null)
            // ->where('dt_isi_bandsaw.tally_id', '=', $id)
            ->whereIn('dt_isi_bandsaw.tally_id', DB::table('tally_table')
                ->select('tally_table.tally_id')
                ->where('tally_table.date_mutation_wip', '!=', NULL)
                ->where('tally_table.status_mutation_wip', '=', 0)
                ->where('hd_mutasi.hd_mutasi_id', '=', $id)
                ->join('dt_isi_bandsaw', 'dt_isi_bandsaw.tally_id', '=', 'tally_table.tally_id')
                ->groupBy('tally_id'))
            // ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'dt_isi_bandsaw.tally_id')
            // ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
            // ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=' ,'hd_lpb.lokasi_kedatangan_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')
            // ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')

            ->join('dt_mutasi', 'dt_mutasi.tally_id', '=', 'tally_table.tally_id')
            ->join('hd_mutasi', 'hd_mutasi.hd_mutasi_id', '=', 'dt_mutasi.hd_mutasi_id')
            ->groupBy('mst_jenis_kayu.jenis_kayu', 'status_data')
            ->union($query)
            ->orderBy('jenis_kayu')->get();


        // dd($infoinkddate2);






        // $model = \DB::table('hd_mutasi')
        //        ->select('hd_mutasi.hd_mutasi_id', 'mst_lokasi_mutasi.lokasi_mutasi', 'hd_mutasi.nomor_mutasi', 'hd_mutasi.tanggal_mutasi', 'hd_mutasi.supir_plat', 'hd_mutasi.catatan')
        //        ->where('hd_mutasi.status', '=', true)
        //        ->where('hd_mutasi.hd_mutasi_id', '=', $id)
        //        ->join('mst_lokasi_mutasi', 'mst_lokasi_mutasi.lokasi_mutasi_id', '=', 'hd_mutasi.lokasi_mutasi_id')
        //        ->orderBy('hd_mutasi.hd_mutasi_id')->first();


        // $data = DB::table('hd_lpb')
        // ->select('mst_jenis_kayu.jenis_kayu', DB::raw('sum(dt_isi_tally.pcs)as pcs, sum(dt_isi_tally.volume)as volume', DB::raw('" " status_data'))
        //     ->where('hd_lpb.status', '=', true)

        //     ->where('tally_table.date_mutation_wip', '!=', NULL)
        //     ->where('tally_table.status_mutation_wip', '=', 0)
        //     ->where('hd_mutasi.hd_mutasi_id', '=', $id)
        //     ->whereNotIn('dt_lpb.tally_id',  DB::table('dt_isi_bandsaw')
        //             ->select('tally_id')
        //             ->groupBy('tally_id')) 
        //     ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', 'hd_lpb.lokasi_kedatangan_id')
        //     ->join('dt_lpb', 'dt_lpb.hd_lpb_id', '=', 'hd_lpb.hd_lpb_id')
        //     ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
        //     ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
        //     ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
        //     ->join('dt_mutasi', 'dt_mutasi.tally_id', '=', 'tally_table.tally_id')
        //     ->join('hd_mutasi', 'hd_mutasi.hd_mutasi_id', '=', 'dt_mutasi.hd_mutasi_id')

        //     ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_tally.deskripsi_id')
        //     ->join('mst_lokasi_outputkd', 'mst_lokasi_outputkd.lokasi_outputkd_id','=', 'tally_table.lokasi_outputkd_id')
        //     ->orderBy('hd_lpb.created_by');

        //     //dd ($query);

        // $data2 = DB::table('dt_isi_bandsaw')
        // ->select('tally_table.tally_no', 'tally_table.date_out_kd', 'mst_jenis_kayu.jenis_kayu', 'mst_deskripsi_tallysheet.nama_deskripsi', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'dt_isi_bandsaw.pcs', 'dt_isi_bandsaw.volume', 'mst_lokasi_kedatangan.lokasi_kedatangan', 'dt_isi_bandsaw.created_date', 'mst_lokasi_outputkd.lokasi_outputkd', DB::raw('"B" status_data'))
        //     ->where('dt_isi_bandsaw.status', '=', true)
        //     // ->where('tally_table.date_in_kd', '=', null)
        //     // ->where('dt_isi_bandsaw.tally_id', '=', $id)
        //     ->whereIn('dt_isi_bandsaw.tally_id', DB::table('tally_table')
        //             ->select('tally_table.tally_id')

        //              ->where('tally_table.date_mutation_wip', '!=', NULL)
        //              ->where('tally_table.status_mutation_wip', '=', 0)
        //             ->where('hd_mutasi.hd_mutasi_id', '=', $id)
        //             ->join('dt_isi_bandsaw','dt_isi_bandsaw.tally_id', '=', 'tally_table.tally_id')
        //             ->groupBy('tally_id'))
        //     ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'dt_isi_bandsaw.tally_id')
        //     ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
        //     ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
        //     ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=' ,'hd_lpb.lokasi_kedatangan_id')
        //     ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')

        //     ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')

        //      ->join('mst_lokasi_outputkd', 'mst_lokasi_outputkd.lokasi_outputkd_id','=', 'tally_table.lokasi_outputkd_id')
        //      ->join('dt_mutasi', 'dt_mutasi.tally_id', '=', 'tally_table.tally_id')
        //     ->join('hd_mutasi', 'hd_mutasi.hd_mutasi_id', '=', 'dt_mutasi.hd_mutasi_id')
        //     ->groupBy('tally_table.tally_no', 'tally_table.date_out_kd', 'mst_jenis_kayu.jenis_kayu', 'mst_deskripsi_tallysheet.nama_deskripsi', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'dt_isi_bandsaw.pcs', 'dt_isi_bandsaw.volume', 'mst_lokasi_kedatangan.lokasi_kedatangan', 'dt_isi_bandsaw.created_date', 'mst_lokasi_outputkd.lokasi_outputkd')
        //     ->union($data)
        //     ->orderBy('tally_no')->get();


        //  dd($query2);

        $now    = new \DateTime();
        $now = date_format($now, 'd/m/Y');
        // $model =  HdLpb::find($id);


        $html = view('produksi/mutation_wip/mutation.content-pdf', [

            'title' => trans('menu.lpb-pdf-report') . "#" . $now,
            'model' => $model,
            'models' => $query2,

            //   "supplierOptions" => SupplierService::getActiveSupplier(),


        ])->render();




        $pdf = new TCPDF();
        $pdf::SetTitle('Report Afkir' . $model->nomor_mutasi . '-' . $now);
        ob_end_clean();
        // $pdf::SetTitle('Afkir-');
        //  $resolution= array(187, 143);
        // $resolution= array(143, 187);
        $pdf::SetMargins(1, 5, 1, true);
        $pdf::AddPage();
        //  $pdf::AddPage('P', $resolution);
        $pdf::writeHTML($html, true, false, true, false, '');



        //  $pdf::AddPage('P', $resolution);
        // $pdf::writeHTML($html2, true, false, true, false, '');
        $pdf::Output('Report Afkir' . $model->nomor_mutasi . '-' . $now . '.pdf');
        // $pdf::Output($query->container_number.' '.$query->created_date.'.pdf');
        // $pdf->SetFont('Helvetica', '', 9);

        // $pdf->AddPage('P', 'A4');
        // $pdf->Cell(0, 0, 'A4 PORTRAIT', 1, 1, 'C');


    }

    public function exportexcel(Request $request, $id)
    {

        $this->id = $id;
        ob_end_clean();
        ob_start();
        Excel::create('Data Mutasi', function ($excel) {

            $excel->sheet('Sheet 1', function ($sheet) {



                $model = \DB::table('hd_mutasi')
                    ->select('hd_mutasi.hd_mutasi_id')

                    ->where('hd_mutasi.hd_mutasi_id', '=', $this->id)
                    ->join('mst_lokasi_mutasi', 'mst_lokasi_mutasi.lokasi_mutasi_id', '=', 'hd_mutasi.lokasi_mutasi_id')
                    ->orderBy('hd_mutasi.hd_mutasi_id')->get();

                // dd($model);

                // $model = \DB::table('hd_lpb')
                //     ->select('hd_lpb.*', 'mst_supplier.supplier_name', 'mst_supplier.pic')
                //     ->where('hd_lpb.hd_lpb_id', '=', $this->id)
                //     ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_lpb.supplier_id')
                //     ->orderBy('hd_lpb.hd_lpb_id')->first();


                $query = DB::table('hd_lpb')
                    ->select(
                        'tally_table.tally_no',
                        'dt_isi_tally.jenis_kayu_id',
                        'dt_isi_tally.deskripsi_id',
                        'dt_isi_tally.tinggi',
                        'dt_isi_tally.lebar',
                        'dt_isi_tally.panjang',
                        'dt_isi_tally.pcs',
                        'dt_isi_tally.volume',
                        'tally_table.chamber_id',
                        'tally_table.lokasi_outputkd_id',
                        'tally_table.date_in_kd',
                        'tally_table.date_out_kd',
                        'hd_lpb.lokasi_kedatangan_id',
                        'hd_lpb.tanggal_kedatangan',
                        'tally_table.date_mutation_wip'
                    )
                    ->where('hd_lpb.status', '=', true)


                    ->where('tally_table.status_mutation_wip', '=', 0)
                    ->where('hd_mutasi.hd_mutasi_id', '=',  $this->id)
                    ->whereNotIn('dt_lpb.tally_id',  DB::table('dt_isi_bandsaw')
                        ->select('tally_id')
                        ->groupBy('tally_id'))
                    ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', 'hd_lpb.lokasi_kedatangan_id')
                    ->join('dt_lpb', 'dt_lpb.hd_lpb_id', '=', 'hd_lpb.hd_lpb_id')
                    ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
                    ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
                    ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
                    ->join('dt_mutasi', 'dt_mutasi.tally_id', '=', 'tally_table.tally_id')
                    ->join('hd_mutasi', 'hd_mutasi.hd_mutasi_id', '=', 'dt_mutasi.hd_mutasi_id')

                    ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_tally.deskripsi_id')

                    ->orderBy('hd_lpb.created_by');

                //dd ($query);

                $query2 = DB::table('dt_isi_bandsaw')
                    ->select(
                        'tally_table.tally_no',
                        'dt_isi_bandsaw.jenis_kayu_id',
                        'dt_isi_bandsaw.deskripsi_id',
                        'dt_isi_bandsaw.tinggi',
                        'dt_isi_bandsaw.lebar',
                        'dt_isi_bandsaw.panjang',
                        'dt_isi_bandsaw.pcs',
                        'dt_isi_bandsaw.volume',
                        'tally_table.chamber_id',
                        'tally_table.lokasi_outputkd_id',
                        'tally_table.date_in_kd',
                        'tally_table.date_out_kd',
                        'hd_lpb.lokasi_kedatangan_id',
                        'hd_lpb.tanggal_kedatangan',
                        'tally_table.date_mutation_wip'
                    )
                    ->where('dt_isi_bandsaw.status', '=', true)

                    ->whereIn('dt_isi_bandsaw.tally_id', DB::table('tally_table')
                        ->select('tally_table.tally_id')

                        ->where('tally_table.status_mutation_wip', '=', 0)
                        ->where('hd_mutasi.hd_mutasi_id', '=',  $this->id)
                        ->join('dt_isi_bandsaw', 'dt_isi_bandsaw.tally_id', '=', 'tally_table.tally_id')
                        ->groupBy('tally_id'))
                    ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'dt_isi_bandsaw.tally_id')
                    ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
                    ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
                    ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=', 'hd_lpb.lokasi_kedatangan_id')
                    ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')

                    ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')


                    ->join('dt_mutasi', 'dt_mutasi.tally_id', '=', 'tally_table.tally_id')
                    ->join('hd_mutasi', 'hd_mutasi.hd_mutasi_id', '=', 'dt_mutasi.hd_mutasi_id')
                    ->groupBy('tally_table.tally_no', 'tally_table.date_mutation_wip',  'dt_isi_bandsaw.jenis_kayu_id', 'dt_isi_bandsaw.deskripsi_id', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'dt_isi_bandsaw.pcs', 'dt_isi_bandsaw.volume',  'tally_table.chamber_id',  'tally_table.lokasi_outputkd_id', 'tally_table.date_in_kd', 'tally_table.date_out_kd', 'hd_lpb.lokasi_kedatangan_id',  'hd_lpb.tanggal_kedatangan')
                    ->union($query)
                    ->orderBy('tally_no')->get();

                // dd($query2);





                foreach ($query2 as $isi) {




                    $data[] = array(
                        $isi->tally_no,
                        $isi->jenis_kayu_id,
                        $isi->deskripsi_id,
                        $isi->chamber_id,
                        $isi->lokasi_outputkd_id,
                        $isi->date_in_kd,
                        $isi->date_out_kd,
                        $isi->tinggi,
                        $isi->lebar,
                        $isi->panjang,
                        $isi->pcs,
                        $isi->volume,
                        $isi->lokasi_kedatangan_id,
                        $isi->date_mutation_wip,
                        $isi->tanggal_kedatangan





                    );
                }



                $sheet->fromArray($data, null, 'A1', false, false);
                $headings = array('tally_no', 'jenis_kayu_id', 'deskripsi_id', 'chamber_id', 'lokasi_outputkd_id', 'date_in_kd', 'date_out_kd', 'tinggi', 'lebar', 'panjang', 'pcs', 'volume', 'lokasi_kedatangan_id', 'date_mutation_wip', 'tanggal_kedatangan');
                $sheet->prependRow(1, $headings);
            });
        })->export('xlsx');
    }




    public function pdfattach(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        $model = \DB::table('hd_mutasi')
            ->select('hd_mutasi.hd_mutasi_id', 'mst_lokasi_mutasi.lokasi_mutasi', 'hd_mutasi.nomor_mutasi', 'hd_mutasi.tanggal_mutasi', 'hd_mutasi.supir_plat', 'hd_mutasi.catatan')
            ->where('hd_mutasi.status', '=', true)
            ->where('hd_mutasi.hd_mutasi_id', '=', $id)
            ->join('mst_lokasi_mutasi', 'mst_lokasi_mutasi.lokasi_mutasi_id', '=', 'hd_mutasi.lokasi_mutasi_id')
            ->orderBy('hd_mutasi.hd_mutasi_id')->first();

        $query = DB::table('hd_lpb')
            ->select(
                'tally_table.tally_no',
                'mst_jenis_kayu.jenis_kayu',
                'mst_deskripsi_tallysheet.nama_deskripsi',
                'dt_isi_tally.tinggi',
                'dt_isi_tally.lebar',
                'dt_isi_tally.panjang',
                'dt_isi_tally.pcs',
                'dt_isi_tally.volume',
                'dt_isi_tally.created_date',
                DB::raw('" " status_data')
            )
            ->where('hd_lpb.status', '=', true)


            ->where('tally_table.status_mutation_wip', '=', 0)
            ->where('hd_mutasi.hd_mutasi_id', '=', $id)
            ->whereNotIn('dt_lpb.tally_id',  DB::table('dt_isi_bandsaw')
                ->select('tally_id')
                ->groupBy('tally_id'))
            ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', 'hd_lpb.lokasi_kedatangan_id')
            ->join('dt_lpb', 'dt_lpb.hd_lpb_id', '=', 'hd_lpb.hd_lpb_id')
            ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
            ->join('dt_mutasi', 'dt_mutasi.tally_id', '=', 'tally_table.tally_id')
            ->join('hd_mutasi', 'hd_mutasi.hd_mutasi_id', '=', 'dt_mutasi.hd_mutasi_id')

            ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_tally.deskripsi_id')

            ->orderBy('tally_table.tally_no');

        //dd ($query);

        $query2 = DB::table('dt_isi_bandsaw')
            ->select(
                'tally_table.tally_no',
                'mst_jenis_kayu.jenis_kayu',
                'mst_deskripsi_tallysheet.nama_deskripsi',
                'dt_isi_bandsaw.tinggi',
                'dt_isi_bandsaw.lebar',
                'dt_isi_bandsaw.panjang',
                'dt_isi_bandsaw.pcs',
                'dt_isi_bandsaw.volume',
                'dt_isi_bandsaw.created_date',
                DB::raw('"B" status_data')
            )
            ->where('dt_isi_bandsaw.status', '=', true)

            ->whereIn('dt_isi_bandsaw.tally_id', DB::table('tally_table')
                ->select('tally_table.tally_id')

                ->where('tally_table.status_mutation_wip', '=', 0)
                ->where('hd_mutasi.hd_mutasi_id', '=', $id)
                ->join('dt_isi_bandsaw', 'dt_isi_bandsaw.tally_id', '=', 'tally_table.tally_id')
                ->groupBy('tally_id'))
            ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'dt_isi_bandsaw.tally_id')
            ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
            ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=', 'hd_lpb.lokasi_kedatangan_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')

            ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')
            ->join('dt_mutasi', 'dt_mutasi.tally_id', '=', 'tally_table.tally_id')
            ->join('hd_mutasi', 'hd_mutasi.hd_mutasi_id', '=', 'dt_mutasi.hd_mutasi_id')
            ->groupBy('tally_table.tally_no',  'mst_jenis_kayu.jenis_kayu', 'mst_deskripsi_tallysheet.nama_deskripsi', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'dt_isi_bandsaw.pcs', 'dt_isi_bandsaw.volume',  'dt_isi_bandsaw.created_date')
            ->union($query)
            ->orderBy('tally_no')->get();


        //  dd($query2);


        //   $data = DB::table('hd_lpb')
        // ->select('tally_table.tally_no', 'tally_table.date_in_kd', 'mst_jenis_kayu.jenis_kayu', 'mst_deskripsi_tallysheet.nama_deskripsi', 
        // 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 'dt_isi_tally.panjang', 'dt_isi_tally.pcs', 'dt_isi_tally.volume', 
        // 'mst_lokasi_kedatangan.lokasi_kedatangan', 'dt_isi_tally.created_date', 'mst_lokasi_outputkd.lokasi_outputkd',
        //  DB::raw('" " status_data'))
        //     ->where('hd_lpb.status', '=', true)

        //     ->where('tally_table.date_mutation_wip', '!=', NULL)
        //     ->where('tally_table.status_mutation_wip', '=', 0)
        //     ->where('hd_mutasi.hd_mutasi_id', '=', $id)
        //     ->whereNotIn('dt_lpb.tally_id',  DB::table('dt_isi_bandsaw')
        //             ->select('tally_id')
        //             ->groupBy('tally_id')) 
        //     ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', 'hd_lpb.lokasi_kedatangan_id')
        //     ->join('dt_lpb', 'dt_lpb.hd_lpb_id', '=', 'hd_lpb.hd_lpb_id')
        //     ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
        //     ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
        //     ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
        //     ->join('dt_mutasi', 'dt_mutasi.tally_id', '=', 'tally_table.tally_id')
        //     ->join('hd_mutasi', 'hd_mutasi.hd_mutasi_id', '=', 'dt_mutasi.hd_mutasi_id')

        //     ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_tally.deskripsi_id')
        //     ->join('mst_lokasi_outputkd', 'mst_lokasi_outputkd.lokasi_outputkd_id','=', 'tally_table.lokasi_outputkd_id')
        //     ->orderBy('hd_lpb.created_by');

        //     //dd ($query);

        // $data2 = DB::table('dt_isi_bandsaw')
        // ->select('tally_table.tally_no', 'tally_table.date_out_kd', 'mst_jenis_kayu.jenis_kayu', 
        // 'mst_deskripsi_tallysheet.nama_deskripsi', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 
        // 'dt_isi_bandsaw.panjang', 'dt_isi_bandsaw.pcs', 'dt_isi_bandsaw.volume', 'mst_lokasi_kedatangan.lokasi_kedatangan', 
        // 'dt_isi_bandsaw.created_date', 'mst_lokasi_outputkd.lokasi_outputkd', DB::raw('"B" status_data'))
        //     ->where('dt_isi_bandsaw.status', '=', true)
        //     // ->where('tally_table.date_in_kd', '=', null)
        //     // ->where('dt_isi_bandsaw.tally_id', '=', $id)
        //     ->whereIn('dt_isi_bandsaw.tally_id', DB::table('tally_table')
        //             ->select('tally_table.tally_id')

        //              ->where('tally_table.date_mutation_wip', '!=', NULL)
        //              ->where('tally_table.status_mutation_wip', '=', 0)
        //             ->where('hd_mutasi.hd_mutasi_id', '=', $id)
        //             ->join('dt_isi_bandsaw','dt_isi_bandsaw.tally_id', '=', 'tally_table.tally_id')
        //             ->groupBy('tally_id'))
        //     ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'dt_isi_bandsaw.tally_id')
        //     ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
        //     ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
        //     ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=' ,'hd_lpb.lokasi_kedatangan_id')
        //     ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')

        //     ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')

        //      ->join('mst_lokasi_outputkd', 'mst_lokasi_outputkd.lokasi_outputkd_id','=', 'tally_table.lokasi_outputkd_id')
        //      ->join('dt_mutasi', 'dt_mutasi.tally_id', '=', 'tally_table.tally_id')
        //     ->join('hd_mutasi', 'hd_mutasi.hd_mutasi_id', '=', 'dt_mutasi.hd_mutasi_id')
        //     ->groupBy('tally_table.tally_no', 'tally_table.date_out_kd', 'mst_jenis_kayu.jenis_kayu', 'mst_deskripsi_tallysheet.nama_deskripsi', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'dt_isi_bandsaw.pcs', 'dt_isi_bandsaw.volume', 'mst_lokasi_kedatangan.lokasi_kedatangan', 'dt_isi_bandsaw.created_date', 'mst_lokasi_outputkd.lokasi_outputkd')
        //     ->union($data)
        //     ->orderBy('tally_no')->get();




        //  dd($data2);

        $now    = new \DateTime();
        $now = date_format($now, 'd/m/Y ');
        // $model =  HdLpb::find($id);


        $header = view('produksi/mutation_wip/mutation.header-pdf', [
            'title' => trans('menu.lpb-pdf-report') . "#" . $now,
            "model"         => $model,
            // "supplierOptions" => SupplierService::getActiveSupplier(),
        ])->render();
        \PDF::setHeaderCallback(function ($pdf) use ($header) {
            $pdf->writeHTML($header);
        });


        // $footer = view('produksi/lpb/setup-lpb.footer-pdf', [
        //     'title' => trans('menu.lpb-pdf-report')."#".$now,
        //     "model"         => $model,
        //     // "supplierOptions" => SupplierService::getActiveSupplier(),
        // ])->render();
        // \PDF::setFooterCallback(function($pdf) use ($footer) {
        //     $pdf->writeHTML($footer);
        // });

        // Custom Footer
        $footer = view('produksi/mutation_wip/mutation.footer-pdf', [
            'title' => trans('menu.lpb-pdf-report') . "#" . $now,

        ])->render();

        \PDF::setFooterCallback(function ($pdf) use ($footer) {
            $pdf->writeHTML($footer);

            // Position at 15 mm from bottom
            $pdf->SetY(-45);
            // Set font
            $pdf->SetFont('helvetica', 'I', 8);
            // Page number

            $tDate = date("F j, Y, g:i a");

            // $pdf::SetY(220);
            $pdf->Cell(63, 3, 'Yang Menyerahkan,', 0, 0, 'C', false);
            $pdf->Cell(63, 3, 'Penerima,', 0, 0, 'C', false);
            $pdf->Cell(63, 3, 'Mengetahui,', 0, 0, 'C', false);
            $pdf->ln();
            $pdf->ln();
            $pdf->ln();
            $pdf->ln();
            $pdf->ln();
            $pdf->ln();
            $pdf->ln();
            $pdf->ln();
            $pdf->ln();
            $pdf->ln();
            $pdf->Cell(0, 5, $pdf->getAliasNumPage() . '/' . $pdf->getAliasNbPages() .'-'. $tDate,  0, false, 'R', 0, '', 0, false, 'T', 'M');
        });

        $html = view('produksi/mutation_wip/mutation.content-pdf-2', [

            'title' => trans('menu.lpb-pdf-report') . "#" . $now,
            'model' => $model,

            'data'  => $query2,
            "supplierOptions" => SupplierService::getActiveSupplier(),
            "woodOptions"   => WoodService::getActiveWood(),


        ])->render();

        $pdf = new TCPDF();

        ob_end_clean();



        $pdf::SetTopMargin(36.5);
        // $pdf::SetTopMargin(36.5);
        $pdf::SetAutoPageBreak(TRUE, 50);

        $pdf::AddPage();

        $pdf::writeHTML($html, true, false, true, false, '');


        $pdf::Output('LAPORAN PENERIMAAN BAHAN SAWN TIMBER-' . $model->nomor_mutasi . '-' . $now . '.pdf');
        // $pdf::Output($query->container_number.' '.$query->created_date.'.pdf');
        // $pdf->SetFont('Helvetica', '', 9);



    }



    public function edit(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $model = HdMutasi::find($id);

        if ($model === null) {
            abort('404');
        }

        return view('produksi/mutation_wip/mutation.add', [
            "model"         => $model,
            "MutationLocationOptions"   => MutationLocationService::getActiveMutationLocation(),
        ]);
    }

    public function save(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        $this->validate($request, [
            'date_mutation_wip'    => 'required|date',
            'tally_no'      => 'required',
            'hd_mutasi_id'      => 'required',
        ]);

        // dd($request->get('date_mutation_wip'), $request->get('tally_no'));

        DB::beginTransaction();
        try {

            // $now    = new \DateTime();
            $tally_split = str_replace("\r\n", ',', $request->get('tally_no'));
            $tally_split = explode(",", $tally_split);
            $query = DB::table('tally_table')
                ->whereIn('tally_no', $tally_split)
                ->where('status_mutation_wip', '=', NULL)
                ->update(['date_mutation_wip' => $request->get('date_mutation_wip'), 'status_mutation_wip' => 0]); //0 mutasi - 1 wip

            $query2 = DB::table('tally_table')
                ->select('tally_id')
                ->whereIn('tally_no', $tally_split)
                ->where('date_mutation_wip', '=', $request->get('date_mutation_wip'))
                ->where('status_mutation_wip', '=', 0)->get();

            foreach ($query2 as $key) {
                $model  = new DtMutasi();

                $model->hd_mutasi_id       = $request->get('hd_mutasi_id');
                $model->tally_id        = $key->tally_id;

                $model->save();
            }

            // dd($query);

            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('fields.add-output-kd') . ' ' . ''])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('fields.add-input-kd') . ' ' . ''])
            );
        }

        return redirect(route('mutation-addtally', ['id' => $request->get('hd_mutasi_id')]));
    }

    public function savehd(Request $request)
    {

        if (!\Gate::allows('access', [self::RESOURCE, 'add']) && !\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $id     = intval($request->get('hd_mutasi_id', 0));

        $this->validate($request, [

            'nomor_mutasi' => 'required|max:255|unique:hd_mutasi,nomor_mutasi,' . $id . ',hd_mutasi_id',
            'tanggal_mutasi' => 'required|date',
            'lokasi_mutasi_id'            => 'required|integer',
            'supir_plat' => 'required|max:255',
            'catatan' => 'required|max:255',

        ]);

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            if (empty($id)) {
                $model  = new HdMutasi();
                $model->created_by      = \Auth::user()->id;
                $model->created_date    = $now;
            } else {
                $model  = HdMutasi::find($id);
                $model->modified_by   = \Auth::user()->id;
                $model->modified_date = $now;
            }


            $model->nomor_mutasi   = $request->get('nomor_mutasi');
            $model->tanggal_mutasi   = $request->get('tanggal_mutasi');
            $model->lokasi_mutasi_id   = $request->get('lokasi_mutasi_id');
            $model->supir_plat   = $request->get('supir_plat');
            $model->catatan   = $request->get('catatan');
            $model->status   = !empty($request->get('status')) ? true : false;

            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('fields.add-mutation') . ' ' . ''])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('fields.add-mutation') . ' ' . ''])
            );
        }

        return redirect(route('mutation-index'));
    }
}
