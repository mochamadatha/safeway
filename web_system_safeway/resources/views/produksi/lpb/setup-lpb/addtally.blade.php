@extends('layouts.master')

@section('title', trans('menu.setup-lpb'))

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">{{ trans('menu.setup-lpb') }}</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="{{ route('dashboard') }}">{{ trans('menu.dashboard') }}</a>
                            </li>
                            <li>
                                <a>{{ trans('menu.produksi') }}</a>
                            </li>
                            <li class="active">
                                {{ trans('menu.setup-lpb') }}
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            @if(Session::has('successMessage'))
            <div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-check-all"></i>
                <strong>{{ Session::get('successMessage') }}.</strong>
            </div>
            @endif

            @if(Session::has('errorMessage'))
            <div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-block-helper"></i>
                <strong>{{ Session::get('errorMessage') }}.</strong>
            </div>
            @endif
            
            <div class="row">
                <div class=" col-md-12">
                    <div class="panel panel-border panel-success">
                        <div class="panel-heading">
                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <h3 class="panel-title">{{ trans('fields.nomor_lpb') }}</h3>
                                </div>
                                <div class="col-md-3">
                                    <h3 class="panel-title">: {{ $model->nomor_lpb }}</h3>
                                </div>
                                <div class="col-md-2">
                                    <h3 class="panel-title">{{ trans('fields.supplier_name') }}</h3>
                                </div>
                                <div class="col-md-5">
                                    <h3 class="panel-title">: {{ $model->supplier_name }}</h3>
                                </div>
                            </div><br>
                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <h3 class="panel-title">{{ trans('fields.date') }}</h3>
                                </div>
                                <div class="col-md-3">
                                    <h3 class="panel-title"> : {{ date('d-m-Y', strtotime($model->tanggal_kedatangan)) }} </h3>
                                </div>
                                <div class="col-md-2">
                                    <h3 class="panel-title">{{ trans('fields.nomor_po') }}</h3>
                                </div>
                                <div class="col-md-5">
                                    <h3 class="panel-title">: {{ $model->nomor_po }} </h3>
                                </div>
                            </div><br>
                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <h3 class="panel-title">{{ trans('fields.plat_no') }}</h3>
                                </div>
                                <div class="col-md-3">
                                    <h3 class="panel-title"> : {{ $model->plat_mobil }} </h3>
                                </div>
                                <div class="col-md-2">
                                    <h3 class="panel-title">{{ trans('fields.nomor_sj') }}</h3>
                                </div>
                                <div class="col-md-5">
                                    <h3 class="panel-title">: {{ $model->nomor_sj }} </h3>
                                </div>
                            </div>
                        </div>
                        <br>
                        

                        <div class="panel-body">
                            <div class="row">
                                <form class="form-horizontal" role="form" method="post" action="{{ route('tallysheet-save') }}">
                                    {{ csrf_field() }}
                                    <div class="col-sm-12 data-table-toolbar text-right">
                                        <div class="form-group">
                                            <button type="button" class="btn btn-primary waves-effect waves-light btn-sm" data-toggle="modal" data-target="#dttally"><i class="fa fa-plus-circle"></i> {{ trans('fields.add-tally-sheet') }} </button>
                                             @can('access', [$resource, 'add'])

                                            <a data-target="#infolpb" data-toggle="modal"   class="btn btn-primary waves-effect btn-sm waves-light" data-original-title="fa-plus">
                                            <i class="fa fa-info"></i> {{ trans('fields.info') }}</a>
                                            </a>
                                            @endcan

                                             <a id="clear-details" href="{{ url('produksi/setup-lpb') }}" class="btn btn-sm btn-danger">
                                                <i class="fa fa-reply"></i> {{ trans('fields.back') }} 
                                            </a>

                           
                                        </div>
                                        <div class="row">
                                            <table class="table table-bordered table-hover table-striped m-0">
                                                <thead>
                                                    <tr>
                                                        <th width="9%" class="text-center">{{ trans('fields.num') }}</th>
                                                        <th width="9%" class="text-center">{{ trans('fields.wood-type') }}</th>
                                                        <th width="9%" class="text-center">{{ trans('fields.description-wood') }}</th>
                                                        <th width="9%" class="text-center">{{ trans('fields.no-tally-sheet') }}</th>
                                                        <th width="5%" class="text-center">{{ trans('fields.t') }}</th>
                                                        <th width="5%" class="text-center">{{ trans('fields.l') }}</th>
                                                        <th width="5%" class="text-center">{{ trans('fields.p') }}</th>
                                                        <th width="9%" class="text-center">{{ trans('fields.pcs') }}</th>
                                                        <th width="9%" class="text-center">{{ trans('fields.m3') }}</th>
                                                        <th width="9%" class="text-center">{{ trans('fields.price') }}</th>
                                                        <th width="9%" class="text-center">{{ trans('fields.total-price') }}</th>
                                                        <th width="8%" class="text-center">{{ trans('fields.note') }}</th>
                                                        <th width="5%" class="text-center">{{ trans('fields.action') }}</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $no = ($models->currentPage() - 1) * $models->perPage() + 1; ?>
                                                    @foreach($models as $isi)
                                                    <tr>
                                                        <th scope="row" style="text-align: center;">{{ $no++ }}</th>
                                                        
                                                        <td>{{ $isi->jenis_kayu }}</td>
                                                        <td>{{ $isi->nama_deskripsi }}</td>
                                                        <td>{{ $isi->tally_no }}</td>
                                                        <td>{{ $isi->tinggi }}</td>
                                                        <td>{{ $isi->lebar }}</td>
                                                        <td>{{ $isi->panjang }}</td>
                                                        <td>{{ $isi->pcs }}</td>
                                                        <td>{{ $isi->volume }}</td>
                                                        {{-- <td>{{ number_format( $isi->harga , 0 , '' , '.' ) . ",-" }}</td>
                                                        <td>{{ number_format( $isi->total_harga , 0 , '' , '.' ) . ",-" }}</td> --}}
                                                        <?php if ($isi->harga_satuan != ''): ?>
                                                            <td>{{ number_format( $isi->harga_satuan , 0 , '' , '.' ) . ",-" }}</td>
                                                            <td>{{ number_format( $isi->total_harga , 0 , '' , '.' ) . ",-" }}</td>    
                                                            <td>{{ $isi->note }}</td>    
                                                        <?php else: ?>
                                                        <td>{{ number_format( $isi->harga , 0 , '' , '.' ) . ",-" }}</td>
                                                        <td>{{ number_format( $isi->total_harga , 0 , '' , '.' ) . ",-" }}</td>
                                                        <td>--</td>
                                                        <?php endif ?>
                                                        <td class="text-center">
                                                            @can('access', [$resource, 'update'])
                                                            <a data-target="#dtharga{{ $isi->dt_lpb_id }}" data-toggle="modal"  class="btn btn-xs btn-warning" data-original-title="Edit">
                                                                <i class="fa fa-pencil"></i>
                                                            </a>
                                                            @endcan

                                                          

                                                             @can('access', [$resource, 'update'])
                                                                <a  data-toggle="modal" data-target="#deletelpb{{ $isi->tally_id }}" class="btn btn-xs btn-danger" >
                                                                    <i class="fa fa-remove"></i> 
                                                                </a>
                                                                <div id="deletelpb{{ $isi->tally_id }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                                <h4 class="modal-title">{{ trans('fields.deletenotally') }}</h4>
                                                                            </div>
                                                                            <div class="modal-body">       
                                                                                <h5>Apakah anda yakin ingin menghapus Tally Ini ??</h5>
                                                                            </div>

                                                                            <div class="modal-footer">
                                                                                <input type="hidden" name="id" class="form-control" value="{{ $isi->tally_id }}">
                                                                                <a href="{{ route('setup-lpb-destroy', ['id' => $isi->tally_id]) }}" data-toggle="tooltip" class="btn btn-danger waves-effect" >Yes</a>
                                                                                <button type="button" class="btn btn-default waves-effect waves-light" data-dismiss="modal">No</button>
                                                                            </div> 
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endcan
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            <div class="data-table-toolbar">
                                                {!! $models->render() !!}
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div> 
                        </div>

                          @foreach($models as $pop)
                        @can('access', [$resource, 'update'])

                          <div id="dtharga{{ $pop->dt_lpb_id }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form role="form" method="post" action="{{ route('setup-lpb-saveeditprice') }}">
                                        {{ csrf_field() }}
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">{{ trans('fields.update-price') }}</h4>
                                        </div>
                                        <div class="modal-body">
                                            <input type="hidden" name="dt_lpb_id" class="form-control" value="{{ $pop->dt_lpb_id }}">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="harga" class="control-label">{{ trans('fields.price') }}</label>
                                                        <input type="number" class="form-control" id="harga" name="harga" value="{{ count($errors) > 0 ? old('harga') : $pop->harga_satuan}}">
                                                    @if($errors->has('harga'))
                                                        <span class="help-block">{{ $errors->first('harga') }}</span>
                                                    @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                     <div class="form-group">
                                                        <label for="label" class="control-label">{{ trans('fields.note') }}</label>
                                                        <textarea class="form-control" rows="7" name="note" value="{{ count($errors) != 0 ? old('note') : $pop->note}}"></textarea>
                                                        @if($errors->has('note'))
                                                        <span class="help-block">{{ $errors->first('note') }}</span>
                                                    @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="col-lg-2"></div>
                                                    <div class="col-lg-4"> Metode Pembayaran </div>
                                                    <div class="col-lg-6">
                                                        <div class="radio radio-success radio-inline">
                                                            <input type="radio" value="0" name="pembayaran" checked="">
                                                            <label for="kubikasi"> Kubikasi </label>
                                                        </div> 
                                                        <div class="radio radio-danger radio-inline">
                                                            <input type="radio" value="1"  name="pembayaran">
                                                            <label for="pcs"> Pcs </label>
                                                        </div>  
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-info waves-effect waves-light">Save changes</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div><!-- /.modal -->

                        @endcan
                        @endforeach

                         <div id="infolpb" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <!-- <form id="form-cancel" role="form" method="post" action="{{ route('raft-result-savematerialms') }}">
                                        {{ csrf_field() }} -->

                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title" id="cancel-text">{{ trans('fields.info') }}</h4>
                                        </div>



                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="card-box table-responsive">
                                                     
                                                        <table id="datatable" class="table table-striped table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th>{{ trans('fields.num') }}</th>
                                                                <th>{{ trans('fields.tally_no') }}</th>
                                                               
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php $no = 1;//($models->currentPage() - 1) * $models->perPage() + 1; ?>
                                                            @foreach($rekap as $in)
                                                            <tr>
                                                                <td>{{ $no++ }}</td>
                                                                <td>{{ $in->tally_no }}</td>
                                                                
                                                            </tr>
                                                            @endforeach
                                                            </tbody>
                                                        </table>

                                                      
                                                            <!--  -->
                                                          
                                                       
                                                    </div>
                                                </div>
                                            </div>  
                                        </div>
                                       
                                       <!--  <div class="modal-footer">
                                            !! $models->render() !!
                                        </div> -->
                                    <!-- </form> -->

                                </div>
                            </div>
                        </div>

                        <div id="dttally" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form role="form" method="post" action="{{ route('tally-lpb-saveaddtally') }}">
                                        {{ csrf_field() }}
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">{{ trans('fields.add-tally-sheet') }}</h4>
                                        </div>
                                        <div class="modal-body">
                                            <input type="hidden" name="hd_lpb_id" class="form-control" value="{{ $model->hd_lpb_id }}">
                                            <input type="hidden" name="supplier_id" class="form-control" value="{{ $model->supplier_id }}">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="field-2" class="control-label">{{ trans('fields.notally') }}</label>
                                                        <select class="form-control" id="tally_id" name="tally_id">
                                                            <option value="">- {{ trans('fields.change') }} {{ trans('fields.notally') }} -</option>
                                                            @foreach($tallyOptions as $tal)
                                                            <option value="{{ $tal->tally_id }}">{{ $tal->tally_no }}  </option>
                                              
                                                            @endforeach
                                                        </select>

                                                        
                                                    </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="col-lg-2"></div>
                                                        <div class="col-lg-4"> Metode Pembayaran </div>
                                                        <div class="col-lg-6">
                                                            <div class="radio radio-success radio-inline">
                                                                <input type="radio" value="0" name="pembayaran" checked="">
                                                                <label for="kubikasi"> Kubikasi </label>
                                                            </div> 
                                                            <div class="radio radio-danger radio-inline">
                                                                <input type="radio" value="1"  name="pembayaran">
                                                                <label for="pcs"> Pcs </label>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-info waves-effect waves-light">Save changes</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div><!-- /.modal -->
                            </div>
                        </div>

                    </div>


                </div>
            </div>
        </div>
        @endsection