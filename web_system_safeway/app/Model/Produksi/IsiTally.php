<?php

namespace App\Model\Produksi;

use Illuminate\Database\Eloquent\Model;

class IsiTally extends Model
{
    protected $table        = 'dt_isi_tally';
    protected $primaryKey   = 'isi_tally_id';
    public $timestamps      = false;
}
