@extends('layouts.master')

@section('title', trans('menu.setup-employee'))

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
        	<div class="row">
				<div class="col-xs-12">
					<div class="page-title-box">
                        <h4 class="page-title">{{ trans('menu.setup-employee') }}</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="{{ route('dashboard') }}">{{ trans('menu.dashboard') }}</a>
                            </li>
                            <li>
                                <a>{{ trans('menu.master-produksi') }}</a>
                            </li>
                            <li class="active">
                                {{ trans('menu.setup-employee') }}
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
				</div>
			</div>
            <!-- end row -->
            
			<div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-border panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ trans('fields.add-employee') }}</h3>
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal" role="form" method="post" action="{{ route('setup-employee-save') }}">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="p-20">

                                            <div class="form-group {{ $errors->has('nama_pegawai') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.employee_name') }}</label>
                                                <div class="col-md-5">
                                                    <input type="text" name="nama_pegawai" class="form-control" value="{{ count($errors) > 0 ? old('nama_pegawai') : $model->nama_pegawai }}">
                                                    @if($errors->has('nama_pegawai'))
                                                        <span class="help-block">{{ $errors->first('nama_pegawai') }}</span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group {{ $errors->has('nama_divisi') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.employee_name') }}</label>
                                                <div class="col-md-5">
                                                    <select class=" form-control" id="divisi_id" name="divisi_id">
                                                        <option value="">- {{ trans('fields.change') }} {{ trans('fields.division_name') }} -</option>
                                                        @foreach($divisionOptions as $div)
                                                        <option value="{{ $div->divisi_id }}" {{ !empty($filters['division']) && $filters['division'] == $div->divisi_id ? 'selected' : ''}} >{{ $div->nama_divisi }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>


                                            <div class="form-group {{ $errors->has('nama_cabang') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.branch_name') }}</label>
                                                <div class="col-md-5">
                                                    <select class="form-control" id="cabang_id" name="cabang_id">
                                                        <option value="">- {{ trans('fields.change') }} {{ trans('fields.branch_name') }} -</option>
                                                            @foreach($branchOptions as $branch)
                                                        <option value="{{ $branch->cabang_id }}" {{ !empty($filters['branch']) && $filters['branch'] == $branch->cabang_id ? 'selected' : ''}} >{{ $branch->nama_cabang }}</option>
                                                            @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group {{ $errors->has('alamat_pegawai') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.employee_address') }}</label>
                                                <div class="col-md-5">
                                                    <input type="text" name="alamat-pegawai" class="form-control" value="{{ count($errors) > 0 ? old('alamat_pegawai') : $model->alamat_pegawai }}">
                                                    @if($errors->has('nama_pegawai'))
                                                        <span class="help-block">{{ $errors->first('employee-adress') }}</span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.email') }}</label>
                                                <div class="col-md-5">
                                                    <input type="text" name="email" class="form-control" value="{{ count($errors) > 0 ? old('email') : $model->email }}">
                                                    @if($errors->has('email'))
                                                        <span class="help-block">{{ $errors->first('email') }}</span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.phone') }}</label>
                                                <div class="col-md-5">
                                                    <input type="text" name="phone" class="form-control" value="{{ count($errors) > 0 ? old('phone') : $model->phone }}">
                                                    @if($errors->has('phone'))
                                                        <span class="help-block">{{ $errors->first('phone') }}</span>
                                                    @endif
                                                </div>
                                            </div>


                                           
                                            <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <div class="checkbox checkbox-primary">
                                                        <?php $status = count($errors) > 0 ? old('status') : $model->status; ?>
                                                        <input id="status" name="status" value="true" type="checkbox" {{ $status == true ? 'checked' : '' }}>
                                                        <label for="status">
                                                            {{ trans('fields.is-active') }}
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="col-sm-12 data-table-toolbar text-right" style="padding-top: 10px;">
                                        <div class="form-group">
                                            <a href="{{ url('master-hrd/setup-employee') }}" class="btn btn-sm btn-warning"><i class="fa fa-reply"></i> {{ trans('fields.cancel') }}</a>
                                            <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i> {{ trans('fields.save') }}</button>
                                        </div>
                                    </div> 
                                </div>
                            </form>
                        </div>
                    </div>
                        <!-- end row -->
                </div> <!-- end card-box -->
            </div><!-- end col -->
        </div>
    </div>
</div>
@endsection