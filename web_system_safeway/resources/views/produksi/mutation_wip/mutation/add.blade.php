@extends('layouts.master')

@section('title', trans('menu.setup-lpb'))

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">{{ trans('menu.mutation') }}</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="{{ route('dashboard') }}">{{ trans('menu.dashboard') }}</a>
                            </li>
                            <li>
                                <a>{{ trans('menu.produksi') }}</a>
                            </li>
                            <li class="active">
                                {{ trans('menu.mutation') }}
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-border panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ trans('fields.add-mutation') }}</h3>
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal" role="form" method="post" action="{{ route('mutation-savehd') }}">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="p-20">
                                             <input type="hidden" name="hd_mutasi_id" class="form-control" value="{{ count($errors) > 0 ? old('hd_mutasi_id') : $model->hd_mutasi_id }}">
                                             <div class="form-group {{ $errors->has('nomor_mutasi') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.nomor_dr') }}</label>
                                                <div class="col-md-5">
                                                    <input type="text" name="nomor_mutasi" class="form-control" value="{{ count($errors) > 0 ? old('nomor_mutasi') : $model->nomor_mutasi }}">
                                                    @if($errors->has('nomor_mutasi'))
                                                        <span class="help-block">{{ $errors->first('nomor_mutasi') }}</span>
                                                    @endif
                                                </div>
                                            </div>

                                       

                                             <div class="form-group {{ $errors->has('tanggal_mutasi') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.mutation_date') }}</label>
                                                <div class="col-md-5">
                                                    <input type="date" name="tanggal_mutasi" class="form-control" value="{{ count($errors) > 0 ? old('tanggal_mutasi') : $model->tanggal_mutasi }}">
                                                    @if($errors->has('tanggal_mutasi'))
                                                        <span class="help-block">{{ $errors->first('tanggal_mutasi') }}</span>
                                                    @endif
                                                </div>
                                            </div>

                                             <div class="form-group {{ $errors->has('lokasi_mutasi_id') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.lokasi_mutasi') }}</label>
                                                <div class="col-md-5">
                                                    <select class=" form-control" id="lokasi_mutasi_id" name="lokasi_mutasi_id">
                                                    <?php $mutasiId = count($errors) > 0 ? old('lokasi_mutasi') : $model->lokasi_mutasi_id; ?>

                                                        <option value="">- {{ trans('fields.change') }} {{ trans('fields.lokasi_mutasi') }} -</option>
                                                        @foreach($MutationLocationOptions as $arr)
                                                        <option value="{{ $arr->lokasi_mutasi_id }}" {{ $arr->lokasi_mutasi_id == $mutasiId ? 'selected' : '' }}>{{ $arr->lokasi_mutasi }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>


                                             <div class="form-group {{ $errors->has('supir_plat') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.driver_plat') }}</label>
                                                <div class="col-md-5">
                                                    <input type="text" name="supir_plat" class="form-control" value="{{ count($errors) > 0 ? old('supir_plat') : $model->supir_plat }}">
                                                    @if($errors->has('supir_plat'))
                                                        <span class="help-block">{{ $errors->first('supir_plat') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group {{ $errors->has('catatan') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.note') }}</label>
                                                <div class="col-md-5">
                                                  <textarea class="form-control" rows="5" name="catatan">{{ count($errors) > 0 ? old('catatan') : $model->catatan }}</textarea>
                                                </div>
                                            </div>

                                            
                                            <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <div class="checkbox checkbox-primary">
                                                        <?php $status = count($errors) > 0 ? old('status') : $model->status; ?>
                                                        <input id="status" name="status" value="true" type="checkbox" {{ $status == true ? 'checked' : '' }}>
                                                        <label for="status">
                                                            {{ trans('fields.is-active') }}
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="col-sm-12 data-table-toolbar text-right" style="padding-top: 10px;">
                                        <div class="form-group">
                                            <a href="{{ url('produksi/mutation') }}" class="btn btn-sm btn-warning"><i class="fa fa-reply"></i> {{ trans('fields.back') }}</a>
                                            <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i> {{ trans('fields.save') }}</button>
                                        </div>
                                    </div> 
                                </div>
                            </form>
                        </div>
                    </div>
                        <!-- end row -->
                </div> <!-- end card-box -->
            </div><!-- end col -->
        </div>
    </div>
</div>
@endsection