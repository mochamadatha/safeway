@extends('layouts.master')

@section('title', trans('menu.setup-lpb'))

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">{{ trans('menu.setup-lpb') }}</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="{{ route('dashboard') }}">{{ trans('menu.dashboard') }}</a>
                            </li>
                            <li>
                                <a>{{ trans('menu.produksi') }}</a>
                            </li>
                            <li class="active">
                                {{ trans('menu.setup-lpb') }}
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            @if(Session::has('successMessage'))
            <div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-check-all"></i>
                <strong>{{ Session::get('successMessage') }}.</strong>
            </div>
            @endif

            @if(Session::has('errorMessage'))
            <div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-block-helper"></i>
                <strong>{{ Session::get('errorMessage') }}.</strong>
            </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-border panel-success">
                        <div class="panel-heading">
                        </div>

                        <div class="panel-body">
                            <div class="row">
                                <form class="form-horizontal" role="form" method="post" action="">
                                    {{ csrf_field() }}
                                    <div class="col-md-12">
                                        <p class="text-muted font-14 m-b-20 pull-right">
                                            @can('access', [$resource, 'add'])
                                            <a href="{{ route('setup-lpb-add') }}" class="btn btn-primary waves-effect w-md waves-light"><i class="fa fa-plus"></i> {{ trans('fields.add-lpb') }}</a>
                                            @endcan

                                            <!-- @can('access', [$resource, 'excel'])
                                            <a type="button" class="btn btn-primary waves-effect w-md waves-light" data-toggle="modal" data-target="#dtexcel" data-original-title="Edit"  method="post" enctype="multipart/form-data"></i> {{ trans('fields.excel-tally') }}</a>
                                            @endcan


                                            @can('access', [$resource, 'excel'])
                                            <a type="button" class="btn btn-primary waves-effect w-md waves-light" data-toggle="modal" data-target="#dtexcelisi" data-original-title="Edit"  method="post" enctype="multipart/form-data"></i> {{ trans('fields.excel-tally-isi') }}</a>
                                            @endcan -->


                                      



                                            <div class="col-md-2">
                                                <input type="text" name="nomor_lpb" class="form-control" value="{{ !empty($filters['nomor_lpb']) ? $filters['nomor_lpb'] : '' }}">
                                                @if($errors->has('nomor_lpb'))
                                                <span class="help-block">{{ $errors->first('nomor_lpb') }}</span>
                                                @endif
                                            </div>




                                            <div class="col-md-1">
                                                <button type="submit" class="btn btn-info waves-effect w-xs waves-light"><i class="fa fa-search"></i> </button>
                                            </div>

                                            <div class="col-md-2">
                                                <input type="text" name="nomor_sj" class="form-control" value="{{ !empty($filters['nomor_lpb']) ? $filters['nomor_lpb'] : '' }}">
                                                @if($errors->has('nomor_sj'))
                                                <span class="help-block">{{ $errors->first('nomor_sj') }}</span>
                                                @endif
                                            </div>


                                            <div class="col-md-1">
                                                <button type="submit" class="btn btn-info waves-effect w-xs waves-light"><i class="fa fa-search"></i> </button>
                                            </div>
                                        </p>
                                    </div>
                                </form>
                            </div>


                            <div class="row">
                                <table class="table table-bordered table-hover table-striped m-0">
                                    <thead>
                                        <tr>
                                            <th width="5%" class="text-center">{{ trans('fields.num') }}</th>
                                            <th width="10%" class="text-center">{{ trans('fields.nomor_lpb') }}</th>
                                            <th width="10%" class="text-center">{{ trans('fields.arrival_date') }}</th>
                                            <th width="10%" class="text-center">{{ trans('fields.supplier_name') }}</th>
                                          
                                            <th width="10%" class="text-center">{{ trans('fields.nomor_po') }}</th>
                                            <th width="5%" class="text-center">{{ trans('fields.nomor_afkir') }}</th>
                                            <th width="10%" class="text-center">{{ trans('fields.plat_no') }}</th>
                                            <th width="10%" class="text-center">{{ trans('fields.plat_no_afkir') }}</th>
                                            <th width="10%" class="text-center">{{ trans('fields.afkir_date') }}</th>
                                            <th width="20%" class="text-center">{{ trans('fields.action') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = ($models->currentPage() - 1) * $models->perPage() + 1; ?>
                                        @foreach($models as $model)
                                        <tr>
                                            <th scope="row" style="text-align: center;">{{ $no++ }}</th>
                                            <td>{{ $model->nomor_lpb }}</td>
                                            <td>{{ date('d-m-Y', strtotime($model->tanggal_kedatangan)) }}</td>
                                            <td>{{ $model->supplier_name }}</td>
                                            <td>{{ $model->nomor_po }}</td>
                                            <td>{{ $model->nomor_sj }}</td>
                                            <td>{{ $model->plat_mobil }}</td>
                                            <td>{{ $model->plat_afkir }}</td>
                                            <td>{{ date('d-m-Y', strtotime($model->tanggal_afkir)) }}</td>
                                            <td class="text-center">

                                                @can('access', [$resource, 'update'])
                                                <a href="{{ route('setup-lpb-edit', ['id' => $model->hd_lpb_id]) }}" data-toggle="tooltip" class="btn btn-xs btn-warning" data-original-title="Edit">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                @endcan

                                                @can('access', [$resource, 'add'])
                                                <a href="{{ route('tally-lpb-add', ['id' => $model->hd_lpb_id]) }}" data-toggle="tooltip" class="btn btn-xs btn-success" data-original-title="Add Tallysheet">
                                                    <i class="fa fa-plus"></i> 
                                                </a>
                                                @endcan

                                                @can('access', [$resource, 'add'])
                                                <a href="{{ route('setup-afkir-add', ['id' => $model->hd_lpb_id]) }}" data-toggle="tooltip" class="btn btn-xs btn-inverse" data-original-title="Add Afkir">
                                                    <i class="fa fi-circle-plus"></i> 
                                                </a>
                                                @endcan


                                                @can('access', [$resource, 'add'])
                                                <a href="{{ route('report-lpb-pdf-accurate', ['id' => $model->hd_lpb_id]) }}" data-toggle="tooltip" class="btn btn-xs  btn-pink waves-light" data-original-title="Print Accurate" target="_blank">
                                                    <i class="fa fa-print"></i>
                                                </a>
                                                @endcan
                                                
                                                @can('access', [$resource, 'add'])
                                                <a href="{{ route('report-lpb-pdf', ['id' => $model->hd_lpb_id]) }}" data-toggle="tooltip" class="btn btn-xs  btn-purple waves-light" data-original-title="Print LPB" target="_blank">
                                                    <i class="fa fa-print"></i>
                                                </a>
                                                @endcan

                                                @can('access', [$resource, 'add'])
                                                <a href="{{ route('report-lpb2-pdf', ['id' => $model->hd_lpb_id]) }}" data-toggle="tooltip" class="btn btn-xs  btn-danger waves-light" data-original-title="Print Ripai" target="_blank">
                                                    <i class="fa fa-print"></i>
                                                </a>
                                                @endcan


                                                  @can('access', [$resource, 'add'])
                                                <a href="{{ route('setup-lpb-export-excel', ['id' => $model->hd_lpb_id]) }}" data-toggle="tooltip" class="btn btn-xs  btn-danger waves-light" data-original-title="Export to Excel" target="_blank"> <i class="fa fa-print"></i></a>
                                                @endcan
 

                                             


                                    


                                               <!-- @can('access', [$resource, 'delete'])
                                                <a  data-toggle="modal" data-target="#deletelpb{{ $model->hd_lpb_id }}" class="btn btn-xs btn-danger" >
                                                    <i class="fa fa-remove"></i> {{ trans('fields.delete') }}
                                                </a>
                                                <div id="deletelpb{{ $model->hd_lpb_id }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                <h4 class="modal-title">{{ trans('fields.deletenotally') }}</h4>
                                                            </div>
                                                            <div class="modal-body">       
                                                                <h5>Apakah anda yakin ingin LPB {{ $model->nomor_lpb }} ??</h5>
                                                            </div>

                                                            <div class="modal-footer">
                                                                <input type="hidden" name="id" class="form-control" value="{{ $model->hd_lpb_id }}">
                                                                <a href="{{ route('generate-notally-delete', ['id' => $model->hd_lpb_id]) }}" data-toggle="tooltip" class="btn btn-danger waves-effect" >Yes</a>
                                                                <button type="button" class="btn btn-default waves-effect waves-light" data-dismiss="modal">No</button>
                                                            </div> 
                                                        </div>
                                                    </div>
                                                </div>
                                                @endcan 

                                                @can('access', [$resource, 'view'])
                                                <a data-toggle="modal" data-toggle="tooltip" data-target="#viewgeneratenotally" class="btn btn-xs btn-success" >
                                                    <i class="fa fa-tv"></i> {{ trans('fields.view') }}
                                                </a>
                                                <div id="viewgeneratenotally" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                <h4 class="modal-title">{{ trans('fields.nomor_lpb') }} : {{ $model->hd_lpb_id }}</h4>
                                                            </div>
                                                            <div class="modal-body">       


                                                                <table class="table table-bordered table-hover table-striped m-0">
                                                                    <thead>
                                                                        <tr>
                                                                            <th width="5%" class="text-center">{{ trans('fields.num') }}</th>
                                                                            <th width="10%" class="text-center">{{ trans('fields.wood-type') }}</th>
                                                                            <th width="10%" class="text-center">{{ trans('fields.description') }}</th>
                                                                            <th width="10%" class="text-center">{{ trans('fields.high') }}</th>
                                                                            <th width="10%" class="text-center">{{ trans('fields.width') }}</th>
                                                                            <th width="10%" class="text-center">{{ trans('fields.length') }}</th>
                                                                            <th width="10%" class="text-center">{{ trans('fields.pcs') }}</th>
                                                                            <th width="10%" class="text-center">{{ trans('fields.m3') }}</th>
                                                                        </tr>
                                                                    </thead>

                                                                </table>
                                                            </div>

                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default waves-effect waves-light" data-dismiss="modal">Kembali</button>
                                                            </div> 
                                                        </div>
                                                    </div>
                                                </div>
                                                @endcan -->
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="data-table-toolbar">
                                    {!! $models->render() !!}
                                </div>
                            </div>


                            <div id="dtexcel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form role="form" method="post" action="{{ route('setup-lpb-import-excel') }}" method="post" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">{{ trans('fields.add-tally') }}</h4>
                                        </div>
                                 
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label for="file" class="col-md-3 control-label">Import</label>
                                                <div class="col-md-6">
                                                    <input type="file" id="file" name="file" class="form-control" autorequired>
                                                    <span class="help-block with-error"></span>
                                                </div>
                                            </div>
                                           
                                        </div><br>


                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                        </div>
                                      
                                    </form>
                                </div>
                            </div>
                        </div>


                         <div id="dtexcelisi" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form role="form" method="post" action="{{ route('setup-lpb-import-excelisi') }}" method="post" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">{{ trans('fields.add-tally') }}</h4>
                                        </div>
                                 
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label for="file" class="col-md-3 control-label">Import</label>
                                                <div class="col-md-6">
                                                    <input type="file" id="file" name="file" class="form-control" autorequired>
                                                    <span class="help-block with-error"></span>
                                                </div>
                                            </div>
                                           
                                        </div><br>


                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                        </div>
                                      
                                    </form>
                                </div>
                            </div>
                        </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection