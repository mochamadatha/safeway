	
	<table id="filters" width="100%" cellpadding="0" cellspacing="0" style="font-size: 11px" border="1px" align="center">
		
		<?php
			$no = 1;
			$total_pcs = 0; 
			$total_volume = 0; 
			$total_harga = 0; 
		?>
		@foreach($isi as $view)
		<tr>
			<td width="6%"><font size="9">{{ $no++ }}</font> </td>
			<td width="15%"><font size="9">{{ $view->jenis_kayu }}</font></td>
            <td width="7%"><font size="9">{{ $view->tinggi }}</font></td>
            <td width="7%"><font size="9">{{ $view->lebar }}</font></td>
            <td width="7%"><font size="9">{{ $view->panjang }}</font></td>
            <td width="10.5%"><font size="9">{{ $view->pcs }}</font></td>
			<td width="10.5%"><font size="9"> {{ number_format( $view->volume , 4 , ',' , ',' ) }}</font></td>
			<td width="14%"> <font size="9">{{ date('d-m-Y', strtotime($view->date_in_kd )) }}</font> </td>
			<td width="14%"> <font size="9">{{ date('d-m-Y', strtotime('+10 days', strtotime($view->date_in_kd))) }}</font>  </td>
			<td width="9%"><font size="9">1</font></td>
	

		
		</tr>

		@endforeach

	</table>