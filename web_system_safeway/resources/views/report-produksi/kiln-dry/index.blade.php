@extends('layouts.master')

@section('title', trans('menu.generate-notally'))

{{-- @section('header')
@parent
<link href="{{ asset('plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection --}}

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">{{ trans('menu.kiln-dry') }}</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="{{ route('dashboard') }}">{{ trans('menu.dashboard') }}</a>
                            </li>
                            <li>
                                <a>{{ trans('menu.produksi') }}</a>
                            </li>
                            <li class="active">
                                {{ trans('menu.kiln-dry') }}
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            @if(Session::has('successMessage'))
            <div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-check-all"></i>
                <strong>{{ Session::get('successMessage') }}.</strong>
            </div>
            @endif

            @if(Session::has('errorMessage'))
            <div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-block-helper"></i>
                <strong>{{ Session::get('errorMessage') }}.</strong>
            </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-border panel-success">
                        <div class="panel-heading">
                        </div>

                        <div class="panel-body">
                            <div class="row">
                                <form class="form-horizontal" role="form" method="post" action="">
                                    {{ csrf_field() }}
                                    <div class="col-md-12">
                                        <p class="text-muted font-14 m-b-20 pull-right">
                                            @can('access', [$resource, 'add'])
                                            <a href="{{ route('kiln-dry-excel') }}" class="btn btn-primary waves-effect w-md waves-light"><i class="fa fa-plus"></i> {{ trans('fields.export-excel') }}</a>
                                            @endcan

                                            <div class="col-md-2">
                                                <select class="form-control" id="status" name="status">
                                                    <option value="">{{ trans('fields.choose-filter-all') }}</option>
                                                    <option value="rm" {{ !empty($filters['status']) && $filters['status'] == 'rm' ? 'selected' : ''}} >Raw Material</option>
                                                    <option value="inkd" {{ !empty($filters['status']) && $filters['status'] == 'inkd' ? 'selected' : ''}} >Dalam KD</option>
                                                    <option value="outkd" {{ !empty($filters['status']) && $filters['status'] == 'outkd' ? 'selected' : ''}} >Output KD</option>
                                                    <option value="wip" {{ !empty($filters['status']) && $filters['status'] == 'wip' ? 'selected' : ''}} >WIP</option>

                                                </select>
                                                @if($errors->has('status'))
                                                    <span class="help-block">{{ $errors->first('status') }}</span>
                                                @endif
                                            </div>

                                     

                                            <div class="col-md-2">
                                                <input type="date" name="start_date" class="form-control" value="{{ !empty($filters['start_date']) ? $filters['start_date'] : '' }}">
                                                @if($errors->has('start_date'))
                                                <span class="help-block">{{ $errors->first('start_date') }}</span>
                                                @endif
                                            </div>

                                            <div class="col-md-2">
                                                <input type="date" name="end_date" class="form-control" value="{{ !empty($filters['end_date']) ? $filters['end_date'] : '' }}">
                                                @if($errors->has('end_date'))
                                                <span class="help-block">{{ $errors->first('end_date') }}</span>
                                                @endif
                                            </div>

                                            <div class="col-md-1">
                                                <button type="submit" class="btn btn-info waves-effect w-xs waves-light"><i class="fa fa-search"></i> </button>
                                            </div>
                                        </p>
                                    </div>
                                </form>
                            </div>


                            <div class="row">
                                <table class="table table-bordered table-hover table-striped m-0">
                                    <thead>
                                        <tr>
                                            <th width="5%" class="text-center">{{ trans('fields.num') }}</th>
                                            <th width="9%" class="text-center">{{ trans('fields.tally_no') }}</th>
                                            <th width="9%" class="text-center">{{ trans('fields.arrival_date') }}</th>
                                            <th width="9%" class="text-center">{{ trans('fields.date-input-kd') }}</th>
                                            <th width="9%" class="text-center">{{ trans('fields.date-output-kd') }}</th>
                                            <th width="9%" class="text-center">{{ trans('fields.wood-type') }}</th>
                                            {{-- <th width="9%" class="text-center">{{ trans('fields.description') }}</th> --}}
                                            <th width="8%" class="text-center">{{ trans('fields.high') }}</th>
                                            <th width="8%" class="text-center">{{ trans('fields.width') }}</th>
                                            <th width="8%" class="text-center">{{ trans('fields.length') }}</th>
                                            <th width="8%" class="text-center">{{ trans('fields.pcs') }}</th>
                                            <th width="8%" class="text-center">{{ trans('fields.m3') }}</th>
                                            {{-- <th width="8%" class="text-center">{{ trans('fields.arrival-location') }}</th> --}}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = ($models->currentPage() - 1) * $models->perPage() + 1; ?>
                                        @foreach($models as $model)
                                        <tr>
                                            <th scope="row" style="text-align: center;">{{ $no++ }}</th>
                                            <td class="text-center">{{ $model->tally_no }}</td>
                                            {{-- <td class="text-center">{{ date('d-m-Y', strtotime($model->tanggal_kedatangan)) }}</td> --}}
                                            <td class="text-center">{{ date('d-m-Y', strtotime($model->date_in_kd)) }}</td>
                                            <td class="text-center">{{ date('d-m-Y', strtotime($model->date_out_kd)) }}</td>
                                            <td class="text-center">{{ $model->jenis_kayu }}</td>
                                            {{-- <td class="text-center">{{ $model->nama_deskripsi }}</td> --}}
                                            <td class="text-center">{{ $model->tinggi }}</td>
                                            <td class="text-center">{{ $model->lebar}}</td>
                                            <td class="text-center">{{ $model->panjang }}</td>
                                            <td class="text-center">{{ $model->pcs }}</td>
                                            <td class="text-center">{{ $model->volume }}</td>
                                            {{-- <td class="text-center">{{ $model->lokasi_kedatangan }}</td> --}}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="data-table-toolbar">
                                    {!! $models->render() !!}
                                </div>
                            </div>

                            <div id="dtexcel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form role="form" method="post" action="{{ route('generate-notally-import-excel') }}" method="post" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">{{ trans('fields.add-tally') }}</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label for="file" class="col-md-3 control-label">Import</label>
                                                <div class="col-md-6">
                                                    <input type="file" id="file" name="file" class="form-control" autorequired>
                                                    <span class="help-block with-error"></span>
                                                </div>
                                            </div>


                                                <div class="form-group">
                                                    <div class="col-md-3 col-md-offset-3">
                                                    <a href="{{ route('generate-notally-export-excel') }}" class="btn btn-success">Export</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><br>


                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div id="dtisi" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form role="form" method="post" action="{{ route('generate-notally-import-excelisi') }}" method="post" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">{{ trans('fields.add-tally') }}</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label for="file" class="col-md-3 control-label">Import</label>
                                                <div class="col-md-6">
                                                    <input type="file" id="file" name="file" class="form-control" autorequired>
                                                    <span class="help-block with-error"></span>
                                                </div>
                                            </div>
                                        </div><br>


                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>



                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
@parent
    <script src="{{ asset('plugins/select2/js/select2.min.js') }}" type="text/javascript"></script>


    <!-- plugin js -->
    <script src="{{ asset('plugins/moment/moment.js') }}"></script>
    <script src="{{ asset('plugins/timepicker/bootstrap-timepicker.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/clockpicker/js/bootstrap-clockpicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

    <!-- Init js -->
    <script src="{{asset('assets/pages/jquery.form-pickers.init.js') }}"></script>

    <!-- App js -->
    <script src="'{{asset('assets/js/jquery.core.js') }}"></script>
    <script src="'{{asset('assets/js/jquery.app.js') }}"></script>
@endsection