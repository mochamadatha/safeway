<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (Auth::check()) {
        return redirect('/dashboard');
    } else {
        return redirect('/login');
    }
});

Route::group(['middleware' => ['menu']], function () {
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

    Route::prefix('master-role')->group(function () {
        Route::prefix('setup-role')->group(function () {
            Route::any('/', 'MasterRole\SetupRoleController@index')->name('setup-role-index');
            Route::get('/add', 'MasterRole\SetupRoleController@add')->name('setup-role-add');
            Route::get('/edit/{id}', 'MasterRole\SetupRoleController@edit')->name('setup-role-edit');
            Route::post('/save', 'MasterRole\SetupRoleController@save')->name('setup-role-save');
        });
    });

    Route::prefix('master-user')->group(function () {
        Route::prefix('setup-status')->group(function () {
            Route::any('/', 'MasterUser\SetupStatusController@index')->name('setup-status-index');
            Route::get('/add', 'MasterUser\SetupStatusController@add')->name('setup-status-add');
            Route::get('/edit/{id}', 'MasterUser\SetupStatusController@edit')->name('setup-status-edit');
            Route::post('/save', 'MasterUser\SetupStatusController@save')->name('setup-status-save');
        });

        Route::prefix('setup-user')->group(function () {
            Route::any('/', 'MasterUser\SetupUserController@index')->name('setup-user-index');
            Route::get('/add', 'MasterUser\SetupUserController@add')->name('setup-user-add');
            Route::get('/edit/{id}', 'MasterUser\SetupUserController@edit')->name('setup-user-edit');
            Route::post('/save', 'MasterUser\SetupUserController@save')->name('setup-user-save');
        });
    });


    Route::prefix('master-hrd')->group(function () {
        Route::prefix('setup-division')->group(function () {
            Route::any('/', 'MasterHRD\SetupDivisionController@index')->name('setup-division-index');
            Route::get('/add', 'MasterHRD\SetupDivisionController@add')->name('setup-division-add');
            Route::get('/edit/{id}', 'MasterHRD\SetupDivisionController@edit')->name('setup-division-edit');
            Route::post('/save', 'MasterHRD\SetupDivisionController@save')->name('setup-division-save');
        });
        Route::prefix('setup-branch')->group(function () {
            Route::any('/', 'MasterHRD\SetupBranchController@index')->name('setup-branch-index');
            Route::get('/add', 'MasterHRD\SetupBranchController@add')->name('setup-branch-add');
            Route::get('/edit/{id}', 'MasterHRD\SetupBranchController@edit')->name('setup-branch-edit');
            Route::post('/save', 'MasterHRD\SetupBranchController@save')->name('setup-branch-save');
        });
        Route::prefix('setup-employee')->group(function () {
            Route::any('/', 'MasterHRD\SetupEmployeeController@index')->name('setup-employee-index');
            Route::get('/add', 'MasterHRD\SetupEmployeeController@add')->name('setup-employee-add');
            Route::get('/edit/{id}', 'MasterHRD\SetupEmployeeController@edit')->name('setup-employee-edit');
            Route::post('/save', 'MasterHRD\SetupEmployeeController@save')->name('setup-employee-save');
        });
    });

    Route::prefix('master-ispm')->group(function () {

        Route::prefix('setup-size')->group(function () {
            Route::any('/', 'MasterIspm\SetupSizeController@index')->name('setup-sizeispm-index');
            Route::get('/add', 'MasterIspm\SetupSizeController@add')->name('setup-sizeispm-add');
            Route::get('/edit/{id}', 'MasterIspm\SetupSizeController@edit')->name('setup-sizeispm-edit');
            Route::post('/save', 'MasterIspm\SetupSizeController@save')->name('setup-sizeispm-save');
        });
    });

    Route::prefix('master-container')->group(function () {
        Route::prefix('setup-costumer')->group(function () {
            Route::any('/', 'MasterContainer\SetupCustomerController@index')->name('setup-customer-index');
            Route::get('/add', 'MasterContainer\SetupCustomerController@add')->name('setup-customer-add');
            Route::get('/edit/{id}', 'MasterContainer\SetupCustomerController@edit')->name('setup-customer-edit');
            Route::post('/save', 'MasterContainer\SetupCustomerController@save')->name('setup-customer-save');
        });
        Route::prefix('setup-agent')->group(function () {
            Route::any('/', 'MasterContainer\SetupAgentController@index')->name('setup-agent-index');
            Route::get('/add', 'MasterContainer\SetupAgentController@add')->name('setup-agent-add');
            Route::get('/edit/{id}', 'MasterContainer\SetupAgentController@edit')->name('setup-agent-edit');
            Route::post('/save', 'MasterContainer\SetupAgentController@save')->name('setup-agent-save');
        });
        Route::prefix('setup-depolocation')->group(function () {
            Route::any('/', 'MasterContainer\SetupDepoLocationController@index')->name('setup-depolocation-index');
            Route::get('/add', 'MasterContainer\SetupDepoLocationController@add')->name('setup-depolocation-add');
            Route::get('/edit/{id}', 'MasterContainer\SetupDepoLocationController@edit')->name('setup-depolocation-edit');
            Route::post('/save', 'MasterContainer\SetupDepoLocationController@save')->name('setup-depolocation-save');
        });
        Route::prefix('setup-inspect')->group(function () {
            Route::any('/', 'MasterContainer\SetupInspectController@index')->name('setup-inspect-index');
            Route::get('/add', 'MasterContainer\SetupInspectController@add')->name('setup-inspect-add');
            Route::get('/edit/{id}', 'MasterContainer\SetupInspectController@edit')->name('setup-inspect-edit');
            Route::post('/save', 'MasterContainer\SetupInspectController@save')->name('setup-inspect-save');
        });
        Route::prefix('setup-size')->group(function () {
            Route::any('/', 'MasterContainer\SetupSizeController@index')->name('setup-size-index');
            Route::get('/add', 'MasterContainer\SetupSizeController@add')->name('setup-size-add');
            Route::get('/edit/{id}', 'MasterContainer\SetupSizeController@edit')->name('setup-size-edit');
            Route::post('/save', 'MasterContainer\SetupSizeController@save')->name('setup-size-save');
        });
    });

    Route::prefix('division-container')->group(function () {
        Route::prefix('container-inspect')->group(function () {
            Route::any('/', 'DivisionContainer\ContainerInspectionReport@index')->name('container-inspect-index');
            Route::get('/add', 'DivisionContainer\ContainerInspectionReport@add')->name('container-inspect-add');
            Route::get('/edit/{id}', 'DivisionContainer\ContainerInspectionReport@edit')->name('container-inspect-edit');
            Route::post('/save', 'DivisionContainer\ContainerInspectionReport@save')->name('container-inspect-save');
            Route::get('/delete/{id}', 'DivisionContainer\ContainerInspectionReport@delete')->name('container-inspect-delete');
            Route::get('/pdf/{id}', 'DivisionContainer\ContainerInspectionReport@reportcontainerpdf')->name('container-inspect-pdf');
        });
    });


    Route::prefix('division-ispm')->group(function () {
        Route::prefix('ispm-inspect')->group(function () {
            Route::any('/', 'DivisionIspm\IspmInspectionReport@index')->name('ispm-inspect-index');
            Route::get('/add', 'DivisionIspm\IspmInspectionReport@add')->name('ispm-inspect-add');
            Route::get('/edit/{id}', 'DivisionIspm\IspmInspectionReport@edit')->name('ispm-inspect-edit');
            Route::post('/save', 'DivisionIspm\IspmInspectionReport@save')->name('ispm-inspect-save');
            Route::get('/delete/{id}', 'DivisionIspm\IspmInspectionReport@delete')->name('ispm-inspect-delete');
            Route::get('/pdf/{id}', 'DivisionIspm\IspmInspectionReport@reportispmpdf')->name('ispm-inspect-pdf');
        });
    });

    // Route::prefix('report-produksi')->group(function () {
    //     Route::prefix('report-lpb')->group(function () {
    //         Route::any('/', 'ReportProduksi\ReportLPB\ReportLPBController@index')->name('report-lpb-index');
    //         Route::get('/add/{id}', 'ReportProduksi\ReportLPB\ReportLPBController@add')->name('report-lpb-add');
    //         Route::get('/edit/{id}', 'ReportProduksi\ReportLPB\ReportLPBController@edit')->name('report-lpb-edit');
    //         Route::post('/save', 'ReportProduksi\ReportLPB\ReportLPBController@save')->name('report-lpb-save');
    //     });
    // });


    Route::prefix('produksi')->group(function () {
        Route::prefix('generate-notally')->group(function () {
            Route::any('/', 'Produksi\Tally\GenerateNoTallyController@index')->name('generate-notally-index');
            Route::get('/add', 'Produksi\Tally\GenerateNoTallyController@add')->name('generate-notally-add');
            Route::get('/printf', 'Produksi\Tally\GenerateNoTallyController@printf')->name('generate-notally-printf');
            Route::post('/print', 'Produksi\Tally\GenerateNoTallyController@print')->name('generate-notally-print');
            Route::get('/delete/{id}', 'Produksi\Tally\GenerateNoTallyController@delete')->name('generate-notally-delete');
            Route::post('/saveadd', 'Produksi\Tally\GenerateNoTallyController@saveadd')->name('generate-notally-saveadd');
            Route::post('/save', 'Produksi\Tally\GenerateNoTallyController@save')->name('generate-notally-save');
            Route::post('/import-excel', 'Produksi\Tally\GenerateNoTallyController@importexcel')->name('generate-notally-import-excel');
            Route::post('/import-excelisi', 'Produksi\Tally\GenerateNoTallyController@importexcelisi')->name('generate-notally-import-excelisi');
            // Route::post('/import-excelisi', 'Produksi\Tally\GenerateNoTallyController@importexcelisi')->name('generate-notally-export-excel');

            Route::get('/export-excel', 'Produksi\Tally\GenerateNoTallyController@exportexcel')->name('generate-notally-export-excel');
            // Route::get('/view/{id}', 'Produksi\SetupLPB\GenerateNoTallyController@viewisi')->name('generate-notally-viewisi');
        });

        Route::prefix('search-lpb')->group(function () {
            Route::any('/', 'Produksi\Tally\SearchLPBController@index')->name('search-lpb-index');
        });



        Route::prefix('tallysheet')->group(function () {
            Route::any('/', 'Produksi\Tally\TallySheetController@index')->name('tallysheet-index');
            Route::get('/add/{id}', 'Produksi\Tally\TallySheetController@add')->name('tallysheet-add');
            Route::get('/edit/{id}', 'Produksi\Tally\TallySheetController@edit')->name('tallysheet-edit');
            Route::post('/save', 'Produksi\Tally\TallySheetController@save')->name('tallysheet-save');
            Route::get('/destroy/{id}', 'Produksi\Tally\TallySheetController@destroy')->name('tallysheet-destroy');
        });
        Route::prefix('bandsaw')->group(function () {
            Route::any('/', 'Produksi\Tally\BandsawController@index')->name('bandsaw-index');
            Route::get('/add/{id}', 'Produksi\Tally\BandsawController@add')->name('bandsaw-add');
            Route::get('/edit/{id}', 'Produksi\Tally\BandsawController@edit')->name('bandsaw-edit');
            Route::post('/save', 'Produksi\Tally\BandsawController@save')->name('bandsaw-save');
            Route::get('/destroy/{id}', 'Produksi\Tally\BandsawController@destroy')->name('bandsaw-destroy');
        });

        Route::prefix('bandsaw-kering')->group(function () {
            Route::any('/', 'Produksi\OutputKD\BandsawKeringController@index')->name('bandsaw-kering-index');
            Route::get('/add/{id}', 'Produksi\OutputKD\BandsawKeringController@add')->name('bandsaw-kering-add');
            Route::get('/edit/{id}', 'Produksi\OutputKD\BandsawKeringController@edit')->name('bandsaw-kering-edit');
            Route::post('/save', 'Produksi\OutputKD\BandsawKeringController@save')->name('bandsaw-kering-save');
            Route::get('/destroy/{id}', 'Produksi\OutputKD\BandsawKeringController@destroy')->name('bandsaw-kering-destroy');
        });
        Route::prefix('afkirproses-kering')->group(function () {
            Route::any('/', 'Produksi\OutputKD\AfkirProsesKeringController@index')->name('afkirproses-kering-index');
            Route::get('/add/{id}', 'Produksi\OutputKD\AfkirProsesKeringController@add')->name('afkirproses-kering-add');
            Route::get('/edit/{id}', 'Produksi\OutputKD\AfkirProsesKeringController@edit')->name('afkirproses-kering-edit');
            Route::post('/save', 'Produksi\OutputKD\AfkirProsesKeringController@save')->name('afkirproses-kering-save');
        });



        Route::prefix('setup-lpb')->group(function () {
            Route::any('/', 'Produksi\SetupLPB\SetupLPBController@index')->name('setup-lpb-index');
            Route::get('/add/', 'Produksi\SetupLPB\SetupLPBController@add')->name('setup-lpb-add');
            Route::post('/save', 'Produksi\SetupLPB\SetupLPBController@save')->name('setup-lpb-save');
            Route::post('/saveeditprice', 'Produksi\SetupLPB\SetupLPBController@saveeditprice')->name('setup-lpb-saveeditprice');
            Route::get('/addtally/{id}', 'Produksi\SetupLPB\SetupLPBController@addtally')->name('tally-lpb-add');
            Route::post('/saveaddtally', 'Produksi\SetupLPB\SetupLPBController@saveaddtally')->name('tally-lpb-saveaddtally');
            Route::get('/edit/{id}', 'Produksi\SetupLPB\SetupLPBController@edit')->name('setup-lpb-edit');
            Route::get('/pdf/{id}', 'Produksi\SetupLPB\SetupLPBController@reportlpbpdf')->name('report-lpb-pdf');
            Route::get('/pdf2/{id}', 'Produksi\SetupLPB\SetupLPBController@reportlpb2pdf')->name('report-lpb2-pdf');
            // Route::get('/export-excel/{id}', 'Produksi\SetupLPB\SetupLPBController@exportexcel')->name('setup-lpb-export-excel');
            Route::get('/export-excel/{id}', 'Produksi\SetupLPB\SetupLPBController@reportlpbexcel')->name('setup-lpb-export-excel');
            Route::get('/accuratepdf/{id}', 'Produksi\SetupLPB\SetupLPBController@reportlpbpdfaccurate')->name('report-lpb-pdf-accurate');
            Route::post('/import-excel', 'Produksi\SetupLPB\SetupLPBController@importexcel')->name('setup-lpb-import-excel');
            Route::post('/import-excelisi', 'Produksi\SetupLPB\SetupLPBController@importexcelisi')->name('setup-lpb-import-excelisi');
            Route::get('/destroy/{id}', 'Produksi\SetupLPB\SetupLPBController@destroy')->name('setup-lpb-destroy');
        });
        Route::prefix('setup-afkir')->group(function () {
            Route::any('/', 'Produksi\SetupLPB\SetupAfkirController@index')->name('setup-afkir-index');
            Route::get('/add/{id}', 'Produksi\SetupLPB\SetupAfkirController@add')->name('setup-afkir-add');
            Route::get('/edit/{id}', 'Produksi\SetupLPB\SetupAfkirController@edit')->name('setup-afkir-edit');
            Route::post('/save', 'Produksi\SetupLPB\SetupAfkirController@save')->name('setup-afkir-save');
            Route::get('/pdf/{id}', 'Produksi\SetupLPB\SetupAfkirController@reportafkirpdf')->name('setup-afkir-pdf');
            Route::get('/pdfdetail/{id}', 'Produksi\SetupLPB\SetupAfkirController@reportafkirdetailpdf')->name('setup-afkir-detail-pdf');
            Route::get('/destroy/{id}', 'Produksi\SetupLPB\SetupAfkirController@destroy')->name('setup-afkir-destroy');
        });
        Route::prefix('raw-material')->group(function () {
            Route::any('/', 'Produksi\RawMaterial\RawMaterialController@index')->name('raw-material-index');
            Route::get('/add/{id}', 'Produksi\RawMaterial\RawMaterialController@add')->name('raw-material-add');
            Route::get('/edit/{id}', 'Produksi\RawMaterial\RawMaterialController@edit')->name('raw-material-edit');
            Route::post('/save', 'Produksi\RawMaterial\RawMaterialController@save')->name('raw-material-save');
            Route::post('/import-excel', 'Produksi\RawMaterial\RawMaterialController@importexcel')->name('raw-material-import-excel');
            Route::get('/export-excel', 'Produksi\RawMaterial\RawMaterialController@exportexcel')->name('raw-material-export-excel');
            Route::post('/import-saldo', 'Produksi\RawMaterial\RawMaterialController@importsaldo')->name('raw-material-import-saldo');
            Route::post('/import-lpb', 'Produksi\RawMaterial\RawMaterialController@importlpb')->name('raw-material-import-lpb');
        });

        Route::prefix('afkir-proses')->group(function () {
            Route::any('/', 'Produksi\RawMaterial\AfkirProsesController@index')->name('afkir-proses-index');
            Route::get('/add/{id}', 'Produksi\RawMaterial\AfkirProsesController@add')->name('afkir-proses-add');
            Route::get('/edit/{id}', 'Produksi\RawMaterial\AfkirProsesController@edit')->name('afkir-proses-edit');
            Route::post('/save', 'Produksi\RawMaterial\AfkirProsesController@save')->name('afkir-proses-save');
        });

        Route::prefix('input-kd')->group(function () {
            Route::any('/', 'Produksi\InputKD\InputKDController@index')->name('input-kd-index');
            // Route::post('/add', 'Produksi\InputKD\InputKDController@add')->name('input-kd-add');
            // Route::get('/edit/{id}', 'Produksi\InputKD\InputKDController@edit')->name('input-kd-edit');
            Route::post('/save', 'Produksi\InputKD\InputKDController@save')->name('input-kd-save');
            Route::get('/export-excel', 'Produksi\InputKD\InputKDController@exportexcel')->name('input-kd-export-excel');
            Route::get('/exportdatakd', 'Produksi\InputKD\InputKDController@exportdatakd')->name('input-kd-exportdatakd');
            Route::get('/pdfchamber1', 'Produksi\InputKD\InputKDController@pdfchamber1')->name('input-kd-pdfchamber1');
            Route::get('/pdfchamber2', 'Produksi\InputKD\InputKDController@pdfchamber2')->name('input-kd-pdfchamber2');
            Route::get('/pdfchamber3', 'Produksi\InputKD\InputKDController@pdfchamber3')->name('input-kd-pdfchamber3');
        });
        Route::prefix('output-kd')->group(function () {
            Route::any('/', 'Produksi\OutputKD\OutputKDController@index')->name('output-kd-index');
            Route::get('/add/{id}', 'Produksi\OutputKD\OutputKDController@add')->name('output-kd-add');
            Route::get('/edit/{id}', 'Produksi\OutputKD\OutputKDController@edit')->name('output-kd-edit');
            Route::post('/save', 'Produksi\OutputKD\OutputKDController@save')->name('output-kd-save');
            Route::get('/export-excel', 'Produksi\OutputKD\OutputKDController@exportexcel')->name('output-kd-export-excel');
            Route::get('/export-excel', 'Produksi\OutputKD\OutputKDController@exportexcel')->name('output-kd-export-excel');
            Route::get('/exportdataoutkd', 'Produksi\OutputKD\OutputKDController@exportdataoutkd')->name('output-kd-exportdataoutkd');

        });



        Route::prefix('mutation')->group(function () {
            Route::any('/', 'Produksi\Mutation_Wip\MutationController@index')->name('mutation-index');
            Route::get('/add', 'Produksi\Mutation_Wip\MutationController@add')->name('mutation-add');
            Route::get('/addtally/{id}', 'Produksi\Mutation_Wip\MutationController@addtally')->name('mutation-addtally');
            Route::get('/edit/{id}', 'Produksi\Mutation_Wip\MutationController@edit')->name('mutation-edit');
            Route::post('/save', 'Produksi\Mutation_Wip\MutationController@save')->name('mutation-save');
            Route::post('/savehd', 'Produksi\Mutation_Wip\MutationController@savehd')->name('mutation-savehd');
            Route::get('/pdf/{id}', 'Produksi\Mutation_Wip\MutationController@pdf')->name('mutation-pdf');
            Route::get('/pdfattach/{id}', 'Produksi\Mutation_Wip\MutationController@pdfattach')->name('mutation-pdfattach');
            Route::get('/export-excel/{id}', 'Produksi\Mutation_Wip\MutationController@exportexcel')->name('mutation-export-excel');
        });
        Route::prefix('wip')->group(function () {
            Route::any('/', 'Produksi\Mutation_Wip\WIPController@index')->name('wip-index');
            Route::get('/add/{id}', 'Produksi\Mutation_Wip\WIPController@add')->name('wip-add');
            Route::any('/stockafkir', 'Produksi\Mutation_Wip\WIPController@stockafkir')->name('wip-stockafkir');
            Route::get('/edit/{id}', 'Produksi\Mutation_Wip\WIPController@edit')->name('wip-edit');
            Route::post('/save', 'Produksi\Mutation_Wip\WIPController@save')->name('wip-save');
            Route::post('/savestockafkir', 'Produksi\Mutation_Wip\WIPController@savestockafkir')->name('wipstockafkir-save');
            Route::post('/savetakeafkir', 'Produksi\Mutation_Wip\WIPController@savetakeafkir')->name('wip-savetakeafkir');
            Route::post('/saveminafkir', 'Produksi\Mutation_Wip\WIPController@saveminafkir')->name('wip-saveminafkir');
            Route::post('/saveplusafkir', 'Produksi\Mutation_Wip\WIPController@saveplusafkir')->name('wip-saveplusafkir');
            Route::get('/exportexcel', 'Produksi\Mutation_Wip\WIPController@exportexcel')->name('wip-exportexcel');
            Route::get('/exportoutwip', 'Produksi\Mutation_Wip\WIPController@exportoutwip')->name('wip-exportoutwip');
            Route::get('/addafkirkering/{id}', 'Produksi\Mutation_Wip\AfkirKeringController@addafkirkering')->name('afkirkring-wip-add');
        });


        Route::prefix('ms/stockms')->group(function () {

            Route::any('/', 'Produksi\Mutation_Wip\MSController@stockms')->name('ms-stockms');
            Route::get('/add/{id}', 'Produksi\Mutation_Wip\MSController@add')->name('ms-add');
            Route::get('/edit/{id}', 'Produksi\Mutation_Wip\MSController@edit')->name('ms-edit');
            Route::post('/save', 'Produksi\Mutation_Wip\MSController@save')->name('ms-save');
            Route::post('/saveisi', 'Produksi\Mutation_Wip\MSController@saveisi')->name('ms-saveisi');
            Route::post('/savetakems', 'Produksi\Mutation_Wip\MSController@savetakems')->name('ms-savetakems');
            Route::post('/saveminms', 'Produksi\Mutation_Wip\MSController@saveminms')->name('ms-saveminms');
            Route::post('/saveplusms', 'Produksi\Mutation_Wip\MSController@saveplusms')->name('ms-saveplusms');
            Route::post('/import-excel', 'Produksi\Mutation_Wip\MSController@importexcel')->name('ms-import-excel');
            Route::get('/destroy/{id}', 'Produksi\Mutation_Wip\MSController@destroy')->name('ms-destroy');
            Route::get('/export-excel', 'Produksi\Mutation_Wip\MSController@exportexcel')->name('ms-export-excel');
        });

        Route::prefix('afkir-kering')->group(function () {
            Route::any('/', 'Produksi\Mutation_Wip\AfkirKeringController@index')->name('afkir-kering-index');
            Route::get('/add/{id}', 'Produksi\Mutation_Wip\AfkirKeringController@add')->name('afkir-kering-add');
            Route::get('/edit/{id}', 'Produksi\Mutation_Wip\AfkirKeringController@edit')->name('afkir-kering-edit');
            Route::post('/save', 'Produksi\Mutation_Wip\AfkirKeringController@save')->name('afkir-kering-save');
        });

        Route::prefix('reproses-afkir')->group(function () {
            Route::any('/', 'Produksi\Mutation_Wip\ReprosesAfkirController@index')->name('reproses-afkir-index');
            Route::get('/add/{id}', 'Produksi\Mutation_Wip\ReprosesAfkirController@add')->name('reproses-afkir-add');
            Route::get('/edit/{id}', 'Produksi\Mutation_Wip\ReprosesAfkirController@edit')->name('reproses-afkir-edit');
            Route::post('/save', 'Produksi\Mutation_Wip\ReprosesAfkirController@save')->name('reproses-afkir-save');
        });

        Route::prefix('reproses-ms')->group(function () {
            Route::any('/', 'Produksi\Mutation_Wip\ReprosesMsController@index')->name('reproses-ms-index');
            Route::get('/add/{id}', 'Produksi\Mutation_Wip\ReprosesMsController@add')->name('reproses-ms-add');
            Route::get('/edit/{id}', 'Produksi\Mutation_Wip\ReprosesMsController@edit')->name('reproses-ms-edit');
            Route::post('/save', 'Produksi\Mutation_Wip\ReprosesMsController@save')->name('reproses-ms-save');
        });

        Route::prefix('proses-ms')->group(function () {
            Route::any('/', 'Produksi\Mutation_Wip\ProsesMsController@index')->name('proses-ms-index');
            Route::get('/add/{id}', 'Produksi\Mutation_Wip\ProsesMsController@add')->name('proses-ms-add');
            Route::get('/edit/{id}', 'Produksi\Mutation_Wip\ProsesMsController@edit')->name('proses-ms-edit');
            Route::post('/save', 'Produksi\Mutation_Wip\ProsesMsController@save')->name('proses-ms-save');
        });



        Route::prefix('hasil-reprosesafkir')->group(function () {
            Route::any('/', 'Produksi\Mutation_Wip\HasilReprosesAfkirController@index')->name('hasil-reprosesafkir-index');
            Route::get('/add/{id}', 'Produksi\Mutation_Wip\HasilReprosesAfkirController@add')->name('hasil-reprosesafkir-add');
            Route::get('/edit/{id}', 'Produksi\Mutation_Wip\HasilReprosesAfkirController@edit')->name('hasil-reprosesafkir-edit');
            Route::post('/save', 'Produksi\Mutation_Wip\HasilReprosesAfkirController@save')->name('hasil-reprosesafkir-save');
            Route::post('/import-excel', 'Produksi\Mutation_Wip\HasilReprosesAfkirController@importexcel')->name('hasil-reprosesafkir-import-excel');
        });

        Route::prefix('hasil-reprosesms')->group(function () {
            Route::any('/', 'Produksi\Mutation_Wip\HasilReprosesMsController@index')->name('hasil-reprosesms-index');
            Route::get('/add/{id}', 'Produksi\Mutation_Wip\HasilReprosesMsController@add')->name('hasil-reprosesms-add');
            Route::get('/edit/{id}', 'Produksi\Mutation_Wip\HasilReprosesMsController@edit')->name('hasil-reprosesms-edit');
            Route::post('/save', 'Produksi\Mutation_Wip\HasilReprosesMsController@save')->name('hasil-reprosesms-save');
        });

        Route::prefix('raft-result')->group(function () {
            Route::any('/', 'Produksi\Raft_Result\RaftResultController@index')->name('raft-result-index');
            Route::get('/add/', 'Produksi\Raft_Result\RaftResultController@add')->name('raft-result-add');
            Route::get('/edit/{id}', 'Produksi\Raft_Result\RaftResultController@edit')->name('raft-result-edit');
            Route::get('/print/{id}', 'Produksi\Raft_Result\RaftResultController@print')->name('raft-result-print');
            Route::post('/save', 'Produksi\Raft_Result\RaftResultController@save')->name('raft-result-save');
            Route::post('/savematerialms', 'Produksi\Raft_Result\RaftResultController@savematerialms')->name('raft-result-savematerialms');
            Route::get('/addmaterial/{id}', 'Produksi\Raft_Result\RaftResultController@addmaterial')->name('raft-result-addmaterial');
        });

        Route::prefix('dashboard')->group(function () {
            Route::any('/', 'Produksi\Dashboard\DashboardController@dashboard')->name('dashboard-dashboard');
        });
    });

    Route::prefix('master-produksi')->group(function () {
        Route::prefix('setup-woodtype')->group(function () {
            Route::any('/', 'MasterProduksi\SetupWoodTypeController@index')->name('setup-woodtype-index');
            Route::get('/add', 'MasterProduksi\SetupWoodTypeController@add')->name('setup-woodtype-add');
            Route::get('/edit/{id}', 'MasterProduksi\SetupWoodTypeController@edit')->name('setup-woodtype-edit');
            Route::post('/save', 'MasterProduksi\SetupWoodTypeController@save')->name('setup-woodtype-save');
            Route::post('/import-excel', 'MasterProduksi\SetupWoodTypeController@importexcel')->name('setup-woodtype-import-excel');
        });
        Route::prefix('setup-description')->group(function () {
            Route::any('/', 'MasterProduksi\SetupDescriptionTallySheetController@index')->name('setup-description-index');
            Route::get('/add', 'MasterProduksi\SetupDescriptionTallySheetController@add')->name('setup-description-add');
            Route::get('/edit/{id}', 'MasterProduksi\SetupDescriptionTallySheetController@edit')->name('setup-description-edit');
            Route::post('/save', 'MasterProduksi\SetupDescriptionTallySheetController@save')->name('setup-description-save');
            Route::post('/import-excel', 'MasterProduksi\SetupDescriptionTallySheetController@importexcel')->name('setup-description-import-excel');

            Route::get('/export-excel', 'MasterProduksi\SetupDescriptionTallySheetController@exportexcel')->name('setup-description-export-excel');
        });
        Route::prefix('setup-supplier')->group(function () {
            Route::any('/', 'MasterProduksi\SetupSupplierController@index')->name('setup-supplier-index');
            Route::get('/add', 'MasterProduksi\SetupSupplierController@add')->name('setup-supplier-add');
            Route::get('/edit/{id}', 'MasterProduksi\SetupSupplierController@edit')->name('setup-supplier-edit');
            Route::post('/save', 'MasterProduksi\SetupSupplierController@save')->name('setup-supplier-save');
            Route::post('/import-excel', 'MasterProduksi\SetupSupplierController@importexcel')->name('setup-supplier-import-excel');
            Route::get('/export-excel', 'MasterProduksi\SetupSupplierController@exportexcel')->name('setup-supplier-export-excel');
        });

        Route::prefix('setup-price')->group(function () {
            Route::any('/', 'MasterProduksi\SetupPriceController@index')->name('setup-price-index');
            Route::get('/add', 'MasterProduksi\SetupPriceController@add')->name('setup-price-add');
            Route::get('/edit/{id}', 'MasterProduksi\SetupPriceController@edit')->name('setup-price-edit');
            Route::post('/save', 'MasterProduksi\SetupPriceController@save')->name('setup-price-save');
            Route::post('/import-excel', 'MasterProduksi\SetupPriceController@importexcel')->name('setup-price-import-excel');
        });

        Route::prefix('setup-chamber')->group(function () {
            Route::any('/', 'MasterProduksi\SetupChamberController@index')->name('setup-chamber-index');
            Route::get('/add', 'MasterProduksi\SetupChamberController@add')->name('setup-chamber-add');
            Route::get('/edit/{id}', 'MasterProduksi\SetupChamberController@edit')->name('setup-chamber-edit');
            Route::post('/save', 'MasterProduksi\SetupChamberController@save')->name('setup-chamber-save');
        });

        Route::prefix('setup-location-outputkd')->group(function () {
            Route::any('/', 'MasterProduksi\SetupLocationOutputKDController@index')->name('setup-location-outputkd-index');
            Route::get('/add', 'MasterProduksi\SetupLocationOutputKDController@add')->name('setup-location-outputkd-add');
            Route::get('/edit/{id}', 'MasterProduksi\SetupLocationOutputKDController@edit')->name('setup-location-outputkd-edit');
            Route::post('/save', 'MasterProduksi\SetupLocationOutputKDController@save')->name('setup-location-outputkd-save');
        });
        Route::prefix('analisa-afkir')->group(function () {
            Route::any('/', 'MasterProduksi\SetupAnalisaAfkirController@index')->name('analisa-afkir-index');
            Route::get('/add', 'MasterProduksi\SetupAnalisaAfkirController@add')->name('analisa-afkir-add');
            Route::get('/edit/{id}', 'MasterProduksi\SetupAnalisaAfkirController@edit')->name('analisa-afkir-edit');
            Route::post('/save', 'MasterProduksi\SetupAnalisaAfkirController@save')->name('analisa-afkir-save');
        });

        Route::prefix('setup-arrival-location')->group(function () {
            Route::any('/', 'MasterProduksi\SetupArrivalLocationController@index')->name('setup-arrival-location-index');
            Route::get('/add', 'MasterProduksi\SetupArrivalLocationController@add')->name('setup-arrival-location-add');
            Route::get('/edit/{id}', 'MasterProduksi\SetupArrivalLocationController@edit')->name('setup-arrival-location-edit');
            Route::post('/save', 'MasterProduksi\SetupArrivalLocationController@save')->name('setup-arrival-location-save');
            Route::post('/import-excel', 'MasterProduksi\SetupArrivalLocationController@importexcel')->name('setup-arrival-location-import-excel');
        });

        Route::prefix('setup-mutation-location')->group(function () {
            Route::any('/', 'MasterProduksi\SetupMutationLocationController@index')->name('setup-mutation-location-index');
            Route::get('/add', 'MasterProduksi\SetupMutationLocationController@add')->name('setup-mutation-location-add');
            Route::get('/edit/{id}', 'MasterProduksi\SetupMutationLocationController@edit')->name('setup-mutation-location-edit');
            Route::post('/save', 'MasterProduksi\SetupMutationLocationController@save')->name('setup-mutation-location-save');
            Route::post('/import-excel', 'MasterProduksi\SetupMutationLocationController@importexcel')->name('setup-mutation-location-import-excel');
            Route::post('/export-excel', 'MasterProduksi\SetupMutationLocationController@exportexcel')->name('setup-mutation-location-export-excel');
        });

        Route::prefix('pallet-name')->group(function () {
            Route::any('/', 'MasterProduksi\SetupPalletNameController@index')->name('setup-pallet-name-index');
            Route::get('/add', 'MasterProduksi\SetupPalletNameController@add')->name('setup-pallet-name-add');
            Route::get('/edit/{id}', 'MasterProduksi\SetupPalletNameController@edit')->name('setup-pallet-name-edit');
            Route::post('/save', 'MasterProduksi\SetupPalletNameController@save')->name('setup-pallet-name-save');
        });
    });

    Route::prefix('report-produksi')->group(function () {
        Route::prefix('search-all-data')->group(function () {
            Route::any('/', 'ReportProduksi\SearchData\SearchAllDataController@index')->name('search-all-data-index');
            Route::get('/add', 'ReportProduksi\SearchData\SearchAllDataController@add')->name('search-all-data-add');
            Route::get('/edit/{id}', 'ReportProduksi\SearchData\SearchAllDataController@edit')->name('search-all-data-edit');
            Route::post('/save', 'ReportProduksi\SearchData\SearchAllDataController@save')->name('search-all-data-save');
            Route::get('/export-excel', 'ReportProduksi\SearchData\SearchAllDataController@exportexcel')->name('all-data-export-excel');
        });

        Route::prefix('search-data-bandsaw')->group(function () {
            Route::any('/', 'ReportProduksi\SearchData\SearchDataBandsawController@index')->name('search-data-bandsaw-index');
            Route::get('/add', 'ReportProduksi\SearchData\SearchDataBandsawController@add')->name('search-data-bandsaw-add');
            Route::get('/edit/{id}', 'ReportProduksi\SearchData\SearchDataBandsawController@edit')->name('search-data-bandsaw-edit');
            Route::post('/save', 'ReportProduksi\SearchData\SearchDataBandsawController@save')->name('search-data-bandsaw-save');
            Route::get('/export-excel', 'ReportProduksi\SearchData\SearchDataBandsawController@exportexcel')->name('data-bandsaw-export-excel');
        });

        Route::prefix('search-data-afkir')->group(function () {
            Route::any('/', 'ReportProduksi\SearchData\SearchDataAfkirController@index')->name('search-data-afkir-index');
            Route::get('/add', 'ReportProduksi\SearchData\SearchDataAfkirController@add')->name('search-data-afkir-add');
            Route::get('/edit/{id}', 'ReportProduksi\SearchData\SearchDataAfkirController@edit')->name('search-data-afkir-edit');
            Route::post('/save', 'ReportProduksi\SearchData\SearchDataAfkirController@save')->name('search-data-afkir-save');
            Route::get('/export-excel', 'ReportProduksi\SearchData\SearchDataAfkirController@exportexcel')->name('data-afkir-export-excel');
        });

        Route::prefix('kiln-dry')->group(function () {
            Route::any('/', 'ReportProduksi\KilnDryController@index')->name('kiln-dry-index');
            Route::get('/add', 'ReportProduksi\KilnDryController@add')->name('kiln-dry-add');
            Route::get('/edit/{id}', 'ReportProduksi\KilnDryController@edit')->name('kiln-dry-edit');
            Route::post('/save', 'ReportProduksi\KilnDryController@save')->name('kiln-dry-save');
            Route::get('/export-excel', 'ReportProduksi\KilnDryController@exportexcel')->name('kiln-dry-excel');
        });
    });


    Route::prefix('spk')->group(function () {
        Route::prefix('spk-bandsaw')->group(function () {
            Route::any('/', 'Spk\SpkBandsawController@index')->name('spk-bandsaw-index');
            Route::get('/add', 'Spk\SpkBandsawController@add')->name('spk-bandsaw-add');
            Route::get('/addtally/{id}', 'Spk\SpkBandsawController@addtally')->name('spk-bandsaw-addtally');
            Route::get('/addplan/{id}', 'Spk\SpkBandsawController@addplan')->name('spk-bandsaw-addplan');
            Route::get('/edit/{id}', 'Spk\SpkBandsawController@edit')->name('spk-bandsaw-edit');
            Route::post('/save', 'Spk\SpkBandsawController@save')->name('spk-bandsaw-save');
            Route::post('/saveplan', 'Spk\SpkBandsawController@saveplan')->name('spk-bandsaw-saveplan');
            Route::post('/saveaddtally', 'Spk\SpkBandsawController@saveaddtally')->name('spk-bandsaw-saveaddtally');
            Route::get('/export-pdf/{id}', 'Spk\SpkBandsawController@exportpdf')->name('spk-bandsaw-pdf');
            // Route::get('/pdf/{id}', 'DivisionContainer\ContainerInspectionReport@reportcontainerpdf')->name('container-inspect-pdf');
        });
    });

    Route::prefix('spk')->group(function () {
        Route::prefix('spk-kd')->group(function () {
            Route::any('/', 'Spk\SpkKdController@index')->name('spk-kd-index');
            Route::get('/add', 'Spk\SpkKdController@add')->name('spk-kd-add');
            Route::get('/addtally/{id}', 'Spk\SpkKdController@addtally')->name('spk-kd-addtally');
            Route::get('/addplan/{id}', 'Spk\SpkKdController@addplan')->name('spk-kd-addplan');
            Route::get('/edit/{id}', 'Spk\SpkKdController@edit')->name('spk-kd-edit');
            Route::post('/save', 'Spk\SpkKdController@save')->name('spk-kd-save');
            Route::post('/saveplan', 'Spk\SpkKdController@saveplan')->name('spk-kd-saveplan');
            Route::post('/saveaddtally', 'Spk\SpkKdController@saveaddtally')->name('spk-kd-saveaddtally');
            Route::get('/export-pdf/{id}', 'Spk\SpkKdController@exportpdf')->name('spk-kd-pdf');
        });
    });
});

Auth::routes();
