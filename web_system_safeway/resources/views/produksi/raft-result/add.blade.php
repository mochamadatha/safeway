@extends('layouts.master')

@section('title', trans('menu.raft-result'))

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">{{ trans('menu.raft-result') }}</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="{{ route('dashboard') }}">{{ trans('menu.dashboard') }}</a>
                            </li>
                            <li>
                                <a>{{ trans('menu.produksi') }}</a>
                            </li>
                            <li class="active">
                                {{ trans('menu.raft-result') }}
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            
                <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-border panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ trans('fields.add-raft-result') }}</h3>
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal" role="form" method="post" action="{{ route('raft-result-save') }}">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="p-20">
                                            <input type="hidden" name="id" class="form-control" value="{{ count($errors) > 0 ? old('hd_pallet_jadi_id') : $model->hd_pallet_jadi_id }}">
                                            
                                            <div class="form-group {{ $errors->has('tanggal_pembuatan') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.create-pallet-date') }}</label>
                                                <div class="col-md-5">
                                                    <input type="date" name="tanggal_pembuatan" class="form-control" value="{{ count($errors) > 0 ? old('tanggal_pembuatan') : $model->tanggal_pembuatan }}">
                                                    @if($errors->has('tanggal_pembuatan'))
                                                        <span class="help-block">{{ $errors->first('tanggal_pembuatan') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group {{ $errors->has('lokasi_kedatangan_id') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.create_pallet_location') }}</label>
                                                <div class="col-md-5">
                                                    <select class=" form-control" id="lokasi_kedatangan_id" name="lokasi_kedatangan_id">
                                                    <?php $arrivallocationOptionsId = count($errors) > 0 ? old('lokasi_kedatangan') : $model->lokasi_kedatangan_id; ?>
                                                        <option value="">- {{ trans('fields.change') }} {{ trans('fields.lokasi_kedatangan') }} -</option>
                                                        @foreach($arrivallocationOptions as $arr)
                                                        <option value="{{ $arr->lokasi_kedatangan_id }}" {{ $arr->lokasi_kedatangan_id == $arrivallocationOptionsId ? 'selected' : '' }}>{{ $arr->lokasi_kedatangan }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group {{ $errors->has('pallet_name_id') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.pallet_name') }}</label>
                                                <div class="col-md-5">
                                                    <select class=" form-control" id="pallet_name_id" name="pallet_name_id">
                                                    <?php $palletOptionsId = count($errors) > 0 ? old('pallet_name') : $model->pallet_name_id; ?>
                                                        <option value="">- {{ trans('fields.change') }} {{ trans('fields.pallet_name') }} -</option>
                                                        @foreach($palletOptions as $arr)
                                                        <option value="{{ $arr->pallet_name_id }}" {{ $arr->pallet_name_id == $palletOptionsId ? 'selected' : '' }}>{{ $arr->pallet }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>


                                            <div class="form-group {{ $errors->has('jumlah_pallet') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.sum_pallet') }}</label>
                                                <div class="col-md-5">
                                                    <input type="text" name="jumlah_pallet" class="form-control" value="{{ count($errors) > 0 ? old('jumlah_pallet') : $model->jumlah_pallet }}">
                                                    @if($errors->has('jumlah_pallet'))
                                                        <span class="help-block">{{ $errors->first('jumlah_pallet') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <div class="checkbox checkbox-primary">
                                                        <?php $status = count($errors) > 0 ? old('status') : $model->status; ?>
                                                        <input id="status" name="status" value="true" type="checkbox" {{ $status == true ? 'checked' : '' }}>
                                                        <label for="status">
                                                            {{ trans('fields.is-active') }}
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="col-sm-12 data-table-toolbar text-right" style="padding-top: 10px;">
                                        <div class="form-group">
                                            <a href="{{ url('produksi/raft-result') }}" class="btn btn-sm btn-warning"><i class="fa fa-reply"></i> {{ trans('fields.back') }}</a>
                                            <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i> {{ trans('fields.save') }}</button>
                                        </div>
                                    </div> 
                                </div>
                            </form>
                        </div>
                    </div><!-- end row -->
                </div> <!-- end card-box -->
            </div><!-- end col -->
        </div>
    </div>
</div>
@endsection