<?php

namespace App\Model\MasterProduksi;

use Illuminate\Database\Eloquent\Model;

class AnalisaAfkir extends Model
{
    protected $table        = 'mst_analisa_afkir';
    protected $primaryKey   = 'analisa_afkir_id';
    public $timestamps      = false;
}
