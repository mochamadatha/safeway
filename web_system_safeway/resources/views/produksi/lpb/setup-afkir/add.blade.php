@extends('layouts.master')

@section('title', trans('menu.setup-lpb'))

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">{{ trans('menu.add-afkir') }}</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="{{ route('dashboard') }}">{{ trans('menu.dashboard') }}</a>
                            </li>
                            <li>
                                <a>{{ trans('menu.produksi') }}</a>
                            </li>
                            <li class="active">
                                {{ trans('menu.add-afkir') }}
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            @if(Session::has('successMessage'))
            <div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-check-all"></i>
                <strong>{{ Session::get('successMessage') }}.</strong>
            </div>
            @endif

            @if(Session::has('errorMessage'))
            <div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-block-helper"></i>
                <strong>{{ Session::get('errorMessage') }}.</strong>
            </div>
            @endif
            
            <div class="row">
                <div class=" col-md-12">
                    <div class="panel panel-border panel-success">
                        <div class="panel-heading">
                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <h3 class="panel-title">{{ trans('fields.nomor_lpb') }}</h3>
                                </div>
                                <div class="col-md-3">
                                    <h3 class="panel-title">: {{ $model->nomor_lpb }} </h3>
                                </div>
                                <div class="col-md-2">
                                    <h3 class="panel-title">{{ trans('fields.supplier_name') }}</h3>
                                </div>
                                <div class="col-md-5">
                                    <h3 class="panel-title">: {{ $model->supplier_name }}</h3>
                                </div>
                            </div><br>
                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <h3 class="panel-title">{{ trans('fields.date') }}</h3>
                                </div>
                                <div class="col-md-3">
                                    <h3 class="panel-title"> : {{ date('d-m-Y', strtotime($model->tanggal_kedatangan)) }}</h3>
                                </div>
                                <div class="col-md-2">
                                    <h3 class="panel-title">{{ trans('fields.nomor_po') }}</h3>
                                </div>
                                <div class="col-md-5">
                                    <h3 class="panel-title">: {{ $model->nomor_po }}</h3>
                                </div>
                            </div><br>
                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <h3 class="panel-title">{{ trans('fields.plat_no') }}</h3>
                                </div>
                                <div class="col-md-3">
                                    <h3 class="panel-title"> :  {{ $model->plat_mobil }}</h3>
                                </div>
                                <div class="col-md-2">
                                    <h3 class="panel-title">{{ trans('fields.nomor_afkir') }}</h3>
                                </div>
                                <div class="col-md-5">
                                    <h3 class="panel-title">:  {{ $model->nomor_sj }}</h3>
                                </div>
                            </div>
                        </div>
                        <br>
                        

                        <div class="panel-body">
                            <div class="row">
                                <form class="form-horizontal" role="form" method="post" action="{{ route('setup-afkir-save') }}">
                                    {{ csrf_field() }}
                                    <div class="col-sm-12 data-table-toolbar text-right">
                                        <div class="form-group">
                                            <button type="button" class="btn btn-primary waves-effect waves-light btn-sm" data-toggle="modal" data-target="#dtafkir"><i class="fa fa-plus-circle"></i> {{ trans('fields.add-afkir') }} </button>

                                             <a id="clear-details" href="{{ route('setup-afkir-pdf', ['id' => $model->hd_lpb_id]) }}"  data-toggle="tooltip" class="btn btn-sm btn-success" target="_blank">
                                                <i class="fa fa-print"></i> {{ trans('fields.print-afkir') }} 
                                            </a>

                                            <a id="clear-details" href="{{ route('setup-afkir-detail-pdf', ['id' => $model->hd_lpb_id]) }}"  data-toggle="tooltip" class="btn btn-sm btn-success" target="_blank">
                                                <i class="fa fa-print"></i> {{ trans('fields.print-afkir-detail') }} 
                                            </a>

                                            <a id="clear-details" href="{{ url('produksi/setup-lpb') }}" class="btn btn-sm btn-danger">
                                                <i class="fa fa-reply"></i> {{ trans('fields.back') }} 
                                            </a>
                                        </div>
                                        <div class="row">
                                            <table class="table table-bordered table-hover table-striped m-0">
                                                <thead>
                                                    <tr>
                                                        <th width="5%" class="text-center">{{ trans('fields.num') }}</th>
                                                        <th width="15%" class="text-center">{{ trans('fields.wood-type') }}</th>
                                                        <th width="5%" class="text-center">{{ trans('fields.t') }}</th>
                                                        <th width="5%" class="text-center">{{ trans('fields.l') }}</th>
                                                        <th width="5%" class="text-center">{{ trans('fields.p') }}</th>
                                                        <th width="10%" class="text-center">{{ trans('fields.pcs') }}</th>
                                                        <th width="10%" class="text-center">{{ trans('fields.m3') }}</th>
                                                        <th width="15%" class="text-center">{{ trans('fields.status') }}</th>
                                                        <th width="25%" class="text-center">{{ trans('fields.action') }}</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $no = ($models->currentPage() - 1) * $models->perPage() + 1; ?>
                                                    @foreach($models as $isi)
                                                    <tr>
                                                        <th scope="row" style="text-align: center;">{{ $no++ }}</th>

                                                        <td>{{ $isi->jenis_kayu }}</td>
                                                        <td>{{ $isi->tinggi }}</td>
                                                        <td>{{ $isi->lebar }}</td>
                                                        <td>{{ $isi->panjang }}</td>
                                                        <td>{{ $isi->pcs }}</td>
                                                        <td>{{ $isi->volume }}</td>
                                                        <td>{{ $isi->status }}</td>

                                                        <td class="text-center">
                                                           @can('access', [$resource, 'update'])
                                                            <a data-target="#dtafkiredit{{ $isi->isi_afkir_id }}" data-toggle="modal"  class="btn btn-xs btn-warning" data-original-title="Edit">
                                                                <i class="fa fa-pencil"></i>
                                                            </a>
                                                            @endcan

                                                             @can('access', [$resource, 'update'])
                                                                <a  data-toggle="modal" data-target="#deleteafkir{{ $isi->isi_afkir_id }}" class="btn btn-xs btn-danger" >
                                                                    <i class="fa fa-remove"></i> 
                                                                </a>
                                                                <div id="deleteafkir{{ $isi->isi_afkir_id }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                                <h4 class="modal-title">{{ trans('fields.deletenotally') }}</h4>
                                                                            </div>
                                                                            <div class="modal-body">       
                                                                                <h5>Apakah anda yakin ingin menghapus Tally Ini ??</h5>
                                                                            </div>

                                                                            <div class="modal-footer">
                                                                                <input type="hidden" name="id" class="form-control" value="{{ $isi->isi_afkir_id }}">
                                                                                <a href="{{ route('setup-afkir-destroy', ['id' => $isi->isi_afkir_id]) }}" data-toggle="tooltip" class="btn btn-danger waves-effect" >Yes</a>
                                                                                <button type="button" class="btn btn-default waves-effect waves-light" data-dismiss="modal">No</button>
                                                                            </div> 
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endcan
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            <div class="data-table-toolbar">
                                                {!! $models->render() !!}
                                            </div>
                                        </div> <!--/ row-->
                                    </div>
                                </form>
                            </div> 
                        </div><!--./ class-->


                        @foreach($models as $model)
                        @can('access', [$resource, 'update'])

                        <div id="dtafkiredit{{ $model->isi_afkir_id }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form role="form" method="post" action="{{ route('setup-afkir-save') }}">
                                        {{ csrf_field() }}
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">{{ trans('fields.add-afkir') }}</h4>
                                        </div>
                                        <div class="modal-body">
                                            
                                            <input type="hidden" name="hd_lpb_id" class="form-control" value="{{ $model->hd_lpb_id }}">
                                            <input type="hidden" name="isi_afkir_id" class="form-control" value="{{ count($errors) > 0 ? old('isi_afkir_id') : $model->isi_afkir_id }}">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="field-1" class="control-label">{{ trans('fields.wood-type') }}</label>
                                                        <!-- <input type="text" class="form-control" id="field-1" placeholder="John"> -->
                                                        


                                                        <select class="form-control" id="jenis_kayu_id" name="jenis_kayu_id">
                                                            <?php $kayuId = count($errors) > 0 ? old('jenis_kayu_id') : $model->jenis_kayu_id; ?>
                                                            <option value="">- {{ trans('fields.change') }} {{ trans('fields.wood-type') }} -</option>
                                                            @foreach($woodOptions as $wood)
                                                            <option value="{{ $wood->jenis_kayu_id }}" {{ $wood->jenis_kayu_id == $kayuId ? 'selected' : '' }}>{{ $wood->jenis_kayu }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="tinggi" class="control-label">{{ trans('fields.high') }}</label>
                                                        <input type="number" class="form-control" id="tinggi" name="tinggi" value="{{ count($errors) > 0 ? old('tinggi') : $model->tinggi }}">
                                                    @if($errors->has('tinggi'))
                                                        <span class="help-block">{{ $errors->first('tinggi') }}</span>
                                                    @endif
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="lebar" class="control-label">{{ trans('fields.width') }}</label>
                                                        <input type="number" class="form-control" id="lebar" name="lebar" value="{{ count($errors) > 0 ? old('lebar') : $model->lebar }}">
                                                    @if($errors->has('lebar'))
                                                        <span class="help-block">{{ $errors->first('lebar') }}</span>
                                                    @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="panjang" class="control-label">{{ trans('fields.length') }}</label>
                                                        <input type="number" class="form-control" id="panjang" name="panjang" value="{{ count($errors) > 0 ? old('panjang') : $model->panjang }}">
                                                    @if($errors->has('panjang'))
                                                        <span class="help-block">{{ $errors->first('panjang') }}</span>
                                                    @endif
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="pcs" class="control-label">{{ trans('fields.pcs') }}</label>
                                                        <input type="number" class="form-control" id="pcs" name="pcs" value="{{ count($errors) > 0 ? old('pcs') : $model->pcs }}">
                                                    @if($errors->has('pcs'))
                                                        <span class="help-block">{{ $errors->first('pcs') }}</span>
                                                    @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="col-lg-2"></div>
                                                    <div class="col-lg-4"> Status Kayu </div>
                                                    <div class="col-lg-6">
                                                        <div class="radio radio-success radio-inline">
                                                            <input type="radio" value="Default" name="status-afkir" checked="">
                                                            <label for="default"> Default </label>
                                                        </div> 
                                                        <div class="radio radio-primary radio-inline">
                                                            <input type="radio" value="Delivery"  name="status-afkir">
                                                            <label for="delivery"> Delivery </label>
                                                        </div>
                                                        <div class="radio radio-danger radio-inline">
                                                            <input type="radio" value="Bakar"  name="status-afkir">
                                                            <label for="bakar"> Bakar </label>
                                                        </div>  
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        @endcan
                        @endforeach
                        <div id="dtafkir" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form role="form" method="post" action="{{ route('setup-afkir-save') }}">
                                        {{ csrf_field() }}
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">{{ trans('fields.add-afkir') }}</h4>
                                        </div>
                                        <div class="modal-body">
                                            
                                            <input type="hidden" name="hd_lpb_id" class="form-control" value="{{ $model->hd_lpb_id }}">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="field-1" class="control-label">{{ trans('fields.wood-type') }}</label>
                                                        <!-- <input type="text" class="form-control" id="field-1" placeholder="John"> -->
                                                        <select class="form-control" id="jenis_kayu_id" name="jenis_kayu_id">
                                                            <option value="">- {{ trans('fields.change') }} {{ trans('fields.wood-type') }} -</option>
                                                            @foreach($woodOptions as $wood)
                                                            <option value="{{ $wood->jenis_kayu_id }}" {{ !empty($filters['wood']) && $filters['wood'] == $wood->jenis_kayu_id ? 'selected' : ''}} >{{ $wood->jenis_kayu }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="tinggi" class="control-label">{{ trans('fields.high') }}</label>
                                                        <input type="number" class="form-control" id="tinggi" name="tinggi" >
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="lebar" class="control-label">{{ trans('fields.width') }}</label>
                                                        <input type="number" class="form-control" id="lebar" name="lebar" >
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="panjang" class="control-label">{{ trans('fields.length') }}</label>
                                                        <input type="number" class="form-control" id="panjang" name="panjang">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="pcs" class="control-label">{{ trans('fields.pcs') }}</label>
                                                        <input type="number" class="form-control" id="pcs" name="pcs">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="col-lg-2"></div>
                                                    <div class="col-lg-4"> Status Kayu </div>
                                                    <div class="col-lg-6">
                                                        <div class="radio radio-success radio-inline">
                                                            <input type="radio" value="Default" name="status-afkir" checked="">
                                                            <label for="default"> Default </label>
                                                        </div> 
                                                        <div class="radio radio-primary radio-inline">
                                                            <input type="radio" value="Delivery"  name="status-afkir">
                                                            <label for="delivery"> Delivery </label>
                                                        </div>
                                                        <div class="radio radio-danger radio-inline">
                                                            <input type="radio" value="Bakar"  name="status-afkir">
                                                            <label for="bakar"> Bakar </label>
                                                        </div>  
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection