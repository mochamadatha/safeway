<?php

namespace App\Model\MasterProduksi;

use Illuminate\Database\Eloquent\Model;

class JenisKayu extends Model
{
    protected $table        = 'mst_jenis_kayu';
    protected $primaryKey   = 'jenis_kayu_id';
    public $timestamps      = false;
}
