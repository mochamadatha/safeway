<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHdLpbTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hd_lpb', function (Blueprint $table) {
            $table->increments('hd_lpb_id');
            // $table->integer('tally_id');
            $table->integer('supplier_id');
            $table->integer('lokasi_kedatangan_id');
            $table->string('nomor_lpb')->unique();
            $table->string('nomor_po');
            $table->string('nomor_sj');
            $table->date('tanggal_kedatangan');
            $table->date('tanggal_afkir')->nullable();
            $table->string('plat_mobil');
            $table->string('plat_afkir')->nullable();
            $table->boolean('status')->default(true);
            $table->integer('created_by');
            $table->dateTime('created_date');
            $table->integer('modified_by')->nullable();
            $table->dateTime('modified_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hd_lpb');
    }
}
