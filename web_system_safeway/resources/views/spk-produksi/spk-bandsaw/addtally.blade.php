@extends('layouts.master')

@section('title', trans('menu.add-tally-spkbandsaw'))

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">{{ trans('menu.add-tally-spkbandsaw') }}</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="{{ route('dashboard') }}">{{ trans('menu.dashboard') }}</a>
                            </li>
                            <li>
                                <a>{{ trans('menu.produksi') }}</a>
                            </li>
                            <li class="active">
                                {{ trans('menu.add-tally-spkbandsaw') }}
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            @if(Session::has('successMessage'))
            <div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-check-all"></i>
                <strong>{{ Session::get('successMessage') }}.</strong>
            </div>
            @endif

            @if(Session::has('errorMessage'))
            <div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-block-helper"></i>
                <strong>{{ Session::get('errorMessage') }}.</strong>
            </div>
            @endif
            
            <div class="row">
                <div class=" col-md-12">
                    <div class="panel panel-border panel-success">
                        <div class="panel-heading">
                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <h3 class="panel-title">{{ trans('fields.nomor_lpb') }}</h3>
                                </div>
                                <div class="col-md-3">
                                    <h3 class="panel-title">: {{ $model->bandsaw_date }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <form class="form-horizontal" role="form" method="post" action="{{ route('spk-bandsaw-saveaddtally') }}">
                                    {{ csrf_field() }}
                                    <div class="col-sm-12 data-table-toolbar text-right">
                                        <div class="form-group">
                                            <button type="button" class="btn btn-primary waves-effect waves-light btn-sm" data-toggle="modal" data-target="#dttally"><i class="fa fa-plus-circle"></i> {{ trans('fields.add-tally-sheet') }} </button>
                                             @can('access', [$resource, 'add'])

                                            <a data-target="#infolpb" data-toggle="modal"   class="btn btn-primary waves-effect btn-sm waves-light" data-original-title="fa-plus">
                                            <i class="fa fa-info"></i> {{ trans('fields.info') }}</a>
                                            </a>
                                            @endcan

                                             <a id="clear-details" href="{{ url('spk/spk-bandsaw') }}" class="btn btn-sm btn-danger">
                                                <i class="fa fa-reply"></i> {{ trans('fields.back') }} 
                                            </a>
                                        </div>

                                        <div id="dttally" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <form role="form" method="post" action="{{ route('spk-bandsaw-saveaddtally') }}">
                                                        {{ csrf_field() }}
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            <h4 class="modal-title">{{ trans('fields.add-tally-sheet') }}</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <input type="hidden" name="hd_spk_bandsaw_id" class="form-control" value="{{ $model->hd_spk_bandsaw_id }}">
                                          
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="field-2" class="control-label">{{ trans('fields.notally') }}</label>
                                                                        <select class="form-control" id="tally_id" name="tally_id">
                                                                            <option value="">- {{ trans('fields.change') }} {{ trans('fields.notally') }} -</option>
                                                                            @foreach($tallyOptions as $tal)
                                                                            <option value="{{ $tal->tally_id }}">{{ $tal->tally_no }}  </option>
                                                              
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                                                    <button type="submit" class="btn btn-info waves-effect waves-light">Save changes</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div><!-- /.modal -->
                                            </div>
                                        </div>
                                        <div class="row">
                                            <table class="table table-bordered table-hover table-striped m-0">
                                                <thead>
                                                    <tr>
                                                        <th width="9%" class="text-center">{{ trans('fields.num') }}</th>
                                                        {{-- <th width="9%" class="text-center">{{ trans('fields.wood-type') }}</th>
                                                        <th width="9%" class="text-center">{{ trans('fields.description-wood') }}</th> --}}
                                                        <th width="9%" class="text-center">{{ trans('fields.no-tally-sheet') }}</th>
                                                        <th width="9%" class="text-center">{{ trans('fields.action') }}</th>
                                                        {{-- <th width="5%" class="text-center">{{ trans('fields.t') }}</th>
                                                        <th width="5%" class="text-center">{{ trans('fields.l') }}</th>
                                                        <th width="5%" class="text-center">{{ trans('fields.p') }}</th>
                                                        <th width="9%" class="text-center">{{ trans('fields.pcs') }}</th>
                                                        <th width="9%" class="text-center">{{ trans('fields.m3') }}</th> --}}
                                                      
                                                    
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $no = ($models->currentPage() - 1) * $models->perPage() + 1; ?>
                                                    @foreach($models as $isi)
                                                    <tr>
                                                        <th scope="row" style="text-align: center;">{{ $no++ }}</th>
                                                        
                                                        {{-- <td>{{ $isi->jenis_kayu }}</td>
                                                        <td>{{ $isi->nama_deskripsi }}</td> --}}
                                                        <td style="text-align: center;">{{ $isi->tally_no }}</td>
                                                        <td>
                                                            @can('access', [$resource, 'add'])
                                                            <a href="{{ route('spk-bandsaw-addplan', ['id' => $isi->tally_id]) }}" data-toggle="tooltip" class="btn btn-xs btn-success" data-original-title="Add Tallysheet">
                                                                <i class="fa fa-plus"></i> 
                                                            </a>
                                                            @endcan

                                                          
                                                        </td>
                                                        {{-- <td>{{ $isi->tinggi }}</td>
                                                        <td>{{ $isi->lebar }}</td>
                                                        <td>{{ $isi->panjang }}</td>
                                                        <td>{{ $isi->pcs }}</td>
                                                        <td>{{ $isi->volume }}</td> --}}
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            <div class="data-table-toolbar">
                                                {!! $models->render() !!}
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection