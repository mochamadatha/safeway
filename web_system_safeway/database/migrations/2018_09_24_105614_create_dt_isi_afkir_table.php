<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDtIsiAfkirTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dt_isi_afkir', function (Blueprint $table) {
            $table->increments('isi_afkir_id');
            $table->integer('hd_lpb_id');
            $table->integer('jenis_kayu_id');
            $table->integer('tinggi');
            $table->integer('lebar');
            $table->integer('panjang');
            $table->integer('pcs');
            $table->double('volume');
            //$table->boolean('status')->default(true);
            $table->string('status');
            $table->integer('created_by');
            $table->dateTime('created_date');
            $table->integer('modified_by')->nullable();
            $table->dateTime('modified_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dt_isi_afkir');
    }
}
