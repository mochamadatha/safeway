@extends('layouts.master')

@section('title', trans('menu.dashboard'))

@section('header')
        <!-- <link href="{{ asset('plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/> -->
        <!-- <link href="{{ asset('plugins/datatables/buttons.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/> -->
        <!-- <link href="{{ asset('plugins/datatables/fixedHeader.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/> -->
        <!-- <link href="{{ asset('plugins/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/> -->
        <!-- <link href="{{ asset('plugins/datatables/scroller.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/> -->
        <!-- <link href="{{ asset('plugins/datatables/dataTables.colVis.css') }}" rel="stylesheet" type="text/css"/> -->
        <link href="{{ asset('plugins/datatables/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
        <!-- <link href="{{ asset('plugins/datatables/fixedColumns.dataTables.min.css') }}" rel="stylesheet" type="text/css"/> -->
@endsection

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
				<div class="col-xs-12">
					<div class="page-title-box">
                        <h4 class="page-title">{{ trans('menu.dashboard') }}</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li class="active">
                                {{ trans('menu.dashboard') }}
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
				</div>
			</div>
            <!-- end row -->

            <div class="row">
                <div class="col-lg-12">
                    <div class="card-box">
                        <h4 class="header-title m-t-0 m-b-30" align="center" >{{ trans('fields.info_data_kd') }}</h4>

                        <table id="datatabless" class="table table-striped table-bordered" >
                             <thead>
                                <tr>
                                    
                                    <th width="10%" class="text-center">{{ trans('fields.chamber_name') }}</th>
                                    <th width="10%" class="text-center">{{ trans('fields.created_date') }}</th>
                                    <th width="10%" class="text-center">{{ trans('fields.count_tally') }}</th>
                                    <th width="10%" class="text-center">{{ trans('fields.pcs') }}</th>
                                    <th width="10%" class="text-center">{{ trans('fields.m3') }}</th>
                                    <th width="20%" class="text-center">{{ trans('fields.date-output-planning') }}</th>
                                    <th width="20%" class="text-center">{{ trans('fields.conclution') }}</th>
                                    <th width="10%" class="text-center">{{ trans('fields.status') }}</th>
                                </tr>
                            </thead>

                            <tbody>


                                @foreach($infoinkddate as $in)
                                
                                <tr>
                                
                                    <td width="10%" class="text-center">{{ $in->chamber_name }}</td>
                                    <td width="10%" class="text-center">{{ date('d-m-Y', strtotime($in->date_in_kd)) }}</td>
                                    <td width="10%" class="text-center">{{ $in-> tally_no}}</td>
                                    <td width="10%" class="text-center">{{ $in->pcs }}</td>
                                    <td width="10%" class="text-center">{{ $in->volume }}</td>
                                    <td width="20%" class="text-center"> 
                                        <?php
                                            $date2 =  date('d-m-Y', strtotime('+10 days', strtotime($in->date_in_kd)));
                                            echo $date2;
                                        ?>
                                    
                                    </td>
                                    <td width="20%" class="text-center">
                                        <?php

                                           
                                            $awal  = new DateTime($in->date_in_kd);
                                            $date3 =  date('d-m-Y');
                                            $now  = new DateTime( $date3 );
                                            $date =  date('d-m-Y', strtotime('+10 days', strtotime($in->date_in_kd)));
                                            $date2  = new DateTime($date );


                                               if ($awal == $now){
                                                    echo "Hari ini masuk KD";

                                               }else if ( $date2 > $now) {
                                                    $interval = $awal->diff($now);
                                                    echo "Hari  ke - $interval->days dalam KD";


                                               } else if ($date2 == $now) {
                                                  
                                                    echo "Waktunya Keluar KD"; 
                                               } else if ( $date2 <  $now){
                                                    $interval = $date2->diff($now);
                                                    echo "Terlambat $interval->days Hari ";
                                               }
                                        ?>
                                    </td>
                                    <td width="20%" class="text-center">{{ $in->status_data }}</td>
                                </tr>
                              
                                @endforeach
                        </tbody>        
                        </table>
                                            
                    </div>
                </div>

          
                 




            <!--     <div class="row">
                <div class="col-lg-6">
                    <div class="card-box">
                        <h4 class="header-title m-t-0 m-b-30">{{ trans('fields.data_rm') }}</h4>

                        <table id="datatable" class="table table-striped table-bordered" >
                            <thead>
                                <tr>
                                    
                                    <th width="20%" class="text-center">{{ trans('fields.wood_type') }}</th>
                                    <th width="15%" class="text-center">{{ trans('fields.p') }}</th>
                                    <th width="15%" class="text-center">{{ trans('fields.l') }}</th>
                                    <th width="15%" class="text-center">{{ trans('fields.t') }}</th>
                                    <th width="20%" class="text-center">{{ trans('fields.pcs') }}</th>
                                    <th width="20%" class="text-center">{{ trans('fields.m3') }}</th>
                                </tr>
                            </thead>
                        <tbody>
                            <?php $no = 1;//($models->currentPage() - 1) * $models->perPage() + 1; ?>
                                @foreach($info as $in)
                                <tr>
                                
                                    <td width="20%" class="text-center">{{ $in->jenis_kayu }}</td>
                                    <td width="15%" class="text-center">{{ $in->tinggi }}</td>
                                    <td width="15%" class="text-center">{{ $in->lebar }}</td>
                                    <td width="15%" class="text-center">{{ $in->panjang }}</td>
                                    <td width="20%" class="text-center">{{ $in->pcs }}</td>
                                    <td width="20%" class="text-center">{{ $in->volume }}</td>
                                </tr>
                                @endforeach
                        </tbody>
                        </table>
                            <table>
                                <?php
                                    $total_pcs = 0; 
                                    $total_volume = 0; 
                                    $total_tally = 0 ;                            
                                ?>
                                @foreach($model as $view)
                                <?php 
                                    $total_pcs += $view->pcs;
                                    $total_volume += $view->volume;    
                                ?>
                                @endforeach
                                <tr>
                                    <td></td>
                                    <td>Total Pcs</td>
                                    <td> : </td>
                                    <td>{{ $total_pcs }}</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Total Volume</td>
                                    <td> : </td>
                                    <td>{{ $total_volume }}</td>
                                </tr>  
                        </table>

                    </div>
                </div>




                 <div class="col-lg-6">
                    <div class="card-box">
                        <h4 class="header-title m-t-0 m-b-30">{{ trans('fields.data_ms') }}</h4>

                          <table id="datatablems" class="table table-striped table-bordered" >
                            <thead>
                                <tr>
                                     <th width="20%" class="text-center">{{ trans('fields.wood_type') }}</th>
                                    <th width="15%" class="text-center">{{ trans('fields.p') }}</th>
                                    <th width="15%" class="text-center">{{ trans('fields.l') }}</th>
                                    <th width="15%" class="text-center">{{ trans('fields.t') }}</th>
                                    <th width="20%" class="text-center">{{ trans('fields.pcs') }}</th>
                                    <th width="20%" class="text-center">{{ trans('fields.m3') }}</th>
                                </tr>
                            </thead>
                        <tbody>
                            <?php $no = 1;//($models->currentPage() - 1) * $models->perPage() + 1; ?>
                                @foreach($infoms as $in)
                                <tr>
                                
                                    <td>{{ $in->jenis_kayu }}</td>
                                    <td>{{ $in->tinggi }}</td>
                                    <td>{{ $in->lebar }}</td>
                                    <td>{{ $in->panjang }}</td>
                                    <td>{{ $in->pcs }}</td>
                                    <td>{{ $in->volume }}</td>
                                </tr>
                                @endforeach
                        </tbody>
                        </table>
                         </table>
                            <table>
                                <?php
                                    $total_pcs = 0; 
                                    $total_volume = 0; 
                                    $total_tally = 0 ;                            
                                ?>
                                @foreach($infoms as $view)
                                <?php 
                                    $total_pcs += $view->pcs;
                                    $total_volume += $view->volume;    
                                ?>
                                @endforeach
                                <tr>
                                    <td></td>
                                    <td>Total Pcs</td>
                                    <td> : </td>
                                    <td>{{ $total_pcs }}</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Total Volume</td>
                                    <td> : </td>
                                    <td>{{ $total_volume }}</td>
                                </tr>  
                        </table>

                    </div>
                </div>



            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="card-box">
                         <h4 class="header-title m-t-0 m-b-30">{{ trans('fields.data_in_kd') }}</h4>
                        <table id="datatableinkd" class="table table-striped table-bordered" >
                            <thead>
                                <tr>
                                    
                                    <th width="20%" class="text-center">{{ trans('fields.wood_type') }}</th>
                                    <th width="15%" class="text-center">{{ trans('fields.p') }}</th>
                                    <th width="15%" class="text-center">{{ trans('fields.l') }}</th>
                                    <th width="10%" class="text-center">{{ trans('fields.t') }}</th>
                                    <th width="20%" class="text-center">{{ trans('fields.pcs') }}</th>
                                    <th width="10%" class="text-center">{{ trans('fields.m3') }}</th>
                                    <th width="20%" class="text-center">{{ trans('fields.chamber_name') }}</th>
                                   

                                </tr>
                            </thead>
                        <tbody>
                            <?php $no = 1;//($models->currentPage() - 1) * $models->perPage() + 1; ?>
                                @foreach($infoinkd as $in)
                                <tr>
                                
                                    <td>{{ $in->jenis_kayu }}</td>
                                    <td>{{ $in->tinggi }}</td>
                                    <td>{{ $in->lebar }}</td>
                                    <td>{{ $in->panjang }}</td>
                                    <td>{{ $in->pcs }}</td>
                                    <td>{{ $in->volume }}</td>
                                    <td>{{ $in->chamber_name }}</td>
                                </tr>
                                @endforeach
                        </tbody>
                        </table>
                        </table>
                            <table>
                                <?php
                                    $total_pcs = 0; 
                                    $total_volume = 0; 
                                                     
                                ?>
                                @foreach($infoinkd as $view)
                                <?php 
                                    $total_pcs += $view->pcs;
                                    $total_volume += $view->volume;    
                                ?>
                                @endforeach
                                <tr>
                                    <td></td>
                                    <td>Total Pcs</td>
                                    <td> : </td>
                                    <td>{{ $total_pcs }}</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Total Volume</td>
                                    <td> : </td>
                                    <td>{{ $total_volume }}</td>
                                </tr>  
                        </table>
                    </div>

                </div>


                <div class="col-lg-6">
                    <div class="card-box">
                         <h4 class="header-title m-t-0 m-b-30">{{ trans('fields.data_out_kd') }}</h4>
                         <table id="datatableoutkd" class="table table-striped table-bordered" >
                            <thead>
                                <tr>
                                    
                                    <th width="20%" class="text-center">{{ trans('fields.wood_type') }}</th>
                                    <th width="15%" class="text-center">{{ trans('fields.p') }}</th>
                                    <th width="15%" class="text-center">{{ trans('fields.l') }}</th>
                                    <th width="15%" class="text-center">{{ trans('fields.t') }}</th>
                                    <th width="20%" class="text-center">{{ trans('fields.pcs') }}</th>
                                    <th width="20%" class="text-center">{{ trans('fields.m3') }}</th>
                                </tr>
                            </thead>
                        <tbody>
                            <?php $no = 1;//($models->currentPage() - 1) * $models->perPage() + 1; ?>
                                @foreach($infooutkd as $in)
                                <tr>
                                
                                    <td>{{ $in->jenis_kayu }}</td>
                                    <td>{{ $in->tinggi }}</td>
                                    <td>{{ $in->lebar }}</td>
                                    <td>{{ $in->panjang }}</td>
                                    <td>{{ $in->pcs }}</td>
                                    <td>{{ $in->volume }}</td>
                                </tr>
                                @endforeach
                        </tbody>
                        </table>
                        </table>
                            <table>
                                <?php
                                    $total_pcs = 0; 
                                    $total_volume = 0; 
                                    $total_tally = 0 ;                            
                                ?>
                                @foreach($infooutkd as $view)
                                <?php 
                                    $total_pcs += $view->pcs;
                                    $total_volume += $view->volume;    
                                ?>
                                @endforeach
                                <tr>
                                    <td></td>
                                    <td>Total Pcs</td>
                                    <td> : </td>
                                    <td>{{ $total_pcs }}</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Total Volume</td>
                                    <td> : </td>
                                    <td>{{ $total_volume }}</td>
                                </tr>  
                        </table>
                    </div>

                </div>

            </div>
        </div> -->
            <!--- end row -->




        <!-- </div> container -->

    <!-- </div> content -->



</div>
@endsection
@section('script')
    <!-- jQuery  -->
        <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables/dataTables.bootstrap.js') }}"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#datatable').dataTable();
                $('#datatablems').dataTable();
                $('#datatableinkd').dataTable();
                $('#datatableoutkd').dataTable();
            });
        </script>
@endsection