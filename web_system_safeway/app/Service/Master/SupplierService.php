<?php

namespace App\Service\Master;

class SupplierService
{
    public static function getActiveSupplier()
    {
        return \DB::table('mst_supplier')
            ->where('status', '=', true)
            ->orderBy('supplier_name')
            ->get();
    }
}
