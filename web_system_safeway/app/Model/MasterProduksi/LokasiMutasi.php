<?php

namespace App\Model\MasterProduksi;

use Illuminate\Database\Eloquent\Model;

class LokasiMutasi extends Model
{
    protected $table        = 'mst_lokasi_mutasi';
    protected $primaryKey   = 'lokasi_mutasi_id';
    public $timestamps      = false;
}
