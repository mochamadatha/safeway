<?php

namespace App\Model\MasterProduksi;

use Illuminate\Database\Eloquent\Model;

class LokasiKedatangan extends Model
{
    protected $table        = 'mst_lokasi_kedatangan';
    protected $primaryKey   = 'lokasi_kedatangan_id';
    public $timestamps      = false;
}
