<?php

namespace App\Http\Controllers\MasterProduksi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\MasterProduksi\Chamber;
use DB;

class SetupChamberController extends Controller
{
    const URL       = 'master-produksi/setup-chamber';
    const RESOURCE  = 'setup-chamber';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        if ($request->isMethod('post')) {
            $request->session()->put('filters', $request->all());
            return redirect(self::URL . '?page=1');
        } elseif (empty($request->get('page'))) {
            $request->session()->forget('filters');
        }
        $filters = $request->session()->get('filters');
        $query = \DB::table('mst_chamber')
            ->orderBy('mst_chamber.chamber_name');

        if (!empty($filters['status']) || !$request->session()->has('filters')) {
            $query->where('mst_chamber.status', '=', true);
        } else {
            $query->where('mst_chamber.status', '=', false);
        }

        if (!empty($filters['chamber_name'])) {
            $query->where('mst_chamber.chamber_name', 'like', '%' . $filters['chamber_name'] . '%');
        }

        return view('master-produksi/chamber.index', [
            "models"        => $query->paginate(10),
            "filters"       => $filters,
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function add(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        $model           = new Chamber();
        $model->status   = true;

        return view('master-produksi/chamber.add', [
            "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }

    public function edit(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $model = Chamber::find($id);

        if ($model === null) {
            abort('404');
        }

        return view('master-produksi/chamber.add', [
            "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }



    public function save(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add']) && !\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $id     = intval($request->get('id', 0));

        $this->validate($request, [
            'chamber_name' => 'required|max:255|unique:mst_chamber,chamber_name,' . $id . ',chamber_id',
        ]);

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            if (empty($id)) {
                $model  = new Chamber();
                $model->created_by      = \Auth::user()->id;
                $model->created_date    = $now;
            } else {
                $model  = Chamber::find($id);
                $model->modified_by   = \Auth::user()->id;
                $model->modified_date = $now;
            }

            $model->chamber_name   = $request->get('chamber_name');
            $model->status   = !empty($request->get('status')) ? true : false;

            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('menu.setup-chamber') . ' ' . $request->get('chamber_name')])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('menu.setup-chamber') . ' ' . $request->get('chamber_namer')])
            );
        }

        return redirect(route('setup-chamber-index'));
    }
}
