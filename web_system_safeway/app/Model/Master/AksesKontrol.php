<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class AksesKontrol extends Model
{
    protected $table        = 'akses_kontrol';
    protected $primaryKey   = 'akses_kontrol_id';
    public $timestamps      = false;
}
