@extends('layouts.master')

@section('title', trans('menu.tallysheet'))

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">{{ trans('menu.reproses-ms') }}</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="{{ route('dashboard') }}">{{ trans('menu.dashboard') }}</a>
                            </li>
                            <li>
                                <a>{{ trans('menu.produksi') }}</a>
                            </li>
                            <li class="active">
                                {{ trans('menu.proses-ms') }}
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            @if(Session::has('successMessage'))
            <div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-check-all"></i>
                <strong>{{ Session::get('successMessage') }}.</strong>
            </div>
            @endif

            @if(Session::has('errorMessage'))
            <div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-block-helper"></i>
                <strong>{{ Session::get('errorMessage') }}.</strong>
            </div>
            @endif
            
            <div class="row">
                <div class=" col-md-12">
                    <div class="panel panel-border panel-success">
                        <div class="panel-heading">
                           
                        </div>
                        

                        <div class="panel-body">
                            <div class="row">
                                    <div class="col-sm-12 data-table-toolbar text-right">
                                        <div class="form-group">
                                            
                                            <a id="clear-details" href="{{ url('produksi/ms/stockms') }}"  class="btn btn-sm btn-success">
                                                <i class="fa fa-reply"></i> {{ trans('fields.back') }} 
                                            </a>
                                        </div>
                                        <div class="row">
                                            <table class="table table-bordered table-hover table-striped m-0">
                                                <thead>
                                                    <tr>
                                                        <th width="10%" class="text-center">{{ trans('fields.num') }}</th>
                                                        <th width="10%" class="text-center">{{ trans('fields.proses-date') }}</th>
                                                        <th width="17%" class="text-center">{{ trans('fields.wood-type') }}</th>
                                                        <th width="10%" class="text-center">{{ trans('fields.high') }}</th>
                                                        <th width="10%" class="text-center">{{ trans('fields.width') }}</th>
                                                        <th width="10%" class="text-center">{{ trans('fields.length') }}</th>
                                                        <th width="10%" class="text-center">{{ trans('fields.pcs') }}</th>
                                                        <th width="10%" class="text-center">{{ trans('fields.m3') }}</th>
                                                        <th width="10%" class="text-center">{{ trans('fields.note') }}</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $no = ($models->currentPage() - 1) * $models->perPage() + 1; ?>
                                                    @foreach($models as $model)
                                                    <tr>
                                                        <th scope="row" style="text-align: center;">{{ $no++ }}</th>
                                                        <td class="text-center">{{ date('d-m-Y ', strtotime($model->proses_date)) }}</td>
                                                        <td class="text-center">{{ $model->jenis_kayu }}</td>
                                                        <td class="text-center">{{ $model->tinggi }}</td>
                                                        <td class="text-center">{{ $model->lebar }}</td>
                                                        <td class="text-center">{{ $model->panjang }}</td>
                                                        <td class="text-center">{{ $model->pcs }}</td>
                                                        <td class="text-center">{{ $model->volume }}</td>
                                                        <td class="text-center">{{ $model->keterangan }}</td>
                                                        
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                          
                                                
                                            </table>
                                            <div class="data-table-toolbar">
                                                {!! $models->render() !!}
                                            </div>  
                                        </div>
                                    </div>
                            </div> 
                        </div>

                        <!--  -->
                    </div>
                </div>

            </div>


        </div>
    </div>
</div>
@endsection