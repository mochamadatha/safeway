<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHdPalletJadiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hd_pallet_jadi', function (Blueprint $table) {
          $table->increments('hd_pallet_jadi_id');
            $table->integer('pallet_name_id');
            $table->integer('lokasi_kedatangan_id');
            $table->date('tanggal_pembuatan');
            $table->integer('jumlah_pallet');
            $table->boolean('status');
            $table->integer('created_by');
            $table->dateTime('created_date');
            $table->integer('modified_by')->nullable();
            $table->dateTime('modified_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hd_pallet_jadi');
    }
}


 