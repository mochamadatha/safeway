<?php
use App\Service\DivisionIspm\SizeService;
use App\Model\DivisionIspm\IspmInspect;

function convbulan($bulan){
        if ($bulan == "01") {
            $kode = "I";
        } elseif ($bulan == "02") {
            $kode = "II";
        }
        elseif ($bulan == "03") {
            $kode = "III";
        }
        elseif ($bulan == "04") {
            $kode = "IV";
        }
        elseif ($bulan == "05") {
            $kode = "V";
        }
        elseif ($bulan == "06") {
            $kode = "VI";
        }
        elseif ($bulan == "07") {
            $kode = "VII";
        }
        elseif ($bulan == "08") {
            $kode = "VIII";
        }
        elseif ($bulan == "09") {
            $kode = "IX";
        }
        elseif ($bulan == "10") {
            $kode = "X";
        }
        elseif ($bulan == "11") {
            $kode = "XI";
        }
        elseif ($bulan == "12") {
            $kode = "XII";
        }
        return $kode;
    }
?>
<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>
<br><br><br>
<table id="filtes" width="100%" cellpadding="0" cellspacing="0" style="font-size: 9px">
    <tr>
        <td width="50%" cellpadding="0" cellspacing="0" >
            <table>
                <tr>
                    <td width="48%" >{{ trans('fields.assignment_letter') }}</td>
                    <td width="2%">:</td>
                    <td width="70%">{{ $model->assignment_letter}} /STUFFING/CCC-GR/<?php echo convbulan(date('m', strtotime($model->inspect_date)))  ?>/{{ date('Y', strtotime($model->inspect_date)) }}</td>
                </tr>
                <tr>
                    <td width="48%" >{{ trans('fields.container_number') }}</td>
                    <td width="2%">:</td>
                    <td width="70%">{{ $model->container_number}}</td>
                </tr>
                <tr>
                    <td width="48%">{{ trans('fields.size') }}</td>
                    <td width="2%">:</td>
                     <td width="50%">{{ $model->size}}</td>
                </tr>
                <tr>
                    <td width="48%">{{ trans('fields.inspection_date') }}</td>
                    <td width="2%">:</td>
                     <td width="50%">{{ date('d M Y', strtotime($model->inspect_date)) }}</td>
                </tr>
                
            </table>
            <br/>
        </td>
        
    </tr>
</table>

<br>

<table id="filtes" width="100%" cellpadding="0" cellspacing="0 " style="font-size: 10px; font-weight: bold;">
    <tr>
        <td width="33%" cellpadding="0" cellspacing="0">
            <table>
                <tr>
                    <td width="100%" align="center">{{ trans('fields.empty') }}</td>
                </tr>
                <tr>
                    <td width="100%" align="center"><img src="{{ asset(Config::get('app.paths.foto-ispm').'/'.$model->empty) }}" height="230" width="230"></td>
                </tr>
                <tr> 
                    <td width="100%" align="center"></td> 
                </tr>
                <tr>
                    <td width="100%" align="center">{{ trans('fields.full_cargo') }}</td>
                </tr>
                <tr>
                    <td width="100%" align="center"><img src="{{ asset(Config::get('app.paths.foto-ispm').'/'.$model->full_cargo) }}" height="230" width="230"></td>
                </tr>
                <tr> 
                    <td width="100%" align="center"></td> 
                </tr>
                <tr>
                    <td width="100%" align="center">{{ trans('fields.door_closed') }}</td>
                </tr>
                <tr>
                    <td width="100%" align="center"><img src="{{ asset(Config::get('app.paths.foto-ispm').'/'.$model->door_closed) }}" height="230" width="230"></td>
                </tr>
                
            </table>
            <br/>
        </td>


        <td width="33%" cellpadding="0" cellspacing="0">
            <table>
                <tr>
                    <td width="100%" align="center">{{ trans('fields.one_cargo') }}</td>
                </tr>
                <tr>
                    <td width="100%" align="center"><img src="{{ asset(Config::get('app.paths.foto-ispm').'/'.$model->one_cargo) }}" height="230" width="230"></td>
                </tr>
                <tr> 
                    <td width="100%" align="center"></td> 
                </tr>
                <tr>
                    <td width="100%" align="center">{{ trans('fields.cargo_label') }}</td>
                </tr>
                <tr>
                    <td width="100%" align="center"><img src="{{ asset(Config::get('app.paths.foto-ispm').'/'.$model->cargo_label) }}" height="230" width="230"></td>
                </tr>
                <tr> 
                    <td width="100%" align="center"></td> 
                </tr>
                <tr>
                    <td width="100%" align="center">{{ trans('fields.agent_seal') }}</td>
                </tr>
                <tr>
                    <td width="100%" align="center"><img src="{{ asset(Config::get('app.paths.foto-ispm').'/'.$model->agent_seal) }}" height="230" width="230"></td>
                </tr>
                
            </table>
            <br/>
        </td>

         <td width="33%" cellpadding="0" cellspacing="0">
            <table>
                <tr>
                    <td width="100%" align="center">{{ trans('fields.half_cargo') }}</td>
                </tr>
                <tr>
                    <td width="100%" align="center"><img src="{{ asset(Config::get('app.paths.foto-ispm').'/'.$model->half_cargo) }}" height="230" width="230"></td>
                </tr>
                <tr> 
                    <td width="100%" align="center"></td> 
                </tr>
                <tr>
                    <td width="100%" align="center">{{ trans('fields.one_door_closed') }}</td>
                </tr>
                <tr>
                    <td width="100%" align="center"><img src="{{ asset(Config::get('app.paths.foto-ispm').'/'.$model->one_door_closed) }}" height="230" width="230"></td>
                </tr>
                <tr> 
                    <td width="100%" align="center"></td> 
                </tr>
                <tr>
                    <td width="100%" align="center">{{ trans('fields.customer_seal') }}</td>
                </tr>
                <tr>
                    <td width="100%" align="center"><img src="{{ asset(Config::get('app.paths.foto-ispm').'/'.$model->customer_seal) }}" height="230" width="230"></td>
                </tr>
                
            </table>
            <br/>
        </td>
    </tr>
</table>

       


</body>
</html>


