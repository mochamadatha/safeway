@extends('layouts.master')

@section('title', trans('menu.raft-result'))

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">{{ trans('menu.add-data-material-ms') }}</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="{{ route('dashboard') }}">{{ trans('menu.dashboard') }}</a>
                            </li>
                            <li>
                                <a>{{ trans('menu.produksi') }}</a>
                            </li>
                            <li class="active">
                                {{ trans('menu.add-data-material-ms') }}
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            @if(Session::has('successMessage'))
            <div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-check-all"></i>
                <strong>{{ Session::get('successMessage') }}.</strong>
            </div>
            @endif

            @if(Session::has('errorMessage'))
            <div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-block-helper"></i>
                <strong>{{ Session::get('errorMessage') }}.</strong>
            </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-border panel-success">
                        <div class="panel-heading">
                        </div>

                        <div class="panel-body">
                            <div class="row">
                                <form class="form-horizontal" role="form" method="post" action="">
                                    {{ csrf_field() }}
                                    <div class="col-md-12">
                                        <p class="text-muted font-14 m-b-20 pull-right">
                                            @can('access', [$resource, 'add'])
                                            <a data-target="#dtms" data-toggle="modal"   class="btn btn-primary waves-effect w-md  waves-light" data-original-title="fa-plus">
                                            <i class="fa fa-plus"></i> {{ trans('fields.add-data-material-ms') }}</a>
                                            @endcan

                                            <div class="col-md-2">
                                                <input type="text" name="jenis_kayu" class="form-control" value="{{ !empty($filters['jenis_kayu']) ? $filters['jenis_kayu'] : '' }}">
                                                @if($errors->has('jenis_kayu'))
                                                <span class="help-block">{{ $errors->first('jenis_kayu') }}</span>
                                                @endif
                                            </div>
                                            

                                            <div class="col-md-4">
                                                <button type="submit" class="btn btn-info waves-effect w-xs waves-light"><i class="fa fa-search"></i> </button>
                                            </div>
                                        </p>
                                    </div>
                                </form>
                            </div>


                            <div id="example" class="row">
                                <table class="table table-bordered table-hover table-striped m-0">
                                    <thead>
                                        <tr>
                                            <th width="5%" class="text-center">{{ trans('fields.num') }}</th>
                                            <th width="10%" class="text-center">{{ trans('fields.wood-type') }}</th>
                                            <th width="5%" class="text-center">{{ trans('fields.high') }}</th>
                                            <th width="5%" class="text-center">{{ trans('fields.width') }}</th>
                                            <th width="5%" class="text-center">{{ trans('fields.length') }}</th>
                                            <th width="5%" class="text-center">{{ trans('fields.pcs') }}</th>
                                            <th width="5%" class="text-center">{{ trans('fields.m3') }}</th>
                                            <th width="5%" class="text-center">{{ trans('fields.action') }}</th>  
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = ($models->currentPage() - 1) * $models->perPage() + 1; ?>
                                        @foreach($models as $model)


                                        <tr>
                                                            <th scope="row" style="text-align: center;">{{ $no++ }}</th>
                                                            <td>{{ $model->jenis_kayu }}</td>
                                                            <td>{{ $model->tinggi }}</td>
                                                            <td>{{ $model->lebar }}</td>
                                                            <td>{{ $model->panjang }}</td>
                                                            <td>{{ $model->pcs }}</td>
                                                            <td>{{ $model->volume }}</td>
                                                            <td class="text-center">
                                                                @can('access', [$resource, 'add'])
                                                                
                                                                    <i class="fa fa-plus"></i>
                                                                {{-- <a  data-toggle="modal" data-target="#dtstockms{{ $model->jumlah_ms_id }}" class="btn btn-xs btn-warning" data-original-title="Edit">
                                                                    <i class="fa fa-plus"></i>
                                                                </a> --}}
                                                                <!-- modal -->
                                                                
                                                                <!-- /.modal -->
                                                                @endcan
                                                            </td>

                                                        </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th width="5%" class="text-center">{{ trans('fields.num') }}</th>
                                            <th width="10%" class="text-center">{{ trans('fields.wood-type') }}</th>
                                            <th width="5%" class="text-center">{{ trans('fields.high') }}</th>
                                            <th width="5%" class="text-center">{{ trans('fields.width') }}</th>
                                            <th width="5%" class="text-center">{{ trans('fields.length') }}</th>
                                            <th width="5%" class="text-center">{{ trans('fields.pcs') }}</th>
                                            <th width="5%" class="text-center">{{ trans('fields.m3') }}</th>
                                            <th width="5%" class="text-center">{{ trans('fields.action') }}</th>  
                                        </tr>
                                    </tfoot>

                                </table>
                                <div class="data-table-toolbar">
                                    {!! $models->render() !!}
                                </div>
                            </div>


                        <div id="dtms" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <!-- <form id="form-cancel" role="form" method="post" action="{{ route('raft-result-savematerialms') }}">
                                    {{ csrf_field() }} -->
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title" id="cancel-text">{{ trans('fields.info') }}</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="card-box table-responsive">
                                                    <table id="datatable" class="table table-striped table-bordered" >
                                                        <thead>
                                                            <tr>
                                                                <th width="5%" class="text-center">{{ trans('fields.num') }}</th>
                                                                <th width="10%" class="text-center">{{ trans('fields.wood-type') }}</th>
                                                                <th width="5%" class="text-center">{{ trans('fields.high') }}</th>
                                                                <th width="5%" class="text-center">{{ trans('fields.width') }}</th>
                                                                <th width="5%" class="text-center">{{ trans('fields.length') }}</th>
                                                                <th width="5%" class="text-center">{{ trans('fields.pcs') }}</th>
                                                                <th width="5%" class="text-center">{{ trans('fields.m3') }}</th>
                                                                <th width="5%" class="text-center">{{ trans('fields.action') }}</th>  
                                                            </tr>
                                                        </thead>

                                                        <tbody>
                                                            <?php $no = ($modelss->currentPage() - 1) * $modelss->perPage() + 1; ?>
                                                            @foreach($modelss as $mod)


                                                            <tr>

                                                                <th scope="row" style="text-align: center;">{{ $no++ }}</th>
                                                                <td>{{ $mod->jenis_kayu }}</td>
                                                                <td>{{ $mod->tinggi }}</td>
                                                                <td>{{ $mod->lebar }}</td>
                                                                <td>{{ $mod->panjang }}</td>
                                                                <td>{{ $mod->pcs }}</td>
                                                                <td>{{ $mod->volume }}</td>
                                                                <td class="text-center">
                                                                    @can('access', [$resource, 'add'])
                                                                    <a  data-id="{{ $mod->jumlah_ms_id }}" data-label="{{ $mod->jenis_kayu }}" data-toggle="modal" data-target="#modal-material" class="btn btn-xs btn-warning cancel-action" data-original-title="Edit">
                                                                        <i class="fa fa-plus"></i>
                                                                    </a>
                                                                    {{-- <a  data-toggle="modal" data-target="#dtstockms{{ $model->jumlah_ms_id }}" class="btn btn-xs btn-warning" data-original-title="Edit">
                                                                        <i class="fa fa-plus"></i>
                                                                    </a> --}}
                                                                    <!-- modal -->
                                                                    
                                                                    <!-- /.modal -->
                                                                    @endcan
                                                                </td>

                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>

                                                    <div class="data-table-toolbar">
                                                        {!! $models->render() !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
        <div id="modal-material" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form id="form-cancel" role="form" method="post" action="{{ route('raft-result-savematerialms') }}">
                        {{ csrf_field() }}

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title" id="cancel-text">{{ trans('fields.taken-ms') }}</h4>
                        </div>



                        <div class="modal-body">

                            <div class="row">
                                <div class="col-md-6">
                                    <label>{{ trans('fields.taken-stock-ms') }}</label>
                                        <div>
                                            <div class="form-group">
                                                <input type="number" class="form-control" name="pcs">
                                                <input type="hidden" id="jumlah_ms_id" name="jumlah_ms_id" value="">
                                                <input type="hidden" name="hd_pallet_jadi_id" value="{{$hd_pallet_id}}">
                                            </div>
                                        </div>
                                    </div>
                            </div>

                            
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close
                            </button>
                            <button id="btn-cancel-po" type="submit" class="btn btn-info waves-effect waves-light">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
        @parent
        <script type="text/javascript">
            $(document).on('ready', function(){

                $('.cancel-action').on('click', function() {
                    $("#jumlah_ms_id").val($(this).data('id'));
                    // $("#cancel-text").html('kdasmaskdma sdklmasd alsdmkasldkmasdkm ' + $(this).data('label') + '?');
                    $("#cancel-text").html('{{ trans('fields.taken-ms') }} ' + $(this).data('label') + '?');
                    clearFormCancel()
                });

                $('#btn-cancel-po').on('click', function(event) {
                    event.preventDefault();
                    if ($('textarea[name="reason"]').val() == '') {
                        $(this).parent().parent().parent().addClass('has-error');
                        $(this).parent().parent().parent().find('span.help-block').html('Reason is required');
                        return
                    } else {
                        clearFormCancel()
                    }

                    $('#form-cancel').trigger('submit');
                });
            });

            var clearFormCancel = function() {
                $('#form-cancel').removeClass('has-error');
                $('#form-cancel').find('span.help-block').html('');
            };
        </script>

        <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables/dataTables.bootstrap.js') }}"></script>

        <script type="text/javascript">
            $(document).ready(function () {
               
                $('#datatable thead tr').clone(true).appendTo( '#datatable  thead' );
                $('#datatable  thead tr:eq(1) th').each( function (i) {
                    var title = $(this).text();
                    $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            
                    $( 'input', this ).on( 'keyup change', function () {
                        if ( table.column(i).search() !== this.value ) {
                            table
                                .column(i)
                                .search( this.value )
                                .draw();
                        }
                    } );
                } );
            
                var table = $('#datatable ').DataTable( {
                    orderCellsTop: true,
                    fixedHeader: true
                } );

                        });

         

        </script>
        @endsection

