<?php

namespace App\Http\Controllers\Produksi\OutputKD;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Produksi\Tally;
use App\Model\Produksi\IsiAfkirProses;
use App\Model\Produksi\HdLpb;
use App\Model\Produksi\LPBDetail;
use App\Service\Master\WoodService;
use App\Service\Master\DesService;
use App\Service\Master\AnalisaAfkirService;
use DB;


class AfkirProsesKeringController extends Controller
{
    const URL       = 'produksi/afkir-kering';
    const RESOURCE  = 'afkir-kering';


    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }



        $query = DB::table('tally_table')
            ->select('tally_table.tally_id', 'tally_table.tally_no')
            // ->where('dt_lpb.status', '=', true)
            ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'tally_table.tally_id')
            ->groupBy('tally_table.tally_id', 'tally_table.tally_no')
            ->orderBy('tally_no');


        return view('produksi/rawmaterial/afkir-proses.index', [
            // "tal"        => $model,
            // "tallyLPBOptions"   => TallyService::getTallyLPB(),
            "models"        => $query->paginate(10),
            "woodOptions"   => WoodService::getActiveWood(),
            "desOptions"   => DesService::getActiveDes(),
            "analisaafkirOptions"   => AnalisaAfkirService::getActiveAnalisaAfkir(),
            // "filters"       => $filters,
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function add(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        // $model = Tally::find($id);
        $model = DB::table('tally_table')
            ->select('tally_table.tally_id', 'tally_table.tally_no', 'tally_table.status')
            ->where('dt_lpb.status', '=', true)
            ->where('dt_lpb.tally_id', '=', $id)
            ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'tally_table.tally_id')
            ->groupBy('tally_table.tally_id', 'tally_table.tally_no', 'tally_table.status')
            ->orderBy('tally_no')->first();

        if ($model === null) {
            abort('404');
        }


        $query = DB::table('dt_isi_afkir_proses')
            ->select('dt_isi_afkir_proses.isi_afkir_proses_id','dt_isi_afkir_proses.created_date','dt_isi_afkir_proses.status', 'dt_isi_afkir_proses.jenis_kayu_id', 'dt_isi_afkir_proses.analisa_afkir_id', 'tally_table.tally_no', 'mst_jenis_kayu.jenis_kayu', 'mst_analisa_afkir.analisa_afkir',  'dt_isi_afkir_proses.tinggi', 'dt_isi_afkir_proses.lebar', 'dt_isi_afkir_proses.panjang', 'dt_isi_afkir_proses.pcs', 'dt_isi_afkir_proses.volume', 'dt_isi_afkir_proses.created_date') //
            ->where('dt_isi_afkir_proses.status', '=', true)
            ->where('dt_isi_afkir_proses.tally_id', '=', $id)
            // ->join('dt_lpb', 'dt_lpb.dt_lpb_id', '=', 'dt_isi_afkir_proses.dt_lpb_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_afkir_proses.tally_id')
            ->join('mst_analisa_afkir', 'mst_analisa_afkir.analisa_afkir_id', '=', 'dt_isi_afkir_proses.analisa_afkir_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_afkir_proses.jenis_kayu_id')
            // ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_afkir_proses.deskripsi_id')
            // ->orderBy('hd_lpb.created_by');
            ->orderBy('dt_isi_afkir_proses.created_date');
            //  dd($query);



        return view('produksi/output-kd/afkirproses-kering.add', [
            "tal"         => $model,
            "models"         => $query->paginate(10),
            "woodOptions"   => WoodService::getActiveWood(),

            "analisaafkirOptions"   => AnalisaAfkirService::getActiveAnalisaAfkir(),
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }


    public function edit(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $model = Tally::find($id);

        if ($model === null) {
            abort('404');
        }


        $query = DB::table('dt_isi_afkir_proses')
            ->select('dt_isi_afkir_proses.*', 'mst_jenis_kayu.jenis_kayu', 'dt_isi_afkir_proses.created_date')
            ->where('tally_id', '=', $id)
            ->where('dt_isi_afkir_proses.status', '=', true)
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_afkir_proses.jenis_kayu_id')
            // ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_afkir_proses.deskripsi_id')
            ->orderBy('dt_isi_afkir_proses.created_by');

            dd($query);

        return view('produksi/output-kd/afkir-kering.add', [
            "model"         => $model,
            "models"         => $query->paginate(10),
            "woodOptions"   => WoodService::getActiveWood(),
            "desOptions"   => DesService::getActiveDes(),
            "analisaafkirOptions"   => AnalisaAfkirService::getActiveAnalisaAfkir(),
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }



    public function save(Request $request)
    {

        // dd('TEst');
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        $id     = intval($request->get('isi_afkir_proses_id', 0));

        $this->validate($request, [
            'tally_id'            => 'required|integer',
            'jenis_kayu_id'       => 'required|integer',
            'tinggi'              => 'required|integer',
            'lebar'               => 'required|integer',
            'panjang'             => 'required|integer',
            'pcs'                 => 'required|integer',
            'analisa_afkir_id'    => 'required|integer',

        ]);

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            if (empty($id)) {
                $model  = new IsiAfkirProses();
                $model->created_by      = \Auth::user()->id;
             
            } else {
                $model  = IsiAfkirProses::find($id);
                $model->modified_by   = \Auth::user()->id;
                $model->modified_date = $now;
            }

            $model->tally_id   = $request->get('tally_id');
            $model->jenis_kayu_id   = $request->get('jenis_kayu_id');
            $model->tinggi   = $request->get('tinggi');
            $model->lebar   = $request->get('lebar');
            $model->panjang   = $request->get('panjang');
            $model->pcs   = $request->get('pcs');
            $model->volume   = ($request->get('tinggi') * $request->get('panjang') * $request->get('lebar') * $request->get('pcs')) / 1000000000;
            $model->keterangan          = 'Output KD';
            $model->analisa_afkir_id   = $request->get('analisa_afkir_id');
            $model->status   = !empty($request->get('status')) ? true : false;
            $model->created_date    = $request->get('created_date');




            $model->save();

            // dd($model);
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('fields.add-afkir-proses') . ' ' . ''])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('fields.add-afkir-proses') . ' ' . ''])
            );
        }

        return redirect(route('afkirproses-kering-add', ['id' => $request->get('tally_id')]));
    }
}
