<?php

namespace App\Http\Controllers\Produksi\Mutation_Wip;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Produksi\Tally;
use App\Model\Produksi\IsiMS;
use App\Service\Master\WoodService;
use Maatwebsite\Excel\Facades\Excel;
use App\Model\Produksi\DtAmbilStokMs;
use App\Model\Produksi\DtProsesStockMs;
use App\Model\Produksi\JumlahMs;
use App\Service\Master\DesService;


// use App\Service\Master\DesService;       
use DB;

class MSController extends Controller
{
    const URL       = 'produksi/ms';
    const RESOURCE  = 'ms';
    // const URL       = 'produksi/ms/stockms';
    // const RESOURCE  = 'ms/stockms';
    // const RESOURCE  = 'ms/stock-ms';



    public function stockms(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        if ($request->isMethod('post')) {
            $request->session()->put('filters', $request->all());
            return redirect(self::URL . '?page=1');
        } elseif (empty($request->get('page'))) {
            $request->session()->forget('filters');
        }
        $filters = $request->session()->get('filters');



        $info = DB::table('jumlah_ms')
            ->select(
                'mst_jenis_kayu.jenis_kayu',
                'jumlah_ms.tinggi',
                'jumlah_ms.lebar',
                'jumlah_ms.panjang',
                DB::raw('SUM(jumlah_ms.pcs) as pcs'),
                DB::raw('SUM(jumlah_ms.volume) as volume')
            )
            ->where('jumlah_ms.pcs', '!=', 0)
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'jumlah_ms.jenis_kayu_id')
            ->groupBy('mst_jenis_kayu.jenis_kayu', 'jumlah_ms.tinggi', 'jumlah_ms.lebar', 'jumlah_ms.panjang');


        $query = DB::table('jumlah_ms')
            ->select('jumlah_ms.jumlah_ms_id', 'jumlah_ms.jenis_kayu_id', 'mst_jenis_kayu.jenis_kayu', 'jumlah_ms.tinggi', 'jumlah_ms.lebar', 'jumlah_ms.panjang', 'jumlah_ms.pcs', 'jumlah_ms.volume', 'jumlah_ms.modified_date')
            ->where('jumlah_ms.pcs', '!=', 0)
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'jumlah_ms.jenis_kayu_id')
            ->orderBy('jumlah_ms.created_date', 'DESC');


        if (!empty($filters['role'])) {
            $query->where('jumlah_ms.jenis_kayu_id', 'like', '%' . $filters['role'] . '%');
        }
        if (!empty($filters['tinggi'])) {
            $query->where('jumlah_ms.tinggi', 'like', '%' . $filters['tinggi'] . '%');
        }
        if (!empty($filters['lebar'])) {
            $query->where('jumlah_ms.lebar', 'like', '%' . $filters['lebar'] . '%');
        }
        if (!empty($filters['panjang'])) {
            $query->where('jumlah_ms.panjang', 'like', '%' . $filters['panjang'] . '%');
        }



        //dd($info);

        return view('produksi/mutation_wip/ms.stockms', [

            "models"         => $query->paginate(10),
            "info"         => $info->paginate(10),

            "woodOptions"   => WoodService::getActiveWood(),
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function importexcel(Request $request)
    {

        //if ($request->Request('file')) {
        $model  = new JumlahMs();
        $now    = new \DateTime();



        //$model->modified_date = $now;
        $path = $request->file('file')->getRealPath();
        $data = EXCEL::load($path, function ($reader) { })->get();

        if (!empty($data) && $data->count()) {
            foreach ($data as $key => $value) {
                $modal = new JumlahMs();
                $modal->jenis_kayu_id = $value->jenis_kayu_id;
                $modal->tinggi = $value->tinggi;
                $modal->lebar = $value->lebar;
                $modal->panjang = $value->panjang;
                $modal->pcs = $value->pcs;
                $modal->volume = $value->volume;
                $modal->created_by = $value->created_by;
                $modal->created_date    =  $value->created_date;
                $modal->save();
            }
        }

        return back();
    }

    public function destroy(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $isi = IsiMS::find($id);
        $direct = $isi->tally_id;
        if ($isi === null) {
            abort('404');
        }
        // dd($isi->);

        // $isi =  LPBDetail::where('dt_lpb_id',$id)->delete();



        DB::beginTransaction();
        try {

            $isi->delete();
            DB::commit();



            $request->session()->flash(
                'successMessage',
                trans('message.delete-message', ['variable' => trans('fields.notally') . ' ' . $id])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.faildel-message', ['variable' => trans('fields.notally') . ' ' . $id])
            );
        }

        return redirect(route('ms-add', ['id' => $direct]));
    }



    public function savetakems(Request $request)
    {
        // dd("hallo : ".$request->get('jumlah_ms_id')." blabla : ".$request->get('pcs'));
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        // $id     = intval($request->get('id_jumlah_afkir', 0));

        $this->validate($request, [
            'jumlah_ms_id'    => 'required|integer',
            'pcs'                 => 'required|integer',
        ]);


        DB::beginTransaction();
        try {

            $query = DB::table('jumlah_ms')
                ->select('jumlah_ms_id', 'jenis_kayu_id', 'tinggi', 'lebar', 'panjang', 'pcs', 'volume')
                ->where('jumlah_ms_id', "=", $request->get('jumlah_ms_id'))
                ->orderBy('created_by')->first();

            //  dd($query);


            $now    = new \DateTime();
            $model  = new DtAmbilStokMs();
            $model->created_by      = \Auth::user()->id;
            $model->created_date    = $now;


            $model->jumlah_ms_id   = $request->get('jumlah_ms_id');
            $model->jenis_kayu_id   = $query->jenis_kayu_id;
            $model->tinggi   = $query->tinggi;
            $model->lebar   = $query->lebar;
            $model->panjang   = $query->panjang;
            $model->pcs   = $request->get('pcs');
            $model->volume   = ($query->tinggi * $query->panjang * $query->lebar * $request->get('pcs')) / 1000000000;

            //mkurangi jumlah
            $newpcs = $query->pcs - $model->pcs;
            $newvolume = $query->volume -  $request->get('pcs');
            $query = DB::table('jumlah_ms')
                ->where('jumlah_ms_id', "=", $request->get('jumlah_ms_id'))
                ->update(['pcs' => $newpcs, 'volume' => $newvolume, 'modified_by' => \Auth::user()->id, 'modified_date' => $now]);





            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('fields.add-mutation') . ' ' . ''])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => $e->getMassage()])
            );
        }

        return redirect(route('ms-stockms'));
    }


    public function saveminms(Request $request)
    {
        // dd("hallo : ".$request->get('jumlah_ms_id')." blabla : ".$request->get('pcs'));
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        // $id     = intval($request->get('id_jumlah_afkir', 0));

        $this->validate($request, [
            'jumlah_ms_id'    => 'required|integer',
            // 'jenis_kayu_id'       => 'required|integer',
            // 'tinggi'              => 'required|integer',
            // 'lebar'               => 'required|integer',
            // 'panjang'             => 'required|integer',
            'pcs'                 => 'required|integer',
        ]);


        DB::beginTransaction();
        try {

            $query = DB::table('jumlah_ms')
                ->select('jumlah_ms_id', 'jenis_kayu_id', 'tinggi', 'lebar', 'panjang', 'pcs', 'volume')
                ->where('jumlah_ms_id', "=", $request->get('jumlah_ms_id'))
                ->orderBy('created_by')->first();

            // dd($query);

            $now    = new \DateTime();
            $model  = new DtProsesStockMs();
            $model->created_by      = \Auth::user()->id;
            $model->created_date    = $now;
            // }else{
            //     $model  =  DtAmbilStockAfkir::find($id);
            //     $model->modified_by   = \Auth::user()->id;
            //     $model->modified_date = $now;
            // }

            $model->jumlah_ms_id   = $request->get('jumlah_ms_id');
            $model->jenis_kayu_id   = $query->jenis_kayu_id;
            $model->tinggi   = $query->tinggi;
            $model->lebar   = $query->lebar;
            $model->panjang   = $query->panjang;
            $model->pcs   = $request->get('pcs');
            $model->proses_date   = $request->get('date_min_ms');
            $model->keterangan = 'Pengurangan';

            $model->volume   = ($query->tinggi * $query->panjang * $query->lebar * $request->get('pcs')) / 1000000000;

            $newpcs = $query->pcs - $model->pcs;
            $newvolume = $query->volume - $model->volume;

            $query = DB::table('jumlah_ms')
                ->where('jumlah_ms_id', "=", $request->get('jumlah_ms_id'))
                ->update(['pcs' => $newpcs, 'volume' => $newvolume, 'modified_by' => \Auth::user()->id, 'modified_date' => $now]);




            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('fields.add-mutation') . ' ' . ''])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => $e->getMessage()])
            );
        }

        return redirect(route('ms-stockms'));
    }




    public function saveplusms(Request $request)
    {
        // dd("hallo : ".$request->get('jumlah_ms_id')." blabla : ".$request->get('pcs'));
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        // $id     = intval($request->get('id_jumlah_afkir', 0));

        $this->validate($request, [
            'jumlah_ms_id'    => 'required|integer',
            // 'jenis_kayu_id'       => 'required|integer',
            // 'tinggi'              => 'required|integer',
            // 'lebar'               => 'required|integer',
            // 'panjang'             => 'required|integer',
            'pcs'                 => 'required|integer',
        ]);


        DB::beginTransaction();
        try {

            $query = DB::table('jumlah_ms')
                ->select('jumlah_ms_id', 'jenis_kayu_id', 'tinggi', 'lebar', 'panjang', 'pcs', 'volume')
                ->where('jumlah_ms_id', "=", $request->get('jumlah_ms_id'))
                ->orderBy('created_by')->first();

            // dd($query);

            $now    = new \DateTime();
            $model  = new DtProsesStockMs();
            $model->created_by      = \Auth::user()->id;
            $model->created_date    = $now;
            // }else{
            //     $model  =  DtAmbilStockAfkir::find($id);
            //     $model->modified_by   = \Auth::user()->id;
            //     $model->modified_date = $now;
            // }

            $model->jumlah_ms_id   = $request->get('jumlah_ms_id');
            $model->jenis_kayu_id   = $query->jenis_kayu_id;
            $model->tinggi   = $query->tinggi;
            $model->lebar   = $query->lebar;
            $model->panjang   = $query->panjang;
            $model->pcs   = $request->get('pcs');
            $model->proses_date   = $request->get('date_min_ms');
            $model->keterangan = 'Penambahan';

            $model->volume   = ($query->tinggi * $query->panjang * $query->lebar * $request->get('pcs')) / 1000000000;

            $newpcs = $query->pcs + $model->pcs;
            $newvolume = $query->volume + $model->volume;

            $query = DB::table('jumlah_ms')
                ->where('jumlah_ms_id', "=", $request->get('jumlah_ms_id'))
                ->update(['pcs' => $newpcs, 'volume' => $newvolume, 'modified_by' => \Auth::user()->id, 'modified_date' => $now]);




            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('fields.add-mutation') . ' ' . ''])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('fields.add-mutation') . ' ' . ''])
            );
        }

        return redirect(route('ms-stockms'));
    }


    public function add(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        $model = Tally::find($id);

        if ($model === null) {
            abort('404');
        }

        $data = \DB::table('tally_table')
            ->select('tally_table.tally_id', 'tally_table.tally_no', 'tally_table.date_mutation_wip', DB::raw('(CASE WHEN (select sum(a.volume) from dt_isi_ms a where a.tally_id = tally_table.tally_id) = NULL THEN 0 ELSE round((select sum(a.volume) from dt_isi_ms a where a.tally_id = tally_table.tally_id)/(select sum(b.volume) from dt_isi_tally b where b.tally_id = tally_table.tally_id)*100, 2) END) as volumes')) //DB::raw('(select sum(a.volume) from dt_isi_ms a where a.tally_id = tally_table.tally_id) as volume_ms , (select sum(b.volume) from dt_isi_tally b where b.tally_id = tally_table.tally_id) as volume_tally')
            // ->select('tally_table.tally_id', 'tally_table.tally_no', 'tally_table.date_mutation_wip', DB::raw('sum(dt_isi_tally.volume) as volumes_t'), DB::raw('sum(dt_isi_ms.volume) as volumes_m')) //

            ->where('tally_table.status', '=', true)
            ->where('tally_id', '=', $id)
            ->where('tally_table.date_in_kd', '!=', NULL)
            ->where('tally_table.date_out_kd', '!=', NULL)
            ->where('tally_table.date_mutation_wip', '!=', NULL)

            ->where('status_mutation_wip', '=', 1)->first();
        // ->join('dt_isi_tally', 'dt_isi_tally.tally_id', '=', 'tally_table.tally_id')
        // ->leftjoin('dt_isi_ms', 'dt_isi_ms.tally_id', '=', 'dt_isi_tally.tally_id')
        // ->groupBy('tally_table.tally_id', 'tally_table.tally_no', 'tally_table.date_mutation_wip')
        // ->orderBy('dt_isi_tally.tally_id')->get();
        // ->orderBy('tally_table.tally_id');

        // dd($data);


        $query = DB::table('dt_isi_ms')
            ->select('dt_isi_ms.*', 'mst_jenis_kayu.jenis_kayu')
            ->where('tally_id', '=', $id)
            ->where('dt_isi_ms.status', '=', true)
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_ms.jenis_kayu_id')
            ->orderBy('dt_isi_ms.created_by');

        return view('produksi/mutation_wip/ms.add', [
            "model"         => $model,
            "data"          =>  $data,
            "models"         => $query->paginate(10),
            "woodOptions"   => WoodService::getActiveWood(),
            "desOptions"   => DesService::getActiveDes(),
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function exportexcel()
    {

        ob_end_clean();
        ob_start();
        Excel::create('Data Stock MS', function ($excel) {

            $excel->sheet('Sheet 1', function ($sheet) {

                $query = DB::table('jumlah_ms')
                    ->select('jumlah_ms.jumlah_ms_id', 'jumlah_ms.jenis_kayu_id', 'mst_jenis_kayu.jenis_kayu', 'jumlah_ms.tinggi', 'jumlah_ms.lebar', 'jumlah_ms.panjang', 'jumlah_ms.pcs', 'jumlah_ms.volume')
                    ->where('jumlah_ms.pcs', '!=', 0)
                    ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'jumlah_ms.jenis_kayu_id')
                    ->orderBy('jenis_kayu')->get();



                foreach ($query as $product) {
                    $data[] = array(

                        $product->jenis_kayu,
                        $product->tinggi,
                        $product->lebar,
                        $product->panjang,
                        $product->pcs,
                        $product->volume,
                        $product->jumlah_ms_id,



                    );
                }

                $sheet->fromArray($data, null, 'A1', false, false);
                $headings = array('Jenis Kayu',  'Tinggi', 'Lebar',  'Panjang', 'Pcs', 'Volume', 'MS ID');
                $sheet->prependRow(1, $headings);
            });
        })->export('xlsx');
    }



    public function edit(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $model = Tally::find($id);

        if ($model === null) {
            abort('404');
        }

        $query = DB::table('dt_isi_ms')
            ->select('dt_isi_ms.*', 'mst_jenis_kayu.jenis_kayu')
            ->where('tally_id', '=', $id)
            ->where('dt_isi_ms.status', '=', true)
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_ms.jenis_kayu_id')
            // ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_ms.deskripsi_id')
            ->orderBy('dt_isi_ms.created_by');

        return view('produksi/tally/ms.edit', [
            "model"         => $model,
            "models"         => $query->paginate(10),
            "woodOptions"   => WoodService::getActiveWood(),
            "desOptions"   => DesService::getActiveDes(),
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function saveisi(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        $id     = intval($request->get('isi_ms_id', 0));

        $this->validate($request, [
            'tally_id'            => 'required|integer',
            'jenis_kayu_id'       => 'required|integer',

            'tinggi'              => 'required|integer',
            'lebar'               => 'required|integer',
            'panjang'             => 'required|integer',
            'pcs'                 => 'required|integer',
        ]);

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            if (empty($id)) {
                $model  = new IsiMS();
                $model->created_by      = \Auth::user()->id;
                $model->created_date    = $now;
            } else {
                $model  = IsiMS::find($id);
                $model->modified_by   = \Auth::user()->id;
                $model->modified_date = $now;
            }

            $model->tally_id   = $request->get('tally_id');
            $model->jenis_kayu_id   = $request->get('jenis_kayu_id');
            // $model->deskripsi_id   = $request->get('deskripsi_id');
            $model->tinggi   = $request->get('tinggi');
            $model->lebar   = $request->get('lebar');
            $model->panjang   = $request->get('panjang');
            $model->pcs   = $request->get('pcs');

            $model->volume   = ($request->get('tinggi') * $request->get('panjang') * $request->get('lebar') * $request->get('pcs')) / 1000000000;

           
            $jml = DB::table('jumlah_ms')
            ->select('jumlah_ms.pcs', 'jumlah_ms.volume')
            ->where('jumlah_ms.status', '=', true)
            ->where('jumlah_ms.jenis_kayu_id', '=', $request->get('jenis_kayu_id'))
            ->where('jumlah_ms.tinggi', '=', $request->get('tinggi'))
            ->where('jumlah_ms.lebar', '=', $request->get('lebar'))
            ->where('jumlah_ms.panjang', '=', $request->get('panjang'))->first();

        // dd($jml);

        if ($jml === null) {
            // dd("null");
            $up  = new JumlahMs();
            $up->jenis_kayu_id   = $request->get('jenis_kayu_id');
            $up->tinggi   = $request->get('tinggi');
            $up->lebar   = $request->get('lebar');
            $up->panjang   = $request->get('panjang');
            $up->pcs   = $request->get('pcs');
            $up->volume   = ($request->get('tinggi') * $request->get('panjang') * $request->get('lebar') * $request->get('pcs')) / 1000000000;
            $up->created_by      = \Auth::user()->id;
            $up->created_date    = $now;
            $up->save();
        } else {
            // dd("gak null");
            // $model  = JumlahMS::find($id);
            $pcs = $jml->pcs + $request->get('pcs');
            $volume = $jml->volume + (($request->get('tinggi') * $request->get('panjang') * $request->get('lebar') * $request->get('pcs')) / 1000000000);
            $modified_by   = \Auth::user()->id;
            $modified_date = $now;
            // dd($modified_date);
            $up = DB::table('jumlah_ms')
                // ->select('jumlah_ms.pcs')
                ->where('jumlah_ms.status', '=', true)
                ->where('jumlah_ms.jenis_kayu_id', '=', $request->get('jenis_kayu_id'))
                ->where('jumlah_ms.tinggi', '=', $request->get('tinggi'))
                ->where('jumlah_ms.lebar', '=', $request->get('lebar'))
                ->where('jumlah_ms.panjang', '=', $request->get('panjang'))
                ->update(['pcs' => $pcs, 'volume' => $volume, 'modified_by' => $modified_by, 'modified_date' => $now]);
            // dd($up);
        }
            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('fields.add-ms') . ' ' . ''])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('fields.add-ms') . ' ' . ''])
            );
        }

        return redirect(route('ms-add', ['id' => $request->get('tally_id')]));
    }

    public function save(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        $id     = intval($request->get('jumlah_ms_id', 0));

        $this->validate($request, [

            'jenis_kayu_id'       => 'required|integer',
            'tinggi'              => 'required|integer',
            'lebar'               => 'required|integer',
            'panjang'             => 'required|integer',
            'pcs'                 => 'required|integer',
        ]);

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            // if(empty($id)){
            //     $model  = new JumlahMS();
            //     $model->created_by      = \Auth::user()->id;
            //     $model->created_date    = $now;
            // }else{
            //     $model  = JumlahMS::find($id);
            //     $model->modified_by   = \Auth::user()->id;
            //     $model->modified_date = $now;
            // }


            $jml = DB::table('jumlah_ms')
                ->select('jumlah_ms.pcs', 'jumlah_ms.volume')
                ->where('jumlah_ms.status', '=', true)
                ->where('jumlah_ms.jenis_kayu_id', '=', $request->get('jenis_kayu_id'))
                ->where('jumlah_ms.tinggi', '=', $request->get('tinggi'))
                ->where('jumlah_ms.lebar', '=', $request->get('lebar'))
                ->where('jumlah_ms.panjang', '=', $request->get('panjang'))->first();

            // dd($jml);

            if ($jml === null) {
                // dd("null");
                $up  = new JumlahMs();
                $up->jenis_kayu_id   = $request->get('jenis_kayu_id');
                $up->tinggi   = $request->get('tinggi');
                $up->lebar   = $request->get('lebar');
                $up->panjang   = $request->get('panjang');
                $up->pcs   = $request->get('pcs');
                $up->volume   = ($request->get('tinggi') * $request->get('panjang') * $request->get('lebar') * $request->get('pcs')) / 1000000000;
                $up->created_by      = \Auth::user()->id;
                $up->created_date    = $now;
                $up->save();
            } else {
                // dd("gak null");
                // $model  = JumlahMS::find($id);
                $pcs = $jml->pcs + $request->get('pcs');
                $volume = $jml->volume + (($request->get('tinggi') * $request->get('panjang') * $request->get('lebar') * $request->get('pcs')) / 1000000000);
                $modified_by   = \Auth::user()->id;
                $modified_date = $now;
                // dd($modified_date);
                $up = DB::table('jumlah_ms')
                    // ->select('jumlah_ms.pcs')
                    ->where('jumlah_ms.status', '=', true)
                    ->where('jumlah_ms.jenis_kayu_id', '=', $request->get('jenis_kayu_id'))
                    ->where('jumlah_ms.tinggi', '=', $request->get('tinggi'))
                    ->where('jumlah_ms.lebar', '=', $request->get('lebar'))
                    ->where('jumlah_ms.panjang', '=', $request->get('panjang'))
                    ->update(['pcs' => $pcs, 'volume' => $volume, 'modified_by' => $modified_by, 'modified_date' => $now]);
                // dd($up);
            }


            // $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('fields.add-ms') . ' ' . ''])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('fields.add-ms') . ' ' . ''])
            );
        }

        return redirect(route('ms-stockms', ['id' => $request->get('tally_id')]));
    }
}










// public function saveminms(Request $request)
//     {
//         // dd("hallo : ".$request->get('jumlah_ms_id')." blabla : ".$request->get('pcs'));
//          if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
//             abort('403');
//         }

//         // $id     = intval($request->get('id_jumlah_afkir', 0));

//         $this->validate($request, [
//             'jumlah_ms_id'    => 'required|integer',
//             // 'jenis_kayu_id'       => 'required|integer',
//             // 'tinggi'              => 'required|integer',
//             // 'lebar'               => 'required|integer',
//             // 'panjang'             => 'required|integer',
//             'pcs'                 => 'required|integer',
//         ]); 


//         DB::beginTransaction();
//          try {

//             $query = DB::table('jumlah_ms')
//                 ->select('jumlah_ms_id', 'jenis_kayu_id', 'tinggi', 'lebar', 'panjang', 'pcs', 'volume')
//                 ->where('jumlah_ms_id', "=", $request->get('jumlah_ms_id'))
//                 ->orderBy('created_by')->first();

//             // dd($query);

//                 $now    = new \DateTime();
//                 $model  = new DtProsesStockMs();
//                 $model->created_by      = \Auth::user()->id;
//                 $model->created_date    = $now;
//             // }else{
//             //     $model  =  DtAmbilStockAfkir::find($id);
//             //     $model->modified_by   = \Auth::user()->id;
//             //     $model->modified_date = $now;
//             // }

//             $model->jumlah_ms_id   = $request->get('jumlah_ms_id');
//             $model->jenis_kayu_id   = $query->jenis_kayu_id;
//             $model->tinggi   = $query->tinggi;
//             $model->lebar   = $query->lebar;
//             $model->panjang   = $query->panjang;
//             $model->pcs   = $request->get('pcs');
//             $model->proses_date   = $request->get('date_min_ms');
//             $model->keterangan = 'Pengurangan';
            
//             $model->volume   = ($query->tinggi*$query->panjang*$query->lebar*$request->get('pcs'))/1000000000;

//             $newpcs = $query->pcs - $model->pcs;
//             $newvolume = $query->volume - $model->volume;

//             $query = DB::table('jumlah_ms')
//                 ->where('jumlah_ms_id', "=", $request->get('jumlah_ms_id'))
//                 ->update([ 'pcs' => $newpcs, 'volume' => $newvolume, 'modified_by' => \Auth::user()->id, 'modified_date' => $now ]);
                



//             $model->save();
//             DB::commit();

//             $request->session()->flash(
//                 'successMessage',
//                 trans('message.saved-message', ['variable' => trans('fields.add-mutation').' '.''])
//             );

//         } catch (\Exception $e) {
//            DB::rollback();
//            $request->session()->flash(
//             'errorMessage',
//             trans('message.failed-message', ['variable' => trans('fields.add-mutation').' '.''])
//         );
//        }

//        return redirect(route('ms-stockms')); 


//     }
