<?php 
// use App\Service\Master\SupplierService; 
// use App\Model\MasterProduksi\Supplier;
// use App\Model\Produksi\HdLpb;


list($titleheader, $date) = explode("#", $title);
?>
<table id="header" width="100%" border="0px" style="font-size:10px">
	<tr>
		<td  width="22%" height="60"> </td>
		<td  width="45%" height="60" align="center" style="font-weight: bold;"> </td>
		<td  width="33%" height="60"> </td>
	</tr>
	
	<tr>
		<td width="47%"> </td>
		<td width="13%" ><b> {{ date('d-m-Y', strtotime($model->tanggal_mutasi)) }}</b></td>
		<td width="40%"> <b>No Mutasi :{{ $model->nomor_mutasi }}</b></td>

	</tr>


	<tr>
		<td width="100%"> </td>
	</tr>

	<tr>
		<td width="20%" height="25"> </td>
		<td width="47%" height="25"> <b>{{ $model->lokasi_mutasi }} </b></td>
		<td width="33%" height="25"> <b>{{ $model->supir_plat}} </b> </td>
	</tr>
	

	<tr>
		<td width="25%" height="32"> </td>
		<td width="75%" height="32"><b> Catatan : {{ $model->catatan }} </b> </td>

	
	</tr>
	
	<tr>
		<td width="40%" height="27"> </td>
		<td width="60%" height="24"><b></b> </td>
	</tr>

	<?php
		
			$total_pcs = 0; 
			$total_volume = 0; 
			
	?>
	<tr>
		<td width="40%" height="17" align="center"><b>Pcs</b> </td>
		<td width="20%" height="17" align="center"><b>Volume</b></td>
	
		<td width="20%" height="17" align="center"><b>Jenis Kayu</b></td>
		<td width="20%" height="17" align="center"><b>Status</b></td>
	</tr>
	@foreach ($models as $view)
	<tr>
		<td width="40%" height="17" align="center"> {{ $view->pcs}}</td>
		<td width="20%" height="17" align="center">  {{ $view->volume}}</td>

		<td width="20%" height="17" align="center"> {{ $view->jenis_kayu}}</td>
		<td width="20%" height="17" align="center"> {{ $view->status_data}}</td>
	</tr>
	@endforeach



</table>