<?php

namespace App\Http\Controllers\MasterContainer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\MasterContainer\customer;
use DB;

class SetupCustomerController extends Controller
{
    const URL       = 'master-container/setup-customer';
    const RESOURCE  = 'setup-customer';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        if ($request->isMethod('post')) {
            $request->session()->put('filters', $request->all());
            return redirect(self::URL . '?page=1');
        } elseif (empty($request->get('page'))) {
            $request->session()->forget('filters');
        }
        $filters = $request->session()->get('filters');
        $query = \DB::table('mst_customer')
            ->orderBy('mst_customer.nama_customer');

        if (!empty($filters['status']) || !$request->session()->has('filters')) {
            $query->where('mst_customer.status', '=', true);
        } else {
            $query->where('mst_customer.status', '=', false);
        }

        if (!empty($filters['nama_customer'])) {
            $query->where('mst_customer.nama_customer', 'like', '%' . $filters['nama_customer'] . '%');
        }

        return view('master-container/setup-customer.index', [
            "models"        => $query->paginate(10),
            "filters"       => $filters,
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function add(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        $model           = new Customer();
        $model->status   = true;

        return view('master-container/setup-customer.add', [
            "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }

    public function edit(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $model = Customer::find($id);

        if ($model === null) {
            abort('404');
        }

        return view('master-container/setup-customer.add', [
            "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }

    public function delete(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $model =  Customer::find($id);

        if ($model === null) {
            abort('404');
        }

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            $model->modified_by   = \Auth::user()->id;
            $model->modified_date = $now;
            $model->status   = false;

            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.delete-message', ['variable' => trans('fields.customer_name') . ' ' . $id])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.faildel-message', ['variable' => trans('fields.customer_name') . ' ' . $id])
            );
        }

        return redirect(route('setup-customer-index'));
    }

    public function save(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add']) && !\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $id     = intval($request->get('id', 0));

        $this->validate($request, [
            'nama_customer' => 'required|max:255|unique:mst_customer,nama_customer,' . $id . ',customer_id',
        ]);

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            if (empty($id)) {
                $model  = new Customer();
                $model->created_by      = \Auth::user()->id;
                $model->created_date    = $now;
            } else {
                $model  = Customer::find($id);
                $model->modified_by   = \Auth::user()->id;
                $model->modified_date = $now;
            }

            $model->nama_customer   = $request->get('nama_customer');
            $model->status   = !empty($request->get('status')) ? true : false;

            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('menu.customer_name') . ' ' . $request->get('nama_customer')])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('menu.customer_name') . ' ' . $request->get('nama_customer')])
            );
        }

        return redirect(route('setup-customer-index'));
    }
}
