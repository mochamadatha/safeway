<?php

namespace App\Http\Controllers\DivisionContainer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\MasterContainer\Agent;
use App\Model\MasterContainer\ContainerInspect;
use App\Model\MasterContainer\Customer;
use App\Model\MasterContainer\DepoLocation;
use App\Model\MasterContainer\Inspect;
use App\Model\MasterContainer\Size;
use App\Service\DivisionContainer\AgentService;
use App\Service\DivisionContainer\CustomerService;
use App\Service\DivisionContainer\DepoLocationService;
use App\Service\DivisionContainer\InspectService;
use App\Service\DivisionContainer\SizeService;
use Elibyy\TCPDF\Facades\TCPDF;
use DB;

class ContainerInspectionReport extends Controller
{
    const URL       = 'division-container/container-inspect';
    const RESOURCE  = 'container-inspect';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        if ($request->isMethod('post')) {
            $request->session()->put('filters', $request->all());
            return redirect(self::URL . '?page=1');
        } elseif (empty($request->get('page'))) {
            $request->session()->forget('filters');
        }

        $filters = $request->session()->get('filters');
        $query = \DB::table('dt_containerinspect')
            ->where('dt_containerinspect.status', '=', true)
            ->orderBy('dt_containerinspect.container_number');

        if (!empty($filters['status']) || !$request->session()->has('filters')) {
            $query->where('dt_containerinspect.status', '=', true);
        } else {
            $query->where('dt_containerinspect.status', '=', false);
        }


        $filters = $request->session()->get('filters');
        $query = \DB::table('dt_containerinspect')
            ->select('dt_containerinspect.*', 'mst_agent.nama_agent', 'mst_customer.nama_customer', 'mst_depolocation.nama_depolocation', 'mst_inspect.nama_inspect', 'mst_size.size')
            ->join('mst_agent', 'mst_agent.agent_id', '=', 'dt_containerinspect.agent_id')
            ->join('mst_customer', 'mst_customer.customer_id', '=', 'dt_containerinspect.customer_id')
            ->join('mst_depolocation', 'mst_depolocation.depolocation_id', '=', 'dt_containerinspect.depolocation_id')
            ->join('mst_inspect', 'mst_inspect.inspect_id', '=', 'dt_containerinspect.inspect_id')
            ->join('mst_size', 'mst_size.size_id', '=', 'dt_containerinspect.size_id')
            ->orderBy('container_number');


        if (!empty($filters['container_number'])) {
            $query->where('dt_containerinspect.container_number', 'like', '%' . $filters['container_number'] . '%');
        }



        return view('division-container/container-inspect.index', [
            "models"        => $query->paginate(10),
            "filters"       => $filters,
            "agentOptions"   => AgentService::getActiveAgent(),
            "customerOptions" => CustomerService::getActiveCustomer(),
            "depoOptions"     => DepoLocationService::getActiveDepoLocation(),
            "inspectOptions"    => InspectService::getActiveInspect(),
            "sizeOptions"   => SizeService::getActiveSize(),
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function reportcontainerpdf(Request $request, $id)
    {


        // $model = ContainerInspect::find($id);

        // $filters = $request->session()->get('filters');
        $query = \DB::table('dt_containerinspect')
            ->select('dt_containerinspect.*', 'mst_agent.nama_agent', 'mst_customer.nama_customer', 'mst_depolocation.nama_depolocation', 'mst_inspect.nama_inspect', 'mst_size.size')
            ->where('dt_containerinspect.containerinspect_id', '=', $id)
            ->join('mst_agent', 'mst_agent.agent_id', '=', 'dt_containerinspect.agent_id')
            ->join('mst_customer', 'mst_customer.customer_id', '=', 'dt_containerinspect.customer_id')
            ->join('mst_depolocation', 'mst_depolocation.depolocation_id', '=', 'dt_containerinspect.depolocation_id')
            ->join('mst_inspect', 'mst_inspect.inspect_id', '=', 'dt_containerinspect.inspect_id')
            ->join('mst_size', 'mst_size.size_id', '=', 'dt_containerinspect.size_id')
            ->orderBy('container_number')->first();

        $now    = new \DateTime();
        $now = date_format($now, 'd/m/Y h:i:s');

        $header = view('division-container/container-inspect.header-pdf', ['title' => trans('menu.containerinspectionreport') . "#" . $now])->render();
        \PDF::setHeaderCallback(function ($pdf) use ($header) {
            $pdf->writeHTML($header);
        });

        $model =  ContainerInspect::find($id);
        if ($model->temperature == null) {

            $html = view('division-container/container-inspect.report-container-8-pdf', [
                // 'model' => $model,
                "model"         => $query,
                "agentOptions"   => AgentService::getActiveAgent(),
                "customerOptions" => CustomerService::getActiveCustomer(),
                "depoOptions"     => DepoLocationService::getActiveDepoLocation(),
                "inspectOptions"    => InspectService::getActiveInspect(),
                "sizeOptions"   => SizeService::getActiveSize(),

            ])->render();

            $pdf = new TCPDF();
            $pdf::SetTitle('Container Inspection Report');
            ob_end_clean();
            $pdf::AddPage();
            $pdf::writeHTML($html, true, false, true, false, '');
            // $pdf::Output($model->container_number.' '.$model->created_date.'.pdf');
            $pdf::Output($query->container_number . ' ' . $query->created_date . '.pdf');
            $pdf->SetFont('Helvetica', '', 9);
        } else {

            $html = view('division-container/container-inspect.report-container-pdf', [
                // 'model' => $model,
                "model"         => $query,
                "agentOptions"   => AgentService::getActiveAgent(),
                "customerOptions" => CustomerService::getActiveCustomer(),
                "depoOptions"     => DepoLocationService::getActiveDepoLocation(),
                "inspectOptions"    => InspectService::getActiveInspect(),
                "sizeOptions"   => SizeService::getActiveSize(),

            ])->render();

            $pdf = new TCPDF();
            $pdf::SetTitle('Container Inspection Report');
            ob_end_clean();
            $pdf::AddPage();
            $pdf::writeHTML($html, true, false, true, false, '');
            // $pdf::Output($model->container_number.' '.$model->created_date.'.pdf');
            $pdf::Output($query->container_number . ' ' . $query->created_date . '.pdf');
            $pdf->SetFont('Helvetica', '', 9);
        }
    }

    public function add(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        $model           = new ContainerInspect();
        $model->status   = true;

        return view('division-container/container-inspect.add', [
            "model"         => $model,
            "agentOptions"   => AgentService::getActiveAgent(),
            "customerOptions" => CustomerService::getActiveCustomer(),
            "depoOptions"     => DepoLocationService::getActiveDepoLocation(),
            "inspectOptions"    => InspectService::getActiveInspect(),
            "sizeOptions"   => SizeService::getActiveSize(),
        ]);
    }


    public function edit(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $model = ContainerInspect::find($id);

        if ($model === null) {
            abort('404');
        }

        return view('division-container/container-inspect.add', [
            "model"         => $model,
            "agentOptions"   => AgentService::getActiveAgent(),
            "customerOptions" => CustomerService::getActiveCustomer(),
            "depoOptions"     => DepoLocationService::getActiveDepoLocation(),
            "inspectOptions"    => InspectService::getActiveInspect(),
            "sizeOptions"   => SizeService::getActiveSize(),
        ]);
    }



    public function delete(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $model =  ContainerInspect::find($id);

        if ($model === null) {
            abort('404');
        }

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            $model->modified_by   = \Auth::user()->id;
            $model->modified_date = $now;
            $model->status   = false;

            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.delete-message', ['variable' => trans('fields.container_number') . ' ' . $id])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.faildel-message', ['variable' => trans('fields.container_number') . ' ' . $id])
            );
        }

        return redirect(route('container-inspect-index'));
    }

    public function save(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add']) && !\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $id     = intval($request->get('id'));

        $this->validate($request, [
            'container_number'  => 'required|string|max:255',
            'size'           => 'required|integer',
            'manufacture_year'  => 'required|integer',
            'previous_cargo'    => 'required|string|max:255',
            'packingin'         => 'required|string|max:255',
            'agent'        => 'required|integer',
            'depolocation' => 'required|integer',
            'customer'     => 'required|integer',
            'safeway_sn'        => 'required|string|max:255',
            'inspect'     => 'required|integer',

        ]);

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            if (empty($id)) {
                $model  = new ContainerInspect();
                $model->created_by      = \Auth::user()->id;
                $model->created_date    = $now;
            } else {
                $model  = ContainerInspect::find($id);
                $model->modified_by   = \Auth::user()->id;
                $model->modified_date = $now;
            }

            $model->container_number        = $request->get('container_number');
            $model->size_id                 = $request->get('size');
            $model->manufacture_year        = $request->get('manufacture_year');
            $model->previous_cargo          = $request->get('previous_cargo');
            $model->packingin               = $request->get('packingin');
            $model->agent_id                = $request->get('agent');
            $model->depolocation_id         = $request->get('depolocation');
            $model->customer_id             = $request->get('customer');
            $model->safeway_sn              = $request->get('safeway_sn');
            $model->inspect_id              = $request->get('inspect');

            $doorclosed = $request->file('doorclosed');
            if ($doorclosed !== null) {
                $fotoName   = "d" . md5($now->format('YmdHis')) . "." . $doorclosed->guessExtension();
                $doorclosed->move(\Config::get('app.paths.foto-container'), $fotoName);
                $model->doorclosed = $fotoName;
            }
            $allinterior = $request->file('allinterior');
            if ($allinterior !== null) {
                $fotoName   = "a" . md5($now->format('YmdHis')) . "." . $allinterior->guessExtension();
                $allinterior->move(\Config::get('app.paths.foto-container'), $fotoName);
                $model->allinterior = $fotoName;
            }
            $floor = $request->file('floor');
            if ($floor !== null) {
                $fotoName   = "f" . md5($now->format('YmdHis')) . "." . $floor->guessExtension();
                $floor->move(\Config::get('app.paths.foto-container'), $fotoName);
                $model->floor = $fotoName;
            }

            $roof = $request->file('roof');
            if ($roof !== null) {
                $fotoName   = "r" . md5($now->format('YmdHis')) . "." . $roof->guessExtension();
                $roof->move(\Config::get('app.paths.foto-container'), $fotoName);
                $model->roof = $fotoName;
            }
            $leftwall = $request->file('leftwall');
            if ($leftwall !== null) {
                $fotoName   = "l" . md5($now->format('YmdHis')) . "." . $leftwall->guessExtension();
                $leftwall->move(\Config::get('app.paths.foto-container'), $fotoName);
                $model->leftwall = $fotoName;
            }
            $rightwall = $request->file('rightwall');
            if ($rightwall !== null) {
                $fotoName   = "w" . md5($now->format('YmdHis')) . "." . $rightwall->guessExtension();
                $rightwall->move(\Config::get('app.paths.foto-container'), $fotoName);
                $model->rightwall = $fotoName;
            }
            $seal = $request->file('seal');
            if ($seal !== null) {
                $fotoName   = "s" . md5($now->format('YmdHis')) . "." . $seal->guessExtension();
                $seal->move(\Config::get('app.paths.foto-container'), $fotoName);
                $model->seal = $fotoName;
            }
            $cscplate = $request->file('cscplate');
            if ($cscplate !== null) {
                $fotoName   = "c" . md5($now->format('YmdHis')) . "." . $cscplate->guessExtension();
                $cscplate->move(\Config::get('app.paths.foto-container'), $fotoName);
                $model->cscplate = $fotoName;
            }
            $temperature = $request->file('temperature');
            if ($temperature !== null) {
                $fotoName   = "t" . md5($now->format('YmdHis')) . "." . $temperature->guessExtension();
                $temperature->move(\Config::get('app.paths.foto-container'), $fotoName);
                $model->temperature = $fotoName;
            }


            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('menu.container-inspect') . ' ' . $request->get('container_number')])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('menu.container-inspect') . ' ' . $request->get('container_number')])
            );
        }

        return redirect(route('container-inspect-index'));
    }
}
