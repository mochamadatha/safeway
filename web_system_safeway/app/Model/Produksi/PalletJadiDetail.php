<?php

namespace App\Model\Produksi;

use Illuminate\Database\Eloquent\Model;

class PalletJadiDetail extends Model
{
    protected $table        = 'dt_pallet_jadi';
    protected $primaryKey   = 'dt_pallet_jadi_id';
    public $timestamps      = false;
}
