<?php

namespace App\Service\Master;

class WoodService
{
    public static function getActiveWood()
    {
        return \DB::table('mst_jenis_kayu')
            ->where('status', '=', true)
            ->orderBy('jenis_kayu')
            ->get();
    }
}
