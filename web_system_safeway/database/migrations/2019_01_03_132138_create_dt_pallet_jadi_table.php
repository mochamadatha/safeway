<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDtPalletJadiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dt_pallet_jadi', function (Blueprint $table) {
            $table->increments('dt_pallet_jadi_id');
            $table->integer('hd_pallet_jadi_id');
            $table->integer('jenis_kayu_id');
            $table->integer('tinggi');
            $table->integer('lebar');
            $table->integer('panjang');
            $table->integer('pcs');
            $table->double('volume');
            $table->boolean('status')->default(true);
            $table->integer('created_by');
            $table->dateTime('created_date');
            $table->integer('modified_by')->nullable();
            $table->dateTime('modified_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dt_pallet_jadi');
    }
}
