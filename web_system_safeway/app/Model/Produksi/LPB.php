<?php

namespace App\Model\Produksi;

use Illuminate\Database\Eloquent\Model;

class LPB extends Model
{
    protected $table        = 'hd_lpb';
    protected $primaryKey   = 'hd_lpb_id';
    public $timestamps      = false;
}
