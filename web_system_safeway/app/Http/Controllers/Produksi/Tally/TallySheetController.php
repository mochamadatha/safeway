<?php

namespace App\Http\Controllers\Produksi\Tally;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Produksi\Tally;
use App\Model\Produksi\IsiTally;
use App\Service\Master\WoodService;
use App\Service\Master\DesService;
use DB;

class TallySheetController extends Controller
{
    const URL       = 'produksi/tally/tallysheet';
    const RESOURCE  = 'tallysheet';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        if ($request->isMethod('post')) {
            $request->session()->put('filters', $request->all());
            return redirect(self::URL . '?page=1');
        } elseif (empty($request->get('page'))) {
            $request->session()->forget('filters');
        }
        $filters = $request->session()->get('filters');
        $query = \DB::table('dt_isi_tally')
            ->where('dt_isi_tally.status', '=', true)
            ->orderBy('dt_isi_tally.created_date');

        if (!empty($filters['status']) || !$request->session()->has('filters')) {
            $query->where('tally_table.status', '=', true);
        } else {
            $query->where('tally_table.status', '=', false);
        }

        if (!empty($filters['tally_no'])) {
            $query->where('tally_table.tally_no', 'like', '%' . $filters['tally_no'] . '%');
        }

        return view('produksi/tally/tallysheet.index', [
            "models"        => $query->paginate(10),
            "filters"       => $filters,
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function add(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        $model = Tally::find($id);

        if ($model === null) {
            abort('404');
        }

        $query = DB::table('dt_isi_tally')
            ->select('dt_isi_tally.*', 'mst_jenis_kayu.jenis_kayu', 'mst_deskripsi_tallysheet.nama_deskripsi')
            ->where('tally_id', '=', $id)
            ->where('dt_isi_tally.status', '=', true)
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
            ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_tally.deskripsi_id')
            ->orderBy('dt_isi_tally.created_by');



        return view('produksi/tally/tallysheet.add', [
            "model"         => $model,
            "models"         => $query->paginate(10),
            "woodOptions"   => WoodService::getActiveWood(),
            "desOptions"   => DesService::getActiveDes(),
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }


    public function edit(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $model = Tally::find($id);

        if ($model === null) {
            abort('404');
        }

        $query = DB::table('dt_isi_tally')
            ->select('dt_isi_tally.*', 'mst_jenis_kayu.jenis_kayu', 'mst_deskripsi_tallysheet.nama_deskripsi')
            ->where('tally_id', '=', $id)
            ->where('dt_isi_tally.status', '=', true)
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
            ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_tally.deskripsi_id')
            ->orderBy('dt_isi_tally.created_by');

        return view('produksi/tally/tallysheet.add', [
            "model"         => $model,
            "models"         => $query->paginate(10),
            "woodOptions"   => WoodService::getActiveWood(),
            "desOptions"   => DesService::getActiveDes(),
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }



    public function save(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        $id     = intval($request->get('isi_tally_id', 0));

        $this->validate($request, [
            'tally_id'            => 'required|integer',
            'jenis_kayu_id'       => 'required|integer',
            'deskripsi_id'        => 'required|integer',
            'tinggi'              => 'required|integer',
            'lebar'               => 'required|integer',
            'panjang'             => 'required|integer',
            'pcs'                 => 'required|integer',
        ]);

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            if (empty($id)) {
                $model  = new IsiTally();
                $model->created_by      = \Auth::user()->id;
                $model->created_date    = $now;
            } else {
                $model  = IsiTally::find($id);
                $model->modified_by   = \Auth::user()->id;
                $model->modified_date = $now;
            }

            $model->tally_id   = $request->get('tally_id');
            $model->jenis_kayu_id   = $request->get('jenis_kayu_id');
            $model->deskripsi_id   = $request->get('deskripsi_id');
            $model->tinggi   = $request->get('tinggi');
            $model->lebar   = $request->get('lebar');
            $model->panjang   = $request->get('panjang');
            $model->pcs   = $request->get('pcs');

            $model->volume   = ($request->get('tinggi') * $request->get('panjang') * $request->get('lebar') * $request->get('pcs')) / 1000000000;
            $model->status   = !empty($request->get('status')) ? true : false;



            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('fields.add-tally-sheet') . ' ' . ''])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('fields.add-tally-sheet') . ' ' . ''])
            );
        }

        return redirect(route('tallysheet-add', ['id' => $request->get('tally_id')]));
    }
}
