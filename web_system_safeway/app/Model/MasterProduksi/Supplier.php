<?php

namespace App\Model\MasterProduksi;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $table        = 'mst_supplier';
    protected $primaryKey   = 'supplier_id';
    public $timestamps      = false;
}
