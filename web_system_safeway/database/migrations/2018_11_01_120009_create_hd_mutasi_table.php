<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHdMutasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hd_mutasi', function (Blueprint $table) {
            $table->increments('hd_mutasi_id');
            $table->integer('lokasi_mutasi_id');
            $table->string('nomor_mutasi');
            $table->date('tanggal_mutasi');
            $table->string('supir_plat');
            $table->string('catatan');
            $table->string('status');
            $table->integer('created_by');
            $table->dateTime('created_date');
            $table->integer('modified_by')->nullable();
            $table->dateTime('modified_date')->nullable();
       
        });

          
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hd_mutasi');
    }
}
