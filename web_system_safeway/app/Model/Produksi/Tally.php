<?php

namespace App\Model\Produksi;

use Illuminate\Database\Eloquent\Model;

class Tally extends Model
{
    protected $table        = 'tally_table';
    protected $primaryKey   = 'tally_id';
    public $timestamps      = false;
}
