<?php

return [
        'setup-role'        => ['view', 'add', 'update',],
        'setup-user'        => ['view', 'add', 'update',],
        'setup-status'      => ['view', 'add', 'update',],
    
             
        'generate-notally'  => ['view', 'add', 'update',],
        'tallysheet'        => ['view', 'add', 'update',],
         'supplier'          => ['view', 'add', 'update',],

       
     
        'setup-woodtype'            => ['view', 'add', 'update',],
        'setup-description'         => ['view', 'add', 'update',],
        'setup-supplier'            => ['view', 'add', 'update',],
        'setup-price'               => ['view', 'add', 'update',],
        'setup-chamber'             => ['view', 'add', 'update',],
        'analisa-afkir'             => ['view', 'add', 'update',],
        'setup-arrival-location'    => ['view', 'add', 'update',],
        'setup-mutation-location'   => ['view', 'add', 'update',],
        'pallet-name'              => ['view', 'add', 'update',],

       
        
        'bandsaw'                   => ['view', 'add', 'update',],
        'setup-lpb'                 => ['view', 'add', 'update',],
        'setup-afkir'               => ['view', 'add', 'update',],
        'raw-material'              => ['view', 'add', 'update',],
        'afkir-proses'              => ['view', 'add', 'update',],
        'input-kd'                  => ['view', 'add', 'update',],
        'output-kd'                 => ['view', 'add', 'update',],
        'mutation'                  => ['view', 'add', 'update',],
        'wip'                       => ['view', 'add', 'update',],
        'ms'                        => ['view', 'add', 'update',],
        // 'stock-ms'                  => ['view', 'add', 'update',],
        'ms-stockms'                => ['view', 'add', 'update',],
        'afkir-kering'              => ['view', 'add', 'update',],
        'reproses-afkir'            => ['view', 'add', 'update',],
        'reproses-ms'               => ['view', 'add', 'update',],
        'hasil-reprosesafkir'       => ['view', 'add', 'update',],
        'hasil-reprosesms'          => ['view', 'add', 'update',],
        'raft-result'               => ['view', 'add', 'update',],
        'dashboard'               => ['view', 'add', 'update',],

        'setup-sizeispm'            => ['view', 'add', 'update',],
        'ispm-inspect'            => ['view', 'add', 'update',],
   
        'container-inspect'            => ['view', 'add', 'update',],
        'setup-customer'            => ['view', 'add', 'update',],
        'setup-agent'            => ['view', 'add', 'update',],
        'setup-depolocation'            => ['view', 'add', 'update',],
        'setup-inspect'            => ['view', 'add', 'update',],
        'setup-size'            => ['view', 'add', 'update',],
        'bandsaw-kering'            => ['view', 'add', 'update',],
        'search-all-data'        => ['view', 'add', 'update',],
        'search-data-bandsaw'        => ['view', 'add', 'update',],

        'spk-bandsaw'        => ['view', 'add', 'update',],

        'search-lpb'        => ['view', 'add', 'update',]


        // 'Item Stock'        => ['view',],
        // 'Receipt Item'      => ['view', 'add',],
        // 'Adjustment Stock'  => ['view', 'add',],
    ];
    