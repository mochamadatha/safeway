<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDtLpbTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dt_lpb', function (Blueprint $table) {
            $table->increments('dt_lpb_id');
            $table->integer('hd_lpb_id');
            $table->integer('tally_id');
            $table->integer('isi_tally_id');
            $table->integer('harga_id');
            $table->integer('harga_satuan')->nullable();
            $table->string('note')->nullable();
            $table->double('total_harga');
            $table->double('total_harga_edit')->nullable();
            $table->boolean('status');
            $table->integer('created_by');
            $table->dateTime('created_date');
            $table->integer('modified_by')->nullable();
            $table->dateTime('modified_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dt_lpb');
    }
}
