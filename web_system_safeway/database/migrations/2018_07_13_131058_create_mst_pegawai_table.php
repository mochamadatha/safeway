<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMstPegawaiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_pegawai', function (Blueprint $table) {
            $table->increments('pegawai_id');
            $table->integer('divisi_id');
            $table->integer('cabang_id');
            $table->string('nama_pegawai')->unique();
            $table->string('alamat_pegawai');
            $table->string('email');
            $table->string('phone');
            $table->boolean('status')->default(true);
            $table->integer('created_by');
            $table->dateTime('created_date');
            $table->integer('modified_by')->nullable();
            $table->dateTime('modified_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_pegawai');
    }
}
