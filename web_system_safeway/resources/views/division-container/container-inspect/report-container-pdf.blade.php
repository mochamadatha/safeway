<?php

use App\Model\MasterContainer\ContainerInspect;
use App\Service\DivisionContainer\AgentService;
use App\Service\DivisionContainer\CustomerService;
use App\Service\DivisionContainer\DepoLocationService;
use App\Service\DivisionContainer\InspectService;
use App\Service\DivisionContainer\SizeService;
?>
<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>
<br><br><br>
<table id="filtes" width="100%" cellpadding="0" cellspacing="0" style="font-size: 9px">
    <tr>
        <td width="50%" cellpadding="0" cellspacing="0" >
            <table>
                <tr>
                    <td width="48%" >{{ trans('fields.container_number') }}</td>
                    <td width="2%">:</td>
                    <td width="50%">{{ $model->container_number}}</td>
                </tr>
                <tr>
                    <td width="48%">{{ trans('fields.size') }}</td>
                    <td width="2%">:</td>
                     <td width="50%">{{ $model->size}}</td>
            
                </tr>
                <tr>
                    <td width="48%">{{ trans('fields.manufacture_year') }}</td>
                    <td width="2%">:</td>
                    <td width="50%">{{ $model->manufacture_year}}</td>
                </tr>
                 <tr>
                    <td width="48%">{{ trans('fields.previous_cargo') }}</td>
                    <td width="2%">:</td>
                    <td width="50%">{{ $model->previous_cargo}}</td>
                </tr>
                 <tr>
                    <td width="48%">{{ trans('fields.packingin') }}</td>
                    <td width="2%">:</td>
                    <td width="50%">{{ $model->packingin}}</td>
                </tr>
            </table>
            <br/>
        </td>
        <td width="50%" cellpadding="0" cellspacing="0">
           <table>
                <tr>
                    <td width="48%">{{ trans('fields.nama_agent') }}</td>
                    <td width="2%">:</td>
                    <td width="50%">{{ $model->nama_agent}}</td>
                </tr>
                <tr>
                    <td width="48%">{{ trans('fields.nama_depolocation') }}</td>
                    <td width="2%">:</td>
                     <td width="50%">{{ $model->nama_depolocation}}</td>
                </tr>
                <tr>
                    <td width="48%">{{ trans('fields.nama_customer') }}</td>
                    <td width="2%">:</td>
                    <td width="50%">{{ $model->nama_customer}}</td>
                </tr>
                 <tr>
                    <td width="48%">{{ trans('fields.safeway_sn') }}</td>
                    <td width="2%">:</td>
                    <td width="50%">{{ $model->safeway_sn}}</td>
                </tr>
                 <tr>
                    <td width="48%">{{ trans('fields.nama_inspect') }}</td>
                    <td width="2%">:</td>
                    <td width="50%">{{ $model->nama_inspect}}</td>
                </tr>
            </table>
            <br/>
        </td>
    </tr>
</table>
<h7>Remark</h7><br>
<table id="filtes" width="100%" height="5%" cellpadding="0" cellspacing="0" border="1">
    <tr>
        <td width="100%" cellpadding="0" cellspacing="0" height="" >
            <table>
                <tr>
                    <td width="100%"> </td>
                    <td width="100%"> </td>
                </tr>
            </table>
            <br/>
        </td>
    </tr>
</table><br> <br>

<table id="filtes" width="100%" cellpadding="0" cellspacing="0" style="font-size: 9px">
    <tr>
        <td width="50%" cellpadding="0" cellspacing="0">
            <table>
                <tr>
                    <td width="48%">{{ trans('fields.floor') }}</td>
                    <td width="2%">:</td>
                    <td width="50%" color="green">Clean</td>
                </tr>
                <tr>
                    <td width="48%">{{ trans('fields.leftside') }}</td>
                    <td width="2%">:</td>
                     <td width="50%" color="green">Clean</td>
            
                </tr>
                <tr>
                    <td width="48%">{{ trans('fields.rightside') }}</td>
                    <td width="2%">:</td>
                    <td width="50%" color="green">Clean</td>
                </tr>
                 <tr>
                    <td width="48%">{{ trans('fields.frontside') }}</td>
                    <td width="2%">:</td>
                    <td width="50%" color="green">Clean</td>
                </tr>
            </table>
            <br/>
        </td>
 

            
        <td width="50%" cellpadding="0" cellspacing="0" style="font-size: 9px">
           <table>
                <tr>
                    <td width="48%">{{ trans('fields.containerlock') }}</td>
                    <td width="2%">:</td>
                    <td width="50%" color="green">OK</td>
                </tr>
                <tr>
                    <td width="48%">{{ trans('fields.roof') }}</td>
                    <td width="2%">:</td>
                     <td width="50%" color="green">Clean</td>
                </tr>
                <tr>
                    <td width="48%">{{ trans('fields.cleanliness') }}</td>
                    <td width="2%">:</td>
                    <td width="50%" color="green" >Clean</td>
                </tr>
                 <tr>
                    <td width="48%">{{ trans('fields.odor') }}</td>
                    <td width="2%">:</td>
                    <td width="50%" color="green">Clean</td>
                </tr>
            </table>
          
        </td>
    </tr>
</table>

<table id="filtes" width="100%" height="5" cellpadding="0" cellspacing="0" style="font-size: 9px">
    <tr>
        <td width="100%" cellpadding="0" cellspacing="0" >
            <table>
                <tr>
                    <td width="24%">{{ trans('fields.door') }}</td>
                    <td width="1%">:</td>
                    <td width="75%" color="green">Door Panel OK,Door Frame OK,Door Header OK,Locking Bar OK,Hinge Component OK, Gasket OK,Rear Corner OK,Rain Gutter OK,Handle OK</td>
                </tr>
            </table>
            <br/>
        </td>
    </tr>
</table><br><br>

<table id="filtes" width="100%" cellpadding="0" cellspacing="0 " style="font-size: 10px; font-weight: bold;">
    <tr>
        <td width="33%" cellpadding="0" cellspacing="0">
            <table>
                <tr>
                    <td width="100%" align="center">{{ trans('fields.doorclosed') }}</td>
                </tr> 
                <tr>
                    <td width="100%" align="center"><img src="{{ asset(Config::get('app.paths.foto-container').'/'.$model->doorclosed) }}" height="130" width="130"></td> 
                </tr>
                <tr> 
                    <td width="100%" align="center"></td> 
                </tr>

                <tr>
                    <td width="100%" align="center">{{ trans('fields.roof') }}</td>
                </tr>
                <tr>
                    <td width="100%" align="center"><img src="{{ asset(Config::get('app.paths.foto-container').'/'.$model->roof) }}" height="130" width="130"></td> 
                </tr> 
                <tr> 
                    <td width="100%" align="center"></td> 
                </tr>

                <tr>
                    <td width="100%" align="center">{{ trans('fields.seal') }}</td>
                </tr>
                <tr>
                    <td width="100%" align="center"><img src="{{ asset(Config::get('app.paths.foto-container').'/'.$model->seal) }}" height="130" width="130"></td>
                </tr>
                
            </table>
            <br/>
        </td>

        <td width="33%" cellpadding="0" cellspacing="0">
            <table>
                <tr>
                    <td width="100%" align="center">{{ trans('fields.allinterior') }}</td>
                </tr>
                <tr>
                    <td width="100%" align="center"><img src="{{ asset(Config::get('app.paths.foto-container').'/'.$model->allinterior) }}" height="130" width="130"></td>
                </tr>
                <tr> 
                    <td width="100%" align="center"></td> 
                </tr>
                <tr>
                    <td width="100%" align="center">{{ trans('fields.leftwall') }}</td>
                </tr>
                <tr>
                    <td width="100%" align="center"><img src="{{ asset(Config::get('app.paths.foto-container').'/'.$model->leftwall) }}" height="130" width="130"></td>
                </tr>
                <tr> 
                    <td width="100%" align="center"></td> 
                </tr>
                <tr>
                    <td width="100%" align="center">{{ trans('fields.cscplate') }}</td>
                </tr>
                <tr>
                    <td width="100%" align="center"><img src="{{ asset(Config::get('app.paths.foto-container').'/'.$model->cscplate) }}" height="130" width="130"></td>
                </tr>
                
            </table>
            <br/>
        </td>

         <td width="33%" cellpadding="0" cellspacing="0">
            <table>
                <tr>
                    <td width="100%" align="center">{{ trans('fields.floor') }}</td>
                </tr>
                <tr>
                    <td width="100%" align="center"><img src="{{ asset(Config::get('app.paths.foto-container').'/'.$model->floor) }}" height="130" width="130"></td>
                </tr>
                <tr> 
                    <td width="100%" align="center"></td> 
                </tr>
                <tr>
                    <td width="100%" align="center">{{ trans('fields.rightwall') }}</td>
                </tr>
                <tr>
                    <td width="100%" align="center"><img src="{{ asset(Config::get('app.paths.foto-container').'/'.$model->rightwall) }}" height="130" width="130"></td>
                </tr>
                <tr> 
                    <td width="100%" align="center"></td> 
                </tr>
                <tr>
                    <td width="100%" align="center">{{ trans('fields.temperature') }}</td>
                </tr>
                <tr>
                    <td width="100%" align="center"><img src="{{ asset(Config::get('app.paths.foto-container').'/'.$model->temperature) }}" height="130" width="130"></td>
                </tr>
                
            </table>
            <br/>
        </td>
    </tr>
</table>

</body>
</html>


