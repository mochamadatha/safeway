<?php 
// use App\Service\Master\SupplierService; 
// use App\Model\MasterProduksi\Supplier;
// use App\Model\Produksi\HdLpb;


list($titleheader, $date) = explode("#", $title);
?>

<br><br>
<table id="header" width="100%" border="1px">
	<tr>
		<td  width="22%" height="60"> </td>
		<td  width="45%" height="60" align="center" style="font-weight: bold;"> PENGAMBILAN AFKIR </td>
		<td  width="33%" height="60"> </td>
	</tr>
	
	<tr>
		<td width="35%"> </td>
		<td width="45%"  ></td>
		<td width="20%"> {{ $date }}</td>

	</tr>
	<tr>
		<td width="100%"> </td>
	</tr>

	<tr>
		<td width="20%" height="25"> </td>
		<td width="47%" height="25"> {{ $model->supplier_name }} </td>
		<td width="33%" height="25"> {{ $model->phone }} </td>
	</tr>
	

	<tr>
		<td width="13%" height="32"> </td>
		<td width="87%" height="32"> {{ $model->supplier_alamat }}</td>
	
	</tr>
	
	<tr>
		<td width="40%" height="47"> </td>
		<td width="60%" height="47"> {{ $model->pic}} - {{ $model->plat_mobil }}</td>
	</tr>

	<?php
		
			$total_pcs = 0; 
			$total_volume = 0; 
			
	?>
	@foreach ($isi as $view)
	<tr>
		<td width="20%" height="17" align="center"> {{ $view->pcs}}</td>
		<td width="40%" height="17" align="center">  {{ $view->volume}}</td>
		<td width="40%" height="17" align="center"> {{ $view->jenis_kayu}}</td>
	</tr>
	@endforeach
		

</table>