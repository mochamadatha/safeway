<?php

namespace App\Model\Produksi;

use Illuminate\Database\Eloquent\Model;

class DtProsesStockMs extends Model
{
    protected $table        = 'dt_proses_stockms';
    protected $primaryKey   = 'proses_stockms_id';
    public $timestamps      = false;
}
