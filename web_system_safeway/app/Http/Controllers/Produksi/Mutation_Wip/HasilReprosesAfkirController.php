<?php

namespace App\Http\Controllers\Produksi\Mutation_Wip;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Produksi\Tally;
use App\Model\Produksi\IsiAfkirProses;
use App\Model\Produksi\HdLpb;
use App\Model\Produksi\LPBDetail;
use App\Service\Master\WoodService;
use App\Model\Produksi\DtAmbilStokAfkir;
use App\Service\Master\DesService;
use App\Service\Master\AnalisaAfkirService;
use App\Model\Produksi\HasilReprosesAfkir;
use App\Model\Produksi\JumlahMs;
use DB;

class HasilReprosesAfkirController extends Controller
{
    const URL       = 'produksi/mutation_wip/hasil-reprosesafkir';
    const RESOURCE  = 'hasil-reprosesafkir';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function add(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        $model = DtAmbilStokAfkir::find($id);

        if ($model === null) {
            abort('404');
        }

        $query = DB::table('dt_hasil_reproses_afkir')
            ->select('dt_hasil_reproses_afkir.*', 'mst_jenis_kayu.jenis_kayu')
            ->where('ambil_stockafkir_id', '=', $id)
            ->where('dt_hasil_reproses_afkir.status', '=', true)
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_hasil_reproses_afkir.jenis_kayu_id')
            ->orderBy('dt_hasil_reproses_afkir.created_by');

        //  dd($query);


        return view('produksi/mutation_wip/hasil-reprosesafkir.add', [
            "model"         => $model,
            "models"         => $query->paginate(10),
            "woodOptions"   => WoodService::getActiveWood(),

            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }


    public function save(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        $id     = intval($request->get('hasil_reproses_afkir_id', 0));

        $this->validate($request, [
            'ambil_stockafkir_id'            => 'required|integer',
            'jenis_kayu_id'       => 'required|integer',

            'tinggi'              => 'required|integer',
            'lebar'               => 'required|integer',
            'panjang'             => 'required|integer',
            'pcs'                 => 'required|integer',
        ]);

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            if (empty($id)) {
                $model  = new HasilReprosesAfkir();
                $model->created_by      = \Auth::user()->id;
                $model->created_date    = $now;
            } else {
                $model  = HasilReprosesAfkir::find($id);
                $model->modified_by   = \Auth::user()->id;
                $model->modified_date = $now;
            }

            $model->ambil_stockafkir_id   = $request->get('ambil_stockafkir_id');
            $model->jenis_kayu_id   = $request->get('jenis_kayu_id');
            $model->tinggi   = $request->get('tinggi');
            $model->lebar   = $request->get('lebar');
            $model->panjang   = $request->get('panjang');
            $model->pcs   = $request->get('pcs');

            $model->volume   = ($request->get('tinggi') * $request->get('panjang') * $request->get('lebar') * $request->get('pcs')) / 1000000000;

            $jml = DB::table('jumlah_ms')
                ->select('jumlah_ms.pcs', 'jumlah_ms.volume')
                ->where('jumlah_ms.status', '=', true)
                ->where('jumlah_ms.jenis_kayu_id', '=', $request->get('jenis_kayu_id'))
                ->where('jumlah_ms.tinggi', '=', $request->get('tinggi'))
                ->where('jumlah_ms.lebar', '=', $request->get('lebar'))
                ->where('jumlah_ms.panjang', '=', $request->get('panjang'))->first();

            // dd($jml->pcs);

            // if($jml===null){
            //     $up  = new JumlahMs();
            //     $up->jenis_kayu_id   = $request->get('jenis_kayu_id');
            //     $up->tinggi   = $request->get('tinggi');
            //     $up->lebar   = $request->get('lebar');
            //     $up->panjang   = $request->get('panjang');
            //     $up->pcs   = $request->get('pcs');
            //     $up->volume   = ($request->get('tinggi')*$request->get('panjang')*$request->get('lebar')*$request->get('pcs'))/1000000000;
            //     $up->created_by      = \Auth::user()->id;
            //     $up->created_date    = $now;
            //     $up->save();
            // }else{
            //     $pcs = $jml->pcs + $request->get('pcs');
            //     $volume = $jml->volume + (($request->get('tinggi')*$request->get('panjang')*$request->get('lebar')*$request->get('pcs'))/1000000000);
            //     $modified_by   = \Auth::user()->id;
            //     $modified_date = $now;
            //     $up = DB::table('jumlah_ms')
            //         ->where('jumlah_ms.status', '=', true)
            //         ->where('jumlah_ms.jenis_kayu_id', '=', $request->get('jenis_kayu_id'))
            //         ->where('jumlah_ms.tinggi', '=', $request->get('tinggi'))
            //         ->where('jumlah_ms.lebar', '=', $request->get('lebar'))
            //         ->where('jumlah_ms.panjang', '=', $request->get('panjang'))
            //         ->update(['pcs' => $pcs, 'volume' => $volume, 'modified_by' => $modified_by, 'modified_date' => $now]);
            // }

            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('fields.add-tally-sheet') . ' ' . ''])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('fields.add-tally-sheet') . ' ' . ''])
            );
        }

        return redirect(route('hasil-reprosesafkir-add', ['id' => $request->get('ambil_stockafkir_id')]));
    }
}
