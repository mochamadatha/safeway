<?php

namespace App\Model\Produksi;

use Illuminate\Database\Eloquent\Model;

class HdMutasi extends Model
{
    protected $table        = 'hd_mutasi';
    protected $primaryKey   = 'hd_mutasi_id';
    public $timestamps      = false;
}
