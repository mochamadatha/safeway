<?php

namespace App\Service\Master;

class BranchService
{
    public static function getActiveBranch()
    {
        return \DB::table('mst_cabang')
            ->where('status', '=', true)
            ->orderBy('nama_cabang')
            ->get();
    }
}
