<?php

namespace App\Model\MasterContainer;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    protected $table        = 'mst_agent';
    protected $primaryKey   = 'agent_id';
    public $timestamps      = false;
}
