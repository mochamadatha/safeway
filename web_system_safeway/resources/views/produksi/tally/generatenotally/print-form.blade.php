@extends('layouts.master')

@section('title', trans('menu.print-notally'))

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">{{ trans('menu.print-notally') }}</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="{{ route('dashboard') }}">{{ trans('menu.dashboard') }}</a>
                            </li>
                            <li>
                                <a>{{ trans('menu.produksi') }}</a>
                            </li>
                            <li class="active">
                                {{ trans('menu.print-notally') }}
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            @if(Session::has('successMessage'))
            <div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-check-all"></i>
                <strong>{{ Session::get('successMessage') }}.</strong>
            </div>
            @endif

            @if(Session::has('errorMessage'))
            <div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-block-helper"></i>
                <strong>{{ Session::get('errorMessage') }}.</strong>
            </div>
            @endif

            
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-border panel-success">
                        <div class="panel-heading">
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <form class="form-horizontal" role="form" method="post" action="{{ route('generate-notally-print') }}">
                                    {{ csrf_field() }}

                                    
                                    <div class="col-md-6">
                                        <!-- membuat no tally awal --> 
                                        <div class="form-group {{ $errors->has('first_tally') ? 'has-error' : '' }}">
                                            <label class="col-md-4 control-label">{{ trans('fields.startnotally') }}</label>
                                            <div class="col-md-8">
                                                <input type="text" name="first_tally" class="form-control" value="{{ !empty($filters['first_tally']) ? $filters['first_tally'] : '' }}">
                                                @if($errors->has('first_tally'))
                                                <span class="help-block">{{ $errors->first('first_tally') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <!-- membuat no tally akhir --> 
                                        <div class="form-group {{ $errors->has('end_tally') ? 'has-error' : '' }}">
                                            <label class="col-md-4 control-label">{{ trans('fields.endnotally') }}</label>
                                            <div class="col-md-8">
                                                <input type="text" name="end_tally" class="form-control" value="{{ !empty($filters['end_tally']) ? $filters['end_tally'] : '' }}">
                                                @if($errors->has('end_tally'))
                                                <span class="help-block">{{ $errors->first('end_tally') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <p class="text-muted font-14 m-b-20 pull-right">

                                                <button type="submit" class="btn btn-primary waves-effect w-md waves-light"><i class="fa fa-print"></i> {{ trans('fields.printnotally') }}</button>
                                            </p>

                                        </div>


                                    </div>
                                </form>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection