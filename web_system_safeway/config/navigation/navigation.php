
<?php

return
    [
        [
            'label'     => 'master-role',
            'icon'      => 'database',
            'children'  =>
            [
                [
                    'label'         => 'setup-role',
                    'route'         => 'setup-role-index',
                    'resource'      => 'setup-role',
                    'privilege'     => 'view',
                ],
            ],
        ],
        [
            'label'     => 'master-user',
            'icon'      => 'users',
            'children'  =>
            [
                [
                    'label'         => 'setup-status',
                    'route'         => 'setup-status-index',
                    'resource'      => 'setup-status',
                    'privilege'     => 'view',
                ],
                [
                    'label'         => 'setup-user',
                    'route'         => 'setup-user-index',
                    'resource'      => 'setup-user',
                    'privilege'     => 'view',
                ],
            ],
        ],

        // [
        // 'label'     => 'master-hrd',
        // 'icon'      => 'black-tie',
        // 'children'  => 
        //     [
        //         [
        //             'label'         => 'setup-division',
        //             'route'         => 'setup-division-index',
        //             'resource'      => 'setup-division',
        //             'privilege'     => 'view',
        //         ],
        //         [
        //             'label'         => 'setup-branch',
        //             'route'         => 'setup-branch-index',
        //             'resource'      => 'setup-branch',
        //             'privilege'     => 'view',
        //         ],
        //         [
        //             'label'         => 'setup-employee',
        //             'route'         => 'setup-employee-index',
        //             'resource'      => 'setup-employee',
        //             'privilege'     => 'view',
        //         ],  
        //     ],  
        // ],


        //  [
        // 'label'     => 'master-ispm',
        // 'icon'      => 'server',
        // 'children'  => 
        //     [
        //         [
        //             'label'         => 'setup-size',
        //             'route'         => 'setup-sizeispm-index',
        //             'resource'      => 'setup-sizeispm',
        //             'privilege'     => 'view',
        //         ], 

        //     ],
        // ],



        // [
        // 'label'     => 'master-container',
        // 'icon'      => 'server',
        // 'children'  => 
        //     [
        //         [
        //             'label'         => 'setup-customer',
        //             'route'         => 'setup-customer-index',
        //             'resource'      => 'setup-customer',
        //             'privilege'     => 'view',
        //         ],
        //         [
        //             'label'         => 'setup-agent',
        //             'route'         => 'setup-agent-index',
        //             'resource'      => 'setup-agent',
        //             'privilege'     => 'view',
        //         ],
        //         [
        //             'label'         => 'setup-depolocation',
        //             'route'         => 'setup-depolocation-index',
        //             'resource'      => 'setup-depolocation',
        //             'privilege'     => 'view',
        //         ],
        //         [
        //             'label'         => 'setup-inspect',
        //             'route'         => 'setup-inspect-index',
        //             'resource'      => 'setup-inspect',
        //             'privilege'     => 'view',
        //         ],
        //         [
        //             'label'         => 'setup-size',
        //             'route'         => 'setup-size-index',
        //             'resource'      => 'setup-size',
        //             'privilege'     => 'view',
        //         ],


        //     ],
        // ],


        // [
        // 'label'     => 'division-container',
        // 'icon'      => 'desktop',
        // 'children'  => 
        //     [
        //         [
        //             'label'         => 'container-inspect',
        //             'route'         => 'container-inspect-index',
        //             'resource'      => 'container-inspect',
        //             'privilege'     => 'view',
        //         ],   

        //     ],
        // ],


        // [
        // 'label'     => 'division-ispm',
        // 'icon'      => 'desktop',
        // 'children'  => 
        //     [
        //         [
        //             'label'         => 'ispm-inspect',
        //             'route'         => 'ispm-inspect-index',
        //             'resource'      => 'ispm-inspect',
        //             'privilege'     => 'view',
        //         ],   

        //     ],
        // ],



        [
            'label'     => 'master-produksi',
            'icon'      => 'users',
            'children'  =>
            [
                [
                    'label'         => 'setup-woodtype',
                    'route'         => 'setup-woodtype-index',
                    'resource'      => 'setup-woodtype',
                    'privilege'     => 'view',
                ],

                [
                    'label'         =>  'setup-description',
                    'route'         =>  'setup-description-index',
                    'resource'      =>  'setup-description',
                    'privilege'     =>  'view',
                ],

                [
                    'label'         => 'setup-supplier',
                    'route'         => 'setup-supplier-index',
                    'resource'      => 'setup-supplier',
                    'privilege'     => 'view',
                ],

                [
                    'label'         => 'setup-price',
                    'route'         => 'setup-price-index',
                    'resource'      => 'setup-price',
                    'privilege'     => 'view',
                ],

                [
                    'label'         => 'setup-mutation-location',
                    'route'         => 'setup-mutation-location-index',
                    'resource'      => 'setup-mutation-location',
                    'privilege'     => 'view',
                ],
                [
                    'label'         => 'setup-arrival-location',
                    'route'         => 'setup-arrival-location-index',
                    'resource'      => 'setup-arrival-location',
                    'privilege'     => 'view',
                ],

                [
                    'label'         => 'setup_chamber',
                    'route'         => 'setup-chamber-index',
                    'resource'      => 'setup-chamber',
                    'privilege'     => 'view',
                ],

                [
                    'label'         => 'setup_location_outputkd',
                    'route'         => 'setup-location-outputkd-index',
                    'resource'      => 'setup-location-outputkd',
                    'privilege'     => 'view',
                ],


                [
                    'label'         => 'analisa-afkir',
                    'route'         => 'analisa-afkir-index',
                    'resource'      => 'analisa-afkir',
                    'privilege'     => 'view',
                ],

                [
                    'label'         => 'setup-pallet-name',
                    'route'         => 'setup-pallet-name-index',
                    'resource'      => 'setup-pallet-name',
                    'privilege'     => 'view',
                ],



            ],
        ],

        [
            'label'     => 'produksi',
            'icon'      => 'black-tie',
            'children'  =>
            [
       

                [
                    'label'         => 'search-lpb',
                    'route'         => 'search-lpb-index',
                    'resource'      => 'search-lpb',
                    'privilege'     => 'view',
                ],

            
                [
                    'label'         => 'generate-notally',
                    'route'         => 'generate-notally-index',
                    'resource'      => 'generate-notally',
                    'privilege'     => 'view',
                ],

                [
                    'label'         => 'setup-lpb',
                    'route'         => 'setup-lpb-index',
                    'resource'      => 'setup-lpb',
                    'privilege'     => 'view',
                ],

                [
                    'label'         => 'raw-material',
                    'route'         => 'raw-material-index',
                    'resource'      => 'raw-material',
                    'privilege'     => 'view',
                ],
                [
                    'label'         => 'input-kd',
                    'route'         => 'input-kd-index',
                    'resource'      => 'input-kd',
                    'privilege'     => 'view',
                ],
                [
                    'label'         => 'output-kd',
                    'route'         => 'output-kd-index',
                    'resource'      => 'output-kd',
                    'privilege'     => 'view',
                ],

                [
                    'label'         => 'mutation',
                    'route'         => 'mutation-index',
                    'resource'      => 'mutation',
                    'privilege'     => 'view',
                ],

                [
                    'label'         => 'wip',
                    'route'         => 'wip-index',
                    'resource'      => 'wip',
                    'privilege'     => 'view',
                ],

                [
                    'label'         => 'stock-ms',
                    'route'         => 'ms-stockms',
                    'resource'      => 'stock-ms',
                    'privilege'     => 'view',
                ],

                [
                    'label'         => 'raft-result',
                    'route'         => 'raft-result-index',
                    'resource'      => 'raft-result',
                    'privilege'     => 'view',
                ],

                // [
                //     'label'         => 'raft-result',
                //     'route'         => 'raft-result-index',
                //     'resource'      => 'raft-result',
                //     'privilege'     => 'view',
                // ],







            ],
        ],


        [
            'label'     => 'report_produksi',
            'icon'      => 'black-tie',
            'children'  =>
            [
                [
                    'label'         => 'search-all-data',
                    'route'         => 'search-all-data-index',
                    'resource'      => 'search-all-data',
                    'privilege'     => 'view',
                ],

                // [
                //     'label'         => 'kiln-dry',
                //     'route'         => 'kiln-dry-index',
                //     'resource'      => 'kiln-dry',
                //     'privilege'     => 'view',
                // ],



                //   [
                //     'label'         => 'search-bandsaw-data',
                //     'route'         => 'search-bandsaw-data-index',
                //     'resource'      => 'search-bandsaw-data',
                //     'privilege'     => 'view',
                // ],

                [
                    'label'         => 'search-data-bandsaw',
                    'route'         => 'search-data-bandsaw-index',
                    'resource'      => 'search-data-bandsaw',
                    'privilege'     => 'view',
                ],

                [
                    'label'         => 'search-data-afkir',
                    'route'         => 'search-data-afkir-index',
                    'resource'      => 'search-data-afkir',
                    'privilege'     => 'view',
                ],





            ],
        ],

        [
            'label'     => 'spk',
            'icon'      => 'black-tie',
            'children'  =>
            [
                [
                    'label'         => 'spk-bandsaw',
                    'route'         => 'spk-bandsaw-index',
                    'resource'      => 'spk-bandsaw-data',
                    'privilege'     => 'view',
                ],

            ],
        ],




    ];
