@extends('layouts.master')

@section('title', trans('menu.setup-price'))

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
        	<div class="row">
				<div class="col-xs-12">
					<div class="page-title-box">
                        <h4 class="page-title">{{ trans('menu.setup-price') }}</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="{{ route('dashboard') }}">{{ trans('menu.dashboard') }}</a>
                            </li>
                            <li>
                                <a>{{ trans('menu.master-produksi') }}</a>
                            </li>
                            <li class="active">
                                {{ trans('menu.setup-price') }}
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
				</div>
			</div>
            <!-- end row -->
            
			<div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-border panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ trans('fields.add-price') }}</h3>
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal" role="form" method="post" action="{{ route('setup-price-save') }}">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="p-20">
                                            <input type="hidden" name="id" class="form-control" value="{{ count($errors) > 0 ? old('id') : $model->harga_id}}">

                                            <div class="form-group {{ $errors->has('supplier_name') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.supplier_name') }}</label>
                                                <div class="col-md-5">
                                                    <select class=" form-control" id="supplier_id" name="supplier_id">
                                                         <?php $supId = count($errors) > 0 ? old('supplier_name') : $model->supplier_id; ?>

                                                        <option value="">- {{ trans('fields.change') }} {{ trans('fields.supplier_name') }} -</option>
                                                        @foreach($supplierOptions as $arr)
                                                          <option value="{{ $arr->supplier_id }}" = {{ $arr->supplier_id == $supId ? 'selected' : ''}} >{{ $arr->supplier_name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>


                                            <div class="form-group {{ $errors->has('jenis_kayu') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.wood-type') }}</label>
                                                <div class="col-md-5">
                                                    <select class="form-control" id="jenis_kayu_id" name="jenis_kayu_id">
                                                        <?php $woodId = count($errors) > 0 ? old('jenis_kayu') : $model->jenis_kayu_id; ?>
                                                        <option value="">- {{ trans('fields.change') }} {{ trans('fields.wood-type') }} -</option>
                                                            @foreach($woodOptions as $wood)
                                                       <option value="{{ $wood->jenis_kayu_id }}" = {{ $wood->jenis_kayu_id == $woodId ? 'selected' : ''}} >{{ $wood->jenis_kayu }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group {{ $errors->has('nama_deskripsi') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.description') }}</label> 
                                                <div class="col-md-5">
                                                    <select class="form-control" id="deskripsi_id" name="deskripsi_id">

                                                         <?php $desId = count($errors) > 0 ? old('nama_deskripsi') : $model->deskripsi_id; ?>
                                                        <option value="">- {{ trans('fields.change') }} {{ trans('fields.description') }} -</option>
                                                            @foreach($desOptions as $des)
                                                        <option value="{{ $des->deskripsi_id }}" = {{ $des->deskripsi_id == $desId ? 'selected' : ''}} >{{ $des->nama_deskripsi }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group {{ $errors->has('harga') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.price') }}</label>
                                                <div class="col-md-5">
                                                    <input type="text" name="harga" class="form-control" value="{{ count($errors) > 0 ? old('harga') : $model->harga }}">
                                                    @if($errors->has('harga'))
                                                        <span class="help-block">{{ $errors->first('harga') }}</span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <div class="checkbox checkbox-primary">
                                                        <?php $status = count($errors) > 0 ? old('status') : $model->status; ?>
                                                        <input id="status" name="status" value="true" type="checkbox" {{ $status == true ? 'checked' : '' }}>
                                                        <label for="status">
                                                            {{ trans('fields.is-active') }}
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="col-sm-12 data-table-toolbar text-right" style="padding-top: 10px;">
                                        <div class="form-group">
                                            <a href="{{ url('master-produksi/setup-price') }}" class="btn btn-sm btn-warning"><i class="fa fa-reply"></i> {{ trans('fields.cancel') }}</a>
                                            <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i> {{ trans('fields.save') }}</button>
                                        </div>
                                    </div> 
                                </div>
                            </form>
                        </div>
                    </div>
                        <!-- end row -->
                </div> <!-- end card-box -->
            </div><!-- end col -->
        </div>
    </div>
</div>
@endsection