<?php

namespace App\Http\Controllers\MasterProduksi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\MasterProduksi\AnalisaAfkir;
use DB;

class SetupAnalisaAfkirController extends Controller
{
    const URL       = 'master-produksi/analisa-afkir';
    const RESOURCE  = 'analisa-afkir';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        if ($request->isMethod('post')) {
            $request->session()->put('filters', $request->all());
            return redirect(self::URL . '?page=1');
        } elseif (empty($request->get('page'))) {
            $request->session()->forget('filters');
        }
        $filters = $request->session()->get('filters');
        $query = \DB::table('mst_analisa_afkir')
            ->orderBy('mst_analisa_afkir.analisa_afkir');

        if (!empty($filters['status']) || !$request->session()->has('filters')) {
            $query->where('mst_analisa_afkir.status', '=', true);
        } else {
            $query->where('mst_analisa_afkir.status', '=', false);
        }

        if (!empty($filters['analisa_afkir'])) {
            $query->where('mst_analisa_afkir.analisa_afkir', 'like', '%' . $filters['analisa_afkir'] . '%');
        }

        return view('master-produksi/analisa-afkir.index', [
            "models"        => $query->paginate(10),
            "filters"       => $filters,
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function add(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        $model           = new AnalisaAfkir();
        $model->status   = true;

        return view('master-produksi/analisa-afkir.add', [
            "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }

    public function edit(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $model = AnalisaAfkir::find($id);

        if ($model === null) {
            abort('404');
        }

        return view('master-produksi/analisa-afkir.add', [
            "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }



    public function save(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add']) && !\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $id     = intval($request->get('id', 0));

        $this->validate($request, [
            'analisa_afkir' => 'required|max:255|unique:mst_analisa_afkir,analisa_afkir,' . $id . ',analisa_afkir_id',
        ]);

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            if (empty($id)) {
                $model  = new AnalisaAfkir();
                $model->created_by      = \Auth::user()->id;
                $model->created_date    = $now;
            } else {
                $model  = AnalisaAfkir::find($id);
                $model->modified_by   = \Auth::user()->id;
                $model->modified_date = $now;
            }

            $model->analisa_afkir   = $request->get('analisa_afkir');
            $model->status   = !empty($request->get('status')) ? true : false;

            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('menu.analisa-afkir') . ' ' . $request->get('analisa_afkir')])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('menu.analisa-afkir') . ' ' . $request->get('analisa_afkir')])
            );
        }

        return redirect(route('analisa-afkir-index'));
    }
}
