<?php

namespace App\Http\Controllers\MasterProduksi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\MasterProduksi\Price;
use App\Model\MasterProduksi\DeskripsiTallySheet;
use App\Model\MasterProduksi\JenisTally;
use App\Model\MasterProduksi\Supplier;
use App\Service\Master\WoodService;
use App\Service\Master\DesService;
use App\Service\Master\SupplierService;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class SetupPriceController extends Controller
{
    const URL       = 'master-produksi/setup-price';
    const RESOURCE  = 'setup-price';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        if ($request->isMethod('post')) {
            $request->session()->put('filters', $request->all());
            return redirect(self::URL . '?page=1');
        } elseif (empty($request->get('page'))) {
            $request->session()->forget('filters');
        }
        $filters = $request->session()->get('filters');
        $query = \DB::table('mst_harga')
            // ->orderBy('mst_harga.harga');
            ->select('mst_harga.*', 'mst_jenis_kayu.jenis_kayu', 'mst_deskripsi_tallysheet.nama_deskripsi', 'mst_supplier.supplier_name')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'mst_harga.jenis_kayu_id')
            ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'mst_harga.deskripsi_id')
            ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'mst_harga.supplier_id')
            ->orderBy('mst_harga.created_by');

        if (!empty($filters['status']) || !$request->session()->has('filters')) {
            $query->where('mst_harga.status', '=', true);
        } else {
            $query->where('mst_harga.status', '=', false);
        }

        $query = \DB::table('mst_harga')
            // ->orderBy('mst_harga.harga');
            ->select('mst_harga.*', 'mst_jenis_kayu.jenis_kayu', 'mst_deskripsi_tallysheet.nama_deskripsi', 'mst_supplier.supplier_name')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'mst_harga.jenis_kayu_id')
            ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'mst_harga.deskripsi_id')
            ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'mst_harga.supplier_id')
            ->orderBy('mst_harga.created_by');

        if (!empty($filters['supplier_name'])) {
            $query->where('mst_supplier.supplier_name', 'like', '%' . $filters['supplier_name'] . '%');
        }

        return view('master-produksi/price.index', [
            "models"        => $query->paginate(10),
            "filters"       => $filters,
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function add(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }


        $model           = new Price();
        $model->status   = true;

        if ($model === null) {
            abort('404');
        }

        $query = DB::table('mst_harga')
            ->select('mst_harga.*', 'mst_jenis_kayu.jenis_kayu', 'mst_deskripsi_tallysheet.nama_deskripsi', 'mst_supplier.supplier_name')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'mst_harga.jenis_kayu_id')
            ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'mst_harga.deskripsi_id')
            ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'mst_harga.supplier_id')
            ->orderBy('mst_harga.created_by');

        return view('master-produksi/price.add', [
            "model"         => $model,
            "models"         => $query->paginate(10),
            "woodOptions"   => WoodService::getActiveWood(),
            "desOptions"   => DesService::getActiveDes(),
            "supplierOptions" => SupplierService::getActiveSupplier(),
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function edit(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $model = Price::find($id);

        if ($model === null) {
            abort('404');
        }

        $filters = $request->session()->get('filters');
        $query = \DB::table('mst_harga')
            // ->orderBy('mst_harga.harga');
            ->select('mst_harga.*', 'mst_jenis_kayu.jenis_kayu', 'mst_deskripsi_tallysheet.nama_deskripsi', 'mst_supplier.supplier_name')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'mst_harga.jenis_kayu_id')
            ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'mst_harga.deskripsi_id')
            ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'mst_harga.supplier_id')
            ->orderBy('mst_harga.created_by');

        return view('master-produksi/price.add', [
            "model"         => $model,
            "models"         => $query->paginate(10),
            "woodOptions"   => WoodService::getActiveWood(),
            "desOptions"   => DesService::getActiveDes(),
            "supplierOptions" => SupplierService::getActiveSupplier(),
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function importexcel(Request $request)
    {

        //if ($request->Request('file')) {
        $model  = new Price();
        $now    = new \DateTime();



        //$model->modified_date = $now;
        $path = $request->file('file')->getRealPath();
        $data = EXCEL::load($path, function ($reader) { })->get();

        if (!empty($data) && $data->count()) {
            foreach ($data as $key => $value) {
                $modal = new Price();
                $modal->supplier_id = $value->supplier_id;
                $modal->jenis_kayu_id = $value->jenis_kayu_id;
                $modal->deskripsi_id = $value->deskripsi_id;
                $modal->harga = $value->harga;
                $modal->created_by = \Auth::user()->id;
                $modal->created_date    = $now;
                $modal->save();
            }
        }

        return back();
    }

    public function save(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        $id     = intval($request->get('id', 0));

        $this->validate($request, [
            'supplier_id'            => 'required|integer',
            'jenis_kayu_id'            => 'required|integer',
            'deskripsi_id'            => 'required|integer',
            'harga'            => 'required|integer',

        ]);

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            if (empty($id)) {
                $model  = new Price();
                $model->created_by      = \Auth::user()->id;
                $model->created_date    = $now;
            } else {
                $model  = Price::find($id);
                $model->modified_by   = \Auth::user()->id;
                $model->modified_date = $now;
            }

            $model->supplier_id   = $request->get('supplier_id');
            $model->jenis_kayu_id   = $request->get('jenis_kayu_id');

            $model->deskripsi_id   = $request->get('deskripsi_id');
            $model->harga   = $request->get('harga');


            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('fields.add-price') . ' ' . ''])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('fields.add-price') . ' ' . ''])
            );
        }

        return redirect(route('setup-price-index', ['id' => $request->get('price_id')]));
    }
}
