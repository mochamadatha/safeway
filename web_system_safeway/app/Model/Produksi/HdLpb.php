<?php

namespace App\Model\Produksi;

use Illuminate\Database\Eloquent\Model;

class HdLpb extends Model
{
    protected $table        = 'hd_lpb';
    protected $primaryKey   = 'hd_lpb_id';
    public $timestamps      = false;
}
