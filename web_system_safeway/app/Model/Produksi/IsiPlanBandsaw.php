<?php

namespace App\Model\Produksi;

use Illuminate\Database\Eloquent\Model;

class IsiPlanBandsaw extends Model
{
    protected $table        = 'dt_isi_planbandsaw';
    protected $primaryKey   = 'isi_planbansaw_id';
    public $timestamps      = false;
}
