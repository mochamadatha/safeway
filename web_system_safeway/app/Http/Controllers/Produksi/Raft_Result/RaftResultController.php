<?php

namespace App\Http\Controllers\Produksi\Raft_Result;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Produksi\HdPalletJadi;
use App\Model\MasterProduksi\PalletName;
use App\Model\Produksi\PalletJadiDetail;
use App\Model\MasterProduksi\LokasiKedatangan;
use App\Service\Master\WoodService;
use App\Service\Master\ArrivalLocationService;
use App\Service\Master\PalletService;
use Maatwebsite\Excel\Facades\Excel;
use Elibyy\TCPDF\Facades\TCPDF;
use DB;

class RaftResultController extends Controller
{
    const URL       = 'produksi/raft-result';
    const RESOURCE  = 'raft-result';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        if ($request->isMethod('post')) {
            $request->session()->put('filters', $request->all());
            return redirect(self::URL . '?page=1');
        } elseif (empty($request->get('page'))) {
            $request->session()->forget('filters');
        }
        $filters = $request->session()->get('filters');
        $query = \DB::table('hd_pallet_jadi')
            ->select('hd_pallet_jadi.*', 'mst_pallet_name.pallet_name', 'mst_pallet_name.kode_pallet', 'mst_pallet_name.tinggi', 'mst_pallet_name.lebar', 'mst_pallet_name.panjang', 'mst_lokasi_kedatangan.lokasi_kedatangan') //, 'tally_table.tally_no'
            ->join('mst_pallet_name', 'mst_pallet_name.pallet_name_id', '=', 'hd_pallet_jadi.pallet_name_id')
            ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=', 'hd_pallet_jadi.lokasi_kedatangan_id')
            //   ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_lpb.supplier_id')
            ->orderBy('hd_pallet_jadi.created_date', 'DESC');

        // dd($query);



        if (!empty($filters['status']) || !$request->session()->has('filters')) {
            $query->where('hd_pallet_jadi.status', '=', true);
        } else {
            $query->where('hd_pallet_jadis.status', '=', false);
        }
        $filters = $request->session()->get('filters');
        $query = \DB::table('hd_pallet_jadi')
            ->select('hd_pallet_jadi.*', 'mst_pallet_name.pallet_name', 'mst_pallet_name.kode_pallet', 'mst_pallet_name.tinggi', 'mst_pallet_name.lebar', 'mst_pallet_name.panjang', 'mst_lokasi_kedatangan.lokasi_kedatangan') //, 'tally_table.tally_no'
            ->join('mst_pallet_name', 'mst_pallet_name.pallet_name_id', '=', 'hd_pallet_jadi.pallet_name_id')
            ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=', 'hd_pallet_jadi.lokasi_kedatangan_id')
            //   ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_lpb.supplier_id')
            ->orderBy('hd_pallet_jadi.created_date', 'DESC');


        if (!empty($filters['kode_pallet'])) {
            $query->where('hd_pallet_jadi.kode_pallet', 'like', '%' . $filters['kode_pallet'] . '%');
        }

        return view('produksi/raft-result.index', [
            "models"        => $query->paginate(10),
            "filters"       => $filters,
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function add(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        $model           = new HdPalletJadi();
        $model->status   = true;

        //dd($model);

        if ($model === null) {
            abort('404');
        }

        // $query = DB::table('hd_lpb')
        // ->select('hd_lpb.*', 'tally_table.tally_no')
        //         ->join('tally_table', 'tally_table.tally_id', '=', 'hd_lpb.tally_id')
        //      //   ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_lpb.supplier_id')
        // ->orderBy('hd_lpb.created_by');

        return view('produksi/raft-result.add', [
            "model"         => $model,

            "arrivallocationOptions"    => ArrivalLocationService::getActiveArrivalLocation(),
            "palletOptions"    => PalletService::getActivePallet(),
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }





    public function addmaterial(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        // dd($id);



        // $model = LPB::find($id);
        $model = DB::table('hd_pallet_jadi')
            ->select('hd_pallet_jadi.*')
            ->where('hd_pallet_jadi.hd_pallet_jadi_id', '=', $id)->first();

        // ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_lpb.supplier_id')->first();

        // dd($model);
        if ($model === null) {
            abort('404');
        }

        $query = DB::table('dt_pallet_jadi')
            ->select('dt_pallet_jadi.*', 'mst_jenis_kayu.jenis_kayu')
            ->where('hd_pallet_jadi_id', '=', $id)
            //->where('dt_lpb.status', '=', true)
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_pallet_jadi.jenis_kayu_id')
            ->orderBy('dt_pallet_jadi.created_by');

        // dd($query);

        $query3 = DB::table('jumlah_ms')
            ->select('jumlah_ms.jumlah_ms_id', 'mst_jenis_kayu.jenis_kayu', 'jumlah_ms.tinggi', 'jumlah_ms.lebar', 'jumlah_ms.panjang', 'jumlah_ms.pcs', 'jumlah_ms.volume')
            ->where('jumlah_ms.pcs', '!=', 0)
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'jumlah_ms.jenis_kayu_id')
            ->orderBy('jumlah_ms.jumlah_ms_id');

        // dd($query);

        return view('produksi/raft-result.addmaterial', [
            "model"         => $model,
            "hd_pallet_id"         => $id,
            "models"         => $query->paginate(10),
            "modelss"          => $query3->paginate(400),

            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function savematerialms(Request $request)
    {
        // dd("hallo : ".$request->get('jumlah_ms_id')." blabla : ".$request->get('pcs'));
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        // $id     = intval($request->get('id_jumlah_afkir', 0));

        $this->validate($request, [
            'jumlah_ms_id'    => 'required|integer',
            'hd_pallet_jadi_id'       => 'required|integer',
            // 'tinggi'              => 'required|integer',
            // 'lebar'               => 'required|integer',
            // 'panjang'             => 'required|integer',
            'pcs'                 => 'required|integer',
        ]);

        // dd($request->get('jumlah_ms_id')." ".$request->get('hd_pallet_jadi_id')." ".$request->get('pcs'));


        DB::beginTransaction();
        try {
            $query = DB::table('jumlah_ms')
                ->select('jumlah_ms_id', 'jenis_kayu_id', 'tinggi', 'lebar', 'panjang', 'pcs', 'volume')
                ->where('jumlah_ms_id', "=", $request->get('jumlah_ms_id'))->first();

            // $query = DB::table('dt_pallet_jadi')
            //     ->select('dt_pallet_jadi_id', 'jenis_kayu_id', 'tinggi', 'lebar', 'panjang', 'pcs', 'volume')
            //     ->where('dt_pallet_jadi_id', "=", $request->get('dt_pallet_jadi_id'))
            //     ->orderBy('created_by')->first();

            // dd($query);

            $now    = new \DateTime();
            $model  = new PalletJadiDetail();
            $model->created_by      = \Auth::user()->id;
            $model->created_date    = $now;
            // }else{
            //     $model  =  DtAmbilStockAfkir::find($id);
            //     $model->modified_by   = \Auth::user()->id;
            //     $model->modified_date = $now;
            // }

            $model->hd_pallet_jadi_id   = $request->get('hd_pallet_jadi_id');
            $model->jenis_kayu_id   = $query->jenis_kayu_id;
            $model->tinggi   = $query->tinggi;
            $model->lebar   = $query->lebar;
            $model->panjang   = $query->panjang;
            $model->pcs   = $request->get('pcs');

            $model->volume   = ($query->tinggi * $query->panjang * $query->lebar * $request->get('pcs')) / 1000000000;

            $newpcs = $query->pcs - $model->pcs;
            $newvolume = $query->volume - $model->volume;

            $query = DB::table('jumlah_ms')
                ->where('jumlah_ms_id', "=", $request->get('jumlah_ms_id'))
                ->update(['pcs' => $newpcs, 'volume' => $newvolume, 'modified_by' => \Auth::user()->id, 'modified_date' => $now]);


            // dd($model);

            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('fields.add-mutation') . ' ' . ''])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('fields.add-mutation') . ' ' . ''])
            );
        }

        return redirect(route('raft-result-addmaterial', ['id' => $request->get('hd_pallet_jadi_id')]));
    }


    public function save(Request $request)
    {

        if (!\Gate::allows('access', [self::RESOURCE, 'add']) && !\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $id     = intval($request->get('id', 0));

        $this->validate($request, [


            // 'nama_status' => 'required|max:255|unique:mst_user_status,nama_status,'.$id.',status_id',
            'tanggal_pembuatan' => 'required|date',
            'lokasi_kedatangan_id'            => 'required|integer',
            'pallet_name_id'            => 'required|integer',
            'jumlah_pallet' => 'required|integer',


        ]);



        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            if (empty($id)) {
                $model  = new  HdPalletJadi();
                $model->created_by      = \Auth::user()->id;
                $model->created_date    = $now;
            } else {
                $model  =  HdPalletJadi::find($id);
                $model->modified_by   = \Auth::user()->id;
                $model->modified_date = $now;
            }


            $model->tanggal_pembuatan   = $request->get('tanggal_pembuatan');
            $model->lokasi_kedatangan_id   = $request->get('lokasi_kedatangan_id');
            $model->pallet_name_id   = $request->get('pallet_name_id');
            $model->jumlah_pallet   = $request->get('jumlah_pallet');
            $model->status   = !empty($request->get('status')) ? true : false;


            //dd($model);
            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('fields.add-raft-result') . ' ' . ''])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('fields.add-raft-result') . ' ' . ''])
            );
        }

        return redirect(route('raft-result-index'));
    }

    protected function complete_zero($number, $max_length)
    {
        $number_length = strlen($number);
        $zero_length = $max_length - $number_length;
        $zero = "";
        for ($i = 1; $i <= $zero_length; $i++) {
            $zero .= '0';
        }
        return $zero . $number;
    }

    protected function convbulan($bulan)
    {
        if ($bulan == "01") {
            $kode = "A";
        } elseif ($bulan == "02") {
            $kode = "B";
        } elseif ($bulan == "03") {
            $kode = "C";
        } elseif ($bulan == "04") {
            $kode = "D";
        } elseif ($bulan == "05") {
            $kode = "E";
        } elseif ($bulan == "06") {
            $kode = "F";
        } elseif ($bulan == "07") {
            $kode = "G";
        } elseif ($bulan == "08") {
            $kode = "H";
        } elseif ($bulan == "09") {
            $kode = "I";
        } elseif ($bulan == "10") {
            $kode = "J";
        } elseif ($bulan == "11") {
            $kode = "K";
        } elseif ($bulan == "12") {
            $kode = "L";
        }
        return $kode;
    }

    public function print(request $request, $id)
    {

        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $model =  HdPalletJadi::find($id);

        $kode = \DB::table('hd_pallet_jadi')
            ->select('mst_pallet_name.kode_pallet', 'hd_pallet_jadi.pallet_name_id') //, 'tally_table.tally_no'
            ->where('hd_pallet_jadi.hd_pallet_jadi_id', '=', $id)
            ->join('mst_pallet_name', 'mst_pallet_name.pallet_name_id', '=', 'hd_pallet_jadi.pallet_name_id')

            //   ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_lpb.supplier_id')
            ->orderBy('hd_pallet_jadi.created_by')->first();
        // dd($kode);
        // dd($model->jumlah_pallet);
        if ($model === null) {
            abort('404');
        }

        // echo $this->convbulan(date("m"));
        // exit();

        for ($i = 1; $i <= $model->jumlah_pallet; $i++) {
            $z[] = $this->complete_zero($i, 3) . date("d") . $this->convbulan(date("m")) . date("y") . $model->lokasi_kedatangan_id . $this->complete_zero($model->pallet_name_id, 3);
            //  $z[] = $this->complete_zero($i,3).date("d").$this->convbulan(date("m")).date("y").$model->lokasi_kedatangan_id;
        }

        // exit();

        // $z[] = 123;
        // $z[] = 7;
        // $z[] = 7;
        // $z[] = 7;
        // $collection = collect([1, 2, 3, 4, 5, 6, 7, 8]);
        $collection = collect($z);
        // dd($collection);
        // $this->validate($request, [
        //     'first_tally'            => 'required|integer',
        //     'end_tally'            => 'required|integer',
        // ]); 

        // // $id     = intval($request->get('id', 0));
        // $first  = $request->get('first_tally');
        // $end    = $request->get('end_tally');
        // if ($first > $end) {
        //     $request->session()->flash(
        //         'errorMessage',
        //         trans('message.failed-generate')
        //         );

        //     return redirect(route('generate-notally-printf'));
        //     exit();

        // }

        // $model = \DB::table('tally_table')
        //             ->select('tally_table.tally_no')
        //             ->where('tally_table.status', '=', true)
        //             ->whereBetween('tally_table.tally_no', [$first, $end])
        //             ->orderBy('tally_table.tally_no')->get();
        $max = count($collection);
        // dd($max);
        $data    = [];
        $dataSem = [];

        foreach ($collection as $key => $value) {
            $dataSem[] = $value;
            if (($key + 1) % 3 == 0) {
                $data[] = $dataSem;
                $dataSem = [];
            } elseif (($key + 1) == $max) {
                $data[] = $dataSem;
                $dataSem = [];
            }
        }
        // dd(count($collection));
        // dd($collection);
        // dd($data);

        //     foreach($data as $row) {
        // echo "<br><br>";
        //     foreach($row as $m) {
        //      echo $m."<br>";
        //     }
        //    }
        //    exit();

        $now    = new \DateTime();
        $now = date_format($now, 'd/m/Y');
        // $model =  HdLpb::find($id);

        //ako comment
        // $header = view('produksi/raft-result.print-barcode', [
        //     'title' => trans('fields.barcode'),
        //     'model' => $data,
        //     'kode'    => $kode,
        //    // 'isi'   => $isi,


        // ])->render();
        // \PDF::setHeaderCallback(function($pdf) use ($header) {
        //     $pdf->writeHTML($header);
        // });
        //ako end

        // $html = view('produksi/tally/generatenotally/', [
        //   "model"         => $model,
        //   "model"         => $model,
        //   "supplierOptions" => SupplierService::getActiveSupplier(),


        //])->render();

        $pdf = new TCPDF();
        $pdf::SetTitle('Barcode Hasil Rakit Pallet');

        // Custom Footer
        // \PDF::setFooterCallback(function($pdf) {

        //         // Position at 15 mm from bottom
        //         $pdf->SetY(-15);
        //         // Set font
        //         $pdf->SetFont('helvetica', 'I', 8);
        //         // Page number
        //         $pdf->Cell(0, 10, 'Page '.$pdf->getAliasNumPage().'/'.$pdf->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');

        // });

        // $pdf::SetTitle('Afkir-');
        //  $resolution= array(187, 143);
        // $resolution= array(143, 187);
        // $pdf::SetMargins(3, 5, 1, 5); //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf::SetMargins(1, 1, 1); //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf::AddPage('', array(160,  195));
        $pdf::SetAutoPageBreak(TRUE, 0);
        // $pdf::AddPage();
        //  $pdf::AddPage('P', $resolution);
        //$pdf::writeHTML(true, false, true, false);

        $style = array(
            'position' => '',
            'align' => 'C',
            'stretch' => true,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0, 0, 0),
            'bgcolor' => false, //array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 9,
            'stretchtext' => 3,


        );

        $x = 3;
        $y = 2;
        $count = 1;
        $tempbatas = 31;

        foreach ($data as $row) {

            foreach ($row as $m) {
                // $pdf::Cell(50, 20, $m->tally_no, 1, 0);
                $pdf::write1DBarcode($m, 'C39', $x, $y, 51, 16, 0.4, $style, 'N');
                $pdf::SetFont('helvetica', '', 9);
                $pdf::Cell(63, 3, $kode->kode_pallet, 1, 0, 'C', false);
                $x += 51;
                $count++;
            }
            if ($count == $tempbatas) {
                $pdf::AddPage('', array(160,  195));
                $x = 3;
                $y = 2;
                $tempbatas += 30;
            } else {
                $x = 3;
                $y += 19;
            }
        }


        ob_end_clean();

        $pdf::Output('Report Afkir' . '.pdf');
        //  // $pdf::Output($query->container_number.' '.$query->created_date.'.pdf');

        // $pdf->SetFont('helveticaBI', '', 30);
        $pdf->SetFont(times, 'BI', 6, '', 'false');
        // $pdf->SetMargins(56.7, 56.7);


    }
}
