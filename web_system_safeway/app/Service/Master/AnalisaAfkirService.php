<?php

namespace App\Service\Master;

class AnalisaAfkirService
{
    public static function getActiveAnalisaAfkir()
    {
        return \DB::table('mst_analisa_afkir')
            ->where('status', '=', true)
            ->orderBy('analisa_afkir')
            ->get();
    }
}
