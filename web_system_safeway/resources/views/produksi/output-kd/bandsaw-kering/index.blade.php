@extends('layouts.master')

@section('title', trans('menu.bandsaw'))

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">{{ trans('menu.bandsaw-and-afkir') }}</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="{{ route('dashboard') }}">{{ trans('menu.dashboard') }}</a>
                            </li>
                            <li>
                                <a>{{ trans('menu.produksi') }}</a>
                            </li>
                            <li class="active">
                                {{ trans('menu.bandsaw-and-afkir') }}
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            @if(Session::has('successMessage'))
            <div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-check-all"></i>
                <strong>{{ Session::get('successMessage') }}.</strong>
            </div>
            @endif

            @if(Session::has('errorMessage'))
            <div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-block-helper"></i>
                <strong>{{ Session::get('errorMessage') }}.</strong>
            </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-border panel-success">
                        <div class="panel-heading">
                        </div>

                        <div class="panel-body">
                            <div class="row">
                                <form class="form-horizontal" role="form" method="post" action="">
                                    {{ csrf_field() }}
                                    <div class="col-md-12">
                                        <p class="text-muted font-14 m-b-20 pull-right">

                                         <div class="col-md-2">
                                            <input type="text" name="tally_no" class="form-control" value="{{ !empty($filters['tally_no']) ? $filters['tally_no'] : '' }}">
                                            @if($errors->has('tally_no'))
                                            <span class="help-block">{{ $errors->first('tally_no') }}</span>
                                            @endif
                                        </div>

                                        <div class="col-md-4">
                                            <button type="submit" class="btn btn-info waves-effect w-xs waves-light"><i class="fa fa-search"></i> </button>
                                        </div>
                                    </p>
                                </div>
                            </form>
                        </div><br>


                        <div class="row">
                            <table class="table table-bordered table-hover table-striped m-0">
                                <thead>
                                    <tr>
                                        <th width="10%" class="text-center">{{ trans('fields.num') }}</th>
                                        <!-- <th width="10%" class="text-center">{{ trans('fields.nomor_lpb') }}</th>  -->
                                        <th width="60%" class="text-center">{{ trans('fields.notally') }}</th>
                                        <th width="30%" class="text-center">{{ trans('fields.action') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = ($models->currentPage() - 1) * $models->perPage() + 1; ?>
                                    @foreach($models as $isi)
                                    <tr>
                                        <th scope="row" style="text-align: center;">{{ $no++ }}</th>
                                        <td>{{ $isi->tally_no }}</td>
                                        <td>
                                            @can('access', [$resource, 'add'])
                                                <a href="{{ route('bandsaw-kering-add', ['id' => $isi->tally_id]) }}" data-toggle="tooltip" class="btn btn-xs btn-primary" >
                                                    <i class="fa fa-plus"></i> {{ trans('fields.add-bandsaw') }}
                                                </a>
                                            @endcan

                                              @can('access', [$resource, 'add'])
                                                <a href="{{ route('afkirproses-kering-add', ['id' => $isi->tally_id]) }}" data-toggle="tooltip" class="btn btn-xs btn-primary" >
                                                    <i class="fa fa-plus"></i> {{ trans('fields.add-afkir') }}
                                                </a>
                                            @endcan
                                        </td>






                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="data-table-toolbar">
                                {!! $models->render() !!}
                            </div>
                        </div>

                    </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection