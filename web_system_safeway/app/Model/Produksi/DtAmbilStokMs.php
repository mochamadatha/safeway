<?php

namespace App\Model\Produksi;

use Illuminate\Database\Eloquent\Model;

class DtAmbilStokMs extends Model
{
    protected $table        = 'dt_ambil_stockms';
    protected $primaryKey   = 'ambil_stockms_id';
    public $timestamps      = false;
}
