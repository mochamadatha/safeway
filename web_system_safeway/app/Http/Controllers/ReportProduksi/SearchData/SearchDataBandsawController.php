<?php

namespace App\Http\Controllers\ReportProduksi\SearchData;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Produksi\LPB;
use App\Model\Produksi\LPBDetail;
use App\Service\Master\TallyService;
use App\Model\MasterProduksi\Supplier;
use App\Service\Master\SupplierService;
use App\Model\Produksi\HdLpb;
use Elibyy\TCPDF\Facades\TCPDF;
use App\Model\Produksi\IsiAfkir;
use App\Model\Produksi\Tally;
use App\Model\Produksi\IsiTally;
use App\Service\Master\WoodService;
use App\Service\Master\DesService;
use App\Model\MasterProduksi\LokasiKedatangan;
use App\Service\Master\ArrivalLocationService;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class SearchDataBandsawController extends Controller
{
    const URL       = 'report-produksi/search-data-bandsaw';
    const RESOURCE  = 'search-data-bandsaw';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        if ($request->isMethod('post')) {
            $request->session()->put('filters', $request->all());
            return redirect(self::URL . '?page=1');
        } elseif (empty($request->get('page'))) {
            $request->session()->forget('filters');
        }
        $filters = $request->session()->get('filters');




        // $query = DB::table('dt_isi_bandsaw')
        //     ->select(
        //         'tally_table.tally_no',
        //         'mst_jenis_kayu.jenis_kayu',
        //         'mst_deskripsi_tallysheet.nama_deskripsi',
        //         'dt_isi_bandsaw.tinggi',
        //         'dt_isi_bandsaw.lebar',
        //         'dt_isi_bandsaw.panjang',
        //         'dt_isi_bandsaw.pcs',
        //         'dt_isi_bandsaw.volume',
        //         'dt_isi_bandsaw.bandsaw_date',
        //         'dt_isi_bandsaw.keterangan'
        //     )

        //     ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')
        //     ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
        //     ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')
        //     ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')

        //     ->orderBy('dt_isi_bandsaw.tally_id');


        $query = DB::table('dt_isi_bandsaw')
            ->select(
                'tally_table.tally_no',
                'mst_jenis_kayu.jenis_kayu',
                'mst_deskripsi_tallysheet.nama_deskripsi',
                'dt_isi_bandsaw.bandsaw_date',
                'dt_isi_bandsaw.tinggi',
                'dt_isi_bandsaw.lebar',
                'dt_isi_bandsaw.panjang',
                'dt_isi_bandsaw.pcs',
                'dt_isi_bandsaw.volume',
                'dt_isi_bandsaw.bandsaw_date',
                'dt_isi_bandsaw.keterangan'
            )

            ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')
            ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')
            ->orderBy('dt_isi_bandsaw.tally_id');

        // $query2 = DB::table('dt_isi_tally')
        // ->select('tally_table.tally_no', 'mst_jenis_kayu.jenis_kayu','mst_deskripsi_tallysheet.nama_deskripsi', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 'dt_isi_tally.panjang', 'dt_isi_tally.pcs', 'dt_isi_tally.volume')

        //     ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')

        //     ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
        //     ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
        //     ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_tally.deskripsi_id')
        //     ->union($query)
        //     ->orderBy('tally_id') ;


        // dd($query2);

        if (!empty($filters['status'])) {
            if (!empty($filters['start_date']) && !empty($filters['end_date'])) {
                if ($filters['status'] == 'basah') {
                    $query->whereBetween('bandsaw_date', [$filters['start_date'], $filters['end_date']]);
                    $query->where('dt_isi_bandsaw.keterangan', '=', 'Basah');
                } elseif ($filters['status'] == 'kering') {
                    $query->whereBetween('bandsaw_date', [$filters['start_date'], $filters['end_date']]);
                    $query->where('dt_isi_bandsaw.keterangan', '=', 'Kering');
                } elseif ($filters['status'] == 'all') {
                    $query->whereBetween('bandsaw_date', [$filters['start_date'], $filters['end_date']]);
                }
                $request->session()->put('excel', $filters); //simpan data
                // dd($request->session()->get('tgl')); //ngambil data
                // $query->where('hd_lpb.nomor_lpb', 'like', '%'.$filters['nomor_lpb'].'%');
            }
        } else {
            $request->session()->forget('excel');
        }

        return view('report-produksi/search-data-bandsaw.index', [
            "models"        => $query->paginate(10),
            "filters"       => $filters,
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }





    public function exportexcel(Request $request)
    {

        $this->ex = $request->session()->get('excel'); //ngambil data
        ob_end_clean();
        ob_start();
        Excel::create('Search Data Bandsaw', function ($excel) {

            $excel->sheet('Sheet 1', function ($sheet) {


                $query = DB::table('dt_isi_bandsaw')
                    ->select(
                        'tally_table.tally_no',
                        'mst_jenis_kayu.jenis_kayu',
                        'mst_deskripsi_tallysheet.nama_deskripsi',
                        'dt_isi_bandsaw.tinggi',
                        'dt_isi_bandsaw.lebar',
                        'dt_isi_bandsaw.panjang',
                        'dt_isi_bandsaw.pcs',
                        'dt_isi_bandsaw.volume',
                        'dt_isi_bandsaw.bandsaw_date',
                        'dt_isi_bandsaw.keterangan'
                    )

                    ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')

                    ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
                    ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')
                    ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id');


                if (!empty($this->ex['status'])) {
                    if (!empty($this->ex['start_date']) && !empty($this->ex['end_date'])) {
                        if ($this->ex['status'] == 'basah') {
                            $query->whereBetween('bandsaw_date', [$this->ex['start_date'], $this->ex['end_date']]);
                            $query->where('dt_isi_bandsaw.keterangan', '=', 'Basah');
                        } elseif ($this->ex['status'] == 'kering') {
                            $query->whereBetween('bandsaw_date', [$this->ex['start_date'], $this->ex['end_date']]);
                            $query->where('dt_isi_bandsaw.keterangan', '=', 'Kering');
                        } elseif ($this->ex['status'] == 'all') {
                            $query->whereBetween('bandsaw_date', [$this->ex['start_date'], $this->ex['end_date']]);
                        }
                    }
                }
                $query->orderBy('tally_table.tally_id');



                //   dd($query->get());

                foreach ($query->get() as $product) {
                    $data[] = array(
                        $product->tally_no,
                        $product->bandsaw_date,
                        $product->jenis_kayu,
                        $product->nama_deskripsi,
                        $product->tinggi,
                        $product->lebar,
                        $product->panjang,
                        $product->pcs,
                        $product->volume,
                        $product->keterangan,

                    );
                }

                $sheet->fromArray($data, null, 'A1', false, false);
                $headings = array('Nomor Tally', 'Jenis Kayu', 'Deskrisi', 'Tinggi', 'Lebar', 'Panjang', 'Pcs', 'Volume', 'Keterangan');
                $sheet->prependRow(1, $headings);
            });
        })->export('xlsx');
    }
}
