<?php

namespace App\Model\MasterContainer;

use Illuminate\Database\Eloquent\Model;

class ContainerInspect extends Model
{
    protected $table        = 'dt_containerinspect';
    protected $primaryKey   = 'containerinspect_id';
    public $timestamps      = false;
}
