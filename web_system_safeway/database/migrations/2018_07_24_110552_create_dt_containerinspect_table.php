<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDtContainerinspectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dt_containerinspect', function (Blueprint $table) {
            $table->increments('containerinspect_id');
            $table->integer('customer_id');
            $table->integer('depolocation_id');
            $table->integer('inspect_id');
            $table->integer('agent_id');
            $table->integer('size_id');
            $table->string('container_number')->unique();
            $table->string('safeway_sn')->unique();
            $table->integer('manufacture_year');
            $table->string('previous_cargo');
            $table->string('packingin');
            $table->string('doorclosed');
            $table->string('allinterior');
            $table->string('floor');
            $table->string('roof');
            $table->string('leftwall');
            $table->string('rightwall');
            $table->string('seal');
            $table->string('cscplate');
            $table->string('temperature')->nullable();
            $table->boolean('status')->default(true);
            $table->integer('created_by');
            $table->dateTime('created_date');
            $table->integer('modified_by')->nullable();
            $table->dateTime('modified_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dt_containerinspect');
    }
}
