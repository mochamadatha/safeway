<?php

namespace App\Http\Controllers\DivisionIspm;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\DivisionIspm\IspmInspect;
use App\Model\Masterispm\Size;
use App\Service\DivisionIspm\SizeService;
use Elibyy\TCPDF\Facades\TCPDF;
use DB;

class IspmInspectionReport extends Controller
{
    const URL       = 'division-ispm/ispm-inspect';
    const RESOURCE  = 'ispm-inspect';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        if ($request->isMethod('post')) {
            $request->session()->put('filters', $request->all());
            return redirect(self::URL . '?page=1');
        } elseif (empty($request->get('page'))) {
            $request->session()->forget('filters');
        }

        $filters = $request->session()->get('filters');
        $query = \DB::table('dt_ispminspect')
            ->where('dt_ispminspect.status', '=', true)
            ->orderBy('dt_ispminspect.container_number');

        if (!empty($filters['status']) || !$request->session()->has('filters')) {
            $query->where('dt_ispminspect.status', '=', true);
        } else {
            $query->where('dt_ispminspect.status', '=', false);
        }


        $filters = $request->session()->get('filters');
        $query = \DB::table('dt_ispminspect')
            ->select('dt_ispminspect.*', 'mst_sizeispm.size')
            ->join('mst_sizeispm', 'mst_sizeispm.sizeispm_id', '=', 'dt_ispminspect.sizeispm_id')
            ->orderBy('container_number');


        if (!empty($filters['container_number'])) {
            $query->where('dt_ispminspect.container_number', 'like', '%' . $filters['container_number'] . '%');
        }



        return view('division-ispm/ispm-inspect.index', [
            "models"        => $query->paginate(10),
            "filters"       => $filters,

            "sizeOptions"   => SizeService::getActiveSize(),
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }


    protected function convbulan($bulan)
    {
        if ($bulan == "01") {
            $kode = "I";
        } elseif ($bulan == "02") {
            $kode = "II";
        } elseif ($bulan == "03") {
            $kode = "III";
        } elseif ($bulan == "04") {
            $kode = "IV";
        } elseif ($bulan == "05") {
            $kode = "V";
        } elseif ($bulan == "06") {
            $kode = "VI";
        } elseif ($bulan == "07") {
            $kode = "VII";
        } elseif ($bulan == "08") {
            $kode = "VIII";
        } elseif ($bulan == "09") {
            $kode = "IX";
        } elseif ($bulan == "10") {
            $kode = "X";
        } elseif ($bulan == "11") {
            $kode = "XI";
        } elseif ($bulan == "12") {
            $kode = "XII";
        }
        return $kode;
    }



    public function reportispmpdf(Request $request, $id)
    {


        // $model = ispmInspect::find($id);

        // $filters = $request->session()->get('filters');
        $query = \DB::table('dt_ispminspect')
            ->select('dt_ispminspect.*', 'mst_sizeispm.size')
            ->where('dt_ispminspect.ispminspect_id', '=', $id)
            ->join('mst_sizeispm', 'mst_sizeispm.sizeispm_id', '=', 'dt_ispminspect.sizeispm_id')
            ->orderBy('container_number')->first();




        $now    = new \DateTime();
        $now = date_format($now, 'd/m/Y h:i:s');


        $header = view('division-ispm/ispm-inspect.header-pdf', ['title' => trans('menu.container_stuffing_report') . "#" . $now])->render();
        \PDF::setHeaderCallback(function ($pdf) use ($header) {
            $pdf->writeHTML($header);
        });

        $model =  IspmInspect::find($id);

        $this->convbulan(date("m"));
        $html = view('division-ispm/ispm-inspect.report-ispm-pdf', [
            // 'model' => $model,
            "model"         => $query,

            "sizeOptions"   => SizeService::getActiveSize(),

        ])->render();

        $pdf = new TCPDF();
        $pdf::SetTitle('ispm Inspection Report');
        ob_end_clean();
        $pdf::AddPage();
        $pdf::writeHTML($html, true, false, true, false, '');
        // $pdf::Output($model->container_number.' '.$model->created_date.'.pdf');
        $pdf::Output($query->container_number . ' ' . $query->created_date . '.pdf');
        $pdf->SetFont('Helvetica', '', 9);
    }

    public function add(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        $model           = new IspmInspect();
        $model->status   = true;

        return view('division-ispm/ispm-inspect.add', [
            "model"         => $model,
            "sizeOptions"   => SizeService::getActiveSize(),
        ]);
    }


    public function edit(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $model = IspmInspect::find($id);

        if ($model === null) {
            abort('404');
        }

        return view('division-ispm/ispm-inspect.add', [
            "model"         => $model,

            "sizeOptions"   => SizeService::getActiveSize(),
        ]);
    }



    public function delete(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $model =  IspmInspect::find($id);

        if ($model === null) {
            abort('404');
        }

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            $model->modified_by   = \Auth::user()->id;
            $model->modified_date = $now;
            $model->status   = false;

            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.delete-message', ['variable' => trans('fields.container_number') . ' ' . $id])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.faildel-message', ['variable' => trans('fields.container_number') . ' ' . $id])
            );
        }

        return redirect(route('ispm-inspect-index'));
    }

    public function save(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add']) && !\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $id     = intval($request->get('id'));

        $this->validate($request, [
            'container_number'  => 'required|string|max:255',
            'size'           => 'required|integer',
            'assignment_letter'  => 'required|integer',
            'inspect_date'    => 'required|date',


        ]);

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            if (empty($id)) {
                $model  = new IspmInspect();
                $model->created_by      = \Auth::user()->id;
                $model->created_date    = $now;
            } else {
                $model  = IspmInspect::find($id);
                $model->modified_by   = \Auth::user()->id;
                $model->modified_date = $now;
            }

            $model->container_number        = $request->get('container_number');
            $model->sizeispm_id                 = $request->get('size');
            $model->assignment_letter        = $request->get('assignment_letter');
            $model->inspect_date    = $request->get('inspect_date');

            $empty = $request->file('empty');
            if ($empty !== null) {
                $fotoName   = "e" . md5($now->format('YmdHis')) . "." . $empty->guessExtension();
                $empty->move(\Config::get('app.paths.foto-ispm'), $fotoName);
                $model->empty = $fotoName;
            }
            $one_cargo = $request->file('one_cargo');
            if ($one_cargo !== null) {
                $fotoName   = "oc" . md5($now->format('YmdHis')) . "." . $one_cargo->guessExtension();
                $one_cargo->move(\Config::get('app.paths.foto-ispm'), $fotoName);
                $model->one_cargo = $fotoName;
            }
            $half_cargo = $request->file('half_cargo');
            if ($half_cargo !== null) {
                $fotoName   = "h" . md5($now->format('YmdHis')) . "." . $half_cargo->guessExtension();
                $half_cargo->move(\Config::get('app.paths.foto-ispm'), $fotoName);
                $model->half_cargo = $fotoName;
            }

            $full_cargo = $request->file('full_cargo');
            if ($full_cargo !== null) {
                $fotoName   = "f" . md5($now->format('YmdHis')) . "." . $full_cargo->guessExtension();
                $full_cargo->move(\Config::get('app.paths.foto-ispm'), $fotoName);
                $model->full_cargo = $fotoName;
            }
            $cargo_label = $request->file('cargo_label');
            if ($cargo_label !== null) {
                $fotoName   = "c" . md5($now->format('YmdHis')) . "." . $cargo_label->guessExtension();
                $cargo_label->move(\Config::get('app.paths.foto-ispm'), $fotoName);
                $model->cargo_label = $fotoName;
            }
            $one_door_closed = $request->file('one_door_closed');
            if ($one_door_closed !== null) {
                $fotoName   = "on" . md5($now->format('YmdHis')) . "." . $one_door_closed->guessExtension();
                $one_door_closed->move(\Config::get('app.paths.foto-ispm'), $fotoName);
                $model->one_door_closed = $fotoName;
            }
            $door_closed = $request->file('door_closed');
            if ($door_closed !== null) {
                $fotoName   = "dc" . md5($now->format('YmdHis')) . "." . $door_closed->guessExtension();
                $door_closed->move(\Config::get('app.paths.foto-ispm'), $fotoName);
                $model->door_closed = $fotoName;
            }
            $agent_seal = $request->file('agent_seal');
            if ($agent_seal !== null) {
                $fotoName   = "as" . md5($now->format('YmdHis')) . "." . $agent_seal->guessExtension();
                $agent_seal->move(\Config::get('app.paths.foto-ispm'), $fotoName);
                $model->agent_seal = $fotoName;
            }
            $customer_seal = $request->file('customer_seal');
            if ($customer_seal !== null) {
                $fotoName   = "cs" . md5($now->format('YmdHis')) . "." . $customer_seal->guessExtension();
                $customer_seal->move(\Config::get('app.paths.foto-ispm'), $fotoName);
                $model->customer_seal = $fotoName;
            }


            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('menu.ispm-inspect') . ' ' . $request->get('container_number')])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('menu.ispm-inspect') . ' ' . $request->get('container_number')])
            );
        }

        return redirect(route('ispm-inspect-index'));
    }
}
