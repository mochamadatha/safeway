<?php

namespace App\Model\MasterContainer;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    protected $table        = 'mst_size';
    protected $primaryKey   = 'size_id';
    public $timestamps      = false;
}
