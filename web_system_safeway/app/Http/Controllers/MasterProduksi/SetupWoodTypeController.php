<?php

namespace App\Http\Controllers\MasterProduksi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\MasterProduksi\JenisKayu;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class SetupWoodTypeController extends Controller
{
    const URL       = 'master-produksi/wood-type';
    const RESOURCE  = 'wood-type';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        if ($request->isMethod('post')) {
            $request->session()->put('filters', $request->all());
            return redirect(self::URL . '?page=1');
        } elseif (empty($request->get('page'))) {
            $request->session()->forget('filters');
        }
        $filters = $request->session()->get('filters');
        $query = \DB::table('mst_jenis_kayu')
            ->orderBy('mst_jenis_kayu.jenis_kayu');

        if (!empty($filters['status']) || !$request->session()->has('filters')) {
            $query->where('mst_jenis_kayu.status', '=', true);
        } else {
            $query->where('mst_jenis_kayu.status', '=', false);
        }

        if (!empty($filters['jenis_kayu'])) {
            $query->where('mst_jenis_kayu.jenis_kayu', 'ilike', '%' . $filters['jenis_kayu'] . '%');
        }

        return view('master-produksi/wood-type.index', [
            "models"        => $query->paginate(10),
            "filters"       => $filters,
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function add(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        $model           = new JenisKayu();
        $model->status   = true;

        return view('master-produksi/wood-type.add', [
            "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }

    public function importexcel(Request $request)
    {

        //if ($request->Request('file')) {
        $model  = new JenisKayu();
        $now    = new \DateTime();



        //$model->modified_date = $now;
        $path = $request->file('file')->getRealPath();
        $data = EXCEL::load($path, function ($reader) { })->get();

        if (!empty($data) && $data->count()) {
            foreach ($data as $key => $value) {
                $modal = new JenisKayu();
                $modal->jenis_kayu = $value->jenis_kayu;
                $modal->created_by = \Auth::user()->id;
                $modal->created_date    = $now;
                $modal->save();
            }
        }

        return back();
    }

    public function edit(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $model = JenisKayu::find($id);

        if ($model === null) {
            abort('404');
        }

        return view('master-produksi/wood-type.add', [
            "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }

    public function save(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add']) && !\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $id     = intval($request->get('id', 0));

        $this->validate($request, [
            'jenis_kayu' => 'required|max:255|unique:mst_jenis_kayu,jenis_kayu,' . $id . ',jenis_kayu_id',
        ]);

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            if (empty($id)) {
                $model  = new JenisKayu();
                $model->created_by      = \Auth::user()->id;
                $model->created_date    = $now;
            } else {
                $model  = JenisKayu::find($id);
                $model->modified_by   = \Auth::user()->id;
                $model->modified_date = $now;
            }

            $model->jenis_kayu   = $request->get('jenis_kayu');
            $model->status   = !empty($request->get('status')) ? true : false;

            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('menu.wood-type') . ' ' . $request->get('jenis_kayu')])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('menu.wood-type') . ' ' . $request->get('jenis_kayu')])
            );
        }

        return redirect(route('setup-woodtype-index'));
    }
}
