<?php

namespace App\Model\Produksi;

use Illuminate\Database\Eloquent\Model;

class HdPalletJadi extends Model
{
    protected $table        = 'hd_pallet_jadi';
    protected $primaryKey   = 'hd_pallet_jadi_id';
    public $timestamps      = false;
}
