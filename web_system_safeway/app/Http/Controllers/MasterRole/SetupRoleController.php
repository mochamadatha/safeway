<?php

namespace App\Http\Controllers\MasterRole;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Master\Role;
use App\Model\Master\AksesKontrol;
use DB;

class SetupRoleController extends Controller
{
    const URL       = 'master-role/setup-role';
    const RESOURCE  = 'setup-role';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        } 

        if ($request->isMethod('post')) {
            $request->session()->put('filters', $request->all());
            return redirect(self::URL.'?page=1');
        } elseif (empty($request->get('page'))) {
            $request->session()->forget('filters');
        }
        $filters = $request->session()->get('filters');
        $query = DB::table('mst_role')
                    ->orderBy('mst_role.nama_role');

        if (!empty($filters['status']) || !$request->session()->has('filters')) {
            $query->where('mst_role.status', '=', true);
        } else {
            $query->where('mst_role.status', '=', false);
        }

        if (!empty($filters['nama_role'])) {
            $query->where('mst_role.nama_role', 'like', '%'.$filters['nama_role'].'%');
        }

        return view('master-role/setup-role.index',[
            "models"        => $query->paginate(10),
            "filters"       => $filters,
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function add(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        } 

        $model           = new Role();
        $model->status   = true;
        
        return view('master-role/setup-role.add',[
            "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }

    public function edit(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        } 

        $model = Role::find($id);

        if ($model===null) {
            abort('404');
        }
        
        return view('master-role/setup-role.add',[
            "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }

    public function save(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add']) && !\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        } 

        $id     = intval($request->get('id', 0));
        
        if(empty($request->get('status')) && $this->userExistThisRole($id)){
            return redirect(\URL::previous())->withInput($request->all())->withErrors(['errorMessage' => trans('message.user-exist-this-role')]);
        }

        $this->validate($request, [
            'nama_role' => 'required|max:255|unique:mst_role,nama_role,'.$id.',role_id',
        ]);

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            if(empty($id)){
                $model  = new Role();
                $model->created_by      = \Auth::user()->id;
                $model->created_date    = $now;
            }else{
                $model  = Role::find($id);
                $model->modified_by   = \Auth::user()->id;
                $model->modified_date = $now;
            }

            $model->nama_role   = $request->get('nama_role');
            $model->status   = !empty($request->get('status')) ? true : false;
            
            if ($model->accessControl()->count() > 0) {
                $model->accessControl()->forceDelete();
            }
            
            $model->save();
            foreach ($request->get('privileges', []) as $resource => $privileges) {
                foreach ($privileges as $privilege => $access) {
                    $accessControl = new AksesKontrol();
                    $accessControl->role_id     = $model->role_id;
                    $accessControl->resource    = $resource;
                    $accessControl->privilege   = $privilege;
                    $accessControl->save();
                }
            }
            DB::commit();


            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('menu.setup-role').' '.$request->get('nama_role')])
                );

        } catch (\Exception $e) {
             DB::rollback();
             $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('menu.setup-role').' '.$request->get('nama_role')])
                );
         }

        return redirect(route('setup-role-index'));
    }


    private function userExistThisRole($roleId){
        $exist = false;
        $user  = DB::table('users')
                    ->join('mst_role', 'mst_role.role_id', '=', 'users.role_id')
                    ->count();
        if($user > 0){
            $exist = true;
        }
        return $exist;
    }
}
