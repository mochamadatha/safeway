@extends('layouts.master')

@section('title', trans('menu.bandsaw-dry'))

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">{{ trans('menu.bandsaw-dry') }}</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="{{ route('dashboard') }}">{{ trans('menu.dashboard') }}</a>
                            </li>
                            <li>
                                <a>{{ trans('menu.produksi') }}</a>
                            </li>
                            <li class="active">
                                {{ trans('menu.bandsaw') }}
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            @if(Session::has('successMessage'))
            <div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-check-all"></i>
                <strong>{{ Session::get('successMessage') }}.</strong>
            </div>
            @endif

            @if(Session::has('errorMessage'))
            <div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-block-helper"></i>
                <strong>{{ Session::get('errorMessage') }}.</strong>
            </div>
            @endif
            
            <div class="row">
                <div class=" col-md-12">
                    <div class="panel panel-border panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">No Tally : {{ $models->tally_no }} </h3><br>
                            <h4 class="text-center">Ukuran Awal </h4><br>

                            <div class="row">
                                <table class="table table-bordered table-hover table-striped m-0">
                                    <thead>
                                        <tr>
                    
                                            <th width="10%" class="text-center">{{ trans('fields.wood-type') }}</th>
                                        
                                            <th width="10%" class="text-center">{{ trans('fields.high') }}</th>
                                            <th width="10%" class="text-center">{{ trans('fields.width') }}</th>
                                            <th width="10%" class="text-center">{{ trans('fields.length') }}</th>
                                            <th width="10%" class="text-center">{{ trans('fields.pcs') }}</th>
                                            <th width="10%" class="text-center">{{ trans('fields.m3') }}</th>
                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                      
                                        @foreach($mode as $mode)
                                     
                                        <tr>
                                 
                                        
                                            <td class="text-center"> {{ $mode->jenis_kayu }}</td>
                            
                                            <td class="text-center">{{ $mode->tinggi }}</td>
                                            <td class="text-center">{{ $mode->lebar }}</td>
                                            <td class="text-center">{{ $mode->panjang }}</td>
                                            <td class="text-center">{{ $mode->pcs }}</td>
                                            <td class="text-center">{{ $mode->volume }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                               
                            </div>
                        </div>
                            

                        <div class="panel-body">
                            <div class="row">
                              <br>  <h4 class="text-center">Rencana Ukuran Baru </h4><br>
                                <form class="form-horizontal" role="form" method="post" action="{{ route('bandsaw-save') }}">
                                    {{ csrf_field() }}
                                    <div class="col-sm-12 data-table-toolbar text-right">
                                        <div class="form-group">
                                            <button type="button" class="btn btn-primary waves-effect waves-light btn-sm" data-toggle="modal" data-target="#dtbandsaw"><i class="fa fa-plus-circle"></i> {{ trans('fields.add-bandsaw-dry') }} </button>
                                            {{-- <a id="clear-details" href="{{ url('spk-bandsaw/addplan/{$id}') }}"  class="btn btn-sm btn-success">
                                                <i class="fa fa-reply"></i> {{ trans('fields.back') }} 
                                            </a> --}}
                                        </div>
                                        <div class="row">
                                            <table class="table table-bordered table-hover table-striped m-0">
                                                <thead>
                                                    <tr>
                                                        <th width="17%" class="text-center">{{ trans('fields.wood-type') }}</th>
                                                        <th width="10%" class="text-center">{{ trans('fields.high') }}</th>
                                                        <th width="10%" class="text-center">{{ trans('fields.width') }}</th>
                                                        <th width="10%" class="text-center">{{ trans('fields.length') }}</th>
                                                        <th width="10%" class="text-center">{{ trans('fields.pcs') }}</th>
                                                        <th width="10%" class="text-center">{{ trans('fields.m3') }}</th>
                                                        <th width="5%" class="text-center">{{ trans('fields.action') }}</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                
                                                    @foreach($model as $model)
                                                 
                                                    <tr>
                                                   
                                                      
                                                        <td class="text-center">{{ $model->jenis_kayu }}</td>
                                                        <td class="text-center">{{ $model->tinggi }}</td>
                                                        <td class="text-center">{{ $model->lebar }}</td>
                                                        <td class="text-center">{{ $model->panjang }}</td>
                                                        <td class="text-center">{{ $model->pcs }}</td>
                                                        <td class="text-center">{{ $model->volume }}</td>
                                                     
                                                        <td class="text-center">
                                                            @can('access', [$resource, 'update'])
                                                            <a data-target="#editbandsaw{{ $model->isi_planbansaw_id }}" data-toggle="modal"  class="btn btn-xs btn-warning" data-original-title="Edit">
                                                                <i class="fa fa-pencil"></i>
                                                            </a>
                                               
                                                            @endcan

                                                            @can('access', [$resource, 'update'])
                                                                <a  data-toggle="modal" data-target="#deletebandsaw{{ $model->isi_planbansaw_id }}" class="btn btn-xs btn-danger" >
                                                                    <i class="fa fa-remove"></i> 
                                                                </a>
                                                                <div id="deletebandsaw{{ $model->isi_planbansaw_id }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                                <h4 class="modal-title">{{ trans('fields.deletenotally') }}</h4>
                                                                            </div>
                                                                            <div class="modal-body">       
                                                                                <h5>Apakah anda yakin ingin menghapus Tally Ini ??</h5>
                                                                            </div>

                                                                            <div class="modal-footer">
                                                                                <input type="hidden" name="id" class="form-control" value="{{ $model->isi_planbansaw_id }}">
                                                                                <a href="{{ route('bandsaw-kering-destroy', ['id' => $model->isi_planbansaw_id]) }}" data-toggle="tooltip" class="btn btn-danger waves-effect" >Yes</a>
                                                                                <button type="button" class="btn btn-default waves-effect waves-light" data-dismiss="modal">No</button>
                                                                            </div> 
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endcan
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            
                                        </div>
                                    </div>
                                </form>
                            </div> 
                        </div>

                        
          
                        <div id="dtbandsaw" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form role="form" method="post" action="{{ route('spk-bandsaw-saveplan') }}">
                                        {{ csrf_field() }}
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">{{ trans('fields.add-bandsaw') }}</h4>
                                        </div>
                                        <div class="modal-body">
                                      
                                            <input type="hidden" name="tally_id" class="form-control" value="{{ $models->tally_id }}">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="field-1" class="control-label">{{ trans('fields.wood-type') }}</label>
                                                        <!-- <input type="text" class="form-control" id="field-1" placeholder="John"> -->
                                                        <select class="form-control" id="jenis_kayu_id" name="jenis_kayu_id">
                                                            <option value="">- {{ trans('fields.change') }} {{ trans('fields.wood-type') }} -</option>
                                                            @foreach($woodOptions as $wood)
                                                            <option value="{{ $wood->jenis_kayu_id }}" {{ !empty($filters['wood']) && $filters['wood'] == $wood->jenis_kayu_id ? 'selected' : ''}} >{{ $wood->jenis_kayu }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="tinggi" class="control-label">{{ trans('fields.high') }}</label>
                                                        <input type="number" class="form-control" id="tinggi" name="tinggi">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="lebar" class="control-label">{{ trans('fields.width') }}</label>
                                                        <input type="number" class="form-control" id="lebar" name="lebar">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="panjang" class="control-label">{{ trans('fields.length') }}</label>
                                                        <input type="number" class="form-control" id="panjang" name="panjang">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="pcs" class="control-label">{{ trans('fields.pcs') }}</label>
                                                        <input type="number" class="form-control" id="pcs" name="pcs">
                                                    </div>
                                                </div>
                                                 
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-info waves-effect waves-light">Save changes</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div><!-- /.modal -->

                        
                        
                    </div>
                </div>

            </div>


        </div>
    </div>
</div>
@endsection