<?php

return [
    'foto-user'         => 'assets/images/users',
    'foto-user-default' => 'assets/images/users/user.png',
    'foto-container'	=> 'assets/images/container',
    'foto-ispm'			=> 'assets/images/ispm'
];