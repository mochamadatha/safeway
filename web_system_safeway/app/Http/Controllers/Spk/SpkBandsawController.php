<?php

namespace App\Http\Controllers\Spk;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Produksi\HdSpkBandsaw;
use App\Model\Produksi\LPB;
use App\Model\Produksi\LPBDetail;
use App\Model\Produksi\SPKBandsawDetail;
use App\Service\Master\TallyService;
use App\Model\MasterProduksi\Supplier;
use App\Service\Master\SupplierService;
use App\Model\Produksi\HdLpb;
use Elibyy\TCPDF\Facades\TCPDF;
use App\Model\Produksi\IsiAfkir;
use App\Model\Produksi\IsiPlanBandsaw;
use App\Service\Master\WoodService;
use App\Model\MasterProduksi\LokasiKedatangan;
use App\Service\Master\ArrivalLocationService;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class SpkBandsawController extends Controller
{
    const URL       = 'spk-produksi/spk-bandsaw';
    const RESOURCE  = 'spk-bandsaw';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        if ($request->isMethod('post')) {
            $request->session()->put('filters', $request->all());
            return redirect(self::URL . '?page=1');
        } elseif (empty($request->get('page'))) {
            $request->session()->forget('filters');
        }
        $filters = $request->session()->get('filters');
        $query = \DB::table('hd_spk_bandsaw')
            ->select('hd_spk_bandsaw.*') //, 'tally_table.tally_no'
           
            ->orderBy('hd_spk_bandsaw.bandsaw_date', 'DESC');

        // dd($query);

        if (!empty($filters['nomor_lpb'])) {
            $query->where('hd_spk_bandsaw.nomor_lpb', 'like', '%' . $filters['nomor_lpb'] . '%');
        }

        if (!empty($filters['nomor_sj'])) {
            $query->where('hd_spk_bandsaw.nomor_sj', 'like', '%' . $filters['nomor_sj'] . '%');
        }



        return view('spk-produksi/spk-bandsaw.index', [
            "models"        => $query->paginate(10),
            "filters"       => $filters,
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function add(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        $model           = new HdSpkBandsaw();

        // dd($model);
        $model->status   = true;

        if ($model === null) {
            abort('404');
        }

        // $query = DB::table('hd_spk_bandsaw')
        // ->select('hd_spk_bandsaw.*', 'tally_table.tally_no')
        //         ->join('tally_table', 'tally_table.tally_id', '=', 'hd_spk_bandsaw.tally_id')
        //      //   ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_spk_bandsaw.supplier_id')
        // ->orderBy('hd_spk_bandsaw.created_by');

        return view('spk-produksi/spk-bandsaw.add', [
            "model"         => $model,
            // "models"         => $query->paginate(10),
            // "tallyOptions"   => TallyService::getActiveTally(),
            "supplierOptions" => SupplierService::getActiveSupplier(),
            "arrivallocationOptions"    => ArrivalLocationService::getActiveArrivalLocation(),
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }


    

    public function addtally(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        // $model = LPB::find($id);
       
        $model = DB::table('hd_spk_bandsaw')
            ->select('hd_spk_bandsaw.*')
            ->where('hd_spk_bandsaw.hd_spk_bandsaw_id', '=', $id)->first();

            // dd($model);

       
        if ($model === null) {
            abort('404');
        }

        // $query = DB::table('dt_spk_bandsaw')
        //     ->select('dt_spk_bandsaw.dt_spk_bandsaw_id', 'mst_jenis_kayu.jenis_kayu', 'tally_table.tally_no', 'mst_deskripsi_tallysheet.nama_deskripsi', 'dt_spk_bandsaw.tally_id', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 'dt_isi_tally.panjang', 'dt_isi_tally.pcs', 'dt_isi_tally.volume')
        //     ->where('hd_spk_bandsaw_id', '=', $id)
        //     ->where('dt_spk_bandsaw.status', '=', true)
        //     ->where('dt_isi_tally.status', '=', true)
        //     ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_spk_bandsaw.isi_tally_id')
        //     ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
        //     ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
        //     ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_tally.deskripsi_id')

        //     ->orderBy('tally_table.tally_no');

        // >select('tally_table.tally_id', 'tally_table.tally_no')
        //     ->where('tally_table.status', '=', true)
        //     ->whereNotIn('tally_table.tally_id', \DB::table('dt_lpb')
        //         ->select('dt_lpb.tally_id')
        //         // ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'tally_table.tally_id')
        //         ->groupBy('dt_lpb.tally_id'))
        //     ->orderBy('tally_table.tally_no')
        //     ->get();

        // $test = DB::table('tally_table')
        //     ->select('tally_table.tally_id', 'tally_table.tally_no')
        //     ->where('tally_table.status', '=', true)
       
        //     ->whereNotIn('tally_table.tally_id', \DB::table('dt_spk_bandsaw')
        //         ->select('dt_spk_bandsaw.tally_id')
        //         // ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'tally_table.tally_id')
        //         ->groupBy('dt_spk_bandsaw.tally_id'))
        //     ->orderBy('tally_table.tally_no')
        //     ->get();

        //     dd($test);



        $querys = DB::table('dt_spk_bandsaw')
            ->select('tally_table.tally_no', 'dt_spk_bandsaw.dt_spk_bandsaw_id', 'dt_spk_bandsaw.tally_id')
            ->where('hd_spk_bandsaw_id', '=', $id)
        
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_spk_bandsaw.tally_id')

            ->orderBy('tally_table.tally_no');

        // dd($querys); 

        return view('spk-produksi/spk-bandsaw.addtally', [
            "model"         => $model,
        
            "models"         => $querys->paginate(40),
            "tallyOptions"   => TallyService::getActiveRm(),
            // "desOptions"   => DesService::getActiveDes(),
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function exportpdf(Request $request, $id)
    {

        $model = DB::table('hd_spk_bandsaw')
            ->select('hd_spk_bandsaw.*')
            ->where('hd_spk_bandsaw.hd_spk_bandsaw_id', '=', $id)->first();


        //  dd($model);


        // $queryss = DB::table('dt_spk_bandsaw')
        // ->select('dt_spk_bandsaw.tally_id', 'tally_table.tally_no', 'mst_jenis_kayu.jenis_kayu',
        // 'dt_isi_tally.panjang', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar',
        // 'dt_isi_tally.pcs','dt_isi_tally.volume')
        // ->where('dt_spk_bandsaw.hd_spk_bandsaw_id', '=', $id)
      
        // ->join ('tally_table', 'tally_table.tally_id', '=', 'dt_spk_bandsaw.tally_id' )
        // ->join ('dt_isi_tally', 'dt_isi_tally.tally_id', '=', 'tally_table.tally_id')
        // ->leftjoin ('dt_isi_planbandsaw', 'dt_isi_planbandsaw.tally_id', '=', 'tally_table.tally_id')
        // ->join ('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', 'dt_isi_tally.jenis_kayu_id')
        // ->get();

        // $querysd =  DB::table('tally_table')
        // ->select('tally_table.tally_no')
        // ->where('')
       
        // $querysd = DB::table('tally_table')
        // ->select('tally_table.tally_no')
        // ->where('dt_spk_bandsaw.hd_spk_bandsaw_id', '=', $id)
        // ->join('dt_spk_bandsaw','dt_spk_bandsaw.tally_id', '=', 'tally_table.tally_id')
        // ->join('dt_isi_tally', 'dt_isi_tally.tally_id', 'tally_table.tally_id')
       
        // ->get();


        $querys=DB::table('tally_table')
        ->select('tally_table.tally_no',)
        ->where('dt_spk_bandsaw.hd_spk_bandsaw_id', '=', $id)
        ->join('dt_spk_bandsaw','dt_spk_bandsaw.tally_id', '=', 'tally_table.tally_id')
        ->get();


        // $this->ids = $querys->tally_id;

        // dd($this->ids);
        $querysd = DB::table('dt_isi_tally')
        ->select('tally_table.tally_no','dt_isi_tally.*','mst_jenis_kayu.jenis_kayu')
        ->where('dt_spk_bandsaw.hd_spk_bandsaw_id', '=', $id)
        ->join ('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
        ->join('dt_spk_bandsaw','dt_spk_bandsaw.tally_id', '=', 'tally_table.tally_id')
        ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')->get();

        //   dd($querysd);

        // $querysd = DB::table('tally_table')
        // ->select('tally_table.tally_no',  'dt_isi_tally.tinggi','dt_isi_tally.lebar', 'dt_isi_tally.panjang', 
        // 'dt_isi_tally.pcs', 'dt_isi_tally.volume','dt_isi_planbandsaw.tinggi as t', 'dt_isi_planbandsaw.lebar as l', 
        // 'dt_isi_planbandsaw.panjang as p', 'dt_isi_planbandsaw.pcs as cs', 'dt_isi_planbandsaw.volume as v')
        // ->where('dt_spk_bandsaw.hd_spk_bandsaw_id', '=', $id)
        // ->join('dt_spk_bandsaw','dt_spk_bandsaw.tally_id', '=', 'tally_table.tally_id')
        // ->join('dt_isi_tally', 'dt_isi_tally.tally_id', 'tally_table.tally_id')
        // ->join('dt_isi_planbandsaw', 'dt_isi_planbandsaw.tally_id', 'tally_table.tally_id')
        // ->get();
    // dd($querysd);

         $queryawal = DB::table('tally_table')
        ->select('tally_table.tally_no',  'dt_isi_tally.tinggi','dt_isi_tally.lebar', 'dt_isi_tally.panjang', 'mst_jenis_kayu.jenis_kayu',
        'dt_isi_tally.pcs', 'dt_isi_tally.volume')
        ->where('dt_spk_bandsaw.hd_spk_bandsaw_id', '=', $id)
  
        ->join('dt_spk_bandsaw','dt_spk_bandsaw.tally_id', '=', 'tally_table.tally_id')
        ->join('dt_isi_tally', 'dt_isi_tally.tally_id', 'tally_table.tally_id')
        ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
        ->get();
    //  dd($queryawal);

        $queryplanning = DB::table('tally_table')
        ->select('tally_table.tally_no', 'dt_isi_planbandsaw.tinggi', 'dt_isi_planbandsaw.lebar', 
        'dt_isi_planbandsaw.panjang', 'dt_isi_planbandsaw.pcs', 'dt_isi_planbandsaw.volume')
        ->where('dt_spk_bandsaw.hd_spk_bandsaw_id', '=', $id)
        ->join('dt_spk_bandsaw','dt_spk_bandsaw.tally_id', '=', 'tally_table.tally_id')
        ->join('dt_isi_planbandsaw', 'dt_isi_planbandsaw.tally_id', 'tally_table.tally_id')
        ->get();

        $sumplanning = DB::table('tally_table')
        ->select(DB::raw('SUM(dt_isi_planbandsaw.pcs) AS sumpcs'),DB::raw('SUM(dt_isi_planbandsaw.volume) AS sumvolume'))
        ->where('dt_spk_bandsaw.hd_spk_bandsaw_id', '=', $id)
        ->join('dt_spk_bandsaw','dt_spk_bandsaw.tally_id', '=', 'tally_table.tally_id')
        ->join('dt_isi_planbandsaw', 'dt_isi_planbandsaw.tally_id', 'tally_table.tally_id')->get();

        $sumquerysd = DB::table('dt_isi_tally')
        ->select(DB::raw('SUM(dt_isi_tally.pcs) AS sumpcs'),DB::raw('SUM(dt_isi_tally.volume) AS sumvolume'))
        ->where('dt_spk_bandsaw.hd_spk_bandsaw_id', '=', $id)
        ->join ('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
        ->join('dt_spk_bandsaw','dt_spk_bandsaw.tally_id', '=', 'tally_table.tally_id')
        ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')->get();


        // $sumplanning = DB::table('tally_table')
        // ->where('dt_spk_bandsaw.hd_spk_bandsaw_id', '=', $id)
        // ->join('dt_spk_bandsaw','dt_spk_bandsaw.tally_id', '=', 'tally_table.tally_id')
        // ->join('dt_isi_planbandsaw', 'dt_isi_planbandsaw.tally_id', 'tally_table.tally_id')
        // ->sum('dt_isi_planbandsaw.pcs', 'dt_isi_planbandsaw.volume');
   
        // dd($sumplanning);
    // dd($queryplanning);


        // dd($model);

        $now    = new \DateTime();
        $now = date_format($now, 'd/m/Y h:i:s');
        // $model =  HdLpb::find($id);


        $header = view('spk-produksi/spk-bandsaw.header-pdf', [
            'title' => trans('menu.lpb-pdf-report') . "#" . $now,
            "model"         => $model,
            // "supplierOptions" => SupplierService::getActiveSupplier(),
        ])->render();
        \PDF::setHeaderCallback(function ($pdf) use ($header) {
            $pdf->writeHTML($header);
        });


        // $footer = view('produksi/lpb/setup-lpb.footer-pdf', [
        //     'title' => trans('menu.lpb-pdf-report')."#".$now,
        //     "model"         => $model,
        //     // "supplierOptions" => SupplierService::getActiveSupplier(),
        // ])->render();
        // \PDF::setFooterCallback(function($pdf) use ($footer) {
        //     $pdf->writeHTML($footer);
        // });

        // Custom Footer
        \PDF::setFooterCallback(function ($pdf) {

            // Position at 15 mm from bottom
            $pdf->SetY(-15);
            // Set font
            $pdf->SetFont('helvetica', 'I', 8);
            
            // Page number
            $pdf->Cell(0, 5, 'FR/09/09/SUB - 25/08/2016 - 00 -  ' . $pdf->getAliasNumPage() . '/' . $pdf->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
        });

        $html = view('spk-produksi/spk-bandsaw.spk-pdf', [
            "queryawal"         => $queryawal,
            "querys"         => $querys,
            "querysd"         => $querysd,
            "queryplanning"         => $queryplanning,
            "sumplanning"   => $sumplanning,
            "sumquerysd"       =>     $sumquerysd,
            "model"         => $model,
            "supplierOptions" => SupplierService::getActiveSupplier(),
            "woodOptions"   => WoodService::getActiveWood(),


        ])->render();

        $pdf = new TCPDF();
        $pdf::SetTitle('LAPORAN PENERIMAAN BAHAN SAWN TIMBER-' . '-' . $now);
        ob_end_clean();

        $pdf::SetTopMargin(40);

        // $pdf::SetTopMargin(41);

        $pdf::AddPage('P');
        $pdf::writeHTML($html, true, false, true, false, '');
        // $pdf::setPageFormat( $orientation 'P' );
        $pdf::Output('LAPORAN PENERIMAAN BAHAN SAWN TIMBER-'  . '-' . $now . '.pdf');
        // $pdf::Output($query->container_number.' '.$query->created_date.'.pdf');
        // $pdf->SetFont('Helvetica', '', 9);



    }



    public function addplan(Request $request, $id)
    {
        // dd('test');
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        // $model = LPB::find($id);
       
        $models = DB::table('dt_spk_bandsaw')
            ->select('dt_spk_bandsaw.*', 'tally_table.tally_no')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_spk_bandsaw.tally_id')
            ->where('dt_spk_bandsaw.tally_id', '=', $id)->first();



        $mode = DB::table('dt_isi_tally')
            ->select('mst_jenis_kayu.jenis_kayu', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 
            'dt_isi_tally.panjang', 'dt_isi_tally.pcs', 'dt_isi_tally.volume')
            ->where('dt_isi_tally.tally_id', '=', $id)
            // ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', 'hd_lpb.lokasi_kedatangan_id')
            // ->join('dt_spk_bandsaw', 'dt_spk_bandsaw.hd_spk_bandsaw_id', '=', 'hd_spk_bandsaw.hd_spk_bandsaw_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
            
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
            ->orderBy('tally_table.tally_id')->get();

            //  dd($mode);

        $querys = DB::table('dt_isi_planbandsaw')
            ->select('tally_table.tally_no', 'dt_isi_planbandsaw.isi_planbansaw_id','mst_jenis_kayu.jenis_kayu',  
            'dt_isi_planbandsaw.tinggi', 
            'dt_isi_planbandsaw.lebar', 'dt_isi_planbandsaw.panjang', 'dt_isi_planbandsaw.pcs', 
            'dt_isi_planbandsaw.volume', 'dt_isi_planbandsaw.created_date')
            ->where('dt_isi_planbandsaw.status', '=', true)
            ->where('dt_isi_planbandsaw.tally_id', '=', $id)
            // ->join('dt_lpb', 'dt_lpb.dt_lpb_id', '=', 'dt_isi_bandsaw.dt_lpb_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_planbandsaw.tally_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_planbandsaw.jenis_kayu_id')
        
            // ->orderBy('hd_lpb.created_by');
            ->orderBy('dt_isi_planbandsaw.created_date')->get();

            
            //  dd($querys);
            //  dd($mode);

       
        if ($models === null) {
            abort('404');
        }

       

        return view('spk-produksi/spk-bandsaw.addplan', [
            "models"         => $models,
            "mode"          => $mode,
             "model"         => $querys,
            "tallyOptions"   => TallyService::getActiveRm(),
            "woodOptions"   => WoodService::getActiveWood(),
            // "desOptions"   => DesService::getActiveDes(),
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function saveplan(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        $id     = intval($request->get('isi_bandsaw_id', 0));

        $this->validate($request, [
            'tally_id'            => 'required|integer',
            'jenis_kayu_id'       => 'required|integer',
            'tinggi'              => 'required|integer',
            'lebar'               => 'required|integer',
            'panjang'             => 'required|integer',
            'pcs'                 => 'required|integer',

        ]);

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            if (empty($id)) {
                $model  = new IsiPlanBandsaw();
                $model->created_by      = \Auth::user()->id;
                $model->created_date    = $now;
            } else {
                $model  = IsiPlanBandsaw::find($id);
                $model->modified_by   = \Auth::user()->id;
                $model->modified_date = $now;
            }

            $model->tally_id   = $request->get('tally_id');
            $model->jenis_kayu_id   = $request->get('jenis_kayu_id');
            $model->tinggi   = $request->get('tinggi');
            $model->lebar   = $request->get('lebar');
            $model->panjang   = $request->get('panjang');
            $model->pcs   = $request->get('pcs');
            $model->volume   = ($request->get('tinggi') * $request->get('panjang') * $request->get('lebar') * $request->get('pcs')) / 1000000000;
  




            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('fields.add-bandsaw') . ' ' . ''])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('fields.add-bandsaw') . ' ' . ''])
            );
        }

        return redirect(route('spk-bandsaw-addplan', ['id' => $request->get('tally_id')]));
    }



    public function adds(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        // $model = Tally::find($id);
        $model = DB::table('tally_table')
            ->select('tally_table.tally_id', 'tally_table.tally_no')
            ->where('dt_lpb.status', '=', true)
            ->where('dt_lpb.tally_id', '=', $id)
            ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'tally_table.tally_id')
            ->groupBy('tally_table.tally_id', 'tally_table.tally_no')
            ->orderBy('tally_no')->first();

        $mode = DB::table('hd_lpb')
            ->select('mst_jenis_kayu.jenis_kayu', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 'dt_isi_tally.panjang', 'dt_isi_tally.pcs', 'dt_isi_tally.volume')

            ->where('dt_lpb.tally_id', '=', $id)
            // ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', 'hd_lpb.lokasi_kedatangan_id')
            ->join('dt_lpb', 'dt_lpb.hd_lpb_id', '=', 'hd_lpb.hd_lpb_id')
            ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
            ->orderBy('tally_table.tally_id')->get();


        if ($model === null) {
            abort('404');
        }



        $query = DB::table('dt_isi_bandsaw')
            ->select('tally_table.tally_no', 'dt_isi_bandsaw.bandsaw_date', 'dt_isi_bandsaw.isi_bandsaw_id', 'dt_isi_bandsaw.jenis_kayu_id', 'dt_isi_bandsaw.deskripsi_id', 'mst_jenis_kayu.jenis_kayu', 'mst_deskripsi_tallysheet.nama_deskripsi', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'dt_isi_bandsaw.pcs', 'dt_isi_bandsaw.volume', 'dt_isi_bandsaw.created_date')
            ->where('dt_isi_bandsaw.status', '=', true)
            ->where('dt_isi_bandsaw.tally_id', '=', $id)
            // ->join('dt_lpb', 'dt_lpb.dt_lpb_id', '=', 'dt_isi_bandsaw.dt_lpb_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')
            ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')
            // ->orderBy('hd_lpb.created_by');
            ->orderBy('dt_isi_bandsaw.created_date');

        // dd($query);


        return view('produksi/output-kd/bandsaw-kering.add', [
            "tal"         => $model,
            "mode"          => $mode,
            "models"         => $query->paginate(10),
            "woodOptions"   => WoodService::getActiveWood(),
            "desOptions"   => DesService::getActiveDes(),
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }


    public function saveaddtally(Request $request)
    {
        // dd('test');
        if (!\Gate::allows('access', [self::RESOURCE, 'add']) && !\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $this->validate($request, [
            'tally_id'            => 'required|integer',
            'hd_spk_bandsaw_id'           => 'required|integer',

        ]);
            

        $tally_id       = $request->get('tally_id');
        $hd_spk_bandsaw_id      = $request->get('hd_spk_bandsaw_id');

        DB::beginTransaction();
        try {
                $now    = new \DateTime();
                $model  = new SPKBandsawDetail();
                $model->created_by      = \Auth::user()->id;
                $model->created_date    = $now;
                $model->hd_spk_bandsaw_id       = $hd_spk_bandsaw_id;
                $model->tally_id        = $tally_id;

                $model->status          = TRUE;

                // Save untuk add tally

                    
                $model->save();
      

                    // dd(  $model->save());

            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('fields.add-tally-sheet') . ' ' . $tally_id])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('fields.add-tally-sheet') . ' ' . $e->getmessage()])
            );
        }

        return redirect(route('spk-bandsaw-addtally', ['id' => $hd_spk_bandsaw_id]));
        
    }


    public function destroy(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $isi = LPBDetail::find($id);
        $direct = $isi->hd_spk_bandsaw_id;

        if ($isi === null) {
            abort('404');
        }
        // dd($isi->);

        // $isi =  LPBDetail::where('dt_spk_bandsaw_id',$id)->delete();



        DB::beginTransaction();
        try {

            $isi->delete();
            DB::commit();



            $request->session()->flash(
                'successMessage',
                trans('message.delete-message', ['variable' => trans('fields.notally') . ' ' . $id])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.faildel-message', ['variable' => trans('fields.notally') . ' ' . $id])
            );
        }

        return redirect(route('tally-lpb-add', ['id' => $direct]));
    }

    

    public function importexcel(Request $request)
    {

        //if ($request->Request('file')) {
        $model  = new HdLpb();
        $now    = new \DateTime();



        //$model->modified_date = $now;
        $path = $request->file('file')->getRealPath();
        $data = EXCEL::load($path, function ($reader) { })->get();

        if (!empty($data) && $data->count()) {
            foreach ($data as $key => $value) {
                $modal = new HdLpb();
                $modal->supplier_id = $value->supplier_id;
                $modal->lokasi_kedatangan_id = $value->lokasi_kedatangan_id;
                $modal->nomor_lpb = $value->nomor_lpb;
                $modal->nomor_po = $value->nomor_po;
                $modal->nomor_sj = $value->nomor_sj;
                $modal->tanggal_kedatangan = $value->tanggal_kedatangan;
                $modal->plat_mobil = $value->plat_mobil;
                $modal->created_by = $value->created_by;
                $modal->created_date    =  $value->created_date;
                $modal->save();
            }
        }

        return back();
    }

    public function importexcelisi(Request $request)
    {

        //if ($request->Request('file')) {
        $model  = new LPBDetail();
        $now    = new \DateTime();



        //$model->modified_date = $now;
        $path = $request->file('file')->getRealPath();
        $data = EXCEL::load($path, function ($reader) { })->get();

        if (!empty($data) && $data->count()) {
            foreach ($data as $key => $value) {
                $modal = new LPBDetail();
                $modal->hd_spk_bandsaw_id = $value->hd_spk_bandsaw_id;
                $modal->tally_id = $value->tally_id;
                $modal->isi_tally_id = $value->isi_tally_id;
                $modal->harga_id = $value->harga_id;
                $modal->harga_satuan = $value->harga_satuan;
                $modal->total_harga = $value->total_harga;
                $modal->status = $value->status;
                $modal->created_by = $value->created_by;
                $modal->created_date    =  $value->created_date;
                $modal->save();
            }
        }

        return back();
    }

    public function saveeditprice(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $id     = intval($request->get('dt_spk_bandsaw_id', 0));

        $this->validate($request, [
            'harga'            => 'required|integer',
            'pembayaran'       => 'required',
            'note'             => 'required|string',

        ]);

        // dd($id);

        DB::beginTransaction();
        try {

            $data = DB::table('dt_spk_bandsaw')
                ->select('dt_isi_tally.pcs', 'dt_isi_tally.volume')
                ->where('dt_spk_bandsaw.dt_spk_bandsaw_id', '=', $id)
                ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_spk_bandsaw.isi_tally_id')->first();

            // dd($data);

            $now    = new \DateTime();
            if (empty($id)) {
                $model  = new LPBDetail();
                $model->created_by      = \Auth::user()->id;
                $model->created_date    = $now;
            } else {
                $model  = LPBDetail::find($id);
                $model->modified_by   = \Auth::user()->id;
                $model->modified_date = $now;
                $model->harga_satuan = $request->get('harga');
                $model->note = $request->get('note');
            }
            if ($request->get('pembayaran') == 0) {
                $model->total_harga_edit        = $data->volume * $model->harga_satuan;
            } elseif ($request->get('pembayaran') == 1) {
                $model->total_harga_edit        = $data->pcs * $model->harga_satuan;
            }

            // $model->total_harga   = $request->get('total_harga');

            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => 'Data'])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => 'Data'])
            );
        }

        return redirect(route('tally-lpb-add', ['id' => $model->hd_spk_bandsaw_id]));
    }



  
    public function reportlpbexcel(Request $request, $id)
    {

        $this->id = $id;
        ob_end_clean();
        ob_start();
        Excel::create('Export data', function ($excel) {

            $excel->sheet('Sheet 1', function ($sheet) {

                $model = \DB::table('hd_spk_bandsaw')
                    // ->select('hd_spk_bandsaw.*', 'mst_supplier.supplier_name', 'mst_supplier.pic')
                    ->select('hd_spk_bandsaw.hd_spk_bandsaw_id')
                    ->where('hd_spk_bandsaw.hd_spk_bandsaw_id', '=', $this->id)
                    ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_spk_bandsaw.supplier_id')
                    ->orderBy('hd_spk_bandsaw.hd_spk_bandsaw_id')->get();

                // dd($model);

                // $model = \DB::table('hd_spk_bandsaw')
                //     ->select('hd_spk_bandsaw.*', 'mst_supplier.supplier_name', 'mst_supplier.pic')
                //     ->where('hd_spk_bandsaw.hd_spk_bandsaw_id', '=', $this->id)
                //     ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_spk_bandsaw.supplier_id')
                //     ->orderBy('hd_spk_bandsaw.hd_spk_bandsaw_id')->first();

                $isi = DB::table('dt_spk_bandsaw')
                    ->select(
                        'dt_spk_bandsaw.dt_spk_bandsaw_id',
                        'mst_jenis_kayu.jenis_kayu',
                        'tally_table.tally_no',
                        'mst_deskripsi_tallysheet.nama_deskripsi',
                        'dt_spk_bandsaw.tally_id',
                        'dt_isi_tally.tinggi',
                        'dt_isi_tally.lebar',
                        'dt_isi_tally.panjang',
                        'dt_isi_tally.pcs',
                        'dt_isi_tally.volume',
                        'mst_harga.harga',
                        'dt_spk_bandsaw.total_harga',
                        'dt_spk_bandsaw.harga_satuan',
                        'dt_spk_bandsaw.note',
                        'dt_spk_bandsaw.total_harga_edit'
                    )
                    ->where('hd_spk_bandsaw_id', '=', $this->id)
                    ->where('dt_spk_bandsaw.status', '=', true)
                    ->where('dt_isi_tally.status', '=', true)
                    ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_spk_bandsaw.isi_tally_id')
                    ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
                    ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
                    ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_tally.deskripsi_id')
                    ->join('mst_harga', 'mst_harga.harga_id', '=', 'dt_spk_bandsaw.harga_id')
                    ->orderBy('tally_table.tally_no')->get();

                foreach ($model as $product) {
                    foreach ($isi as $isi) {


                        $data[] = array(
                            // $product->,
                            $isi->jenis_kayu,
                            $isi->nama_deskripsi,
                            $isi->tally_no,
                            $isi->tinggi,
                            $isi->lebar,
                            $isi->panjang,
                            $isi->pcs,
                            $isi->volume,
                            $isi->harga,

                        );
                    }
                }
                $sheet->fromArray($data, null, 'A1', false, false);
                $headings = array('jenis_kayu', 'nama_deskripsi', 'tally_no', 'tinggi', 'lebar', 'panjang', 'pcs', 'volume', 'Satuan Harga');
                $sheet->prependRow(1, $headings);
            });


            $query = DB::table('dt_isi_afkir')
                ->select('dt_isi_afkir.*', 'mst_jenis_kayu.jenis_kayu')
                ->where('hd_spk_bandsaw_id', '=', $this->id)
                ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_afkir.jenis_kayu_id')
                ->orderBy('dt_isi_afkir.created_by')->get();




            $excel->sheet('Data Afkir LPB', function ($sheet) {

                $model = \DB::table('hd_spk_bandsaw')
                    // ->select('hd_spk_bandsaw.*', 'mst_supplier.supplier_name', 'mst_supplier.pic')
                    ->select('hd_spk_bandsaw.hd_spk_bandsaw_id')
                    ->where('hd_spk_bandsaw.hd_spk_bandsaw_id', '=', $this->id)
                    ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_spk_bandsaw.supplier_id')
                    ->orderBy('hd_spk_bandsaw.hd_spk_bandsaw_id')->get();

                // dd($model);

                // $model = \DB::table('hd_spk_bandsaw')
                //     ->select('hd_spk_bandsaw.*', 'mst_supplier.supplier_name', 'mst_supplier.pic')
                //     ->where('hd_spk_bandsaw.hd_spk_bandsaw_id', '=', $this->id)
                //     ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_spk_bandsaw.supplier_id')
                //     ->orderBy('hd_spk_bandsaw.hd_spk_bandsaw_id')->first();



                $query = DB::table('dt_isi_afkir')
                    ->select('dt_isi_afkir.*', 'mst_jenis_kayu.jenis_kayu')
                    ->where('hd_spk_bandsaw_id', '=', $this->id)
                    ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_afkir.jenis_kayu_id')
                    ->orderBy('dt_isi_afkir.created_by')->get();

                foreach ($model as $product) {
                    foreach ($query as $isi) {
                        $data[] = array(
                            // $product->,
                            $isi->jenis_kayu,
                            $isi->tinggi,
                            $isi->lebar,
                            $isi->panjang,
                            $isi->pcs,
                            $isi->volume,





                        );
                    }
                }




                $sheet->fromArray($data, null, 'A1', false, false);
                $headings = array('jenis_kayu', 'tinggi', 'lebar', 'panjang', 'pcs', 'volume');
                $sheet->prependRow(1, $headings);
            });
        })->export('xlsx');
    }




    public function reportlpbpdf(Request $request, $id)
    {

        $model = \DB::table('hd_spk_bandsaw')
            ->select('hd_spk_bandsaw.*', 'mst_supplier.supplier_name', 'mst_supplier.pic')
            ->where('hd_spk_bandsaw.hd_spk_bandsaw_id', '=', $id)
            ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_spk_bandsaw.supplier_id')
            ->orderBy('hd_spk_bandsaw.hd_spk_bandsaw_id')->first();



        $isi = DB::table('dt_spk_bandsaw')
            ->select('dt_spk_bandsaw.dt_spk_bandsaw_id', 'mst_jenis_kayu.jenis_kayu', 'tally_table.tally_no', 'mst_deskripsi_tallysheet.nama_deskripsi', 'dt_spk_bandsaw.tally_id', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 'dt_isi_tally.panjang', 'dt_isi_tally.pcs', 'dt_isi_tally.volume', 'mst_harga.harga', 'dt_spk_bandsaw.total_harga', 'dt_spk_bandsaw.harga_satuan', 'dt_spk_bandsaw.note', 'dt_spk_bandsaw.total_harga_edit')
            ->where('hd_spk_bandsaw_id', '=', $id)
            ->where('dt_spk_bandsaw.status', '=', true)
            ->where('dt_isi_tally.status', '=', true)
            ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_spk_bandsaw.isi_tally_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
            ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_tally.deskripsi_id')
            ->join('mst_harga', 'mst_harga.harga_id', '=', 'dt_spk_bandsaw.harga_id')
            ->orderBy('dt_isi_tally.tinggi')->get();


        $total = DB::table('dt_spk_bandsaw')
            ->select('mst_jenis_kayu.jenis_kayu', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 'dt_isi_tally.panjang', DB::raw('sum(dt_isi_tally.pcs) as pcs, sum(dt_isi_tally.volume) as volume')) //, 'dt_isi_tally.volume'
            ->where('hd_spk_bandsaw_id', '=', $id)
            ->where('dt_spk_bandsaw.status', '=', true)
            ->where('dt_isi_tally.status', '=', true)
            ->groupBy('mst_jenis_kayu.jenis_kayu', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 'dt_isi_tally.panjang')
            ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_spk_bandsaw.isi_tally_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
            // ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_tally.deskripsi_id')
            ->join('mst_harga', 'mst_harga.harga_id', '=', 'dt_spk_bandsaw.harga_id')
            ->orderBy('dt_spk_bandsaw.created_by')->get();

        $jumlah = DB::table('dt_spk_bandsaw')
            ->select('mst_jenis_kayu.jenis_kayu', 'mst_deskripsi_tallysheet.nama_deskripsi', DB::raw('sum(dt_isi_tally.pcs) as pcs, sum(dt_isi_tally.volume) as volume'))
            ->where('hd_spk_bandsaw_id', '=', $id)
            ->where('dt_spk_bandsaw.status', '=', true)
            ->where('dt_isi_tally.status', '=', true)
            ->groupBy('mst_jenis_kayu.jenis_kayu', 'mst_deskripsi_tallysheet.nama_deskripsi')
            ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_spk_bandsaw.isi_tally_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
            ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_tally.deskripsi_id')
            ->orderBy('dt_spk_bandsaw.created_by')->get();


        // dd($jumlah);

        $afkir = DB::table('dt_isi_afkir')
            ->select('dt_isi_afkir.*', 'mst_jenis_kayu.jenis_kayu')
            ->where('hd_spk_bandsaw_id', '=', $id)
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_afkir.jenis_kayu_id')
            ->orderBy('dt_isi_afkir.created_by')->get();


        // dd($model);

        $now    = new \DateTime();
        $now = date_format($now, 'd/m/Y h:i:s');
        // $model =  HdLpb::find($id);


        $header = view('produksi/lpb/setup-lpb.header-pdf', [
            'title' => trans('menu.lpb-pdf-report') . "#" . $now,
            "model"         => $model,
            // "supplierOptions" => SupplierService::getActiveSupplier(),
        ])->render();
        \PDF::setHeaderCallback(function ($pdf) use ($header) {
            $pdf->writeHTML($header);
        });


        // $footer = view('produksi/lpb/setup-lpb.footer-pdf', [
        //     'title' => trans('menu.lpb-pdf-report')."#".$now,
        //     "model"         => $model,
        //     // "supplierOptions" => SupplierService::getActiveSupplier(),
        // ])->render();
        // \PDF::setFooterCallback(function($pdf) use ($footer) {
        //     $pdf->writeHTML($footer);
        // });

        // Custom Footer
        \PDF::setFooterCallback(function ($pdf) {

            // Position at 15 mm from bottom
            $pdf->SetY(-15);
            // Set font
            $pdf->SetFont('helvetica', 'I', 8);
            // Page number
            $pdf->Cell(0, 5, 'FR/09/09/SUB - 25/08/2016 - 00 -  ' . $pdf->getAliasNumPage() . '/' . $pdf->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
        });

        $html = view('produksi/lpb/setup-lpb.lpb-pdf-report', [
            "isi"         => $isi,
            "total"         => $total,
            "afkir"       => $afkir,
            "jumlah"        => $jumlah,
            "supplierOptions" => SupplierService::getActiveSupplier(),
            "woodOptions"   => WoodService::getActiveWood(),


        ])->render();

        $pdf = new TCPDF();
        $pdf::SetTitle('LAPORAN PENERIMAAN BAHAN SAWN TIMBER-' . $model->nomor_lpb . '-' . $now);
        ob_end_clean();

        $pdf::SetTopMargin(41);

        $pdf::SetTopMargin(41);

        $pdf::AddPage();
        $pdf::writeHTML($html, true, false, true, false, '');
        $pdf::Output('LAPORAN PENERIMAAN BAHAN SAWN TIMBER-' . $model->nomor_lpb . '-' . $now . '.pdf');
        // $pdf::Output($query->container_number.' '.$query->created_date.'.pdf');
        // $pdf->SetFont('Helvetica', '', 9);



    }


    public function reportlpb2pdf(Request $request, $id)
    {

        $model = \DB::table('hd_spk_bandsaw')
            ->select('hd_spk_bandsaw.*', 'mst_supplier.supplier_name', 'mst_supplier.pic')
            ->where('hd_spk_bandsaw.hd_spk_bandsaw_id', '=', $id)

            ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_spk_bandsaw.supplier_id')
            ->orderBy('hd_spk_bandsaw.hd_spk_bandsaw_id')->first();



        $isi = DB::table('dt_spk_bandsaw')
            ->select('dt_spk_bandsaw.dt_spk_bandsaw_id', 'mst_jenis_kayu.jenis_kayu', 'tally_table.tally_no', 'mst_deskripsi_tallysheet.nama_deskripsi', 'dt_spk_bandsaw.tally_id', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 'dt_isi_tally.panjang', 'dt_isi_tally.pcs', 'dt_isi_tally.volume', 'mst_harga.harga', 'dt_spk_bandsaw.total_harga', 'dt_spk_bandsaw.harga_satuan', 'dt_spk_bandsaw.note', 'dt_spk_bandsaw.total_harga_edit')
            ->where('hd_spk_bandsaw_id', '=', $id)
            ->where('dt_spk_bandsaw.status', '=', true)
            ->where('dt_isi_tally.status', '=', true)
            ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_spk_bandsaw.isi_tally_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
            ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_tally.deskripsi_id')
            ->join('mst_harga', 'mst_harga.harga_id', '=', 'dt_spk_bandsaw.harga_id')
            ->orderBy('tally_table.tally_no')->get();


        $total = DB::table('dt_spk_bandsaw')
            ->select('mst_jenis_kayu.jenis_kayu', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 'dt_isi_tally.panjang', DB::raw('sum(dt_isi_tally.pcs) as pcs, sum(dt_isi_tally.volume) as volume')) //, 'dt_isi_tally.volume'
            ->where('hd_spk_bandsaw_id', '=', $id)
            ->where('dt_spk_bandsaw.status', '=', true)
            ->where('dt_isi_tally.status', '=', true)
            ->groupBy('mst_jenis_kayu.jenis_kayu', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 'dt_isi_tally.panjang')
            ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_spk_bandsaw.isi_tally_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
            // ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_tally.deskripsi_id')
            ->join('mst_harga', 'mst_harga.harga_id', '=', 'dt_spk_bandsaw.harga_id')
            ->orderBy('dt_spk_bandsaw.created_by')->get();

        $jumlah = DB::table('dt_spk_bandsaw')
            ->select('mst_jenis_kayu.jenis_kayu', 'mst_deskripsi_tallysheet.nama_deskripsi', DB::raw('sum(dt_isi_tally.pcs) as pcs, sum(dt_isi_tally.volume) as volume'))
            ->where('hd_spk_bandsaw_id', '=', $id)
            ->where('dt_spk_bandsaw.status', '=', true)
            ->where('dt_isi_tally.status', '=', true)
            ->groupBy('mst_jenis_kayu.jenis_kayu', 'mst_deskripsi_tallysheet.nama_deskripsi')
            ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_spk_bandsaw.isi_tally_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
            ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_tally.deskripsi_id')
            ->orderBy('dt_spk_bandsaw.created_by')->get();


        // dd($jumlah);

        $afkir = DB::table('dt_isi_afkir')
            ->select('dt_isi_afkir.*', 'mst_jenis_kayu.jenis_kayu')
            ->where('hd_spk_bandsaw_id', '=', $id)
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_afkir.jenis_kayu_id')
            ->orderBy('dt_isi_afkir.created_by')->get();


        // dd($model);

        $now    = new \DateTime();
        $now = date_format($now, 'd/m/Y h:i:s');
        // $model =  HdLpb::find($id);


        $header = view('produksi/lpb/setup-lpb.header-pdf', [
            'title' => trans('menu.lpb-pdf-report') . "#" . $now,
            "model"         => $model,
            // "supplierOptions" => SupplierService::getActiveSupplier(),
        ])->render();
        \PDF::setHeaderCallback(function ($pdf) use ($header) {
            $pdf->writeHTML($header);
        });


        // $footer = view('produksi/lpb/setup-lpb.footer-pdf', [
        //     'title' => trans('menu.lpb-pdf-report')."#".$now,
        //     "model"         => $model,
        //     // "supplierOptions" => SupplierService::getActiveSupplier(),
        // ])->render();
        // \PDF::setFooterCallback(function($pdf) use ($footer) {
        //     $pdf->writeHTML($footer);
        // });

        // Custom Footer
        \PDF::setFooterCallback(function ($pdf) {

            // Position at 15 mm from bottom
            $pdf->SetY(-15);
            // Set font
            $pdf->SetFont('helvetica', 'I', 8);
            // Page number
            $pdf->Cell(0, 5, 'FR/09/09/SUB - 25/08/2016 - 00 -  ' . $pdf->getAliasNumPage() . '/' . $pdf->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
        });

        $html = view('produksi/lpb/setup-lpb.lpb-pdf-report', [
            "isi"         => $isi,
            "total"         => $total,
            "afkir"       => $afkir,
            "jumlah"        => $jumlah,
            "supplierOptions" => SupplierService::getActiveSupplier(),
            "woodOptions"   => WoodService::getActiveWood(),


        ])->render();

        $pdf = new TCPDF();
        $pdf::SetTitle('LAPORAN PENERIMAAN BAHAN SAWN TIMBER-' . $model->nomor_lpb . '-' . $now);
        ob_end_clean();

        $pdf::SetTopMargin(41);

        $pdf::SetTopMargin(41);

        $pdf::AddPage();
        $pdf::writeHTML($html, true, false, true, false, '');
        $pdf::Output('LAPORAN PENERIMAAN BAHAN SAWN TIMBER-' . $model->nomor_lpb . '-' . $now . '.pdf');
        // $pdf::Output($query->container_number.' '.$query->created_date.'.pdf');
        // $pdf->SetFont('Helvetica', '', 9);



    }

    public function reportlpbpdfaccurate(Request $request, $id)
    {

        $model = \DB::table('hd_spk_bandsaw')
            ->select('hd_spk_bandsaw.*', 'mst_supplier.supplier_name', 'mst_supplier.pic')
            ->where('hd_spk_bandsaw.hd_spk_bandsaw_id', '=', $id)
            ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_spk_bandsaw.supplier_id')
            ->orderBy('hd_spk_bandsaw.hd_spk_bandsaw_id')->first();



        $total = DB::table('dt_spk_bandsaw')
            ->select('mst_jenis_kayu.jenis_kayu', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 'dt_isi_tally.panjang', 'dt_spk_bandsaw.harga_satuan', 'mst_harga.harga', DB::raw('sum(dt_isi_tally.pcs) as pcs, sum(dt_isi_tally.volume) as volume')) //, 'dt_isi_tally.volume'
            ->where('hd_spk_bandsaw_id', '=', $id)
            ->where('dt_spk_bandsaw.status', '=', true)
            ->where('dt_isi_tally.status', '=', true)
            ->groupBy('mst_jenis_kayu.jenis_kayu', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 'dt_isi_tally.panjang', 'dt_spk_bandsaw.harga_satuan', 'mst_harga.harga')
            ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_spk_bandsaw.isi_tally_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
            // ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_tally.deskripsi_id')
            ->join('mst_harga', 'mst_harga.harga_id', '=', 'dt_spk_bandsaw.harga_id')
            ->orderBy('dt_spk_bandsaw.created_date')->get();



        //dd($total);

        $now    = new \DateTime();
        $now = date_format($now, 'd/m/Y h:i:s');
        // $model =  HdLpb::find($id);


        $header = view('produksi/lpb/setup-lpb.header-pdf', [
            'title' => trans('menu.lpb-pdf-report') . "#" . $now,
            "model"         => $model,
            // "supplierOptions" => SupplierService::getActiveSupplier(),
        ])->render();
        \PDF::setHeaderCallback(function ($pdf) use ($header) {
            $pdf->writeHTML($header);
        });

        $html = view('produksi/lpb/setup-lpb.lpb-pdf-report-accurate', [

            "total"         => $total,

            "supplierOptions" => SupplierService::getActiveSupplier(),
            "woodOptions"   => WoodService::getActiveWood(),


        ])->render();

        $pdf = new TCPDF();
        $pdf::SetTitle('LAPORAN PENERIMAAN BAHAN SAWN TIMBER-' . $model->nomor_lpb . '-' . $now);
        ob_end_clean();

        $pdf::AddPage();
        $pdf::writeHTML($html, true, false, true, false, '');
        $pdf::Output('LAPORAN PENERIMAAN BAHAN SAWN TIMBER-' . $model->nomor_lpb . '-' . $now . '.pdf');

        // $pdf::Output($query->container_number.' '.$query->created_date.'.pdf');
        // $pdf->SetFont('Helvetica', '', 9);



    }



    public function Status(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $model = Status::find($id);

        if ($model === null) {
            abort('404');
        }

        return view('produksi/lpb/setup-lpb.add', [
            "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }

    public function edit(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }


        $model = LPB::find($id);

        if ($model === null) {
            abort('404');
        }

        // $query = DB::table('hd_spk_bandsaw')
        // ->select('hd_spk_bandsaw.*', 'tally_table.tally_no')
        //         ->join('tally_table', 'tally_table.tally_id', '=', 'hd_spk_bandsaw.tally_id')
        //      //   ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_spk_bandsaw.supplier_id')
        // ->orderBy('hd_spk_bandsaw.created_by');

        return view('produksi/lpb/setup-lpb.add', [
            "model"         => $model,
            // "models"         => $query->paginate(10),
            // "tallyOptions"   => TallyService::getActiveTally(),
            "supplierOptions" => SupplierService::getActiveSupplier(),
            "arrivallocationOptions"    => ArrivalLocationService::getActiveArrivalLocation(),
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }



    public function save(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add']) && !\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $id     = intval($request->get('id', 0));

        $this->validate($request, [
            
            'bandsaw_date' => 'required|date',



        ]);

        DB::beginTransaction();


        try {

            $now    = new \DateTime();
            if (empty($id)) {
                $model  = new HdSpkBandsaw();
                $model->created_by      = \Auth::user()->id;
                $model->created_date    = $now;
            } else {
                $model  = HdSpkBandsaw::find($id);
                $model->modified_by   = \Auth::user()->id;
                $model->modified_date = $now;
            }


         
            $model->bandsaw_date  = $request->get('bandsaw_date');
        //    dd($model->bandsaw_date);

            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('fields.add-lpb') . ' ' . ''])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('fields.add-lpb') . ' ' . ''])
            );
        }

        return redirect(route('spk-bandsaw-index'));
    }

    public function savedetail(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add']) && !\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $id     = intval($request->get('hd_spk_bandsaw_id', 0));

        $this->validate($request, [
            'hd_spk_bandsaw_id'            => 'required|integer',
            'tally_id'            => 'required|integer',
        ]);

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            if (empty($id)) {
                $model  = new LPBDetail();
                $model->created_by      = \Auth::user()->id;
                $model->created_date    = $now;
            } else {
                $model  = LPBDetail::find($id);
                $model->modified_by   = \Auth::user()->id;
                $model->modified_date = $now;
            }


            $model->nomor_lpb               = $request->get('nomor_lpb');
            $model->tanggal_kedatangan      = $request->get('tanggal_kedatangan');
            $model->supplier_id             = $request->get('supplier_id');
            $model->nomor_po                = $request->get('nomor_po');
            $model->nomor_sj                = $request->get('nomor_sj');
            $model->plat_mobil              = $request->get('plat_mobil');


            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('fields.add-lpb') . ' ' . ''])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('fields.add-lpb') . ' ' . ''])
            );
        }

        return redirect(route('setup-lpb-index'));
    }
}
