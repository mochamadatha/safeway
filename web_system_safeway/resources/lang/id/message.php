<?php

return [
    'saved-message'         => ':variable berhasil disimpan',
    'delete-message'     	=> ':variable berhasil dihapus',
    'generate-message'     	=> ':variable berhasil digenerate',
    'faildel-message'        => ':variable gagal dihapus',
    'failed-message'        => ':variable gagal disimpan',
    'import-message'     	=> ':variable berhasil di import',
    'uom-exist'				=> 'Satuan ini sudah ada!',
    'item-exist'		    => 'Barang ini sudah ada!',
    'item-required'		    => 'Masukkan minimal 1 barang!',
    'item-price-required'	=> 'Masukkan minimal 1 harga barang',
    'user-exist-this-role'	=> 'Tidak dapat menonaktifkan posisi, karena ada pengguna yang menggunakan posisi ini',
    'failed-generate'			=> 'Tally awal tidak boleh lebih besar dari tally akhir',

    'is-base-or-parent-base-required'		=> 'Jika bukan satuan dasar maka wajib diisi satuan rujukan, jika satuan dasar maka rujukan diabaikan',
    'ask-delete-tally'                      =>  'Apakah anda yakin ingin menghapus no tally',
];
