<?php 
// use App\Service\Master\SupplierService; 
// use App\Model\MasterProduksi\Supplier;
// use App\Model\Produksi\HdLpb;


list($titlefooter, $date) = explode("#", $title);
?>

<br/><br/>
<table id="header" width="100%" height="100%" border="1px">
	<tr>

		<th  width="100%"  align="center " style="font-size: 12px; font-weight: bold;" >LAPORAN AFKIR DETAIL</th>

	</tr>

	<tr>
		<td width="34%"   style="font-size: 9px; font-weight: bold;" >
			<table>
				<<!-- tr>
					<td width="30%"> TANGGAL</td>
					<td width="5%"> :</td>
					<td width="65%"> {{ $date }} </td>
				</tr> -->
			</table>
		</td>
	</tr>
    <tr>
		<td width="66%" colspan="2" rowspan="2"  style="font-size: 9px; font-weight: bold;" >
			<table>
				<tr>
					<td width="30%">Plat Nomor Afkir </td>
					<td width="5%"> : </td>
					<td width="65%">  {{ $model->plat_afkir}} </td>
				</tr>
			</table>
			<table>
				<tr>
					<td width="30%"></td>
					<td width="5%"> </td>
					<td width="65%">  </td>
				</tr>
			</table>
			<table>
				<tr>
					<td width="30%"> </td>
					<td width="5%"> </td>
					<td width="65%"><font size="7"> </font> </td>
				</tr>
			</table>
		</td>

		<td width="34%"  height="15" style="font-size: 9px; font-weight: bold;" >
			<table>
				<tr>
					<td width="50%"> Nomor Afkir</td>
					<td width="5%"> :</td>
					<td width="45%"> {{ $model->nomor_sj}}   </td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
	
		<td  width="34%" height="15" style="font-size: 9px; font-weight: bold;" >
			<table>
				<tr>
					<td width="50%"> Tanggal Ambil</td>
					<td width="5%"> :</td>
					<td width="45%">  {{ $model->tanggal_afkir}} </td>
				</tr>
			</table>
		</td>
	</tr>
	<tr  bgcolor="#008d95" style="color: white; font-weight: bold; font-size: 10px;" align="center">
	
		<td rowspan="2" width="5%" height="22">No</td>
			<td rowspan="2" width="30%" height="22">Jenis Kayu</td>
			<td colspan="3" width="33%" height="22">Ukuran</td>
			<td colspan="2" width="32%" height="22">Jumlah Diterima</td>

		
	</tr>
	<tr bgcolor="#008d95" style="color: white; font-weight: bold; font-size: 10px;" align="center">
		<td height="19">T</td>
		<td height="19">L</td>
		<td height="19">P</td>
		<td height="19">PCS</td>
		<td height="19">m3</td>
	</tr>
</table>
