<?php

namespace App\Model\MasterProduksi;

use Illuminate\Database\Eloquent\Model;

class DeskripsiTallySheet extends Model
{
    protected $table        = 'mst_deskripsi_tallysheet';
    protected $primaryKey   = 'deskripsi_id';
    public $timestamps      = false;
}
