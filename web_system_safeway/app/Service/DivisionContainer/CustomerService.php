<?php

namespace App\Service\DivisionContainer;

class CustomerService
{
    public static function getActiveCustomer()
    {
        return \DB::table('mst_Customer')
            ->where('status', '=', true)
            ->orderBy('nama_customer')
            ->get();
    }
}
