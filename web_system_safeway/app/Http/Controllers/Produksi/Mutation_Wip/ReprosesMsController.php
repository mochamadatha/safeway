<?php

namespace App\Http\Controllers\Produksi\Mutation_Wip;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Produksi\Tally;
use App\Model\Produksi\HasilReprosesMs;
use App\Model\Produksi\DtAmbilStockMs;
use App\Model\Produksi\HdLpb;
use App\Model\Produksi\LPBDetail;
use App\Service\Master\WoodService;
use DB;

class ReprosesMsController extends Controller
{
    const URL       = 'produksi/mutation_wip/reproses-ms';
    const RESOURCE  = 'reproses-ms';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        // $query = DB::table('dt_ambil_stockms')
        // ->select('dt_ambil_stockms.ambil_stockms_id', 'mst_jenis_kayu.jenis_kayu', 'dt_ambil_stockms.tinggi', 
        // 'dt_ambil_stockms.lebar', 'dt_ambil_stockms.panjang', 'dt_ambil_stockms.pcs', 
        // 'dt_ambil_stockms.volume')
        // ->where('dt_ambil_stockms.pcs', '!=', 0)
        // ->where('dt_hasil_reproses_ms.status', '=', true)
        // ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_ambil_stockms.jenis_kayu_id')
        // ->join('dt_hasil_reproses_ms', 'dt_hasil_reproses_ms.ambil_stockms_id', '=', 'dt_ambil_stockms.ambil_stockms_id')
        // ->groupBy('dt_ambil_stockms.ambil_stockms_id', 'mst_jenis_kayu.jenis_kayu',
        //  'dt_ambil_stockms.tinggi', 'dt_ambil_stockms.lebar', 'dt_ambil_stockms.panjang', 'dt_ambil_stockms.pcs', 'dt_ambil_stockms.volume')
        // ->orderBy('dt_ambil_stockms.ambil_stockms_id', 'desc')->get();

        $query = DB::table('dt_ambil_stockms')
            ->select('dt_ambil_stockms.ambil_stockms_id', 'mst_jenis_kayu.jenis_kayu', 'dt_ambil_stockms.tinggi', 'dt_ambil_stockms.lebar', 'dt_ambil_stockms.panjang', 'dt_ambil_stockms.pcs', 'dt_ambil_stockms.volume')
            ->where('dt_ambil_stockms.pcs', '!=', 0)
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_ambil_stockms.jenis_kayu_id')
            ->orderBy('dt_ambil_stockms.created_date', 'desc');



        //  dd($query);





        return view('produksi/mutation_wip/reproses-ms.index', [
            "models"        => $query->paginate(10),

            // "filters"       => $filters,
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }
}
