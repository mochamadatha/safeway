@extends('layouts.master')

@section('title', trans('menu.setup-woodtype'))

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
        	<div class="row">
				<div class="col-xs-12">
					<div class="page-title-box">
                        <h4 class="page-title">{{ trans('menu.setup-pallet-name') }}</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="{{ route('dashboard') }}">{{ trans('menu.dashboard') }}</a>
                            </li>
                            <li>
                                <a>{{ trans('menu.master-produksi') }}</a>
                            </li>
                            <li class="active">
                                {{ trans('menu.setup-pallet-name') }}
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
				</div>
			</div>
            <!-- end row -->
            
			<div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-border panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ trans('fields.add-pallet-name') }}</h3>
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal" role="form" method="post" action="{{ route('setup-pallet-name-save') }}">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="p-20">
                                            <input type="hidden" name="id" class="form-control" value="{{ count($errors) > 0 ? old('id') : $model->pallet_name_id}}">
                                             <div class="form-group {{ $errors->has('kode_pallet') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.kode_pallet') }}</label>
                                                <div class="col-md-10">
                                                    <input type="text" name="kode_pallet" class="form-control" value="{{ count($errors) > 0 ? old('kode_pallet') : $model->kode_pallet }}">
                                                    @if($errors->has('kode_pallet'))
                                                        <span class="help-block">{{ $errors->first('kode_pallet') }}</span>
                                                    @endif
                                                </div>
                                            </div>


                                          


                                            <div class="form-group {{ $errors->has('pallet_name') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.pallet_name') }}</label>
                                                <div class="col-md-10">
                                                    <input type="text" name="pallet_name" class="form-control" value="{{ count($errors) > 0 ? old('pallet_name') : $model->pallet_name }}">
                                                    @if($errors->has('pallet_name'))
                                                        <span class="help-block">{{ $errors->first('pallet_name') }}</span>
                                                    @endif
                                                </div>
                                            </div>

                                             
                                           
                                             <div class="form-group {{ $errors->has('tinggi') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.high') }}</label>
                                                <div class="col-md-10">
                                                    <input type="text" name="tinggi" class="form-control" value="{{ count($errors) > 0 ? old('tinggi') : $model->tinggi }}">
                                                    @if($errors->has('jenis_kayu'))
                                                        <span class="help-block">{{ $errors->first('tinggi') }}</span>
                                                    @endif
                                                </div>
                                            </div>

                                              <div class="form-group {{ $errors->has('lebar') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.width') }}</label>
                                                <div class="col-md-10">
                                                    <input type="text" name="lebar" class="form-control" value="{{ count($errors) > 0 ? old('lebar') : $model->lebar }}">
                                                    @if($errors->has('lebar'))
                                                        <span class="help-block">{{ $errors->first('lebar') }}</span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group {{ $errors->has('panjang') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.length') }}</label>
                                                <div class="col-md-10">
                                                    <input type="text" name="panjang" class="form-control" value="{{ count($errors) > 0 ? old('panjang') : $model->panjang }}">
                                                    @if($errors->has('panjang'))
                                                        <span class="help-block">{{ $errors->first('panjang') }}</span>
                                                    @endif
                                                </div>
                                            </div>



                                            <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <div class="checkbox checkbox-primary">
                                                        <?php $status = count($errors) > 0 ? old('status') : $model->status; ?>
                                                        <input id="status" name="status" value="true" type="checkbox" {{ $status == true ? 'checked' : '' }}>
                                                        <label for="status">
                                                            {{ trans('fields.is-active') }}
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 data-table-toolbar text-right" style="padding-top: 10px;">
                                        <div class="form-group">
                                            <a href="{{ url('master-produksi/pallet-name') }}" class="btn btn-sm btn-warning"><i class="fa fa-reply"></i> {{ trans('fields.cancel') }}</a>
                                            <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i> {{ trans('fields.save') }}</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                        <!-- end row -->
                </div> <!-- end card-box -->
            </div><!-- end col -->
        </div>
    </div>
</div>
@endsection