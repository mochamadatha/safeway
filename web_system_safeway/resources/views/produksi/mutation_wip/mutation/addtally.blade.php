@extends('layouts.master')

@section('title', trans('menu.mutation'))

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">{{ trans('menu.mutation') }}</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="{{ route('dashboard') }}">{{ trans('menu.dashboard') }}</a>
                            </li>
                            <li>
                                <a>{{ trans('menu.produksi') }}</a>
                            </li>
                            <li class="active">
                                {{ trans('menu.mutation') }}
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            @if(Session::has('successMessage'))
            <div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-check-all"></i>
                <strong>{{ Session::get('successMessage') }}.</strong>
            </div>
            @endif

            @if(Session::has('errorMessage'))
            <div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-block-helper"></i>
                <strong>{{ Session::get('errorMessage') }}.</strong>
            </div>
            @endif
            
            <div class="row">
                <div class=" col-md-12">
                    <div class="panel panel-border panel-success">
                        <div class="panel-heading">
                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <h3 class="panel-title">{{ trans('fields.nomor_dr') }}</h3>
                                </div>
                                <div class="col-md-3">
                                    <h3 class="panel-title">: {{ $model->nomor_mutasi }}</h3>
                                </div>
                                <div class="col-md-2">
                                    <h3 class="panel-title">{{ trans('fields.lokasi_mutasi') }}</h3>
                                </div>
                                 <div class="col-md-3">
                                    <h3 class="panel-title">: {{ $model->lokasi_mutasi }}</h3>
                                </div>
                                
                            </div><br>
                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <h3 class="panel-title">{{ trans('fields.date') }}</h3>
                                </div>
                                <div class="col-md-3">
                                    <h3 class="panel-title"> : {{ date('d-m-Y', strtotime($model->tanggal_mutasi)) }} </h3>
                                </div>
                                 <div class="col-md-2">
                                    <h3 class="panel-title">{{ trans('fields.note') }}</h3>
                                </div>
                                <div class="col-md-3">
                                       <h3 class="panel-title"> : {{ $model->catatan }} </h3>
                                </div>
                                 
                               
                            </div><br>
                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <h3 class="panel-title">{{ trans('fields.driver_plat') }}</h3>
                                </div>
                                <div class="col-md-3">
                                    <h3 class="panel-title"> : {{ $model->supir_plat }} </h3>
                                </div>
                                
                            </div>
                        </div>
                        <br>
                        

                        <div class="panel-body">
                            <div class="row">
                                <form class="form-horizontal" role="form" method="post" action="{{ route('tallysheet-save') }}">
                                    {{ csrf_field() }}
                                    <div class="col-sm-12 data-table-toolbar text-right">
                                        <div class="form-group">
                                            <button type="button" class="btn btn-primary waves-effect waves-light btn-sm" data-toggle="modal" data-target="#dtmutasi"><i class="fa fa-plus-circle"></i> {{ trans('fields.add-mutation') }} </button>
                                             <a id="clear-details" href="{{ url('produksi/mutation') }}" class="btn btn-sm btn-danger">
                                                <i class="fa fa-reply"></i> {{ trans('fields.back') }} 
                                            </a>
                                        </div>
                                        <div class="row">
                                            <table class="table table-bordered table-hover table-striped m-0">
                                                <thead>
                                                    <tr>
                                                        <th width="5%" class="text-center">{{ trans('fields.num') }}</th>
                                                         <th width="15%" class="text-center">{{ trans('fields.no-tally-sheet') }}</th>
                                                        <th width="15%" class="text-center">{{ trans('fields.wood-type') }}</th>
                                                        <th width="15%" class="text-center">{{ trans('fields.description-wood') }}</th>
                                                        <th width="5%" class="text-center">{{ trans('fields.t') }}</th>
                                                        <th width="5%" class="text-center">{{ trans('fields.l') }}</th>
                                                        <th width="5%" class="text-center">{{ trans('fields.p') }}</th>
                                                        <th width="10%" class="text-center">{{ trans('fields.pcs') }}</th>
                                                        <th width="10%" class="text-center">{{ trans('fields.m3') }}</th>
                                                    
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                <?php $no = ($models->currentPage() - 1) * $models->perPage() + 1; ?>
                                                @foreach($models as $isi)
                                                <tr>
                                                    <th scope="row" style="text-align: center;">{{ $no++ }}</th>
                                                    <td style="text-align: center;">{{ $isi->tally_no }}</td>
                                                    <td style="text-align: center;">{{ $isi->jenis_kayu }}</td>
                                              
                                                    <td style="text-align: center;">{{ $isi->nama_deskripsi }}</td>        
                                                    <td style="text-align: center;">{{ $isi->tinggi }}</td>
                                                    <td style="text-align: center;">{{ $isi->lebar }}</td>
                                                    <td style="text-align: center;">{{ $isi->panjang }}</td>
                                                    <td style="text-align: center;">{{ $isi->pcs }}</td>
                                                    <td style="text-align: center;">{{ $isi->volume }}</td>


                                                </tr>
                                                @endforeach
                                                </tbody>
                                                
                                            </table>
                                            <div class="data-table-toolbar pull-right">
                                                {!! $models->render() !!}
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div> 
                        </div>

                        <div id="dtmutasi" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form role="form" method="post" action="{{ route('mutation-save') }}">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="hd_mutasi_id" class="form-control" value="{{ $model->hd_mutasi_id }}">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">{{ trans('fields.add-mutation') }}</h4>
                                        </div>



                                        <div class="modal-body">

                                            <div class="row">
                                                  <div class="col-md-6">
                                                    <label>{{ trans('fields.date-mutation') }}</label>
                                                        <div>
                                                            <div class="form-group">
                                                                <input type="date" class="form-control" placeholder="mm/dd/yyyy" id="datepicker-autoclose" name="date_mutation_wip">
                            
                                                            </div><!-- input-group -->
                                                        </div>
                                                    </div>
                                                
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                     <div class="form-group">
                                                        <label for="label" class="control-label">{{ trans('fields.notally') }}</label>
                                                        <textarea class="form-control" rows="7" name="tally_no"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                       
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close
                                            </button>
                                            <button type="submit" class="btn btn-info waves-effect waves-light">Save changes</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div><!-- /.modal -->
                         
                    </div>


                </div>
            </div>
        </div>
        @endsection