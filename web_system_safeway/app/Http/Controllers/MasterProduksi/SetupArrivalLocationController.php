<?php

namespace App\Http\Controllers\MasterProduksi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\MasterProduksi\LokasiKedatangan;
//use Maatwebsite\Excel\Excel;

use Maatwebsite\Excel\Facades\Excel;
use DB;

class SetupArrivalLocationController extends Controller
{
    const URL       = 'master-produksi/setup-arrival-location';
    const RESOURCE  = 'setup-arrival-location';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        if ($request->isMethod('post')) {
            $request->session()->put('filters', $request->all());
            return redirect(self::URL . '?page=1');
        } elseif (empty($request->get('page'))) {
            $request->session()->forget('filters');
        }
        $filters = $request->session()->get('filters');
        $query = \DB::table('mst_lokasi_kedatangan')
            ->orderBy('mst_lokasi_kedatangan.lokasi_kedatangan');

        if (!empty($filters['status']) || !$request->session()->has('filters')) {
            $query->where('mst_lokasi_kedatangan.status', '=', true);
        } else {
            $query->where('mst_lokasi_kedatangan.status', '=', false);
        }

        if (!empty($filters['lokasi_kedatangan'])) {
            $query->where('mst_lokasi_kedatangan.lokasi_kedatangan', 'ilike', '%' . $filters['lokasi_kedatangan'] . '%');
        }

        return view('master-produksi/arrival-location.index', [
            "models"        => $query->paginate(10),
            "filters"       => $filters,
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function add(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        $model           = new LokasiKedatangan();
        $model->status   = true;

        return view('master-produksi/arrival-location.add', [
            "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }

    public function edit(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $model = LokasiKedatangan::find($id);

        if ($model === null) {
            abort('404');
        }

        return view('master-produksi/arrival-location.add', [
            "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }

    public function importexcel(Request $request)
    {

        //if ($request->Request('file')) {
        $model  = new LokasiKedatangan();
        $now    = new \DateTime();



        //$model->modified_date = $now;
        $path = $request->file('file')->getRealPath();
        $data = EXCEL::load($path, function ($reader) { })->get();

        if (!empty($data) && $data->count()) {
            foreach ($data as $key => $value) {
                $modal = new LokasiKedatangan();
                $modal->lokasi_kedatangan = $value->lokasi_kedatangan;
                $modal->created_by = \Auth::user()->id;
                $modal->created_date    =  $now;
                $modal->save();
            }
        }

        return back();
    }

    public function save(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add']) && !\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $id     = intval($request->get('id', 0));

        $this->validate($request, [
            'lokasi_kedatangan' => 'required|max:255|unique:mst_lokasi_kedatangan,lokasi_kedatangan,' . $id . ',lokasi_kedatangan_id',
        ]);

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            if (empty($id)) {
                $model  = new LokasiKedatangan();
                $model->created_by      = \Auth::user()->id;
                $model->created_date    = $now;
            } else {
                $model  = LokasiKedatangan::find($id);
                $model->modified_by   = \Auth::user()->id;
                $model->modified_date = $now;
            }

            $model->lokasi_kedatangan   = $request->get('lokasi_kedatangan');
            $model->status   = !empty($request->get('status')) ? true : false;

            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('menu.wood-type') . ' ' . $request->get('lokasi_kedatangan')])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('menu.wood-type') . ' ' . $request->get('lokasi_kedatangan')])
            );
        }

        return redirect(route('setup-arrival-location-index'));
    }
}
