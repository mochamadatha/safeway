<?php

return [
    'dashboard'        => 'Beranda',
    'master-role'      => 'Master Posisi',
    'setup-role'     => 'Setup Posisi',
    'master-user'      => 'Master Pengguna',
    'setup-status'   => 'Setup Status',
    'setup-user'     => 'Setup Pengguna',
    'student'          => 'Kesiswaan',
    'student-identity' => 'Biodata Siswa',
    'class-type'      => 'Jenis Kelas',
    'class'           => 'Kelas',
    'study-group'     => 'Rombel',
    'study-guardian'  => 'Wali Rombel',
    'master-item'           => 'Master Barang',
    'master-conversion'     => 'Master Konversi',
    'master-supplier'       => 'Master Pemasok',
    'item-stock'            => 'Stok Barang',
    'receipt-item'          => 'Terima Barang',
    'adjustment-stock'      => 'Penyesuaian Stock',

    'produksi'              => 'Produksi',
    'generate-notally'      => 'Generate No Tally',
    'print-notally'         => 'Print No Tally',
    'tallysheet'            => 'TallySheet',
    'report-lpb'            => 'Report LPB',

    'master-produksi'       => 'Master Produksi',
    'setup-woodtype'    => 'Setup Jenis Kayu',
    'wood-type'         => 'Jenis Kayu',
    'setup-description' =>  'Setup Deskripsi',
    'setup-supplier'    => 'Setup Supplier',
    'setup-price'       => 'Setup Harga Satuan',

    'master-hrd'            => 'Master HRD',
    'setup-division'    => 'Setup Divisi',

    'setup-branch'       => 'Setup Kantor Cabang',
    'setup-employee'     => 'Setup Pegawai',

    'setup-lpb'          =>  'Setup LPB',
    'setup-customer'     => 'Setup Customer',
    'setup-agent'        =>  'Setup Agent',
    'master-container'   => 'Master Container',
    'setup-depolocation'     =>  'Setup Depo Loc',
    'setup-inspect'      =>  'Setup Inspect',
    'setup-size'     =>  'Setup Size',

    //container
    'division-container'        => 'Division Container',
    'container-inspect'      => 'Container Inspect',
    'containerinspectionreport'  => 'Container Inspection Report',
    'add-afkir'      => 'Tambah Isi Afkir',

    'report-produksi'  => 'Report Produksi',
    'setup-arrival-location'   => 'Lokasi Kedatangan',

    'setup_chamber'    => 'Setup Chamber',
    'raw-material'   => 'Raw Material',
    'bandsaw'        =>  'Bandsaw',
    'analisa-afkir'  => 'Analisa Afkir',
    'input-kd'       => 'Data Dalam KD',
    'output-kd'      => 'Data Output KD',
    'bandsaw-and-afkir' => 'Bandsaw dan Afkir',
    'afkir-proses'         =>  'Afkir',
    'add-afkir-proses'         =>  'Tambah Afkir',
    'setup-mutation-location'  => 'Lokasi Mutasi',
    'mutation'       => 'Mutasi',
    'wip'          => 'WIP',
    'ms'           => 'Material Supply',
    'stock-ms'      => 'Stock MS',
    'stock-afkir'    => 'Stock Afkir',
    'afkir-kering'   => 'Afkir Kering',
    'reproses-afkir'   =>  "Reproses Afkir",
    'resultafkirreproses'    => "Hasil Reproses Afkir",
    'reproses-ms'        => 'Reproses MS',

    'resultmsreproses'   => 'Hasil Reproses MS',
    'add-result-reproses-ms' => 'Tambah Hasil Reproses MS',
    'setup-pallet-name'   =>  'Setup Pallet Name',
    'setup-afkir'         => 'Setup Afkir',
    'hasil-reprosesafkir' => 'Hasil Reproses Afkir',
    'hasil-reprosesms'    => 'Hasl Reproses MS',
    'raft-result'   => 'Hasil Rakit',
    'pallet-name'   => 'Pallet Name',
    'add-data-material-ms' => 'Tambah Data Material MS',
    'setup_location_outputkd'  => 'Setup Lokasi Output KD',
    'master-ispm'     =>  'Master ISPM',
    'division-ispm'    => 'Division ISPM',
    'ispm-inspect'   => 'ISPM Inspect',
    'container_stuffing_report' => 'CONTAINER STUFFING REPORT',
    'bandsaw-dry'          => 'Bandsaw Kering',
    'afkirproses-kering'  => 'Afkir Kering',
    'report_produksi'   =>  'Report Produksi',
    'search-all-data'      =>  'Search All Data',
    'search-data-bandsaw'  => 'Search Data Bandsaw',
    'search-data-afkir'    => 'Search Data Afkir',
    'search-lpb'        => 'Search LPB',
    'spk-bandsaw'       => 'SPK Bandsaw',
    'spk'       => 'SPK'



];
