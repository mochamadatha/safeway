@extends('layouts.master')

@section('title', trans('menu.generate-notally'))

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">{{ trans('menu.search-data-afkir') }}</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="{{ route('dashboard') }}">{{ trans('menu.dashboard') }}</a>
                            </li>
                            <li>
                                <a>{{ trans('menu.produksi') }}</a>
                            </li>
                            <li class="active">
                                {{ trans('menu.search-data-afkir') }}
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            @if(Session::has('successMessage'))
            <div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-check-all"></i>
                <strong>{{ Session::get('successMessage') }}.</strong>
            </div>
            @endif

            @if(Session::has('errorMessage'))
            <div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-block-helper"></i>
                <strong>{{ Session::get('errorMessage') }}.</strong>
            </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-border panel-success">
                        <div class="panel-heading">
                        </div>

                        <div class="panel-body">
                            <div class="row">
                                <form class="form-horizontal" role="form" method="post" action="">
                                    {{ csrf_field() }}
                                    <div class="col-md-12">
                                        <p class="text-muted font-14 m-b-20 pull-right">
                                            @can('access', [$resource, 'add'])
                                            <a href="{{ route('data-afkir-export-excel') }}" class="btn btn-primary waves-effect w-md waves-light"><i class="fa fa-plus"></i> {{ trans('fields.export-excel') }}</a>
                                            @endcan

                                            <div class="col-md-2">
                                                <select class="form-control" id="status" name="status">
                                                    <option value="">{{ trans('fields.choose-filter-all') }}</option>
                                                    <option value="basah" {{ !empty($filters['status']) && $filters['status'] == 'basah' ? 'selected' : ''}} >Raw Material</option>
                                                    <option value="kering" {{ !empty($filters['status']) && $filters['status'] == 'kering' ? 'selected' : ''}} >Output KD</option>
                                                    <option value="wip" {{ !empty($filters['status']) && $filters['status'] == 'wip' ? 'selected' : ''}} >WIP</option>
                                                    <option value="all" {{ !empty($filters['status']) && $filters['status'] == 'all' ? 'selected' : ''}} >All</option>
                                                </select>
                                                @if($errors->has('status'))
                                                    <span class="help-block">{{ $errors->first('status') }}</span>
                                                @endif
                                            </div>

                                            

                                            <div class="col-md-3">
                                                <input type="date" name="start_date" class="form-control" value="{{ !empty($filters['start_date']) ? $filters['start_date'] : '' }}">
                                                @if($errors->has('start_date'))
                                                <span class="help-block">{{ $errors->first('start_date') }}</span>
                                                @endif
                                            </div>

                                            <div class="col-md-3">
                                                <input type="date" name="end_date" class="form-control" value="{{ !empty($filters['end_date']) ? $filters['end_date'] : '' }}">
                                                @if($errors->has('end_date'))
                                                <span class="help-block">{{ $errors->first('end_date') }}</span>
                                                @endif
                                            </div>

                                            <div class="col-md-2">
                                                <button type="submit" class="btn btn-info waves-effect w-xs waves-light"><i class="fa fa-search"></i> </button>
                                            </div>
                                        </p>
                                    </div>
                                </form>
                            </div>


                            <div class="row">
                                <table class="table table-bordered table-hover table-striped m-0">
                                    <thead>
                                        <tr>
                                            <th width="5%" class="text-center">{{ trans('fields.num') }}</th>
                                            <th width="11%" class="text-center">{{ trans('fields.tally_no') }}</th>
                                            <th width="11%" class="text-center">{{ trans('fields.afkir_date') }}</th>
                                            <th width="12%" class="text-center">{{ trans('fields.wood-type') }}</th>
                    
                                            <th width="10%" class="text-center">{{ trans('fields.high') }}</th>
                                            <th width="10%" class="text-center">{{ trans('fields.width') }}</th>
                                            <th width="10%" class="text-center">{{ trans('fields.length') }}</th>
                                            <th width="10%" class="text-center">{{ trans('fields.pcs') }}</th>
                                            <th width="10%" class="text-center">{{ trans('fields.m3') }}</th>
                                            <th width="10%" class="text-center">{{ trans('fields.keterangan') }}</th>
                                            <th width="10%" class="text-center">{{ trans('fields.analisa-afkir') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = ($models->currentPage() - 1) * $models->perPage() + 1; ?>
                                        @foreach($models as $model)
                                        <tr>
                                            <th scope="row" style="text-align: center;">{{ $no++ }}</th>
                                            <td class="text-center">{{ $model->tally_no }}</td>
                                            <td class="text-center">{{ date('d-m-Y', strtotime($model->created_date)) }}</td>
                                            <td class="text-center">{{ $model->jenis_kayu }}</td>
                                            <td class="text-center">{{ $model->tinggi }}</td>
                                            <td class="text-center">{{ $model->lebar}}</td>
                                            <td class="text-center">{{ $model->panjang }}</td>
                                            <td class="text-center">{{ $model->pcs }}</td>
                                            <td class="text-center">{{ $model->volume }}</td>
                                            <td class="text-center">{{ $model->keterangan }}</td>
                                            <td class="text-center">{{ $model->analisa_afkir }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="data-table-toolbar">
                                    {!! $models->render() !!}
                                </div>
                            </div>

                            <div id="dtexcel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form role="form" method="post" action="{{ route('data-afkir-export-excel') }}" method="post" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">{{ trans('fields.add-tally') }}</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label for="file" class="col-md-3 control-label">Import</label>
                                                <div class="col-md-6">
                                                    <input type="file" id="file" name="file" class="form-control" autorequired>
                                                    <span class="help-block with-error"></span>
                                                </div>
                                            </div>


                                                <div class="form-group">
                                                    <div class="col-md-3 col-md-offset-3">
                                                        <a href="{{ route('generate-notally-export-excel') }}" class="btn btn-success">Export</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><br>


                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div id="dtisi" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form role="form" method="post" action="{{ route('generate-notally-import-excelisi') }}" method="post" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">{{ trans('fields.add-tally') }}</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label for="file" class="col-md-3 control-label">Import</label>
                                                <div class="col-md-6">
                                                    <input type="file" id="file" name="file" class="form-control" autorequired>
                                                    <span class="help-block with-error"></span>
                                                </div>
                                            </div>
                                        </div><br>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
