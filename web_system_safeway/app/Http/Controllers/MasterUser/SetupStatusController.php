<?php

namespace App\Http\Controllers\MasterUser;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Master\Status;
use DB;

class SetupStatusController extends Controller
{
    const URL       = 'master-user/setup-status';
    const RESOURCE  = 'setup-status';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        } 

        if ($request->isMethod('post')) {
            $request->session()->put('filters', $request->all());
            return redirect(self::URL.'?page=1');
        } elseif (empty($request->get('page'))) {
            $request->session()->forget('filters');
        }
        $filters = $request->session()->get('filters');
        $query = \DB::table('mst_user_status')
                    ->orderBy('mst_user_status.nama_status');

        if (!empty($filters['status']) || !$request->session()->has('filters')) {
            $query->where('mst_user_status.status', '=', true);
        } else {
            $query->where('mst_user_status.status', '=', false);
        }

        if (!empty($filters['nama_status'])) {
            $query->where('mst_user_status.nama_status', 'like', '%'.$filters['nama_status'].'%');
        }

        return view('master-user/setup-status.index',[
            "models"        => $query->paginate(10),
            "filters"       => $filters,
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function add(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        } 

        $model           = new Status();
        $model->status   = true;
        
        return view('master-user/setup-status.add',[
            "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }

    public function edit(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        } 

        $model = Status::find($id);

        if ($model===null) {
            abort('404');
        }
        
        return view('master-user/setup-status.add',[
            "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }

    public function save(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add']) && !\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        } 

        $id     = intval($request->get('id', 0));

        $this->validate($request, [
            'nama_status' => 'required|max:255|unique:mst_user_status,nama_status,'.$id.',status_id',
        ]);

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            if(empty($id)){
                $model  = new Status();
                $model->created_by      = \Auth::user()->id;
                $model->created_date    = $now;
            }else{
                $model  = Status::find($id);
                $model->modified_by   = \Auth::user()->id;
                $model->modified_date = $now;
            }

            $model->nama_status   = $request->get('nama_status');
            $model->status   = !empty($request->get('status')) ? true : false;
            
            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('menu.setup-role').' '.$request->get('nama_status')])
                );

        } catch (\Exception $e) {
             DB::rollback();
             $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('menu.setup-role').' '.$request->get('nama_status')])
                );
         }

        return redirect(route('setup-status-index'));
    }
}
