<?php

namespace App\Http\Controllers\Produksi\Mutation_Wip;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Produksi\Tally;
use App\Model\Produksi\HasilReprosesAfkir;
use App\Model\Produksi\DtAmbilStockAfkir;
use App\Model\Produksi\HdLpb;
use App\Model\Produksi\LPBDetail;
use App\Service\Master\WoodService;
use DB;

class ReprosesAfkirController extends Controller
{
    const URL       = 'produksi/mutation_wip/reproses-afkir';
    const RESOURCE  = 'reproses-afkir';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }


        $query = DB::table('dt_ambil_stockafkir')
            ->select(
                'dt_ambil_stockafkir.ambil_stockafkir_id',
                'mst_jenis_kayu.jenis_kayu',
                'dt_ambil_stockafkir.tinggi',
                'dt_ambil_stockafkir.lebar',
                'dt_ambil_stockafkir.panjang',
                'dt_ambil_stockafkir.pcs',
                'dt_ambil_stockafkir.volume'
               
            )
            ->where('dt_ambil_stockafkir.pcs', '!=', 0)
            ->where('dt_ambil_stockafkir.status', '=', true)
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_ambil_stockafkir.jenis_kayu_id')
           
            ->groupBy(
                'dt_ambil_stockafkir.ambil_stockafkir_id',
                'mst_jenis_kayu.jenis_kayu',
                'dt_ambil_stockafkir.tinggi',
                'dt_ambil_stockafkir.lebar',
                'dt_ambil_stockafkir.panjang',
                'dt_ambil_stockafkir.pcs',
                'dt_ambil_stockafkir.volume'
            )
            ->orderBy('dt_ambil_stockafkir.pcs', 'desc');

        //   dd($query);



        return view('produksi/mutation_wip/reproses-afkir.index', [
            "models"        => $query->paginate(10),

            // "filters"       => $filters,
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }
}
