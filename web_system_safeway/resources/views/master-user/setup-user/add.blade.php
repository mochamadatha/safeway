@extends('layouts.master')

@section('title', trans('menu.setup-user'))

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
        	<div class="row">
				<div class="col-xs-12">
					<div class="page-title-box">
                        <h4 class="page-title">{{ trans('menu.setup-user') }}</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="{{ route('dashboard') }}">{{ trans('menu.dashboard') }}</a>
                            </li>
                            <li>
                                <a>{{ trans('menu.master-user') }}</a>
                            </li>
                            <li class="active">
                                {{ trans('menu.setup-user') }}
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
				</div>
			</div>
            <!-- end row -->
            
			<div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-border panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ trans('fields.add-user') }}</h3>
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal" role="form" method="post" action="{{ route('setup-user-save') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="p-20">
                                            <input type="hidden" name="id" class="form-control" value="{{ count($errors) > 0 ? old('id') : $model->id }}">
                                            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.name') }}</label>
                                                <div class="col-md-10">
                                                    <input type="text" name="name" class="form-control" value="{{ count($errors) > 0 ? old('name') : $model->name }}">
                                                    @if($errors->has('name'))
                                                        <span class="help-block">{{ $errors->first('name') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group {{ $errors->has('username') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.username') }}</label>
                                                <div class="col-md-10">
                                                    <input type="text" name="username" class="form-control" value="{{ count($errors) > 0 ? old('username') : $model->username }}" {{ $model->id == null ? ' ' : ' readonly' }}>
                                                    @if($errors->has('username'))
                                                        <span class="help-block">{{ $errors->first('username') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.email') }}</label>
                                                <div class="col-md-10">
                                                    <input type="text" name="email" class="form-control" value="{{ count($errors) > 0 ? old('email') : $model->email }}">
                                                    @if($errors->has('email'))
                                                        <span class="help-block">{{ $errors->first('email') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.password') }}</label>
                                                <div class="col-md-10">
                                                    <input type="password" name="password" class="form-control" value="{{ count($errors) > 0 ? old('password') : '' }}">
                                                    @if($errors->has('password'))
                                                        <span class="help-block">{{ $errors->first('password') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group {{ $errors->has('foto') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.foto') }}</label>
                                                <div class="col-md-10">
                                                    <input type="file" id="foto" name="foto" style="display:none">
                                                    <div class="btn btn-photo well text-center" style="padding: 5px; margin: 0px;">
                                                        @if(!empty($model->foto))
                                                        <img height="150" src="{{ asset(Config::get('app.paths.foto-user').'/'.$model->foto) }}"/><span></span>
                                                        @else
                                                        <img height="150" hidden/><span>{{ trans('fields.choose-file') }}</span>
                                                        @endif
                                                    </div>
                                                    @if($errors->has('foto'))
                                                        <span class="help-block">{{ $errors->first('foto') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group {{ $errors->has('role') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.role') }}</label>
                                                <div class="col-md-10">
                                                    <select class="form-control" id="role" name="role">
                                                    <?php $roleId = count($errors) > 0 ? old('role') : $model->role_id; ?>
                                                        <option value="">{{ trans('fields.please-select') }}</option>
                                                    @foreach($roleOptions as $role)
                                                        <option value="{{ $role->role_id }}" {{ $role->role_id == $roleId ? 'selected' : '' }}>{{ $role->nama_role }}</option>
                                                    @endforeach
                                                    </select>
                                                    @if($errors->has('role'))
                                                        <span class="help-block">{{ $errors->first('role') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group {{ $errors->has('status_user') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.status') }}</label>
                                                <div class="col-md-10">
                                                    <select class="form-control" id="status_user" name="status_user">
                                                    <?php $statusId = count($errors) > 0 ? old('status_user') : $model->status_id; ?>
                                                        <option value="">{{ trans('fields.please-select') }}</option>
                                                    @foreach($statusOptions as $status_user)
                                                        <option value="{{ $status_user->status_id }}" {{ $status_user->status_id == $statusId ? 'selected' : '' }}>{{ $status_user->nama_status }}</option>
                                                    @endforeach
                                                    </select>
                                                    @if($errors->has('status_user'))
                                                        <span class="help-block">{{ $errors->first('status_user') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.is-active') }}</label>
                                                <div class="col-sm-10">
                                                    <div class="checkbox checkbox-primary">
                                                        <?php $status = count($errors) > 0 ? old('status') : $model->status; ?>
                                                        <input id="status" name="status" value="true" type="checkbox" {{ $status == true ? 'checked' : '' }}>
                                                        <label for="status">
                                                            {{ trans('fields.yes') }}
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 data-table-toolbar text-right">
                                        <div class="form-group">
                                            <a href="{{ url('master-user/setup-user') }}" class="btn btn-sm btn-warning"><i class="fa fa-reply"></i> {{ trans('fields.cancel') }}</a>
                                            <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i> {{ trans('fields.save') }}</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- end row -->
                    </div> <!-- end card-box -->
                </div><!-- end col -->
            </div>
	   </div>
    </div>
</div>
@endsection

@section('script')
@parent
<script type="text/javascript">
$(document).on('ready', function() {
    $('.btn-photo').on('click', function(){
        $(this).parent().find('input[type="file"]').click();
    });

    $("#foto").on('change', function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            var $img    = $(this).parent().find('img');
            var $span   = $(this).parent().find('span');
            reader.onload = function (e) {
                $img.attr('src', e.target.result);
                $img.show();
                $span.hide();
            }
            reader.readAsDataURL(this.files[0]);
        }
    });
});
</script>
@endsection