<?php

namespace App\Http\Controllers\Produksi\RawMaterial;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Model\Produksi\LPB;
use App\Model\Produksi\LPBDetail;
use App\Service\Master\TallyService;
use App\Model\MasterProduksi\Supplier;
use App\Service\Master\SupplierService;
use App\Model\Produksi\HdLpb;
use App\Model\Produksi\IsiBandsaw;
use Elibyy\TCPDF\Facades\TCPDF;
use App\Model\Produksi\IsiAfkir;
use App\Service\Master\WoodService;
use App\Model\MasterProduksi\LokasiKedatangan;
use App\Service\Master\ArrivalLocationService;
use Maatwebsite\Excel\Facades\Excel;
use App\Model\Produksi\Tally;
use App\Model\Produksi\IsiTally;
use DB;

class RawMaterialController extends Controller
{
    const URL       = 'produksi/raw-material';
    const RESOURCE  = 'raw-material';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        if ($request->isMethod('post')) {
            $request->session()->put('filters', $request->all());
            return redirect(self::URL . '?page=1');
        } elseif (empty($request->get('page'))) {
            $request->session()->forget('filters');
        }

        $page = Input::get('page', 1);
        $paginate = 10;

        $info = DB::table('hd_lpb')
        ->select(
            'mst_jenis_kayu.jenis_kayu',
            'dt_isi_tally.tinggi',
            'dt_isi_tally.lebar',
            'dt_isi_tally.panjang', 
            DB::raw('SUM(dt_isi_tally.pcs) as pcs'),
            DB::raw('SUM(dt_isi_tally.volume) as volume')
        )
        ->where('hd_lpb.status', '=', true)
        ->where('tally_table.date_in_kd', '=', NULL)
        ->where('tally_table.date_out_kd', '=', NULL)
        ->where('dt_isi_tally.status', '=', true)
        ->where('tally_table.date_mutation_wip', '=', NULL)
        ->whereNotIn('dt_lpb.tally_id',  DB::table('dt_isi_bandsaw')
            ->select('tally_id')
            ->groupBy('tally_id'))
        // ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', 'hd_lpb.lokasi_kedatangan_id')
        ->join('dt_lpb', 'dt_lpb.hd_lpb_id', '=', 'hd_lpb.hd_lpb_id')
        ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
        ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
        ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
        ->groupBy('mst_jenis_kayu.jenis_kayu', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 'dt_isi_tally.panjang');



    $info2 = DB::table('dt_isi_bandsaw')
        ->select('mst_jenis_kayu.jenis_kayu', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', DB::raw('SUM(dt_isi_bandsaw.pcs) as pcs'), DB::raw('SUM(dt_isi_bandsaw.volume) as volume'))
        ->where('dt_isi_bandsaw.status', '=', true)
        ->where('tally_table.date_in_kd', '=', NULL)
        ->where('tally_table.date_out_kd', '=', NULL)
        ->where('tally_table.date_mutation_wip', '=', NULL)
        // ->where('tally_table.date_in_kd', '=', null)
        // ->where('dt_isi_bandsaw.tally_id', '=', $id)
        ->whereIn('dt_isi_bandsaw.tally_id', DB::table('tally_table')
            ->select('tally_table.tally_id')
            ->where('tally_table.date_in_kd', '=', NULL)

            ->join('dt_isi_bandsaw', 'dt_isi_bandsaw.tally_id', '=', 'tally_table.tally_id')
            ->groupBy('tally_id'))
        // ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'dt_isi_bandsaw.tally_id')
        // ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
        ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
        // ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=' ,'hd_lpb.lokasi_kedatangan_id')
        ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')
        // ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')
        ->join('mst_lokasi_outputkd', 'mst_lokasi_outputkd.lokasi_outputkd_id', '=', 'tally_table.lokasi_outputkd_id')
        ->groupBy('mst_jenis_kayu.jenis_kayu', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang')
        ->union($info)
        ->orderBy('jenis_kayu')->get();


        // $info = DB::table('dt_lpb')
        //    ->select('tally_table.tally_no', 'mst_jenis_kayu.jenis_kayu', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 'dt_isi_tally.panjang', 'dt_isi_tally.pcs', 'dt_isi_tally.volume', 'mst_lokasi_kedatangan.lokasi_kedatangan', 'hd_lpb.tanggal_kedatangan')
        //        ->where('dt_lpb.status', '=', true)
        //        ->where('tally_table.date_in_kd', '=', null)
        //         ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')

        //        ->whereNotIn('dt_lpb.tally_id',  DB::table('dt_isi_bandsaw')
        //                ->select('tally_id')
        //                ->groupBy('tally_id'))
        //        ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
        //        ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=' ,'hd_lpb.lokasi_kedatangan_id')
        //        ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
        //        ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
        //        ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
        //        // ->orderBy('hd_lpb.created_by');
        //        ->orderBy('dt_lpb.created_date', 'desc');

        // $filters = $request->session()->get('filters');
        // if (!empty($filters['tally_no'])) {
        //     $query->where('tally_table.tally_no', 'like', '%'.$filters['tally_no'].'%');
        // }



        // $info2 = DB::table('dt_isi_bandsaw')
        // ->select('tally_table.tally_no', 'mst_jenis_kayu.jenis_kayu', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'dt_isi_bandsaw.pcs', 'dt_isi_bandsaw.volume', 'mst_lokasi_kedatangan.lokasi_kedatangan', 'hd_lpb.tanggal_kedatangan')
        //     ->where('dt_isi_bandsaw.status', '=', true)
        //     ->where('tally_table.date_in_kd', '=', null)
        //      ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')
        //     // ->where('dt_isi_bandsaw.tally_id', '=', $id)
        //     ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'dt_isi_bandsaw.tally_id')
        //     ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
        //     ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
        //     ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=' ,'hd_lpb.lokasi_kedatangan_id')
        //     ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')
        //     ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')
        //     ->groupBy('tally_table.tally_no', 'mst_jenis_kayu.jenis_kayu', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'dt_isi_bandsaw.pcs', 'dt_isi_bandsaw.volume', 'mst_lokasi_kedatangan.lokasi_kedatangan', 'hd_lpb.tanggal_kedatangan')
        //     ->union($info)
        //     ->orderBy('tanggal_kedatangan')->get(); 

        //  dd($info2);

        

        // $info = DB::table('hd_lpb')
        //     ->select(
        //         'mst_jenis_kayu.jenis_kayu',
        //         'dt_isi_tally.isi_tally_id',
        //         'dt_isi_tally.tinggi',
        //         'dt_isi_tally.lebar',
        //         'dt_isi_tally.panjang',
        //         DB::raw('SUM(dt_isi_tally.pcs) as pcs'),
        //         DB::raw('SUM(dt_isi_tally.volume) as volume')
        //     )
        //     ->where('hd_lpb.status', '=', true)
        //     ->where('tally_table.date_in_kd', '=', NULL)
        //     ->where('dt_isi_tally.status', '=', true)
        //     ->where('tally_table.date_mutation_wip', '=', NULL)
        //     ->whereNotIn('dt_lpb.tally_id',  DB::table('dt_isi_bandsaw')
        //         ->select('tally_id')
        //         ->groupBy('tally_id'))
        //     // ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', 'hd_lpb.lokasi_kedatangan_id')
        //     ->join('dt_lpb', 'dt_lpb.hd_lpb_id', '=', 'hd_lpb.hd_lpb_id')
        //     ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
        //     ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
        //     ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')

        //     ->groupBy( 'dt_isi_tally.isi_tally_id','mst_jenis_kayu.jenis_kayu', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 'dt_isi_tally.panjang');



        // $info2 = DB::table('dt_isi_bandsaw')
        //     ->select('mst_jenis_kayu.jenis_kayu', 'dt_isi_bandsaw.tinggi','dt_isi_bandsaw.isi_bandsaw_id', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', DB::raw('SUM(dt_isi_bandsaw.pcs) as pcs'), DB::raw('SUM(dt_isi_bandsaw.volume) as volume'))
        //     ->where('dt_isi_bandsaw.status', '=', true)
        //     ->where('tally_table.date_in_kd', '=', null)
        //     ->where('tally_table.date_mutation_wip', '=', NULL)
        //     // ->where('tally_table.date_in_kd', '=', null)
        //     // ->where('dt_isi_bandsaw.tally_id', '=', $id)
        //     ->whereIn('dt_isi_bandsaw.tally_id', DB::table('tally_table')
        //         ->select('tally_table.tally_id')
        //         ->where('tally_table.date_in_kd', '=', NULL)

        //         ->join('dt_isi_bandsaw', 'dt_isi_bandsaw.tally_id', '=', 'tally_table.tally_id')
        //         ->groupBy('tally_id'))
        //     // ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'dt_isi_bandsaw.tally_id')
        //     // ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
        //     ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
        //     // ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=' ,'hd_lpb.lokasi_kedatangan_id')
        //     ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')
        //     // ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')

        //     ->groupBy('mst_jenis_kayu.jenis_kayu', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang','dt_isi_bandsaw.isi_bandsaw_id')
        //     ->union($info)
        //     ->orderBy('jenis_kayu')->get();



                //  dd($info2);

        $filters = $request->session()->get('filters');

        $query = DB::table('dt_lpb')
            ->select(
                'tally_table.tally_no',
                'dt_isi_tally.isi_tally_id',
                'mst_jenis_kayu.jenis_kayu',
                'dt_isi_tally.deskripsi_id',
                'dt_isi_tally.tinggi',
                'dt_isi_tally.lebar',
                'dt_isi_tally.panjang',
                'dt_isi_tally.pcs',
                'dt_isi_tally.volume',
                'mst_lokasi_kedatangan.lokasi_kedatangan',
                'hd_lpb.tanggal_kedatangan',
                'mst_supplier.supplier_name',
                DB::raw('"Non BS" status_data')
            )
            ->where('dt_lpb.status', '=', true)
            ->where('tally_table.date_in_kd', '=', null)
            ->where('tally_table.date_mutation_wip', '=', NULL)
            ->where('dt_isi_tally.status', '=', true)
            ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')
            ->where('tally_table.tally_no', 'like', '%' . $filters['tally_no'] . '%')
            ->whereNotIn('dt_lpb.tally_id',  DB::table('dt_isi_bandsaw')
                ->select('tally_id')
                ->groupBy('tally_id'))
            ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
            ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=', 'hd_lpb.lokasi_kedatangan_id')
            ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_lpb.supplier_id')
            ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')

            // ->orderBy('hd_lpb.created_by');
            ->orderBy('tally_table.tally_no', 'desc');

        // $filters = $request->session()->get('filters');
        // if (!empty($filters['tally_no'])) {
        //     $query->where('tally_table.tally_no', 'like', '%'.$filters['tally_no'].'%');
        // }
  


        $query2 = DB::table('dt_isi_bandsaw')
            ->select(
                'tally_table.tally_no',
                'dt_isi_bandsaw.isi_bandsaw_id',
                'mst_jenis_kayu.jenis_kayu',
                'dt_isi_bandsaw.deskripsi_id',
                'dt_isi_bandsaw.tinggi',
                'dt_isi_bandsaw.lebar',
                'dt_isi_bandsaw.panjang',
                'dt_isi_bandsaw.pcs',
                'dt_isi_bandsaw.volume',
                'mst_lokasi_kedatangan.lokasi_kedatangan',
                'hd_lpb.tanggal_kedatangan',
                'mst_supplier.supplier_name',
                DB::raw('"BS" status_data')
            )
            ->where('dt_isi_bandsaw.status', '=', true)
            ->where('tally_table.date_in_kd', '=', null)
            ->where('tally_table.date_mutation_wip', '=', NULL)
            ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')
            ->where('tally_table.tally_no', 'like', '%' . $filters['tally_no'] . '%')
            // ->where('dt_isi_bandsaw.tally_id', '=', $id)
            ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'dt_isi_bandsaw.tally_id')
            ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')

            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
            ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=', 'hd_lpb.lokasi_kedatangan_id')
            ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_lpb.supplier_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')
            ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')
            ->groupBy('tally_table.tally_no', 'dt_isi_bandsaw.isi_bandsaw_id', 'mst_jenis_kayu.jenis_kayu','dt_isi_bandsaw.deskripsi_id','dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'dt_isi_bandsaw.pcs', 'dt_isi_bandsaw.volume', 'mst_lokasi_kedatangan.lokasi_kedatangan', 'hd_lpb.tanggal_kedatangan', 'mst_supplier.supplier_name')
            ->union($query)
            ->orderBy('tally_no')->get();

            // dd($query2);


        $t_pcs = 0;
        $t_volume = 0;
        foreach ($query2 as $i) {
            $t_pcs += $i->pcs;
            $t_volume += $i->volume;
        }
        // if (!empty($filters['tally_no'])) {
        //     $query2->where('tally_table.tally_no', 'like', '%'.$filters['tally_no'].'%');
        // }

        $slice = array_slice($query2->toArray(), $paginate * ($page - 1), $paginate);
        $result = new LengthAwarePaginator($slice, count($query2), $paginate);
        // return View::make('produksi/mutation_wip/mutation.addtally');
        // dd(url()->current());
        $result = $result->setPath(url()->current());



        return view('produksi/rawmaterial.index', [

            "models"         => $result,
            "model"         => $query2,
            "info"         => $info2,
            "t_pcs"      => $t_pcs,
            "t_volume"      => $t_volume,
            // "modelk"         => $jumlah2,     

            // "models"         => $query2->paginate(10),
            // "tallyLPBOptions"   => TallyService::getTallyLPB(),
            // "desOptions"   => DesService::getActiveDes(),
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }


    public function exportexcel()
    {

        ob_end_clean();
        ob_start();
        Excel::create('Export data', function ($excel) {

            $excel->sheet('Sheet 1', function ($sheet) {

                // $query = DB::table('dt_lpb')
                //     ->select(
                //         'tally_table.tally_no',
                //         'dt_isi_tally.isi_tally_id',
                //         'mst_jenis_kayu.jenis_kayu',
                //         'dt_isi_tally.deskripsi_id',
                //         'dt_isi_tally.tinggi',
                //         'dt_isi_tally.lebar',
                //         'dt_isi_tally.panjang',
                //         'dt_isi_tally.pcs',
                //         'dt_isi_tally.volume',
                //         'mst_lokasi_kedatangan.lokasi_kedatangan',
                //         'hd_lpb.tanggal_kedatangan',
                //         'mst_supplier.supplier_name',
                //         DB::raw('"Non BS" status_data')
                //     )
                //     ->where('dt_lpb.status', '=', true)
                //     ->where('dt_isi_tally.status', '=', true)
                //     ->where('tally_table.date_in_kd', '=', null)
                //     ->where('tally_table.date_mutation_wip', '=', NULL)
                //     ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')

                //     ->whereNotIn('dt_lpb.tally_id',  DB::table('dt_isi_bandsaw')
                //         ->select('tally_id')
                //         ->groupBy('tally_id'))
                //     ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
                //     ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=', 'hd_lpb.lokasi_kedatangan_id')
                //     ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_lpb.supplier_id')
                //     ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
                //     ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
                //     ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')

                //     // ->orderBy('hd_lpb.created_by');
                //     ->orderBy('tally_table.tally_no', 'desc');

                // // $filters = $request->session()->get('filters');
                // // if (!empty($filters['tally_no'])) {
                // //     $query->where('tally_table.tally_no', 'like', '%'.$filters['tally_no'].'%');
                // // }

                // // dd($query);



                // $query2 = DB::table('dt_isi_bandsaw')
                //     ->select(
                //         'tally_table.tally_no',
                //         'mst_jenis_kayu.jenis_kayu',
                //         'dt_isi_bandsaw.isi_bandsaw_id',
                //         'dt_isi_bandsaw.deskripsi_id',
                //         'dt_isi_bandsaw.tinggi',
                //         'dt_isi_bandsaw.lebar',
                //         'dt_isi_bandsaw.panjang',
                //         'dt_isi_bandsaw.pcs',
                //         'dt_isi_bandsaw.volume',
                //         'mst_lokasi_kedatangan.lokasi_kedatangan',
                //         'hd_lpb.tanggal_kedatangan',
                //         'mst_supplier.supplier_name',
                //         DB::raw('"BS" status_data')
                //     )
                //     ->where('dt_isi_bandsaw.status', '=', true)
                //     ->where('tally_table.date_in_kd', '=', null)
                //     ->where('tally_table.date_mutation_wip', '=', NULL)
                //     ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')

                //     // ->where('dt_isi_bandsaw.tally_id', '=', $id)
                //     ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'dt_isi_bandsaw.tally_id')
                //     ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')

                //     ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
                //     ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=', 'hd_lpb.lokasi_kedatangan_id')
                //     ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_lpb.supplier_id')
                //     ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')
                //     ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')
                //     ->groupBy('tally_table.tally_no', 'dt_isi_bandsaw.isi_bandsaw_id','mst_jenis_kayu.jenis_kayu_id','mst_jenis_kayu.jenis_kayu', 'dt_isi_bandsaw.deskripsi_id', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang',
                //      'dt_isi_bandsaw.pcs', 'dt_isi_bandsaw.volume', 'mst_lokasi_kedatangan.lokasi_kedatangan',
                //       'hd_lpb.tanggal_kedatangan', 'mst_supplier.supplier_name')
                //     ->union($query)
                //     ->orderBy('tally_no')->get();

                $query = DB::table('dt_lpb')
            ->select(
                'tally_table.tally_no',
                'dt_isi_tally.isi_tally_id',
                'mst_jenis_kayu.jenis_kayu',
                'dt_isi_tally.deskripsi_id',
                'dt_isi_tally.tinggi',
                'dt_isi_tally.lebar',
                'dt_isi_tally.panjang',
                'dt_isi_tally.pcs',
                'dt_isi_tally.volume',
                'mst_lokasi_kedatangan.lokasi_kedatangan',
                'hd_lpb.tanggal_kedatangan',
                'mst_supplier.supplier_name',
                DB::raw('"Non BS" status_data')
            )
            ->where('dt_lpb.status', '=', true)
            ->where('tally_table.date_in_kd', '=', null)
            ->where('tally_table.date_mutation_wip', '=', NULL)
            ->where('dt_isi_tally.status', '=', true)
            ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')
            // ->where('tally_table.tally_no', 'like', '%' . $filters['tally_no'] . '%')
            ->whereNotIn('dt_lpb.tally_id',  DB::table('dt_isi_bandsaw')
                ->select('tally_id')
                ->groupBy('tally_id'))
            ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
            ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=', 'hd_lpb.lokasi_kedatangan_id')
            ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_lpb.supplier_id')
            ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')

            // ->orderBy('hd_lpb.created_by');
            ->orderBy('tally_table.tally_no', 'desc');

        // $filters = $request->session()->get('filters');
        // if (!empty($filters['tally_no'])) {
        //     $query->where('tally_table.tally_no', 'like', '%'.$filters['tally_no'].'%');
        // }
  


        $query2 = DB::table('dt_isi_bandsaw')
            ->select(
                'tally_table.tally_no',
                'dt_isi_bandsaw.isi_bandsaw_id',
                'mst_jenis_kayu.jenis_kayu',
                'dt_isi_bandsaw.deskripsi_id',
                'dt_isi_bandsaw.tinggi',
                'dt_isi_bandsaw.lebar',
                'dt_isi_bandsaw.panjang',
                'dt_isi_bandsaw.pcs',
                'dt_isi_bandsaw.volume',
                'mst_lokasi_kedatangan.lokasi_kedatangan',
                'hd_lpb.tanggal_kedatangan',
                'mst_supplier.supplier_name',
                DB::raw('"BS" status_data')
            )
            ->where('dt_isi_bandsaw.status', '=', true)
            ->where('tally_table.date_in_kd', '=', null)
            ->where('tally_table.date_mutation_wip', '=', NULL)
            ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')
            // ->where('tally_table.tally_no', 'like', '%' . $filters['tally_no'] . '%')
            // ->where('dt_isi_bandsaw.tally_id', '=', $id)
            ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'dt_isi_bandsaw.tally_id')
            ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')

            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
            ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=', 'hd_lpb.lokasi_kedatangan_id')
            ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_lpb.supplier_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')
            ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')
            ->groupBy('tally_table.tally_no', 'dt_isi_bandsaw.isi_bandsaw_id', 'mst_jenis_kayu.jenis_kayu','dt_isi_bandsaw.deskripsi_id','dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'dt_isi_bandsaw.pcs', 'dt_isi_bandsaw.volume', 'mst_lokasi_kedatangan.lokasi_kedatangan', 'hd_lpb.tanggal_kedatangan', 'mst_supplier.supplier_name')
            ->union($query)
            ->orderBy('tally_no')->get();


                    

                foreach ($query2 as $product) {

                    // dd($product);
                    $data[] = array(
                        $product->supplier_name,
                        $product->tanggal_kedatangan,
                        $product->tally_no,
                        $product->jenis_kayu,
                        $product->tinggi,
                        $product->lebar,
                        $product->panjang,
                        $product->pcs,
                        $product->volume,
                        $product->lokasi_kedatangan,
                        $product->status_data,


                    );
                }

                $sheet->fromArray($data, null, 'A1', false, false);
                $headings = array('supplier_name', 'tanggal_kedatangan', 'Nomor Tally', 'Jenis Kayu',  'Tinggi', 'Lebar',  'Panjang', 'Pcs', 'Volume', 'Lokasi Kedatangan', 'Status');
                $sheet->prependRow(1, $headings);
            });
        })->export('xlsx');
    }


    public function importlpb(Request $request)
    {

        // //if ($request->Request('file')) {
        //      $model  = new IsiBandsaw();
        //      $now    = new \DateTime();



        //     //$model->modified_date = $now;
        //     $path = $request->file('file')->getRealPath();
        //     $data = EXCEL::load($path, function($reader){})->get();

        //     if (!empty($data) && $data->count()) {
        //         foreach ($data as $key => $value) {
        //             $modal = new IsiBandsaw();
        //             $modal->tally_id = $value->tally_id;
        //             $modal->bandsaw_date = $value->bandsaw_date;
        //             $modal->jenis_kayu_id = $value->jenis_kayu_id;
        //             $modal->deskripsi_id = $value->deskripsi_id;
        //             $modal->tinggi = $value->tinggi;
        //             $modal->lebar = $value->lebar;
        //             $modal->panjang = $value->panjang;
        //             $modal->pcs = $value->pcs;
        //             $modal->volume = $value->volume;
        //             $modal->keterangan = $value->keterangan;
        //             $modal->created_by = $value->created_by;
        //             $modal->created_date    =  $value->created_date;

        //             $modal->save();
        //         }
        //     }
        // dd("masuk");
        //if ($request->Request('file')) {

        // dd($cek);
        // dd($maxtally+1);

        //  $model         = new IsiBandsaw();
        $now           = new \DateTime();



        //$model->modified_date = $now;
        $path = $request->file('file')->getRealPath();
        $data = EXCEL::load($path, function ($reader) { })->get();

        if (!empty($data) && $data->count()) {

            $count = 0;
            foreach ($data as $key => $value) {
                // $tally_id = 0;

                $cek = DB::table('tally_table')->select('tally_id')->where('tally_no', '=', $value->tally_no)->first();
                if ($cek != NULL) {
                    $tally_id =  $cek->tally_id;
                    // echo ",lama ".$tally_id."\n";
                } else {
                    $tally_id = DB::table('tally_table')->max('tally_id');
                    $tally_id = $tally_id + 1;
                    $tally = new Tally();
                    $tally->tally_id        = $tally_id;
                    $tally->tally_no        = $value->tally_no;
                    // dd( $tally->tally_no);
                    // dd($cek);
                    $tally->date_in_kd        = $value->date_in_kd;
                    $tally->date_out_kd        = $value->date_out_kd;
                    $tally->chamber_id        = $value->chamber_id;
                    $tally->lokasi_outputkd_id        = $value->lokasi_outputkd_id;
                    $tally->created_by      = \Auth::user()->id;
                    $tally->created_date    = $now;
                    $tally->save();
                    // echo ",baru ".$tally_id."\n";

                }
                $isi_tally     = new IsiTally();
                $isi_tally_id = DB::table('dt_isi_tally')->max('isi_tally_id');
                $isi_tally->isi_tally_id    = $isi_tally_id + 1;
                $isi_tally->tally_id        = $tally_id;
                // dd(  $isi_tally->tally_id  );
                $isi_tally->jenis_kayu_id   = $value->jenis_kayu_id;
                $isi_tally->deskripsi_id    = $value->deskripsi_id;
                $isi_tally->tinggi          = $value->tinggi;
                $isi_tally->lebar           = $value->lebar;
                $isi_tally->panjang         = $value->panjang;
                $isi_tally->pcs             = $value->pcs;
                $isi_tally->volume          = $value->volume;
                $isi_tally->created_by      = \Auth::user()->id;
                $isi_tally->created_date    = $now;

                $isi_tally->save();
                // echo ",isi tally ".$isi_tally->isi_tally_id."\n";


                $cek2 = DB::table('hd_lpb')->select('hd_lpb_id')->where('nomor_lpb', '=', $value->nomor_lpb)->first();
                if ($cek2 != NULL) {
                    $hd_lpb_id =  $cek2->hd_lpb_id;
                    // echo ",lama ".$tally_id."\n";
                } else {
                    $hd_lpb     = new LPB();
                    $hd_lpb_id = DB::table('hd_lpb')->max('hd_lpb_id');
                    // $nomor_lpb = DB::table('hd_lpb')->max('nomor_lpb');
                    // $nomor_po = DB::table('hd_lpb')->max('nomor_po');
                    $hd_lpb->hd_lpb_id       = $hd_lpb_id + 1;
                    $hd_lpb->supplier_id      = $value->supplier_id;
                    $hd_lpb->lokasi_kedatangan_id   = $value->lokasi_kedatangan_id;
                    $hd_lpb->nomor_lpb    = $value->nomor_lpb;
                    $hd_lpb->nomor_po          = $value->nomor_po;
                    $hd_lpb->nomor_sj           = "importlpb";
                    $hd_lpb->tanggal_kedatangan   = $value->tanggal_kedatangan;
                    $hd_lpb->plat_mobil             = "importlpb";
                    $hd_lpb->created_by      = \Auth::user()->id;
                    $hd_lpb->created_date    = $now;

                    $hd_lpb->save();
                    // echo ",hd_lpb ".$hd_lpb->hd_lpb_id."\n";
                }

                // dd($cek2);


                $dt_lpb     = new LPBDetail();
                $dt_lpb_id = DB::table('dt_lpb')->max('dt_lpb_id');
                $dt_lpb->dt_lpb_id       = $dt_lpb_id + 1;
                $dt_lpb->hd_lpb_id      = $hd_lpb_id;
                // dd( $dt_lpb->hd_lpb_id);
                $dt_lpb->tally_id   = $tally_id;
                $dt_lpb->isi_tally_id    = $isi_tally_id;
                $dt_lpb->harga_id          = 1;
                $dt_lpb->total_harga       = 0;
                $dt_lpb->status       = 1;
                $dt_lpb->created_by      = \Auth::user()->id;
                $dt_lpb->created_date    = $now;

                $dt_lpb->save();










                $count++;
            }
        }





        $request->session()->flash(
            'successMessage',
            trans('message.import-message', ['variable' => $count])
        );

        return redirect(route('raw-material-index'));
        // return back();

    }

    public function importsaldo(Request $request)
    {

        // //if ($request->Request('file')) {
        //      $model  = new IsiBandsaw();
        //      $now    = new \DateTime();



        //     //$model->modified_date = $now;
        //     $path = $request->file('file')->getRealPath();
        //     $data = EXCEL::load($path, function($reader){})->get();

        //     if (!empty($data) && $data->count()) {
        //         foreach ($data as $key => $value) {
        //             $modal = new IsiBandsaw();
        //             $modal->tally_id = $value->tally_id;
        //             $modal->bandsaw_date = $value->bandsaw_date;
        //             $modal->jenis_kayu_id = $value->jenis_kayu_id;
        //             $modal->deskripsi_id = $value->deskripsi_id;
        //             $modal->tinggi = $value->tinggi;
        //             $modal->lebar = $value->lebar;
        //             $modal->panjang = $value->panjang;
        //             $modal->pcs = $value->pcs;
        //             $modal->volume = $value->volume;
        //             $modal->keterangan = $value->keterangan;
        //             $modal->created_by = $value->created_by;
        //             $modal->created_date    =  $value->created_date;

        //             $modal->save();
        //         }
        //     }
        // dd("masuk");
        //if ($request->Request('file')) {

        // dd($cek);
        // dd($maxtally+1);

        //  $model         = new IsiBandsaw();
        $now           = new \DateTime();



        //$model->modified_date = $now;
        $path = $request->file('file')->getRealPath();
        $data = EXCEL::load($path, function ($reader) { })->get();

        if (!empty($data) && $data->count()) {
            $hd_lpb_id = 0;
            $count = 0;
            foreach ($data as $key => $value) {
                // $tally_id = 0;

                $cek = DB::table('tally_table')->select('tally_id')->where('tally_no', '=', $value->tally_no)->first();
                if ($cek != NULL) {
                    $tally_id =  $cek->tally_id;
                    // echo ",lama ".$tally_id."\n";
                } else {
                    $tally_id = DB::table('tally_table')->max('tally_id');
                    $tally_id = $tally_id + 1;
                    $tally = new Tally();
                    $tally->tally_id        = $tally_id;
                    $tally->tally_no        = $value->tally_no;
                    $tally->date_in_kd        = $value->date_in_kd;
                    $tally->date_out_kd        = $value->date_out_kd;
                    $tally->chamber_id        = $value->chamber_id;
                    $tally->lokasi_outputkd_id        = $value->lokasi_outputkd_id;
                    $tally->created_by      = \Auth::user()->id;
                    $tally->created_date    = $now;
                    $tally->save();
                    // echo ",baru ".$tally_id."\n";
                }

                $isi_tally     = new IsiTally();
                $isi_tally_id = DB::table('dt_isi_tally')->max('isi_tally_id');
                $isi_tally->isi_tally_id    = $isi_tally_id + 1;
                $isi_tally->tally_id        = $tally_id;
                $isi_tally->jenis_kayu_id   = $value->jenis_kayu_id;
                $isi_tally->deskripsi_id    = $value->deskripsi_id;
                $isi_tally->tinggi          = $value->tinggi;
                $isi_tally->lebar           = $value->lebar;
                $isi_tally->panjang         = $value->panjang;
                $isi_tally->pcs             = $value->pcs;
                $isi_tally->volume          = $value->volume;
                $isi_tally->created_by      = \Auth::user()->id;
                $isi_tally->created_date    = $now;

                $isi_tally->save();
                // echo ",isi tally ".$isi_tally->isi_tally_id."\n";

                
                $cek = DB::table('hd_lpb')->select('nomor_lpb')->where('nomor_lpb', '=', $value->nomor_lpb)->first();
                if ($cek != NULL )  {
                    $nomor_lpb =  $cek->nomor_lpb;

                }else {
                    $hd_lpb     = new LPB();
                    $hd_lpb_id = DB::table('hd_lpb')->max('hd_lpb_id');
                    // $nomor_lpb = DB::table('hd_lpb')->max('nomor_lpb');
                    // $nomor_po = DB::table('hd_lpb')->max('nomor_po');
                    $hd_lpb->hd_lpb_id       = $hd_lpb_id + 1;
                    
                    $hd_lpb->supplier_id      = $value->supplier_id;
                    $hd_lpb->lokasi_kedatangan_id   = $value->lokasi_kedatangan_id;
                    $hd_lpb->nomor_lpb    = $value->nomor_lpb;
                    $hd_lpb->nomor_po    = "Saldo Awal";
                    // $hd_lpb->nomor_lpb    = $this->randomString();
                    // $hd_lpb->nomor_po          = $this->randomString();
                    $hd_lpb->nomor_sj           = "Saldo";
                    $hd_lpb->tanggal_kedatangan   = $value->tanggal_kedatangan;
                    $hd_lpb->plat_mobil             = "Saldo";
                    $hd_lpb->created_by      = \Auth::user()->id;
                    $hd_lpb->created_date    = $now;

                    $hd_lpb->save();
                    // echo ",hd_lpb ".$hd_lpb->hd_lpb_id."\n";
                }

                
                $dt_lpb     = new LPBDetail();
                $dt_lpb_id = DB::table('dt_lpb')->max('dt_lpb_id');
                $dt_lpb->dt_lpb_id       = $dt_lpb_id + 1;
                $dt_lpb->hd_lpb_id      = $hd_lpb_id;
                $dt_lpb->tally_id   = $tally_id;
                $dt_lpb->isi_tally_id    = $isi_tally_id + 1;
                $dt_lpb->harga_id          = 1;
                $dt_lpb->total_harga       = 0;
                $dt_lpb->status       = 1;
                $dt_lpb->created_by      = \Auth::user()->id;
                $dt_lpb->created_date    = $now;

                $dt_lpb->save();
                $count++;
            }
        }
        $request->session()->flash(
            'successMessage',
            trans('message.import-message', ['variable' => $count])
        );

        return redirect(route('raw-material-index'));
        // return back();

    }





    public function importexcel(Request $request)
    {

        // //if ($request->Request('file')) {
        //      $model  = new IsiBandsaw();
        //      $now    = new \DateTime();



        //     //$model->modified_date = $now;
        //     $path = $request->file('file')->getRealPath();
        //     $data = EXCEL::load($path, function($reader){})->get();

        //     if (!empty($data) && $data->count()) {
        //         foreach ($data as $key => $value) {
        //             $modal = new IsiBandsaw();
        //             $modal->tally_id = $value->tally_id;
        //             $modal->bandsaw_date = $value->bandsaw_date;
        //             $modal->jenis_kayu_id = $value->jenis_kayu_id;
        //             $modal->deskripsi_id = $value->deskripsi_id;
        //             $modal->tinggi = $value->tinggi;
        //             $modal->lebar = $value->lebar;
        //             $modal->panjang = $value->panjang;
        //             $modal->pcs = $value->pcs;
        //             $modal->volume = $value->volume;
        //             $modal->keterangan = $value->keterangan;
        //             $modal->created_by = $value->created_by;
        //             $modal->created_date    =  $value->created_date;

        //             $modal->save();
        //         }
        //     }
        // dd("masuk");
        //if ($request->Request('file')) {

        // dd($cek);
        // dd($maxtally+1);

        //  $model         = new IsiBandsaw();
        $now           = new \DateTime();



        //$model->modified_date = $now;
        $path = $request->file('file')->getRealPath();
        $data = EXCEL::load($path, function ($reader) { })->get();

        if (!empty($data) && $data->count()) {
            $hd_lpb_id = 0;
            $count = 0;
            foreach ($data as $key => $value) {
                // $tally_id = 0;

                $cek = DB::table('tally_table')->select('tally_id')->where('tally_no', '=', $value->tally_no)->first();
                if ($cek != NULL) {
                    $tally_id =  $cek->tally_id;
                    // echo ",lama ".$tally_id."\n";
                } else {
                    $tally_id = DB::table('tally_table')->max('tally_id');
                    $tally_id = $tally_id + 1;
                    $tally = new Tally();
                    $tally->tally_id        = $tally_id;
                    $tally->tally_no        = $value->tally_no;
                    $tally->date_in_kd        = $value->date_in_kd;
              
                    $tally->date_out_kd        = $value->date_out_kd;
                    $tally->chamber_id        = $value->chamber_id;
                    $tally->lokasi_outputkd_id        = $value->lokasi_outputkd_id;
                    $tally->created_by      = \Auth::user()->id;
                    $tally->created_date    = $now;
                    $tally->save();
                    // echo ",baru ".$tally_id."\n";
                }

                $isi_tally     = new IsiTally();
                $isi_tally_id = DB::table('dt_isi_tally')->max('isi_tally_id');
                $isi_tally->isi_tally_id    = $isi_tally_id + 1;
                $isi_tally->tally_id        = $tally_id;
                $isi_tally->jenis_kayu_id   = $value->jenis_kayu_id;
                $isi_tally->deskripsi_id    = $value->deskripsi_id;
                $isi_tally->tinggi          = $value->tinggi;
                $isi_tally->lebar           = $value->lebar;
                $isi_tally->panjang         = $value->panjang;
                $isi_tally->pcs             = $value->pcs;
                $isi_tally->volume          = $value->volume;
                $isi_tally->created_by      = \Auth::user()->id;
                $isi_tally->created_date    = $now;

                $isi_tally->save();
                // echo ",isi tally ".$isi_tally->isi_tally_id."\n";

                if ($hd_lpb_id == 0) {
                    $hd_lpb     = new LPB();
                    $hd_lpb_id = DB::table('hd_lpb')->max('hd_lpb_id');
                    // $nomor_lpb = DB::table('hd_lpb')->max('nomor_lpb');
                    // $nomor_po = DB::table('hd_lpb')->max('nomor_po');
                    $hd_lpb->hd_lpb_id       = $hd_lpb_id + 1;
                    $hd_lpb->supplier_id      = 1;
                    $hd_lpb->lokasi_kedatangan_id   = $value->lokasi_kedatangan_id;
                    $hd_lpb->nomor_lpb    = $this->randomString();
                    $hd_lpb->nomor_po          = $this->randomString();
                    $hd_lpb->nomor_sj           = "Mutasi";
                    $hd_lpb->tanggal_kedatangan   = $value->date_mutation_wip;
                    $hd_lpb->plat_mobil             = "Mutasi";
                    $hd_lpb->created_by      = \Auth::user()->id;
                    $hd_lpb->created_date    = $now;

                    $hd_lpb->save();
                    // echo ",hd_lpb ".$hd_lpb->hd_lpb_id."\n";
                }


                $dt_lpb     = new LPBDetail();
                $dt_lpb_id = DB::table('dt_lpb')->max('dt_lpb_id');
                $dt_lpb->dt_lpb_id       = $dt_lpb_id + 1;
                $dt_lpb->hd_lpb_id      = $hd_lpb_id + 1;
                $dt_lpb->tally_id   = $tally_id;
                $dt_lpb->isi_tally_id    = $isi_tally_id + 1;
                $dt_lpb->harga_id          = 1;
                $dt_lpb->total_harga       = 0;
                $dt_lpb->status       = 1;
                $dt_lpb->created_by      = \Auth::user()->id;
                $dt_lpb->created_date    = $now;

                $dt_lpb->save();
                $count++;
            }
        }
        $request->session()->flash(
            'successMessage',
            trans('message.import-message', ['variable' => $count])
        );

        return redirect(route('raw-material-index'));
        // return back();

    }

    function randomString($length = 5)
    {
        $str = "";
        $characters = array_merge(range('A', 'Z'), range('a', 'z'), range('0', '9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str  .= $characters[$rand];
        }
        return $str;
    }
}
