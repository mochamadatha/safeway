<?php

namespace App\Model\Produksi;

use Illuminate\Database\Eloquent\Model;

class DtMutasi extends Model
{
    protected $table        = 'dt_mutasi';
    protected $primaryKey   = 'dt_mutasi_id';
    public $timestamps      = false;
}
