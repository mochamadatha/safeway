<?php

namespace App\Service\Master;

class TallyService
{

    public static function getActiveTally()
    {
        return \DB::table('tally_table')
            ->select('tally_table.tally_id', 'tally_table.tally_no')
            ->where('tally_table.status', '=', true)
            ->whereNotIn('tally_table.tally_id', \DB::table('dt_lpb')
                ->select('dt_lpb.tally_id')
                // ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'tally_table.tally_id')
                ->groupBy('dt_lpb.tally_id'))
            ->orderBy('tally_table.tally_no')
            ->get();
    }


    public static function getActiveRm()
    {
        return \DB::table('tally_table')
            ->select('tally_table.tally_id', 'tally_table.tally_no')
            ->where('tally_table.status', '=', true)
            ->where('tally_table.date_in_kd', '=', null)
            ->whereNotIn('tally_table.tally_id', \DB::table('dt_spk_bandsaw')
                ->select('dt_spk_bandsaw.tally_id')
                // ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'tally_table.tally_id')
                ->groupBy('dt_spk_bandsaw.tally_id'))
            ->orderBy('tally_table.tally_no')
            ->get();
    }


    // public static function getActiveTally()
    // {
    //     return \DB::table('tally_table')
    //             ->where('status', '=', true)
    //             ->where('tally_table.date_in_kd', '=', null) 
    //             ->orderBy('tally_no')
    //             ->get();
    // }

    public static function  getTallyFind()
    {
        return \DB::table('tally_table')
            ->select('tally_table.tally_id', 'tally_table.tally_no')
            ->where('dt_lpb.status', '=', true)
            ->whereNotIn('tally_table.tally_id', \DB::table('dt_lpb')
                ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'tally_table.tally_id')
                ->groupBy('tally_table.tally_id', 'tally_table.tally_no')
                ->orderBy('tally_no'))
            ->get();
    }

    public static function getTallyF()
    {
        return \DB::table('tally_table')
            ->where('tally_table.date_in_kd', '=', null)
            ->orderBy('tally_no')
            ->get();
    }

    //  public static function getTallyFind(){
    //     return \DB::table('tally_table')

    //         ->where('tally_table.date_in_kd', '=', null)
    //         ->whereNotIn('tally_table.tally_id', DB::table('dt_lpb')
    //             ->select('tally_id')
    //             ->groupBy('tally_id'))
    //         ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'tally_table.tally_id')
    //         ->orderBy('tally_id')
    //         ->get();
    // }
}
