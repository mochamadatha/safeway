@extends('layouts.master')

@section('title', trans('menu.setup-supplier'))

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
        	<div class="row">
				<div class="col-xs-12">
					<div class="page-title-box">
                        <h4 class="page-title">{{ trans('menu.setup-supplier') }}</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="{{ route('dashboard') }}">{{ trans('menu.dashboard') }}</a>
                            </li>
                            <li>
                                <a>{{ trans('menu.master-produksi') }}</a>
                            </li>
                            <li class="active">
                                {{ trans('menu.setup-supplier') }}
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
				</div>
			</div>
            <!-- end row -->
            @if(Session::has('successMessage'))
            <div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-check-all"></i>
                <strong>{{ Session::get('successMessage') }}.</strong>
            </div>
            @endif

            @if(Session::has('errorMessage'))
            <div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-block-helper"></i>
                <strong>{{ Session::get('errorMessage') }}.</strong>
            </div>
            @endif

			<div class="row">
			    <div class="col-md-12">
			        <div class="panel panel-border panel-success">
			        	<div class="panel-heading">
                        </div>
                        <div class="panel-body">
				            <div class="row">
				                <form class="form-horizontal" role="form" method="post" action="">
				                    {{ csrf_field() }}
				                   <!-- <div class="col-md-6">
				                        <div class="form-group {{ $errors->has('jenis_kayu') ? 'has-error' : '' }}">
				                            <label class="col-md-4 control-label">{{ trans('fields.wood-type') }}</label>
				                            <div class="col-md-8">
				                                <input type="text" name="jenis_kayus" class="form-control" value="{{ !empty($filters['jenis_kayu']) ? $filters['jenis_kayu'] : '' }}">
				                                @if($errors->has('jenis_kayu'))
				                                    <span class="help-block">{{ $errors->first('jenis_kayu') }}</span>
				                                @endif
				                            </div>
				                        </div>
				                        <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
				                            <div class="col-sm-offset-4 col-sm-8">
				                                <div class="checkbox checkbox-primary">
				                                    <?php $status = !empty($filters['status']) || !Session::has('filters'); ?>
				                                    <input id="status" name="status" value="true" type="checkbox" {{ $status == true ? 'checked' : '' }}>
				                                    <label for="status">
				                                        {{ trans('fields.is-active') }}
				                                    </label>
				                                </div>
				                            </div>
				                        </div>
				                    </div> -->
				                    <div class="col-md-12">
				                        <p class="text-muted font-14 m-b-20 pull-right">
				                            @can('access', [$resource, 'add'])
				                            <a href="{{ route('setup-supplier-add') }}" class="btn btn-primary waves-effect w-md waves-light"><i class="fa fa-plus"></i> {{ trans('fields.add-new') }}</a>
				                            @endcan
				                             @can('access', [$resource, 'excel'])
				                          	<a type="button" class="btn btn-primary waves-effect w-md waves-light" data-toggle="modal" data-target="#dtexcel" data-original-title="Edit"  method="post" enctype="multipart/form-data"></i> {{ trans('fields.excel') }}</a>
											@endcan
											@can('access', [$resource, 'add'])
											<a href="{{ route('setup-supplier-export-excel') }}" class="btn btn-success waves-effect w-md waves-light"> {{ trans('fields.export-excel') }}</a>
											@endcan
				                         <!--   <button type="submit" class="btn btn-info waves-effect w-md waves-light"><i class="fa fa-search"></i> {{ trans('fields.search') }}</button> -->

				                         	<div class="col-md-2">
                                                <input type="text" name="supplier_name" class="form-control" value="{{ !empty($filters['supplier_name']) ? $filters['supplier_name'] : '' }}">
                                                @if($errors->has('supplier_name'))
                                                <span class="help-block">{{ $errors->first('supplier_name') }}</span>
                                                @endif
                                            </div>

                                            <div class="col-md-4">
                                                <button type="submit" class="btn btn-info waves-effect w-xs waves-light"><i class="fa fa-search"></i> </button>
                                            </div>
				                        </p>
				                    </div>
				                </form>
				            </div>
				            <div class="row">
				                <table class="table table-bordered table-hover table-striped m-0">
				                    <thead>
				                        <tr>
				                            <th width="5%" class="text-center">{{ trans('fields.num') }}</th>
				                            <th width="10%" class="text-center">{{ trans('fields.supplier_name') }}</th>
				                            <th width="10%" class="text-center">{{ trans('fields.address') }}</th>
				                            <th width="10%" class="text-center">{{ trans('fields.pic') }}</th>
				                            <th width="10%" class="text-center">{{ trans('fields.phone') }}</th>
				                            <th width="10%" class="text-center">{{ trans('fields.note') }}</th>
				                            <th width="10%" class="text-center">{{ trans('fields.action') }}</th>
				                        </tr>
				                    </thead>
				                    <tbody>
				                        <?php $no = ($models->currentPage() - 1) * $models->perPage() + 1; ?>
				                        @foreach($models as $model)
				                        <tr>
				                            <th scope="row" style="text-align: center;">{{ $no++ }}</th>
				                            <td>{{ $model->supplier_name }}</td>
				                            <td>{{ $model->supplier_alamat }}</td>
				                            <td>{{ $model->pic }}</td>
				                            <td>{{ $model->phone }}</td>
				                            <td>{{ $model->note }}</td>
				                            <td class="text-center">
				                            @can('access', [$resource, 'update'])
				                                <a href="{{ route('setup-supplier-edit', ['id' => $model->supplier_id]) }}" data-toggle="tooltip" class="btn btn-xs btn-warning" data-original-title="Edit">
				                                    <i class="fa fa-pencil"></i>
				                                </a>
				                            @endcan
				                     

				                        </tr>
				                        @endforeach
				                    </tbody>
				                </table>
				                <div class="data-table-toolbar">
				                    {!! $models->render() !!}
				                </div>
				            </div>

				            	<div id="dtexcel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form role="form" method="post" action="{{ route('setup-supplier-import-excel') }}" method="post" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">{{ trans('fields.add-afkir') }}</h4>
                                        </div>
                                 
                                        <div class="modal-body">
                                            <div class="form-group">
                                            	<label for="file" class="col-md-3 control-label">Import</label>
                                            	<div class="col-md-6">
                                            		<input type="file" id="file" name="file" class="form-control" autorequired>
                                            		<span class="help-block with-error"></span>
                                            	</div>
                                            </div>
                                           
                                        </div><br>


                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                        </div>
                                      
                                    </form>
                                </div>
                            </div>
                        </div>
			            </div>
			        </div>
			    </div>
			</div>
		</div>
	</div>
</div>
@endsection