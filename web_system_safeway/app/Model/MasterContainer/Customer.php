<?php

namespace App\Model\MasterContainer;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table        = 'mst_customer';
    protected $primaryKey   = 'customer_id';
    public $timestamps      = false;
}
