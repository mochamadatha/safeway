<?php

namespace App\Model\Produksi;

use Illuminate\Database\Eloquent\Model;

class JumlahMs extends Model
{
    protected $table        = 'jumlah_ms';
    protected $primaryKey   = 'jumlah_ms_id';
    public $timestamps      = false;
}
