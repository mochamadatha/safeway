<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDtAmbilStockafkirTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dt_ambil_stockafkir', function (Blueprint $table) {
            $table->increments('ambil_stockafkir_id');
            $table->integer('id_jumlah_afkir');
            $table->integer('jenis_kayu_id');
            $table->integer('tinggi');
            $table->integer('lebar');
            $table->integer('panjang');
            $table->integer('pcs');
            $table->double('volume');
            $table->boolean('status')->default(true);
            $table->integer('created_by');
            $table->dateTime('created_date');
            $table->integer('modified_by')->nullable();
            $table->dateTime('modified_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dt_ambil_stockafkir');
    }
}
