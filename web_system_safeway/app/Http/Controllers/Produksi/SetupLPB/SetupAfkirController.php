<?php

namespace App\Http\Controllers\Produksi\SetupLPB;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Produksi\LPB;
use App\Model\Produksi\LPBDetail;
use App\Service\Master\TallyService;
use App\Model\Produksi\HdLpb;
use Elibyy\TCPDF\Facades\TCPDF;
use App\Model\Produksi\IsiAfkir;
use App\Service\Master\WoodService;
use DB;

class SetupAfkirController extends Controller
{
    const URL       = 'produksi/setup-afkir';
    const RESOURCE  = 'setup-afkir';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function add(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }


        $model = DB::table('hd_lpb')
            ->select('hd_lpb.*', 'mst_supplier.supplier_name')
            ->where('hd_lpb.hd_lpb_id', '=', $id)
            ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_lpb.supplier_id')->first();

        // dd($model);
        if ($model === null) {
            abort('404');
        }

        $query = DB::table('dt_isi_afkir')
            ->select('dt_isi_afkir.*', 'mst_jenis_kayu.jenis_kayu')
            ->where('hd_lpb_id', '=', $id)
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_afkir.jenis_kayu_id')
            ->orderBy('dt_isi_afkir.created_by');


        return view('produksi/lpb/setup-afkir.add', [
            "model"         => $model,
            "models"         => $query->paginate(10),
            "woodOptions"   => WoodService::getActiveWood(),
            // "desOptions"   => DesService::getActiveDes(),
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function edit(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }


        $model = DB::table('hd_lpb')
            ->select('hd_lpb.*', 'mst_supplier.supplier_name')
            ->where('hd_lpb.hd_lpb_id', '=', $id)
            ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_lpb.supplier_id')->first();

        // dd($model);
        if ($model === null) {
            abort('404');
        }

        $query = DB::table('dt_isi_afkir')
            ->select('dt_isi_afkir.*', 'mst_jenis_kayu.jenis_kayu')
            ->where('hd_lpb_id', '=', $id)
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_afkir.jenis_kayu_id')
            ->orderBy('dt_isi_afkir.created_by');


        return view('produksi/lpb/setup-afkir.add', [
            "model"         => $model,
            "models"         => $query->paginate(10),
            "woodOptions"   => WoodService::getActiveWood(),
            // "desOptions"   => DesService::getActiveDes(),
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function destroy(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $isi = IsiAfkir::find($id);
        $direct = $isi->hd_lpb_id;

        if ($isi === null) {
            abort('404');
        }
        // dd($isi->);

        // $isi =  LPBDetail::where('dt_lpb_id',$id)->delete();



        DB::beginTransaction();
        try {

            $isi->delete();
            DB::commit();



            $request->session()->flash(
                'successMessage',
                trans('message.delete-message', ['variable' => trans('fields.notally') . ' ' . $id])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.faildel-message', ['variable' => trans('fields.notally') . ' ' . $id])
            );
        }

        return redirect(route('setup-afkir-add', ['id' => $direct]));
    }




    public function reportafkirpdf(Request $request, $id)
    {

        $model = \DB::table('hd_lpb')
            ->select('hd_lpb.*', 'mst_supplier.supplier_name', 'mst_supplier.phone', 'mst_supplier.supplier_alamat', 'mst_supplier.pic')
            ->where('hd_lpb.hd_lpb_id', '=', $id)
            ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_lpb.supplier_id')
            ->orderBy('hd_lpb.hd_lpb_id')->first();



        $isi = DB::table('dt_isi_afkir')
            ->select('mst_jenis_kayu.jenis_kayu', DB::raw('sum(dt_isi_afkir.pcs)as pcs, sum(dt_isi_afkir.volume)as volume'))
            ->where('hd_lpb_id', '=', $id)
            ->groupBy('mst_jenis_kayu.jenis_kayu')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_afkir.jenis_kayu_id')
            ->orderBy('dt_isi_afkir.created_by')->get();

        // dd($isi);

        $now    = new \DateTime();
        $now = date_format($now, 'd/m/Y');
        // $model =  HdLpb::find($id);


        $header = view('produksi/lpb/setup-afkir.afkir-report-header-pdf', [
            'title' => trans('menu.lpb-pdf-report') . "#" . $now,
            'model' => $model,
            'isi'   => $isi,


        ])->render();
        \PDF::setHeaderCallback(function ($pdf) use ($header) {
            $pdf->writeHTML($header);
        });

        $html = view('produksi/lpb/setup-afkir/afkir-report-pdf', [
            "isi"         => $isi,
            "model"         => $model,
            //   "supplierOptions" => SupplierService::getActiveSupplier(),


        ])->render();

        $pdf = new TCPDF();
        $pdf::SetTitle('Report Afkir' . $model->nomor_lpb . '-' . $now);
        ob_end_clean();
        // $pdf::SetTitle('Afkir-');
        //  $resolution= array(187, 143);
        // $resolution= array(143, 187);
        $pdf::AddPage();
        //  $pdf::AddPage('P', $resolution);
        $pdf::writeHTML($html, true, false, true, false, '');
        $pdf::Output('Report Afkir' . $model->nomor_lpb . '-' . $now . '.pdf');
        // $pdf::Output($query->container_number.' '.$query->created_date.'.pdf');
        // $pdf->SetFont('Helvetica', '', 9);



    }


    public function reportafkirdetailpdf(Request $request, $id)
    {

        $model = \DB::table('hd_lpb')
            ->select('hd_lpb.*', 'mst_supplier.supplier_name', 'mst_supplier.phone', 'mst_supplier.supplier_alamat', 'mst_supplier.pic', 'hd_lpb.tanggal_afkir')
            ->where('hd_lpb.hd_lpb_id', '=', $id)
            ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_lpb.supplier_id')
            ->orderBy('hd_lpb.hd_lpb_id')->first();



        $query = DB::table('dt_isi_afkir')
            ->select('dt_isi_afkir.*', 'mst_jenis_kayu.jenis_kayu')
            ->where('hd_lpb_id', '=', $id)
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_afkir.jenis_kayu_id')
            ->orderBy('dt_isi_afkir.created_by')->get();



        //  dd($query);

        $now    = new \DateTime();
        $now = date_format($now, 'd/m/Y');
        // $model =  HdLpb::find($id);


        $header = view('produksi/lpb/setup-afkir.header-report', [
            'title' => trans('menu.lpb-pdf-report') . "#" . $now,
            'model' => $model,



        ])->render();
        \PDF::setHeaderCallback(function ($pdf) use ($header) {
            $pdf->writeHTML($header);
        });

        \PDF::setFooterCallback(function ($pdf) {



            // Position at 15 mm from bottom
            $pdf->SetY(-15);
            // Set font
            $pdf->SetFont('helvetica', 'I', 8);
            // Page number
            $pdf->Cell(0, 10, 'Page ' . $pdf->getAliasNumPage() . '/' . $pdf->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
        });

        $html = view('produksi/lpb/setup-afkir/isi-report', [
            "query"         => $query,
            "model"         => $model,
            //   "supplierOptions" => SupplierService::getActiveSupplier(),


        ])->render();



        $pdf = new TCPDF();
        $pdf::SetTitle('Report Afkir' . $model->nomor_lpb . '-' . $now);
        ob_end_clean();
        // $pdf::SetTitle('Afkir-');
        //  $resolution= array(187, 143);
        // $resolution= array(143, 187);
        $pdf::AddPage();
        //  $pdf::AddPage('P', $resolution);
        $pdf::writeHTML($html, true, true, true, false, '');
        $pdf::Output('Report Afkir' . $model->nomor_lpb . '-' . $now . '.pdf');
        // $pdf::Output($query->container_number.' '.$query->created_date.'.pdf');
        // $pdf->SetFont('Helvetica', '', 9);



    }



    public function save(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        $id     = intval($request->get('isi_afkir_id', 0));


        //dd($request->get('pcs'));


        $this->validate($request, [
            'hd_lpb_id'             => 'required|integer',

            'jenis_kayu_id'         => 'required|integer',
            'tinggi'                => 'required|integer',
            'lebar'                 => 'required|integer',
            'panjang'               => 'required|integer',
            'pcs'                   => 'required|integer',
        ]);

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            if (empty($id)) {
                $model  = new IsiAfkir();
                $model->created_by      = \Auth::user()->id;
                $model->created_date    = $now;
            } else {
                $model  = IsiAfkir::find($id);
                $model->modified_by   = \Auth::user()->id;
                $model->modified_date = $now;
            }

            $model->hd_lpb_id       = $request->get('hd_lpb_id');

            $model->jenis_kayu_id   = $request->get('jenis_kayu_id');
            $model->tinggi          = $request->get('tinggi');
            $model->lebar           = $request->get('lebar');
            $model->panjang         = $request->get('panjang');
            $model->pcs             = $request->get('pcs');
            $model->volume          = ($request->get('tinggi') * $request->get('panjang') * $request->get('lebar') * $request->get('pcs')) / 1000000000;
            $model->status          = $request->get('status-afkir');




            // dd($key->volume." ".$key->pcs." ".$price->harga);

            //$model->status          = TRUE;




            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('fields.add-afkir') . ' ' . ''])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('fields.add-afkir') . ' ' . ''])
            );
        }

        return redirect(route('setup-afkir-add', ['id' => $request->get('hd_lpb_id')]));
        // return redirect(route('setup-afkir-add', ['id' => $isi_afkir_id]));
    }
}
