<?php

namespace App\Model\Produksi;

use Illuminate\Database\Eloquent\Model;

class JumlahAfkir extends Model
{
    protected $table        = 'jumlah_afkir';
    protected $primaryKey   = 'id_jumlah_afkir';
    public $timestamps      = false;
}
