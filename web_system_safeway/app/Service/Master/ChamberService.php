<?php

namespace App\Service\Master;

class ChamberService
{
    public static function getActiveChamber()
    {
        return \DB::table('mst_chamber')
            ->where('status', '=', true)
            ->orderBy('chamber_name')
            ->get();
    }
}
