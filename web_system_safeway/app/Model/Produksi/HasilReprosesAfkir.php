<?php

namespace App\Model\Produksi;

use Illuminate\Database\Eloquent\Model;

class HasilReprosesAfkir extends Model
{
    protected $table        = 'dt_hasil_reproses_afkir';
    protected $primaryKey   = 'hasil_reproses_afkir_id';
    public $timestamps      = false;
}
