<?php

namespace App\Http\Controllers\MasterProduksi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\MasterProduksi\Supplier;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class SetupSupplierController extends Controller
{
    const URL       = 'master-produksi/setup-supplier';
    const RESOURCE  = 'setup-supplier';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        if ($request->isMethod('post')) {
            $request->session()->put('filters', $request->all());
            return redirect(self::URL . '?page=1');
        } elseif (empty($request->get('page'))) {
            $request->session()->forget('filters');
        }
        $filters = $request->session()->get('filters');
        $query = \DB::table('mst_supplier')
            ->orderBy('mst_supplier.supplier_name');

        if (!empty($filters['status']) || !$request->session()->has('filters')) {
            $query->where('mst_supplier.status', '=', true);
        } else {
            $query->where('mst_supplier.status', '=', false);
        }


        $filters = $request->session()->get('filters');
        $query = \DB::table('mst_supplier')
            ->orderBy('mst_supplier.supplier_name');

        if (!empty($filters['supplier_name'])) {
            $query->where('mst_supplier.supplier_name', 'like', '%' . $filters['supplier_name'] . '%');
        }



        return view('master-produksi/supplier.index', [
            "models"        => $query->paginate(10),
            "filters"       => $filters,
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function add(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        $model           = new Supplier();
        $model->status   = true;

        return view('master-produksi/supplier.add', [
            "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }

    public function exportexcel()
    {

        ob_end_clean();
        ob_start();
        Excel::create('Export data', function ($excel) {

            $excel->sheet('Sheet 1', function ($sheet) {

                $query = DB::table('mst_supplier')
                    ->select(
                        'supplier_name',
                        'supplier_alamat',
                        'pic',
                        'phone',
                        'note'
                    )
                    ->orderBy('mst_supplier.supplier_id', 'desc')->get();
    
                        // dd($query);
                // $filters = $request->session()->get('filters');
                // if (!empty($filters['tally_no'])) {
                //     $query->where('tally_table.tally_no', 'like', '%'.$filters['tally_no'].'%');
                // }
                    
                foreach ($query as $product) {
                    $data[] = array(
                        $product->supplier_name,
                        $product->supplier_alamat,
                        $product->pic,
                        $product->phone,
                        $product->note,
                


                    );
                }



                $sheet->fromArray($data, null, 'A1', false, false);
                $headings = array('supplier name', 'supplier alamat', 'pic', 'phone', 'note');
                $sheet->prependRow(1, $headings);
            });
        })->export('xlsx');
    }


    public function importexcel(Request $request)
    {

        //if ($request->Request('file')) {
        $model  = new Supplier();
        $now    = new \DateTime();



        //$model->modified_date = $now;
        $path = $request->file('file')->getRealPath();
        $data = EXCEL::load($path, function ($reader) { })->get();

        if (!empty($data) && $data->count()) {
            foreach ($data as $key => $value) {
                $modal = new Supplier();
                $modal->supplier_name = $value->supplier_name;
                $modal->supplier_alamat = $value->supplier_alamat;
                $modal->pic = $value->pic;
                $modal->phone = $value->phone;
                $modal->note = $value->note;
                $modal->created_by = \Auth::user()->id;
                $modal->created_date    = $now;
                $modal->save();
            }
        }

        return back();
    }

    public function edit(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $model = Supplier::find($id);

        if ($model === null) {
            abort('404');
        }

        return view('master-produksi/supplier.add', [
            "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }

    public function save(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add']) && !\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $id     = intval($request->get('id', 0));

        $this->validate($request, [
            'supplier_name' => 'required|max:255|unique:mst_supplier,supplier_name,' . $id . ',supplier_id',
            'supplier_alamat' => 'required|max:255',
            'pic' => 'required|max:255',
            'phone' => 'required|max:20',

        ]);

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            if (empty($id)) {
                $model  = new Supplier();
                $model->created_by      = \Auth::user()->id;
                $model->created_date    = $now;
            } else {
                $model  = Supplier::find($id);
                $model->modified_by   = \Auth::user()->id;
                $model->modified_date = $now;
            }

            $model->supplier_name   = $request->get('supplier_name');
            $model->supplier_alamat   = $request->get('supplier_alamat');
            $model->pic   = $request->get('pic');
            $model->phone   = $request->get('phone');
            $model->note   = $request->get('note');
            //$model->supplier_name   = $request->get('supplier_name');

            $model->status   = !empty($request->get('status')) ? true : false;

            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('menu.supplier_name') . ' ' . $request->get('supplier_name')])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('menu.supplier_name') . ' ' . $request->get('supplier_name')])
            );
        }

        return redirect(route('setup-supplier-index'));
    }
}
