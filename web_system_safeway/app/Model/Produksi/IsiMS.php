<?php

namespace App\Model\Produksi;

use Illuminate\Database\Eloquent\Model;

class IsiMS extends Model
{
    protected $table        = 'dt_isi_ms';
    protected $primaryKey   = 'isi_ms_id';
    public $timestamps      = false;
}
