<?php

namespace App\Model\Produksi;

use Illuminate\Database\Eloquent\Model;

class HdSpkBandsaw extends Model
{
    protected $table        = 'hd_spk_bandsaw';
    protected $primaryKey   = 'hd_spk_bandsaw_id';
    public $timestamps      = false;
}
