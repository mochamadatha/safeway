<?php 

namespace App\Service\Master;

class MutationLocationService
{
    public static function getActiveMutationLocation()
    {
        return \DB::table('mst_lokasi_mutasi')
                ->where('status', '=', true)
                ->orderBy('lokasi_mutasi')
                ->get();
    }
}