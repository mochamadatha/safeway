<?php

namespace App\Http\Controllers\ReportProduksi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Produksi\LPB;
use App\Model\Produksi\LPBDetail;
use App\Service\Master\TallyService;
use App\Model\MasterProduksi\Supplier;
use App\Service\Master\SupplierService;
use App\Model\Produksi\HdLpb;
use Elibyy\TCPDF\Facades\TCPDF;
use App\Model\Produksi\IsiAfkir;
use App\Model\Produksi\Tally;
use App\Model\Produksi\IsiTally;
use App\Service\Master\WoodService;
use App\Service\Master\DesService;
use App\Model\MasterProduksi\LokasiKedatangan;
use App\Service\Master\ArrivalLocationService;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class KilnDryController extends Controller
{
    const URL       = 'report-produksi/kiln-dry';
    const RESOURCE  = 'kiln-dry';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        if ($request->isMethod('post')) {
            $request->session()->put('filters', $request->all());
            return redirect(self::URL . '?page=1');
        } elseif (empty($request->get('page'))) {
            $request->session()->forget('filters');
        }
        $filters = $request->session()->get('filters');


        // $query = DB::table('hd_lpb')
        //     ->select(
        //         'tally_table.tally_no',
        //         'tally_table.date_mutation_wip',
        //         'tally_table.date_in_kd',
        //         'tally_table.date_out_kd',
        //         'hd_lpb.tanggal_kedatangan',
        //         'mst_jenis_kayu.jenis_kayu',
        //         'mst_deskripsi_tallysheet.nama_deskripsi',
        //         'dt_isi_tally.isi_tally_id',
        //         'dt_isi_tally.tinggi',
        //         'dt_isi_tally.lebar',
        //         'dt_isi_tally.panjang',
        //         'dt_isi_tally.pcs',
        //         'dt_isi_tally.volume',
        //         'mst_lokasi_kedatangan.lokasi_kedatangan'
        //     )
        //     // ->where('hd_lpb.status', '=', true)

        //     // ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')
        //     ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', 'hd_lpb.lokasi_kedatangan_id')
        //     ->join('dt_lpb', 'dt_lpb.hd_lpb_id', '=', 'hd_lpb.hd_lpb_id')
        //     ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
        //     ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
        //     ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
        //     ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_tally.deskripsi_id')


        //     ->orderBy('tally_table.tally_id');

        $query = DB::table('dt_lpb')
        ->select(
            'dt_lpb.dt_lpb_id',
            'mst_jenis_kayu.jenis_kayu',
            'tally_table.tally_no',
            'tally_table.date_in_kd',
            'tally_table.date_out_kd',
            'mst_deskripsi_tallysheet.nama_deskripsi',
            'dt_lpb.tally_id',
            'dt_isi_tally.isi_tally_id',
            'dt_isi_tally.tinggi',
            'dt_isi_tally.lebar',
            'dt_isi_tally.panjang',
            'dt_isi_tally.pcs',
            'dt_isi_tally.volume',
            'mst_harga.harga',
            'dt_lpb.total_harga',
            'dt_lpb.harga_satuan',
            'dt_lpb.note',
            'dt_lpb.total_harga_edit',
            'hd_lpb.tanggal_kedatangan'
        )
  
        ->where('dt_lpb.status', '=', true)
        ->where('dt_isi_tally.status', '=', true)
        ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
        ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
        ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
        ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_tally.deskripsi_id')
        ->join('mst_harga', 'mst_harga.harga_id', '=', 'dt_lpb.harga_id')
        ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
        ->orderBy('tally_table.tally_no');
        
     
        if (!empty($filters['status'])) {
            if (!empty($filters['start_date']) && !empty($filters['end_date'])) {
                if ($filters['status'] == 'rm') {
                    $query->whereBetween('hd_lpb.tanggal_kedatangan', [$filters['start_date'], $filters['end_date']]);
                } elseif ($filters['status'] == 'inkd') {
                    $query->whereBetween('date_in_kd', [$filters['start_date'], $filters['end_date']]);
                } elseif ($filters['status'] == 'outkd') {
                    $query->whereBetween('date_out_kd', [$filters['start_date'], $filters['end_date']]);
                } elseif ($filters['status'] == 'wip') {
                    $query->whereBetween('date_mutation_wip', [$filters['start_date'], $filters['end_date']]);
                    $query->where('tally_table.status_mutation_wip', '=', '1');
                    // $query->where('dt_isi_afkir_proses.keterangan', '=', 'Raw Material');
                }
                // dd($filters['start_date']." ".$filters['end_date']);
                $request->session()->put('excel', $filters); //simpan data
                // dd($request->session()->get('tgl')); //ngambil data
                // $query->where('hd_lpb.nomor_lpb', 'like', '%'.$filters['nomor_lpb'].'%');
            }
        } else {
            $request->session()->forget('excel');
        }

        return view('report-produksi/kiln-dry.index', [
            "models"        => $query->paginate(10),
            "filters"       => $filters,
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

   


    public function exportexcel(Request $request)
    {

        
        $this->ex = $request->session()->get('excel'); //ngambil data

        // dd( $this->ex );
        ob_end_clean();
        ob_start();

        // dd($this->ex['start_date']);
        Excel::create('Search All Data ', function ($excel) {

            $excel->sheet('Sheet 1', function ($sheet) {


       
                $query = DB::table('hd_lpb')
                ->select(
                    'tally_table.tally_no',
                    'tally_table.date_in_kd',
                    'tally_table.date_out_kd',
                    'tally_table.date_mutation_wip',
                    'mst_jenis_kayu.jenis_kayu',
                    'dt_isi_tally.deskripsi_id',
                    'dt_isi_tally.tinggi',
                    'dt_isi_tally.lebar',
                    'dt_isi_tally.panjang',
                    'dt_isi_tally.pcs',
                    'dt_isi_tally.volume',
                    'hd_lpb.tanggal_kedatangan',
                    'mst_supplier.supplier_name',
                    'dt_isi_tally.created_date',
                    'mst_lokasi_kedatangan.lokasi_kedatangan',
                    DB::raw('"Non BS" status_data')
                )
                ->where('hd_lpb.status', '=', true)
                // ->where('tally_table.date_in_kd', '=', NULL)
                // ->where('tally_table.date_out_kd', '=', NULL)
                // ->where('tally_table.date_mutation_wip', '=', NULL)
                ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')
                // ->where('tally_table.tally_no', 'like', '%' . $filters['tally_no'] . '%')
                ->whereNotIn('dt_lpb.tally_id',  DB::table('dt_isi_bandsaw')
                    ->select('tally_id')
                    ->groupBy('tally_id'))
                ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', 'hd_lpb.lokasi_kedatangan_id')
                ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_lpb.supplier_id')
                ->join('dt_lpb', 'dt_lpb.hd_lpb_id', '=', 'hd_lpb.hd_lpb_id')
                ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
                ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
                ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
        
                ->join('mst_lokasi_outputkd', 'mst_lokasi_outputkd.lokasi_outputkd_id', '=', 'tally_table.lokasi_outputkd_id')
                ->orderBy('hd_lpb.created_by');
        
        
            //dd ($query);
        
            $query2 = DB::table('dt_isi_bandsaw')
                ->select(
                    'tally_table.tally_no',
                    'tally_table.date_in_kd',
                    'tally_table.date_out_kd',
                    'tally_table.date_mutation_wip',
                    'mst_jenis_kayu.jenis_kayu',
                    'dt_isi_bandsaw.deskripsi_id',
                    'dt_isi_bandsaw.tinggi',
                    'dt_isi_bandsaw.lebar',
                    'dt_isi_bandsaw.panjang',
                    'dt_isi_bandsaw.pcs',
                    'dt_isi_bandsaw.volume',
                    'hd_lpb.tanggal_kedatangan',
                    'mst_supplier.supplier_name',
                    'dt_isi_bandsaw.created_date',
                    'mst_lokasi_kedatangan.lokasi_kedatangan',
                    DB::raw('"BS" status_data')
                )
                ->where('dt_isi_bandsaw.status', '=', true)
                // ->where('tally_table.tally_no', 'like', '%' . $filters['tally_no'] . '%')
                // ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')
                ->where('tally_table.date_in_kd', '=', null)
                // ->where('dt_isi_bandsaw.tally_id', '=', $id)
                ->whereIn('dt_isi_bandsaw.tally_id', DB::table('tally_table')
                    ->select('tally_table.tally_id')
                    // ->where('tally_table.date_in_kd', '=', NULL)
                    // ->where('tally_table.date_out_kd', '!=', NULL)
                    // ->where('tally_table.date_mutation_wip', '=', NULL)
                    // ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')
                    ->join('dt_isi_bandsaw', 'dt_isi_bandsaw.tally_id', '=', 'tally_table.tally_id')
                    ->groupBy('tally_id'))
                ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'dt_isi_bandsaw.tally_id')
                ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
                ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
                ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=', 'hd_lpb.lokasi_kedatangan_id')
                ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')
                ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_lpb.supplier_id')
        
                // ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')
        
                ->join('mst_lokasi_outputkd', 'mst_lokasi_outputkd.lokasi_outputkd_id', '=', 'tally_table.lokasi_outputkd_id')
                ->groupBy('tally_table.tally_no', 'tally_table.date_out_kd','tally_table.date_in_kd', 'dt_isi_bandsaw.deskripsi_id','mst_jenis_kayu.jenis_kayu', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'dt_isi_bandsaw.pcs', 'dt_isi_bandsaw.volume', 'dt_isi_bandsaw.created_date', 'mst_lokasi_kedatangan.lokasi_kedatangan', 'hd_lpb.tanggal_kedatangan', 'mst_supplier.supplier_name')
                ->union($query)
                ->orderBy('tally_table.tally_no');

                
                if (!empty($this->ex['status'])) {
                    if (!empty($this->ex['start_date']) && !empty($this->ex['end_date'])) {
                        if ($this->ex['status'] == 'rm') {
                            $query->whereBetween('tanggal_kedatangan', [$this->ex['start_date'], $this->ex['end_date']]);
                        } elseif ($this->ex['status'] == 'inkd') {
                            $query->whereBetween('date_in_kd', [$this->ex['start_date'], $this->ex['end_date']]);
                        } elseif ($this->ex['status'] == 'outkd') {
                            $query->whereBetween('date_out_kd', [$this->ex['start_date'], $this->ex['end_date']]);
                        } elseif ($this->ex['status'] == 'wip') {
                            $query->whereBetween('date_mutation_wip', [$this->ex['start_date'], $this->ex['end_date']]);
                            $query->where('tally_table.status_mutation_wip', '=', 1);
                        }
                    }
                }
                $query->orderBy('tally_table.tally_no');



                //    dd($query->get());

                  foreach ($query2->get() as $product) {
                    $data[] = array(
                        $product->tally_no,
                        $product->tanggal_kedatangan,
                        $product->date_in_kd,
                        $product->date_out_kd,
                        $product->date_mutation_wip,
                        $product->jenis_kayu,
                        // $product->nama_deskripsi,
                        $product->tinggi,
                        $product->lebar,
                        $product->panjang,
                        $product->pcs,
                        $product->volume,
                        $product->lokasi_kedatangan,
                        //  $product->  'dt_isi_tally.isi_tally_id',,  'dt_isi_tally.isi_tally_id',


                    );
                }

                $sheet->fromArray($data, null, 'A1', false, false);
                $headings = array('Nomor Tally', 'Tanggal Kedatangan', 'Tanggal Masuk KD', 'Tanggal Keluar KD', 'Tanggal Masuk WIP', 'Jenis Kayu', 'Deskripsi', 'Tinggi', 'Lebar', 'Panjang', 'Pcs', 'Volume', 'Lokasi Kedatangan', 'Chamber Name');
                $sheet->prependRow(1, $headings);
            });
        })->export('xlsx');
    }


    public function exportpdf(Request $request, $id)
    {

        $model = DB::table('hd_spk_bandsaw')
            ->select('hd_spk_bandsaw.*')
            ->where('hd_spk_bandsaw.hd_spk_bandsaw_id', '=', $id)->first();



        $querys=DB::table('tally_table')
        ->select('tally_table.tally_no')
        ->where('dt_spk_bandsaw.hd_spk_bandsaw_id', '=', $id)
        ->join('dt_spk_bandsaw','dt_spk_bandsaw.tally_id', '=', 'tally_table.tally_id')
        ->get();


        $querysd = DB::table('dt_isi_tally')
        ->select('tally_table.tally_no','dt_isi_tally.*','mst_jenis_kayu.jenis_kayu')
        ->where('dt_spk_bandsaw.hd_spk_bandsaw_id', '=', $id)
        ->join ('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
        ->join('dt_spk_bandsaw','dt_spk_bandsaw.tally_id', '=', 'tally_table.tally_id')
        ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')->get();

       
         $queryawal = DB::table('tally_table')
        ->select('tally_table.tally_no',  'dt_isi_tally.tinggi','dt_isi_tally.lebar', 'dt_isi_tally.panjang', 'mst_jenis_kayu.jenis_kayu',
        'dt_isi_tally.pcs', 'dt_isi_tally.volume')
        ->where('dt_spk_bandsaw.hd_spk_bandsaw_id', '=', $id)
  
        ->join('dt_spk_bandsaw','dt_spk_bandsaw.tally_id', '=', 'tally_table.tally_id')
        ->join('dt_isi_tally', 'dt_isi_tally.tally_id', 'tally_table.tally_id')
        ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
        ->get();
    //  dd($queryawal);

        $queryplanning = DB::table('tally_table')
        ->select('tally_table.tally_no', 'dt_isi_planbandsaw.tinggi', 'dt_isi_planbandsaw.lebar', 
        'dt_isi_planbandsaw.panjang', 'dt_isi_planbandsaw.pcs', 'dt_isi_planbandsaw.volume')
        ->where('dt_spk_bandsaw.hd_spk_bandsaw_id', '=', $id)
        ->join('dt_spk_bandsaw','dt_spk_bandsaw.tally_id', '=', 'tally_table.tally_id')
        ->join('dt_isi_planbandsaw', 'dt_isi_planbandsaw.tally_id', 'tally_table.tally_id')
        ->get();

        $sumplanning = DB::table('tally_table')
        ->select(DB::raw('SUM(dt_isi_planbandsaw.pcs) AS sumpcs'),DB::raw('SUM(dt_isi_planbandsaw.volume) AS sumvolume'))
        ->where('dt_spk_bandsaw.hd_spk_bandsaw_id', '=', $id)
        ->join('dt_spk_bandsaw','dt_spk_bandsaw.tally_id', '=', 'tally_table.tally_id')
        ->join('dt_isi_planbandsaw', 'dt_isi_planbandsaw.tally_id', 'tally_table.tally_id')->get();

        $sumquerysd = DB::table('dt_isi_tally')
        ->select(DB::raw('SUM(dt_isi_tally.pcs) AS sumpcs'),DB::raw('SUM(dt_isi_tally.volume) AS sumvolume'))
        ->where('dt_spk_bandsaw.hd_spk_bandsaw_id', '=', $id)
        ->join ('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
        ->join('dt_spk_bandsaw','dt_spk_bandsaw.tally_id', '=', 'tally_table.tally_id')
        ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')->get();



        $now    = new \DateTime();
        $now = date_format($now, 'd/m/Y h:i:s');
        // $model =  HdLpb::find($id);


        $header = view('spk-produksi/spk-bandsaw.header-pdf', [
            'title' => trans('menu.lpb-pdf-report') . "#" . $now,
            "model"         => $model,
            // "supplierOptions" => SupplierService::getActiveSupplier(),
        ])->render();
        \PDF::setHeaderCallback(function ($pdf) use ($header) {
            $pdf->writeHTML($header);
        });


        // $footer = view('produksi/lpb/setup-lpb.footer-pdf', [
        //     'title' => trans('menu.lpb-pdf-report')."#".$now,
        //     "model"         => $model,
        //     // "supplierOptions" => SupplierService::getActiveSupplier(),
        // ])->render();
        // \PDF::setFooterCallback(function($pdf) use ($footer) {
        //     $pdf->writeHTML($footer);
        // });

        // Custom Footer
        \PDF::setFooterCallback(function ($pdf) {

            // Position at 15 mm from bottom
            $pdf->SetY(-15);
            // Set font
            $pdf->SetFont('helvetica', 'I', 8);
            
            // Page number
            $pdf->Cell(0, 5, 'FR/09/09/SUB - 25/08/2016 - 00 -  ' . $pdf->getAliasNumPage() . '/' . $pdf->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
        });

        $html = view('spk-produksi/spk-bandsaw.spk-pdf', [
            "queryawal"         => $queryawal,
            "querys"         => $querys,
            "querysd"         => $querysd,
            "queryplanning"         => $queryplanning,
            "sumplanning"   => $sumplanning,
            "sumquerysd"       =>     $sumquerysd,
            "model"         => $model,
            "supplierOptions" => SupplierService::getActiveSupplier(),
            "woodOptions"   => WoodService::getActiveWood(),


        ])->render();

        $pdf = new TCPDF();
        $pdf::SetTitle('LAPORAN PENERIMAAN BAHAN SAWN TIMBER-' . '-' . $now);
        ob_end_clean();

        $pdf::SetTopMargin(40);

        // $pdf::SetTopMargin(41);

        $pdf::AddPage('P');
        $pdf::writeHTML($html, true, false, true, false, '');
        // $pdf::setPageFormat( $orientation 'P' );
        $pdf::Output('LAPORAN PENERIMAAN BAHAN SAWN TIMBER-'  . '-' . $now . '.pdf');
        // $pdf::Output($query->container_number.' '.$query->created_date.'.pdf');
        // $pdf->SetFont('Helvetica', '', 9);



    }
}
