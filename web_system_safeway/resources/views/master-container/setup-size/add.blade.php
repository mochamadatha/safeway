@extends('layouts.master')

@section('title', trans('menu.setup-size'))

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
        	<div class="row">
				<div class="col-xs-12">
					<div class="page-title-box">
                        <h4 class="page-title">{{ trans('menu.setup-size') }}</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="{{ route('dashboard') }}">{{ trans('menu.dashboard') }}</a>
                            </li>
                            <li>
                                <a>{{ trans('menu.master-container') }}</a>
                            </li>
                            <li class="active">
                                {{ trans('menu.setup-size') }}
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
				</div>
			</div>
            <!-- end row -->
            
			<div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-border panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ trans('fields.add-size') }}</h3>
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal" role="form" method="post" action="{{ route('setup-size-save') }}">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="p-20">
                                            <input type="hidden" name="id" class="form-control" value="{{ count($errors) > 0 ? old('id') : $model->size_id }}">
                                            <div class="form-group {{ $errors->has('size') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.size') }}</label>
                                                <div class="col-md-10">
                                                    <input type="text" name="size" class="form-control" value="{{ count($errors) > 0 ? old('size') : $model->size }}">
                                                    @if($errors->has('size'))
                                                        <span class="help-block">{{ $errors->first('size') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <div class="checkbox checkbox-primary">
                                                        <?php $status = count($errors) > 0 ? old('status') : $model->status; ?>
                                                        <input id="status" name="status" value="true" type="checkbox" {{ $status == true ? 'checked' : '' }}>
                                                        <label for="status">
                                                            {{ trans('fields.is-active') }}
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 data-table-toolbar text-right" style="padding-top: 10px;">
                                        <div class="form-group">
                                            <a href="{{ url('master-container/setup-size') }}" class="btn btn-sm btn-warning"><i class="fa fa-reply"></i> {{ trans('fields.cancel') }}</a>
                                            <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i> {{ trans('fields.save') }}</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                        <!-- end row -->
                </div> <!-- end card-box -->
            </div><!-- end col -->
        </div>
    </div>
</div>
@endsection