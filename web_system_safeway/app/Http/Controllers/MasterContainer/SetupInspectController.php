<?php

namespace App\Http\Controllers\MasterContainer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\MasterContainer\Inspect;
use DB;

class SetupInspectController extends Controller
{
    const URL       = 'master-container/setup-inspect';
    const RESOURCE  = 'setup-inspect';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        if ($request->isMethod('post')) {
            $request->session()->put('filters', $request->all());
            return redirect(self::URL . '?page=1');
        } elseif (empty($request->get('page'))) {
            $request->session()->forget('filters');
        }
        $filters = $request->session()->get('filters');
        $query = \DB::table('mst_inspect')
            ->orderBy('mst_inspect.nama_inspect');

        if (!empty($filters['status']) || !$request->session()->has('filters')) {
            $query->where('mst_inspect.status', '=', true);
        } else {
            $query->where('mst_inspect.status', '=', false);
        }

        if (!empty($filters['nama_inspect'])) {
            $query->where('mst_inspect.nama_inspect', 'like', '%' . $filters['nama_inspect'] . '%');
        }

        return view('master-container/setup-inspect.index', [
            "models"        => $query->paginate(10),
            "filters"       => $filters,
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function add(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        $model           = new Inspect();
        $model->status   = true;

        return view('master-container/setup-inspect.add', [
            "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }

    public function edit(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $model = Inspect::find($id);

        if ($model === null) {
            abort('404');
        }

        return view('master-container/setup-inspect.add', [
            "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }

    public function delete(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $model =  Inspect::find($id);

        if ($model === null) {
            abort('404');
        }

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            $model->modified_by   = \Auth::user()->id;
            $model->modified_date = $now;
            $model->status   = false;

            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.delete-message', ['variable' => trans('fields.name_inspect') . ' ' . $id])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.faildel-message', ['variable' => trans('fields.name_inspect') . ' ' . $id])
            );
        }

        return redirect(route('setup-inspect-index'));
    }

    public function save(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add']) && !\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $id     = intval($request->get('id', 0));

        $this->validate($request, [
            'nama_inspect' => 'required|max:255|unique:mst_inspect,nama_inspect,' . $id . ',inspect_id',
        ]);

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            if (empty($id)) {
                $model  = new Inspect();
                $model->created_by      = \Auth::user()->id;
                $model->created_date    = $now;
            } else {
                $model  = Inspect::find($id);
                $model->modified_by   = \Auth::user()->id;
                $model->modified_date = $now;
            }

            $model->nama_inspect   = $request->get('nama_inspect');
            $model->status   = !empty($request->get('status')) ? true : false;

            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('menu.name_inspect') . ' ' . $request->get('name_inspect')])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('menu.name_inspect') . ' ' . $request->get('nama_inspect')])
            );
        }

        return redirect(route('setup-inspect-index'));
    }
}
