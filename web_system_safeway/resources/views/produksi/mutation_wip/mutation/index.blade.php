@extends('layouts.master')

@section('title', trans('menu.mutation'))

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">{{ trans('menu.mutation') }}</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="{{ route('dashboard') }}">{{ trans('menu.dashboard') }}</a>
                            </li>
                            <li>
                                <a>{{ trans('menu.produksi') }}</a>
                            </li>
                            <li class="active">
                                {{ trans('menu.mutation') }}
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            @if(Session::has('successMessage'))
            <div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-check-all"></i>
                <strong>{{ Session::get('successMessage') }}.</strong>
            </div>
            @endif

            @if(Session::has('errorMessage'))
            <div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-block-helper"></i>
                <strong>{{ Session::get('errorMessage') }}.</strong>
            </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-border panel-success">
                        <div class="panel-heading">
                        </div>

                        <div class="panel-body">
                            <div class="row">
                                <form class="form-horizontal" role="form" method="post" action="">
                                    {{ csrf_field() }}
                                    <div class="col-md-12">
                                        <p class="text-muted font-14 m-b-20 pull-right">
                                            @can('access', [$resource, 'add'])
                                            <a href="{{ route('mutation-add') }}" class="btn btn-primary waves-effect w-md waves-light"><i class="fa fa-plus"></i> {{ trans('fields.add-mutation') }}</a>
                                            @endcan

                                      

                                            <div class="col-md-2">
                                                <input type="text" name="tally_no" class="form-control" value="{{ !empty($filters['tally_no']) ? $filters['tally_no'] : '' }}">
                                                @if($errors->has('tally_no'))
                                                <span class="help-block">{{ $errors->first('tally_no') }}</span>
                                                @endif
                                            </div>

                                            <div class="col-md-4">
                                                <button type="submit" class="btn btn-info waves-effect w-xs waves-light"><i class="fa fa-search"></i> </button>
                                            </div>
                                        </p>
                                    </div>
                                </form>
                            </div>


                            <div class="row">
                                <table class="table table-bordered table-hover table-striped m-0">
                                    <thead>
                                        <tr>
                                            <th width="5%" class="text-center">{{ trans('fields.num') }}</th>
                                            <th width="20%" class="text-center">{{ trans('fields.mutation-location') }}</th>
                                            <th width="15%" class="text-center">{{ trans('fields.no_dr') }}</th>
                                            <th width="15%" class="text-center">{{ trans('fields.date-mutation') }}</th> 
                                            <th width="15%" class="text-center">{{ trans('fields.driver_plat') }}</th>
                                            <th width="15" class="text-center">{{ trans('fields.note') }}</th>
                                            <th width="15%" class="text-center">{{ trans('fields.action') }}</th>  
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = ($models->currentPage() - 1) * $models->perPage() + 1; ?>
                                        @foreach($models as $model)
                                        <tr>
                                            <th scope="row" style="text-align: center;">{{ $no++ }}</th>
                                            <td>{{ $model->lokasi_mutasi }}</td>
                                            <td>{{ $model->nomor_mutasi }}</td>
                                            <td>{{ date('d-m-Y', strtotime($model->tanggal_mutasi)) }}</td>
                                            <td>{{ $model->supir_plat }}</td>
                                            <td>{{ $model->catatan }}</td>
                                            <td class="text-center">
                                                @can('access', [$resource, 'update'])
                                                <a href="{{ route('mutation-edit', ['id' => $model->hd_mutasi_id]) }}" data-toggle="tooltip" class="btn btn-xs btn-warning" data-original-title="Edit">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                @endcan

                                                 @can('access', [$resource, 'update'])
                                                <a href="{{ route('mutation-addtally', ['id' => $model->hd_mutasi_id]) }}" data-toggle="tooltip" class="btn btn-xs btn-custom" data-original-title="add">
                                                    <i class="fa fa-plus"></i>
                                                </a>
                                                @endcan
                                                 @can('access', [$resource, 'update'])
                                                <a href="{{ route('mutation-pdf', ['id' => $model->hd_mutasi_id]) }}" data-toggle="tooltip" class="btn btn-xs btn-custom" data-original-title="print">
                                                    <i class="fa fa-print"></i>
                                                </a>
                                                @endcan
                                                 @can('access', [$resource, 'update'])
                                                <a href="{{ route('mutation-pdfattach', ['id' => $model->hd_mutasi_id]) }}" data-toggle="tooltip" class="btn btn-xs btn-custom" data-original-title="print">
                                                    <i class="fa fa-print"></i>
                                                </a>
                                                @endcan
                                                @can('access', [$resource, 'update'])
                                                <a href="{{ route('mutation-export-excel', ['id' => $model->hd_mutasi_id]) }}" data-toggle="tooltip" class="btn btn-xs btn-custom" data-original-title="Export Excel">
                                                    <i class="fa fa-print"></i>
                                                </a>
                                                @endcan




                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                    
                                    

                                </table>
                                <div class="data-table-toolbar pull-right">
                                    {!! $models->render() !!}
                                </div>
                            </div>


                        
                     
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

