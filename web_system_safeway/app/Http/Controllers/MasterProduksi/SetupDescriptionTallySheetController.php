<?php

namespace App\Http\Controllers\MasterProduksi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\MasterProduksi\DeskripsiTallySheet;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class SetupDescriptionTallySheetController extends Controller
{
    const URL       = 'master-produksi/description-tallysheet';
    const RESOURCE  = 'description-tallysheet';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        if ($request->isMethod('post')) {
            $request->session()->put('filters', $request->all());
            return redirect(self::URL . '?page=1');
        } elseif (empty($request->get('page'))) {
            $request->session()->forget('filters');
        }
        $filters = $request->session()->get('filters');
        $query = \DB::table('mst_deskripsi_tallysheet')
            ->orderBy('mst_deskripsi_tallysheet.nama_deskripsi');

        if (!empty($filters['status']) || !$request->session()->has('filters')) {
            $query->where('mst_deskripsi_tallysheet.status', '=', true);
        } else {
            $query->where('mst_deskripsi_tallysheet.status', '=', false);
        }

        if (!empty($filters['nama_deskripsi'])) {
            $query->where('mst_deskripsi_tallysheet.nama_deskripsi', 'ilike', '%' . $filters['nama_deskripsi'] . '%');
        }

        return view('master-produksi/description-tallysheet.index', [
            "models"        => $query->paginate(10),
            "filters"       => $filters,
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function add(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        $model           = new DeskripsiTallySheet();
        $model->status   = true;

        return view('master-produksi/description-tallysheet.add', [
            "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }

    public function edit(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $model = DeskripsiTallySheet::find($id);

        if ($model === null) {
            abort('404');
        }

        return view('master-produksi/description-tallysheet.add', [
            "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }


    public function exportexcel()
    {

        ob_end_clean();
        ob_start();
        Excel::create('Export data', function ($excel) {

            $excel->sheet('Sheet 1', function ($sheet) {

                $products = DB::table('mst_deskripsi_tallysheet')
                    ->select('nama_deskripsi', 'status')
                    ->get();
                foreach ($products as $product) {
                    $data[] = array(
                        $product->nama_deskripsi,
                        $product->status,

                    );
                }

                $sheet->fromArray($data, null, 'A1', false, false);
                $headings = array('nama_deskripsi', 'status');
                $sheet->prependRow(1, $headings);
            });
        })->export('xlsx');
    }




    public function importexcel(Request $request)
    {

        //if ($request->Request('file')) {
        $model  = new DeskripsiTallySheet();
        $now    = new \DateTime();



        //$model->modified_date = $now;
        $path = $request->file('file')->getRealPath();
        $data = EXCEL::load($path, function ($reader) { })->get();

        if (!empty($data) && $data->count()) {
            foreach ($data as $key => $value) {
                $modal = new DeskripsiTallySheet();
                $modal->nama_deskripsi = $value->nama_deskripsi;
                $modal->created_by = \Auth::user()->id;
                $modal->created_date    = $now;
                $modal->save();
            }
        }

        return back();
    }



    public function save(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add']) && !\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $id     = intval($request->get('id', 0));

        $this->validate($request, [
            'nama_deskripsi' => 'required|max:255|unique:mst_deskripsi_tallysheet,nama_deskripsi,' . $id . ',deskripsi_id',
        ]);

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            if (empty($id)) {
                $model  = new DeskripsiTallySheet();
                $model->created_by      = \Auth::user()->id;
                $model->created_date    = $now;
            } else {
                $model  = DeskripsiTallySheet::find($id);
                $model->modified_by   = \Auth::user()->id;
                $model->modified_date = $now;
            }

            $model->nama_deskripsi   = $request->get('nama_deskripsi');
            $model->status   = !empty($request->get('status')) ? true : false;

            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('menu.setup-description') . ' ' . $request->get('nama_deskripsi')])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('menu.setup-description') . ' ' . $request->get('nama_deskripsi')])
            );
        }

        return redirect(route('setup-description-index'));
    }
}
