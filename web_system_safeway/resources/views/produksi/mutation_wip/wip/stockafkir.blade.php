@extends('layouts.master')

@section('title', trans('menu.bandsaw'))

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">{{ trans('menu.stock-afkir') }}</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="{{ route('dashboard') }}">{{ trans('menu.dashboard') }}</a>
                            </li>
                            <li>
                                <a>{{ trans('menu.produksi') }}</a>
                            </li>
                            <li class="active">
                                {{ trans('menu.stock-afkir') }}
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            @if(Session::has('successMessage'))
            <div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-check-all"></i>
                <strong>{{ Session::get('successMessage') }}.</strong>
            </div>
            @endif

            @if(Session::has('errorMessage'))
            <div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-block-helper"></i>
                <strong>{{ Session::get('errorMessage') }}.</strong>
            </div>
            @endif
            
            <div class="row">
                <div class=" col-md-12">
                    <div class="panel panel-border panel-success">
                        <div class="panel-heading">
                            
                        </div>
                        

                        <div class="panel-body">
                            <div class="row">
                                <!-- <form class="form-horizontal" role="form" method="post" action="{{ route('afkir-kering-save') }}"> -->
                                    {{ csrf_field() }}
                                    <div class="col-sm-12 data-table-toolbar text-right">
                                        <div class="form-group">

                                              @can('access', [$resource, 'add'])
                                            <button type="button" class="btn btn-primary waves-effect w-md waves-light" data-toggle="modal" data-target="#dtafkir"><i class="fa fa-plus-circle"></i> {{ trans('fields.add-data-afkir') }} </button>
                                            @endcan


                                           @can('access', [$resource, 'add'])
                                            <a href="{{ route('reproses-afkir-index') }}" class="btn btn-primary waves-effect w-md waves-light"><i class="fa fa-plus"></i> {{ trans('fields.stock-reproses-afkir') }}</a>
                                            @endcan
                                            <a id="clear-details" href="{{ url('produksi/wip') }}"  class="btn btn-sm btn-success">
                                                <i class="fa fa-reply"></i> {{ trans('fields.back') }} 
                                            </a>
                                        </div>
                                        <div class="row">
                                            <table class="table table-bordered table-hover table-striped m-0">
                                                <thead>
                                                    <tr>
                                                        <th width="5%" class="text-center">{{ trans('fields.num') }}</th>
                                                        <th width="10%" class="text-center">{{ trans('fields.wood-type') }}</th>
                                                        <th width="10%" class="text-center">{{ trans('fields.high') }}</th>
                                                        <th width="10%" class="text-center">{{ trans('fields.width') }}</th>
                                                        <th width="10%" class="text-center">{{ trans('fields.length') }}</th>
                                                        <th width="10%" class="text-center">{{ trans('fields.pcs') }}</th>
                                                        <th width="10%" class="text-center">{{ trans('fields.m3') }}</th>
                                                        <th width="5%" class="text-center">{{ trans('fields.action') }}</th>
                                                    </tr>
                                                </thead>
                                              	 <tbody>
                                                    <?php $no = ($models->currentPage() - 1) * $models->perPage() + 1; ?>
                                                    @foreach($models as $model)
                                                    <tr>
                                                        <th scope="row" style="text-align: center;">{{ $no++ }}</th>
                                                       
                                                        <td>{{ $model->jenis_kayu }}</td>
                                                        <td>{{ $model->tinggi }}</td>
                                                        <td>{{ $model->lebar }}</td>
                                                        <td>{{ $model->panjang }}</td>
                                                        <td>{{ $model->pcs }}</td>
                                                        <td>{{ $model->volume }}</td>
                                                        <td class="text-center">
                                                            @can('access', [$resource, 'add'])
                                                            <a  data-toggle="modal" data-target="#dtstockafkir{{ $model->id_jumlah_afkir }}" class="btn btn-xs btn-danger" data-original-title="Edit">

                                                                {{ trans('fields.reproses-afkir') }}</i>
                                                            </a>
                                                            <!-- modal -->
                                                             <div id="dtstockafkir{{ $model->id_jumlah_afkir }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <form role="form" method="post" action="{{ route('wip-savetakeafkir') }}">
                                                                            {{ csrf_field() }}

                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                                <h4 class="modal-title">{{ trans('fields.taken-afkir') }}</h4>
                                                                            </div>



                                                                            <div class="modal-body">

                                                                                <div class="row">
                                                                                      <div class="col-md-6">
                                                                                        <label>{{ trans('fields.taken-stockafkir') }}</label>
                                                                                            <div>
                                                                                                <div class="form-group">
                                                                                                     <input type="number" class="form-control" name="pcs">
                                                                                                     <input type="hidden" name="id_jumlah_afkir" class="form-control" value="{{ $model->id_jumlah_afkir }}">
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                </div>

                                                                                
                                                                            </div>
                                                                           
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close
                                                                                </button>
                                                                                <button type="submit" class="btn btn-info waves-effect waves-light">Save changes</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal -->
                                                            @endcan



                                                           <!--  @can('access', [$resource, 'add'])
                                                            <a  data-toggle="modal" data-target="#dtminafkir{{ $model->id_jumlah_afkir }}" class="btn btn-xs btn-warning" data-original-title="Edit">
                                                                
                                                                {{ trans('fields.minafkir') }}
                                                            </a> -->
                                                            <!-- modal -->
                                                             <!-- <div id="dtminafkir{{ $model->id_jumlah_afkir }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <form role="form" method="post" action="{{ route('wip-saveminafkir') }}">
                                                                            {{ csrf_field() }}

                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                                <h4 class="modal-title">{{ trans('fields.taken-afkir') }}</h4>
                                                                            </div>



                                                                            <div class="modal-body">

                                                                                <div class="row">
                                                                                      <div class="col-md-6">
                                                                                        <label>{{ trans('fields.taken-stockafkir') }}</label>
                                                                                            <div>
                                                                                                <div class="form-group">
                                                                                                     <input type="number" class="form-control" name="pcs">
                                                                                                     <input type="hidden" name="id_jumlah_afkir" class="form-control" value="{{ $model->id_jumlah_afkir }}">
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                </div>

                                                                                
                                                                            </div>
                                                                           
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close
                                                                                </button>
                                                                                <button type="submit" class="btn btn-info waves-effect waves-light">Save changes</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div> -->
                                                            <!-- /.modal -->
                                                          <!--   @endcan -->
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                          
                                                
                                            </table>
                                            <div class="data-table-toolbar">
                                                {!! $models->render() !!}
                                            </div>
                                        </div>
                                    </div>
                                <!-- </form> -->
                            </div> 
                        </div>
                     
                    </div>
                </div>

            </div>


                     <div id="dtafkir" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form role="form" method="post" action="{{ route('wipstockafkir-save') }}">
                                        {{ csrf_field() }}
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">{{ trans('fields.add-tally-sheet') }}</h4>
                                        </div>
                                        <div class="modal-body">
                                      
                                           
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="field-1" class="control-label">{{ trans('fields.wood-type') }}</label>
                                                        <select class="form-control" id="jenis_kayu_id" name="jenis_kayu_id">
                                                            <option value="">- {{ trans('fields.change') }} {{ trans('fields.wood-type') }} -</option>
                                                            @foreach($woodOptions as $wood)
                                                            <option value="{{ $wood->jenis_kayu_id }}" {{ !empty($filters['wood']) && $filters['wood'] == $wood->jenis_kayu_id ? 'selected' : ''}} >{{ $wood->jenis_kayu }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                               
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="tinggi" class="control-label">{{ trans('fields.high') }}</label>
                                                        <input type="number" class="form-control" id="tinggi" name="tinggi">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="lebar" class="control-label">{{ trans('fields.width') }}</label>
                                                        <input type="number" class="form-control" id="lebar" name="lebar">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="panjang" class="control-label">{{ trans('fields.length') }}</label>
                                                        <input type="number" class="form-control" id="panjang" name="panjang">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="pcs" class="control-label">{{ trans('fields.pcs') }}</label>
                                                        <input type="number" class="form-control" id="pcs" name="pcs">
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div><!-- /.modal -->


        </div>
    </div>
</div>
@endsection