<?php

namespace App\Model\Produksi;

use Illuminate\Database\Eloquent\Model;

class IsiBandsaw extends Model
{
    protected $table        = 'dt_isi_bandsaw';
    protected $primaryKey   = 'isi_bandsaw_id';
    public $timestamps      = false;
}
