<?php

namespace App\Http\Controllers\Produksi\Mutation_Wip;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Service\Master\TallyService;
use App\Model\MasterProduksi\Supplier;
use App\Service\Master\SupplierService;
use App\Model\Produksi\IsiAfkirProses;
use App\Service\Master\WoodService;
use Elibyy\TCPDF\Facades\TCPDF;
use App\Model\Produksi\IsiBandsaw;
use App\Model\Produksi\IsiAfkir;
use App\Model\Produksi\IsiMS;
use App\Model\Produksi\DtAmbilStokAfkir;
use App\Service\Master\ChamberService;
use App\Model\MasterProduksi\LokasiMutasi;
use App\Service\Master\MutationLocationService;
use App\Model\Produksi\JumlahAfkir;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class WIPController extends Controller
{
    const URL       = 'produksi/wip';
    const RESOURCE  = 'wip';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        if ($request->isMethod('post')) {
            $request->session()->put('filters', $request->all());
            return redirect(self::URL . '?page=1');
        } elseif (empty($request->get('page'))) {
            $request->session()->forget('filters');
        }


        $filters = $request->session()->get('filters');
        $query = \DB::table('tally_table')
            ->select('tally_table.tally_no')
            // ->select('tally_table.tally_id', 'tally_table.tally_no', 'tally_table.date_mutation_wip', DB::raw('sum(dt_isi_tally.volume) as volumes_t'), DB::raw('sum(dt_isi_ms.volume) as volumes_m')) //
            ->where('tally_table.status', '=', true)
            ->where('tally_table.date_in_kd', '!=', NULL)
            ->where('tally_table.date_out_kd', '!=', NULL)
            ->where('tally_table.date_mutation_wip', '!=', NULL)
            ->where('status_mutation_wip', '=', 1)

            ->orderBy('dt_isi_tally.tally_id');




        //dd($query2);




        if (!empty($filters['status']) || !$request->session()->has('filters')) {
            $query->where('tally_table.status', '=', true);
        } else {
            $query->where('tally_table.status', '=', false);
        }

        $filters = $request->session()->get('filters');


        // dd ($query);

        // $query2 = \DB::table('dt_isi_tally')
        //         ->select('tally_table.tally_id', 'tally_table.tally_no', 'tally_table.date_mutation_wip', DB::raw('round((sum(dt_isi_tally.volume)/sum(dt_isi_ms.volume))*100, 2) as volumes'))
        //         // ->select('tally_table.tally_id', 'tally_table.tally_no', 'tally_table.date_mutation_wip') //, DB::raw('sum(dt_isi_tally.volume) as volumes')
        //         ->where('tally_table.status', '=', true)
        //             ->where('tally_table.date_in_kd', '!=', NULL)
        //             ->where('tally_table.date_out_kd', '!=', NULL)
        //             ->where('tally_table.date_mutation_wip', '!=', NULL)
        //             ->where('status_mutation_wip', '=', 1)
        //         ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
        //         ->join('dt_isi_ms', 'dt_isi_ms.tally_id', '=', 'dt_isi_tally.tally_id')
        //         ->groupBy('tally_table.tally_id', 'tally_table.tally_no', 'tally_table.date_mutation_wip')
        //         ->orderBy('dt_isi_tally.created_by')->get();


        // dd ($query2);  

        $query = \DB::table('tally_table')
            ->select(
                'tally_table.tally_id',
                'tally_table.tally_no',
                'tally_table.date_mutation_wip',
                DB::raw('(CASE WHEN (select sum(a.volume) from dt_isi_ms a where a.tally_id = tally_table.tally_id) = NULL THEN 0 ELSE round((select sum(a.volume)
                  from dt_isi_ms a where a.tally_id = tally_table.tally_id)/(select sum(b.volume) from dt_isi_tally b where b.tally_id = tally_table.tally_id)*100, 2) END) as volumes')
            )
            //DB::raw('(select sum(a.volume) from dt_isi_ms a where a.tally_id = tally_table.tally_id) as volume_ms , (select sum(b.volume) from dt_isi_tally b where b.tally_id = tally_table.tally_id) as volume_tally')
            // ->select('tally_table.tally_id', 'tally_table.tally_no', 'tally_table.date_mutation_wip', DB::raw('sum(dt_isi_tally.volume) as volumes_t'), DB::raw('sum(dt_isi_ms.volume) as volumes_m')) //
            ->where('tally_table.status', '=', true)
            ->where('tally_table.date_in_kd', '!=', NULL)
            ->where('tally_table.date_out_kd', '!=', NULL)
            ->where('tally_table.date_mutation_wip', '!=', NULL)
            ->where('status_mutation_wip', '=', 1)
            // ->join('dt_isi_tally', 'dt_isi_tally.tally_id', '=', 'tally_table.tally_id')
            // ->leftjoin('dt_isi_ms', 'dt_isi_ms.tally_id', '=', 'dt_isi_tally.tally_id')
            // ->groupBy('tally_table.tally_id', 'tally_table.tally_no', 'tally_table.date_mutation_wip')
            // ->orderBy('dt_isi_tally.tally_id')->get();
            ->orderBy('tally_table.tally_id');

        // dd($query);

        if (!empty($filters['tally_no'])) {
            $query->where('tally_table.tally_no', 'like', '%' . $filters['tally_no'] . '%');
        }

        return view('produksi/mutation_wip/wip.index', [
            // "models"        => $query->paginate(10),
            "models"        => $query->paginate(10),
            // "query2"        => $query2,
            "filters"       => $filters,
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function stockafkir(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        // $model = LPB::find($id);

        // $query = DB::table('dt_isi_afkir_proses')
        // ->select('mst_jenis_kayu.jenis_kayu', 'dt_isi_afkir_proses.tinggi', 'dt_isi_afkir_proses.lebar', 'dt_isi_afkir_proses.panjang',DB::raw('sum(dt_isi_afkir_proses.pcs) as pcs, sum(dt_isi_afkir_proses.volume) as volume'))
        // // ->where('dt_isi_afkir_proses.keterangan', '=', 'WIP')
        // ->where('dt_isi_afkir_proses.status', '=', true)
        // ->groupBy('mst_jenis_kayu.jenis_kayu', 'dt_isi_afkir_proses.tinggi', 'dt_isi_afkir_proses.lebar', 'dt_isi_afkir_proses.panjang')
        // ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_afkir_proses.jenis_kayu_id')
        // ->orderBy('dt_isi_afkir_proses.created_by');

        $query = DB::table('jumlah_afkir')
            ->select('jumlah_afkir.id_jumlah_afkir', 'mst_jenis_kayu.jenis_kayu', 'jumlah_afkir.tinggi', 'jumlah_afkir.lebar', 'jumlah_afkir.panjang', 'jumlah_afkir.pcs', 'jumlah_afkir.volume')
            ->where('jumlah_afkir.pcs', '!=', 0)
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'jumlah_afkir.jenis_kayu_id')
            ->orderBy('jumlah_afkir.pcs', 'desc');

        // dd($query);

        return view('produksi/mutation_wip/wip.stockafkir', [

            "models"         => $query->paginate(10),
            //"tallyOptions"   => TallyService::getActiveTally(),
            // "desOptions"   => DesService::getActiveDes(),
            "woodOptions"   => WoodService::getActiveWood(),
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }


    public function exportexcel()
    {

        ob_end_clean();
        ob_start();
        Excel::create('Export data', function ($excel) {

            $excel->sheet('Ukuran Awal', function ($sheet) {

                //         $query = \DB::table('tally_table')
                //             ->select(
                //                 'tally_table.tally_id',
                //                 'tally_table.tally_no',
                //                 'tally_table.date_mutation_wip',
                //                 'dt_isi_tally.tinggi',
                //                 'dt_isi_tally.lebar',
                //                 'dt_isi_tally.panjang',
                //                 'dt_isi_tally.pcs',
                //                 'dt_isi_tally.volume',
                //                 'dt_isi_ms.tinggi as t',
                //                 'dt_isi_ms.lebar as l',
                //                 'dt_isi_ms.panjang as p',
                //                 'dt_isi_ms.pcs as pc',
                //                 'dt_isi_ms.volume as vol',
        
        
                //                 DB::raw('(CASE WHEN (select sum(a.volume) from dt_isi_ms a where a.tally_id = tally_table.tally_id) = NULL THEN 0 ELSE round((select sum(a.volume)
                //   from dt_isi_ms a where a.tally_id = tally_table.tally_id)/(select sum(b.volume) from dt_isi_tally b where b.tally_id = tally_table.tally_id)*100, 2) END) as volumes')
                //             )
                //             //DB::raw('(select sum(a.volume) from dt_isi_ms a where a.tally_id = tally_table.tally_id) as volume_ms , (select sum(b.volume) from dt_isi_tally b where b.tally_id = tally_table.tally_id) as volume_tally')
                //             // ->select('tally_table.tally_id', 'tally_table.tally_no', 'tally_table.date_mutation_wip', DB::raw('sum(dt_isi_tally.volume) as volumes_t'), DB::raw('sum(dt_isi_ms.volume) as volumes_m')) //
                //             ->where('tally_table.status', '=', true)
                //             ->where('tally_table.date_in_kd', '!=', NULL)
                //             ->where('tally_table.date_out_kd', '!=', NULL)
                //             ->where('tally_table.date_mutation_wip', '!=', NULL)
                //             ->where('status_mutation_wip', '=', 1)
                //             ->join('dt_isi_tally', 'dt_isi_tally.tally_id', '=', 'tally_table.tally_id')
                //             ->join('dt_isi_ms', 'dt_isi_ms.tally_id', '=', 'tally_table.tally_id')
                //             // ->groupBy('tally_table.tally_id', 'tally_table.tally_no', 'tally_table.date_mutation_wip')
                //             // ->orderBy('dt_isi_tally.tally_id')->get();
                //             ->orderBy('tally_table.tally_id')->get();
                //         //   dd($query);
        
                $query = \DB::table('tally_table')
                            ->select(
                                'tally_table.tally_id',
                                'tally_table.tally_no',
                                'tally_table.date_mutation_wip',
                                'mst_jenis_kayu.jenis_kayu',
                                'dt_isi_tally.tinggi as t',
                                'dt_isi_tally.lebar as l',
                                'dt_isi_tally.panjang as p',
                                'dt_isi_tally.pcs as pc',
                                'dt_isi_tally.volume as vol'
                            )
                            //DB::raw('(select sum(a.volume) from dt_isi_ms a where a.tally_id = tally_table.tally_id) as volume_ms , (select sum(b.volume) from dt_isi_tally b where b.tally_id = tally_table.tally_id) as volume_tally')
                            // ->select('tally_table.tally_id', 'tally_table.tally_no', 'tally_table.date_mutation_wip', DB::raw('sum(dt_isi_tally.volume) as volumes_t'), DB::raw('sum(dt_isi_ms.volume) as volumes_m')) //
                            ->where('tally_table.status', '=', true)
                            ->where('tally_table.date_in_kd', '!=', NULL)
                            ->where('tally_table.date_out_kd', '!=', NULL)
                            ->where('tally_table.date_mutation_wip', '!=', NULL)
                            ->where('status_mutation_wip', '=', 1)
                            ->join('dt_isi_tally', 'dt_isi_tally.tally_id', '=', 'tally_table.tally_id')
                            ->join('mst_jenis_kayu','mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id' )
                            // ->groupBy('tally_table.tally_id', 'tally_table.tally_no', 'tally_table.date_mutation_wip')
                            // ->orderBy('dt_isi_tally.tally_id')->get();
                            ->orderBy('tally_table.tally_id')->get();
                        //   dd($query);
        
        
        
                    //  dd($query);
                                
                        foreach ($query as $product) {
                            $data[] = array(
                                $product->tally_no,
                                $product->date_mutation_wip,
                                $product->jenis_kayu,
                                // $product->tinggi,
                                // $product->lebar,
                                // $product->panjang,
                                // $product->pcs,
                                // $product->volume,
                                $product->t,
                                $product->l,
                                $product->p,
                                $product->pc,
                                $product->vol
        
        
        
        
        
                            );
                        }
                        // dd($product);
        
                        $sheet->fromArray($data, null, 'A1', false, false);
                        // $headings = array('Nomor Tally', 'Tanggal WIP',  'Tinggi', 'Lebar RM','Panjang RM', 'Pcs RM', 'Volume RM', 'Tinggi MS', 'Lebar MS','Panjang MS', 'Pcs MS', 'Volume MS');
                        $headings = array('Nomor Tally', 'Tanggal WIP', 'jenis kayu', 'Tinggi', 'Lebar','Panjang', 'Pcs', 'Volume');
                        $sheet->prependRow(1, $headings);
                    });
        

            $excel->sheet('Ukuran Akhir (MS)', function ($sheet) {

        //         $query = \DB::table('tally_table')
        //             ->select(
        //                 'tally_table.tally_id',
        //                 'tally_table.tally_no',
        //                 'tally_table.date_mutation_wip',
        //                 'dt_isi_tally.tinggi',
        //                 'dt_isi_tally.lebar',
        //                 'dt_isi_tally.panjang',
        //                 'dt_isi_tally.pcs',
        //                 'dt_isi_tally.volume',
        //                 'dt_isi_ms.tinggi as t',
        //                 'dt_isi_ms.lebar as l',
        //                 'dt_isi_ms.panjang as p',
        //                 'dt_isi_ms.pcs as pc',
        //                 'dt_isi_ms.volume as vol',


        //                 DB::raw('(CASE WHEN (select sum(a.volume) from dt_isi_ms a where a.tally_id = tally_table.tally_id) = NULL THEN 0 ELSE round((select sum(a.volume)
        //   from dt_isi_ms a where a.tally_id = tally_table.tally_id)/(select sum(b.volume) from dt_isi_tally b where b.tally_id = tally_table.tally_id)*100, 2) END) as volumes')
        //             )
        //             //DB::raw('(select sum(a.volume) from dt_isi_ms a where a.tally_id = tally_table.tally_id) as volume_ms , (select sum(b.volume) from dt_isi_tally b where b.tally_id = tally_table.tally_id) as volume_tally')
        //             // ->select('tally_table.tally_id', 'tally_table.tally_no', 'tally_table.date_mutation_wip', DB::raw('sum(dt_isi_tally.volume) as volumes_t'), DB::raw('sum(dt_isi_ms.volume) as volumes_m')) //
        //             ->where('tally_table.status', '=', true)
        //             ->where('tally_table.date_in_kd', '!=', NULL)
        //             ->where('tally_table.date_out_kd', '!=', NULL)
        //             ->where('tally_table.date_mutation_wip', '!=', NULL)
        //             ->where('status_mutation_wip', '=', 1)
        //             ->join('dt_isi_tally', 'dt_isi_tally.tally_id', '=', 'tally_table.tally_id')
        //             ->join('dt_isi_ms', 'dt_isi_ms.tally_id', '=', 'tally_table.tally_id')
        //             // ->groupBy('tally_table.tally_id', 'tally_table.tally_no', 'tally_table.date_mutation_wip')
        //             // ->orderBy('dt_isi_tally.tally_id')->get();
        //             ->orderBy('tally_table.tally_id')->get();
        //         //   dd($query);

        $query = \DB::table('tally_table')
                    ->select(
                        'tally_table.tally_id',
                        'tally_table.tally_no',
                        'tally_table.date_mutation_wip',
                        'mst_jenis_kayu.jenis_kayu',
                        'dt_isi_ms.tinggi as t',
                        'dt_isi_ms.lebar as l',
                        'dt_isi_ms.panjang as p',
                        'dt_isi_ms.pcs as pc',
                        'dt_isi_ms.volume as vol'
                    )
                    //DB::raw('(select sum(a.volume) from dt_isi_ms a where a.tally_id = tally_table.tally_id) as volume_ms , (select sum(b.volume) from dt_isi_tally b where b.tally_id = tally_table.tally_id) as volume_tally')
                    // ->select('tally_table.tally_id', 'tally_table.tally_no', 'tally_table.date_mutation_wip', DB::raw('sum(dt_isi_tally.volume) as volumes_t'), DB::raw('sum(dt_isi_ms.volume) as volumes_m')) //
                    ->where('tally_table.status', '=', true)
                    ->where('tally_table.date_in_kd', '!=', NULL)
                    ->where('tally_table.date_out_kd', '!=', NULL)
                    ->where('tally_table.date_mutation_wip', '!=', NULL)
                    ->where('status_mutation_wip', '=', 1)
                    ->join('dt_isi_ms', 'dt_isi_ms.tally_id', '=', 'tally_table.tally_id')
                    ->join('mst_jenis_kayu','mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_ms.jenis_kayu_id' )
                    // ->groupBy('tally_table.tally_id', 'tally_table.tally_no', 'tally_table.date_mutation_wip')
                    // ->orderBy('dt_isi_tally.tally_id')->get();
                    ->orderBy('tally_table.tally_id')->get();
                //   dd($query);



            //  dd($query);
                        
                foreach ($query as $product) {
                    $data[] = array(
                        $product->tally_no,
                        $product->date_mutation_wip,
                        $product->jenis_kayu,
                        // $product->tinggi,
                        // $product->lebar,
                        // $product->panjang,
                        // $product->pcs,
                        // $product->volume,
                        $product->t,
                        $product->l,
                        $product->p,
                        $product->pc,
                        $product->vol





                    );
                }
                // dd($product);

                $sheet->fromArray($data, null, 'A1', false, false);
                // $headings = array('Nomor Tally', 'Tanggal WIP',  'Tinggi', 'Lebar RM','Panjang RM', 'Pcs RM', 'Volume RM', 'Tinggi MS', 'Lebar MS','Panjang MS', 'Pcs MS', 'Volume MS');
                $headings = array('Nomor Tally', 'Tanggal WIP','jenis_kayu',  'Tinggi', 'Lebar','Panjang', 'Pcs', 'Volume');
                $sheet->prependRow(1, $headings);
            });

            $excel->sheet('Rendemen', function ($sheet) {

                $query = \DB::table('tally_table')
                    ->select(
                        'tally_table.tally_id',
                        'tally_table.tally_no',
                        'tally_table.date_mutation_wip',
                        DB::raw('(CASE WHEN (select sum(a.volume) from dt_isi_ms a where a.tally_id = tally_table.tally_id) = NULL THEN 0 ELSE round((select sum(a.volume)
                        from dt_isi_ms a where a.tally_id = tally_table.tally_id)/(select sum(b.volume) from dt_isi_tally b where b.tally_id = tally_table.tally_id)*100, 2) END) as volumes')
                    )
                    //DB::raw('(select sum(a.volume) from dt_isi_ms a where a.tally_id = tally_table.tally_id) as volume_ms , (select sum(b.volume) from dt_isi_tally b where b.tally_id = tally_table.tally_id) as volume_tally')
                    // ->select('tally_table.tally_id', 'tally_table.tally_no', 'tally_table.date_mutation_wip', DB::raw('sum(dt_isi_tally.volume) as volumes_t'), DB::raw('sum(dt_isi_ms.volume) as volumes_m')) //
                    ->where('tally_table.status', '=', true)
                    ->where('tally_table.date_in_kd', '!=', NULL)
                    ->where('tally_table.date_out_kd', '!=', NULL)
                    ->where('tally_table.date_mutation_wip', '!=', NULL)
                    ->where('status_mutation_wip', '=', 1)
                    // ->join('dt_isi_tally', 'dt_isi_tally.tally_id', '=', 'tally_table.tally_id')
                    // ->leftjoin('dt_isi_ms', 'dt_isi_ms.tally_id', '=', 'dt_isi_tally.tally_id')
                    // ->groupBy('tally_table.tally_id', 'tally_table.tally_no', 'tally_table.date_mutation_wip')
                    // ->orderBy('dt_isi_tally.tally_id')->get();
                    ->orderBy('tally_table.tally_id')->get();
                                
                        foreach ($query as $product) {
                            $data[] = array(
                                $product->tally_no,
                                $product->date_mutation_wip,
                                // $product->tinggi,
                                // $product->lebar,
                                // $product->panjang,
                                // $product->pcs,
                                // $product->volume,
                                // $product->t,
                                // $product->l,
                                // $product->p,
                                // $product->pc,
                                $product->volumes
        
                            );
                        }
                        // dd($product);
        
                        $sheet->fromArray($data, null, 'A1', false, false);
                        // $headings = array('Nomor Tally', 'Tanggal WIP',  'Tinggi', 'Lebar RM','Panjang RM', 'Pcs RM', 'Volume RM', 'Tinggi MS', 'Lebar MS','Panjang MS', 'Pcs MS', 'Volume MS');
                        $headings = array('Nomor Tally', 'Tanggal WIP',  'Rendemen');
                        $sheet->prependRow(1, $headings);
                    });

            
                    $excel->sheet('Data Bandsaw', function ($sheet) {

                        $query = \DB::table('tally_table')
                        ->select(
                            'tally_table.tally_id',
                            'tally_table.tally_no',
                           
                            'mst_jenis_kayu.jenis_kayu',
                            'dt_isi_bandsaw.bandsaw_date as date',
                            'dt_isi_bandsaw.tinggi as t',
                            'dt_isi_bandsaw.lebar as l',
                            'dt_isi_bandsaw.panjang as p',
                            'dt_isi_bandsaw.pcs as pc',
                            'dt_isi_bandsaw.volume as vol',
                            'dt_isi_bandsaw.keterangan as ket'
                        )
                        //DB::raw('(select sum(a.volume) from dt_isi_ms a where a.tally_id = tally_table.tally_id) as volume_ms , (select sum(b.volume) from dt_isi_tally b where b.tally_id = tally_table.tally_id) as volume_tally')
                        // ->select('tally_table.tally_id', 'tally_table.tally_no', 'tally_table.date_mutation_wip', DB::raw('sum(dt_isi_tally.volume) as volumes_t'), DB::raw('sum(dt_isi_ms.volume) as volumes_m')) //
                        ->where('tally_table.status', '=', true)
                        ->where('tally_table.date_in_kd', '!=', NULL)
                        ->where('tally_table.date_out_kd', '!=', NULL)
                        ->where('tally_table.date_mutation_wip', '!=', NULL)
                        ->where('status_mutation_wip', '=', 1)
                        ->join('dt_isi_bandsaw', 'dt_isi_bandsaw.tally_id', '=', 'tally_table.tally_id')
                        ->join('mst_jenis_kayu','mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id' )
                        // ->groupBy('tally_table.tally_id', 'tally_table.tally_no', 'tally_table.date_mutation_wip')
                        // ->orderBy('dt_isi_tally.tally_id')->get();
                        ->orderBy('tally_table.tally_id')->get();
                                        
                        foreach ($query as $product) {
                            $data[] = array(
                                $product->tally_no,
                                $product->date,
                                $product->jenis_kayu,
                                // $product->tinggi,
                                // $product->lebar,
                                // $product->panjang,
                                // $product->pcs,
                                // $product->volume,
                                $product->t,
                                $product->l,
                                $product->p,
                                $product->pc,
                                $product->vol,
                                $product->ket
                
                
                
                
                
                                    );
                                }
                                // dd($product);
                
                                $sheet->fromArray($data, null, 'A1', false, false);
                                // $headings = array('Nomor Tally', 'Tanggal WIP',  'Tinggi', 'Lebar RM','Panjang RM', 'Pcs RM', 'Volume RM', 'Tinggi MS', 'Lebar MS','Panjang MS', 'Pcs MS', 'Volume MS');
                                $headings = array('Nomor Tally', 'Tanggal Bandsaw',  'Jenis Kayu', 'Tinggi', 'Lebar', 'Panjang', 'pcs', 'Volumes', 'Keterangan');
                                $sheet->prependRow(1, $headings);
                            });
        })->export('xlsx');

        
    }




    public function saveminafkir(Request $request)
    {
        // dd("hallo : ".$request->get('id_jumlah_afkir')." blabla : ".$request->get('pcs'));
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        // $id     = intval($request->get('id_jumlah_afkir', 0));

        $this->validate($request, [
            'id_jumlah_afkir'    => 'required|integer',
            // 'jenis_kayu_id'       => 'required|integer',
            // 'tinggi'              => 'required|integer',
            // 'lebar'               => 'required|integer',
            // 'panjang'             => 'required|integer',
            'pcs'                 => 'required|integer',
        ]);


        DB::beginTransaction();
        try {

            $query = DB::table('jumlah_afkir')
                ->select('id_jumlah_afkir', 'jenis_kayu_id', 'tinggi', 'lebar', 'panjang', 'pcs', 'volume')
                ->where('id_jumlah_afkir', "=", $request->get('id_jumlah_afkir'))
                ->orderBy('created_by')->first();

            // dd($query);

            $now    = new \DateTime();
            $model  = new DtProsesStockMs();
            $model->created_by      = \Auth::user()->id;
            $model->created_date    = $now;
            // }else{
            //     $model  =  DtAmbilStockAfkir::find($id);
            //     $model->modified_by   = \Auth::user()->id;
            //     $model->modified_date = $now;
            // }

            $model->id_jumlah_afkir   = $request->get('id_jumlah_afkir');
            $model->jenis_kayu_id   = $query->jenis_kayu_id;
            $model->tinggi   = $query->tinggi;
            $model->lebar   = $query->lebar;
            $model->panjang   = $query->panjang;
            $model->pcs   = $request->get('pcs');
            $model->proses_date   = $request->get('date_min_ms');
            $model->keterangan = 'Pengurangan';

            $model->volume   = ($query->tinggi * $query->panjang * $query->lebar * $request->get('pcs')) / 1000000000;

            $newpcs = $query->pcs - $model->pcs;
            $newvolume = $query->volume - $model->volume;

            $query = DB::table('jumlah_afkir')
                ->where('id_jumlah_afkir', "=", $request->get('id_jumlah_afkir'))
                ->update(['pcs' => $newpcs, 'volume' => $newvolume, 'modified_by' => \Auth::user()->id, 'modified_date' => $now]);




            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('fields.add-mutation') . ' ' . ''])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('fields.add-mutation') . ' ' . ''])
            );
        }

        return redirect(route('ms-stockms'));
    }


    public function savetakeafkir(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        // $id     = intval($request->get('id_jumlah_afkir', 0));

        $this->validate($request, [
            'id_jumlah_afkir'    => 'required|integer',
            // 'jenis_kayu_id'       => 'required|integer',
            // 'tinggi'              => 'required|integer',
            // 'lebar'               => 'required|integer',
            // 'panjang'             => 'required|integer',
            'pcs'                 => 'required|integer',
        ]);

        // dd($request->get('id_jumlah_afkir')." ".$request->get('pcs'));

        DB::beginTransaction();
        try {

            $query = DB::table('jumlah_afkir')
                ->select('id_jumlah_afkir', 'jenis_kayu_id', 'tinggi', 'lebar', 'panjang', 'pcs', 'volume')
                ->where('id_jumlah_afkir', "=", $request->get('id_jumlah_afkir'))
                ->orderBy('created_by')->first();

            // dd($query);

            $now    = new \DateTime();
            $model  = new DtAmbilStokAfkir();
            $model->created_by      = \Auth::user()->id;
            $model->created_date    = $now;
            // }else{
            //     $model  =  DtAmbilStockAfkir::find($id);
            //     $model->modified_by   = \Auth::user()->id;
            //     $model->modified_date = $now;
            // }

            $model->id_jumlah_afkir   = $request->get('id_jumlah_afkir');
            $model->jenis_kayu_id   = $query->jenis_kayu_id;
            $model->tinggi   = $query->tinggi;
            $model->lebar   = $query->lebar;
            $model->panjang   = $query->panjang;
            $model->pcs   = $request->get('pcs');

            $model->volume   = ($query->tinggi * $query->panjang * $query->lebar * $request->get('pcs')) / 1000000000;

            $newpcs = $query->pcs - $model->pcs;
            $newvolume = $query->volume - $model->volume;

            $query = DB::table('jumlah_afkir')
                ->where('id_jumlah_afkir', "=", $request->get('id_jumlah_afkir'))
                ->update(['pcs' => $newpcs, 'volume' => $newvolume, 'modified_by' => \Auth::user()->id, 'modified_date' => $now]);




            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('fields.add-mutation') . ' ' . ''])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('fields.add-mutation') . ' ' . ''])
            );
        }

        return redirect(route('wip-stockafkir'));
    }


    public function savestockafkir(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        $id     = intval($request->get('id_jumlah_afkir', 0));

        $this->validate($request, [

            'jenis_kayu_id'       => 'required|integer',
            'tinggi'              => 'required|integer',
            'lebar'               => 'required|integer',
            'panjang'             => 'required|integer',
            'pcs'                 => 'required|integer',
        ]);

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            // if(empty($id)){
            //     $model  = new JumlahMS();
            //     $model->created_by      = \Auth::user()->id;
            //     $model->created_date    = $now;
            // }else{
            //     $model  = JumlahMS::find($id);
            //     $model->modified_by   = \Auth::user()->id;
            //     $model->modified_date = $now;
            // }


            $jml = DB::table('jumlah_afkir')
                ->select('jumlah_afkir.pcs', 'jumlah_afkir.volume')
                ->where('jumlah_afkir.status', '=', true)
                ->where('jumlah_afkir.jenis_kayu_id', '=', $request->get('jenis_kayu_id'))
                ->where('jumlah_afkir.tinggi', '=', $request->get('tinggi'))
                ->where('jumlah_afkir.lebar', '=', $request->get('lebar'))
                ->where('jumlah_afkir.panjang', '=', $request->get('panjang'))->first();

            // dd($jml);

            if ($jml === null) {
                // dd("null");
                $up  = new JumlahAfkir();
                $up->jenis_kayu_id   = $request->get('jenis_kayu_id');
                $up->tinggi   = $request->get('tinggi');
                $up->lebar   = $request->get('lebar');
                $up->panjang   = $request->get('panjang');
                $up->pcs   = $request->get('pcs');
                $up->volume   = ($request->get('tinggi') * $request->get('panjang') * $request->get('lebar') * $request->get('pcs')) / 1000000000;
                $up->created_by      = \Auth::user()->id;
                $up->created_date    = $now;
                $up->save();
            } else {
                // dd("gak null");
                // $model  = JumlahMS::find($id);
                $pcs = $jml->pcs + $request->get('pcs');
                $volume = $jml->volume + (($request->get('tinggi') * $request->get('panjang') * $request->get('lebar') * $request->get('pcs')) / 1000000000);
                $modified_by   = \Auth::user()->id;
                $modified_date = $now;
                // dd($modified_date);
                $up = DB::table('jumlah_afkir')
                    // ->select('jumlah_afkir.pcs')
                    ->where('jumlah_afkir.status', '=', true)
                    ->where('jumlah_afkir.jenis_kayu_id', '=', $request->get('jenis_kayu_id'))
                    ->where('jumlah_afkir.tinggi', '=', $request->get('tinggi'))
                    ->where('jumlah_afkir.lebar', '=', $request->get('lebar'))
                    ->where('jumlah_afkir.panjang', '=', $request->get('panjang'))
                    ->update(['pcs' => $pcs, 'volume' => $volume, 'modified_by' => $modified_by, 'modified_date' => $now]);
                // dd($up);
            }


            // $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('fields.add-ms') . ' ' . ''])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('fields.add-ms') . ' ' . ''])
            );
        }

        return redirect(route('wip-stockafkir', ['id' => $request->get('tally_id')]));
    }




    public function save(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        $this->validate($request, [
            'date_mutation_wip'    => 'required|date',
            // 'lokasi_mutasi_id'    => 'required|integer',
            'tally_no'      => 'required',
        ]);

        DB::beginTransaction();
        try {

            // $now    = new \DateTime();
            $tally_split = str_replace("\r\n", ',', $request->get('tally_no'));
            $tally_split = explode(",", $tally_split);
            $query = DB::table('tally_table')
                ->whereIn('tally_no', $tally_split)
                ->update(['date_mutation_wip' => $request->get('date_mutation_wip'), 'status_mutation_wip' => 1]);




            // $query->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('fields.add-mutation') . ' ' . ''])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('fields.add-mutation') . ' ' . ''])
            );
        }

        return redirect(route('wip-index'));
    }
}
