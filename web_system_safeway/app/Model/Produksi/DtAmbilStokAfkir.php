<?php

namespace App\Model\Produksi;

use Illuminate\Database\Eloquent\Model;

class DtAmbilStokAfkir extends Model
{
    protected $table        = 'dt_ambil_stockafkir';
    protected $primaryKey   = 'ambil_stockafkir_id';
    public $timestamps      = false;
}
