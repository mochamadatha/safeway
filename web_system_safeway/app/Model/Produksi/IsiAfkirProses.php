<?php

namespace App\Model\Produksi;

use Illuminate\Database\Eloquent\Model;

class IsiAfkirProses extends Model
{
    protected $table        = 'dt_isi_afkir_proses';
    protected $primaryKey   = 'isi_afkir_proses_id';
    public $timestamps      = false;
}
