<?php

namespace App\Service\DivisionIspm;

class SizeService
{
    public static function getActiveSize()
    {
        return \DB::table('mst_sizeispm')
            ->where('status', '=', true)
            ->orderBy('size')
            ->get();
    }
}
