<?php

namespace App\Http\Controllers\Produksi\Tally;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Produksi\Tally;
use App\Model\Produksi\IsiTally;
use App\Model\Produksi\IsiBandsaw;
use App\Model\Produksi\HdLpb;
use App\Model\Produksi\LPBDetail;
use App\Service\Master\WoodService;
use App\Service\Master\DesService;
use DB;

class BandsawController extends Controller
{
    const URL       = 'produksi/bandsaw';
    const RESOURCE  = 'bandsaw';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        if ($request->isMethod('post')) {
            $request->session()->put('filters', $request->all());
            return redirect(self::URL . '?page=1');
        } elseif (empty($request->get('page'))) {
            $request->session()->forget('filters');
        }


        $filters = $request->session()->get('filters');


        $query = DB::table('tally_table')
            ->select('tally_table.tally_id', 'tally_table.tally_no')
            ->where('dt_lpb.status', '=', true)
            // ->where('tally_table.date_in_kd', '=', NULL)
            // ->where('tally_table.date_out_kd', '=', NULL)
            //  ->where('tally_table.date_mutation_wip', '=', NULL)
            ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'tally_table.tally_id')
            ->groupBy('tally_table.tally_id', 'tally_table.tally_no')
            ->orderBy('tally_no');

        if (!empty($filters['status']) || !$request->session()->has('filters')) {
            $query->where('tally_table.status', '=', true);
        } else {
            $query->where('tally_table.status', '=', false);
        }

        $filters = $request->session()->get('filters');
        $query = DB::table('tally_table')
            ->select('tally_table.tally_id', 'tally_table.tally_no')
            ->where('dt_lpb.status', '=', true)

            // ->where('tally_table.date_in_kd', '=', NULL)
            // ->where('tally_table.date_out_kd', '=', NULL)
            //  ->where('tally_table.date_mutation_wip', '=', NULL)
            ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'tally_table.tally_id')
            ->groupBy('tally_table.tally_id', 'tally_table.tally_no')
            ->orderBy('tally_no');

        if (!empty($filters['tally_no'])) {
            $query->where('tally_table.tally_no', 'like', '%' . $filters['tally_no'] . '%');
        }

        return view('produksi/rawmaterial/bandsaw.index', [
            // "tal"        => $model,
            // "tallyLPBOptions"   => TallyService::getTallyLPB(),
            "models"        => $query->paginate(10),
            "woodOptions"   => WoodService::getActiveWood(),
            "desOptions"   => DesService::getActiveDes(),
            // "filters"       => $filters,
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }



    public function add(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        // $model = Tally::find($id);
        $model = DB::table('tally_table')
            ->select('tally_table.tally_id', 'tally_table.tally_no')
            ->where('dt_lpb.status', '=', true)
            ->where('dt_lpb.tally_id', '=', $id)
            ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'tally_table.tally_id')
            ->groupBy('tally_table.tally_id', 'tally_table.tally_no')
            ->orderBy('tally_no')->first();

        $mode = DB::table('hd_lpb')
            ->select('mst_jenis_kayu.jenis_kayu', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 'dt_isi_tally.panjang', 'dt_isi_tally.pcs', 'dt_isi_tally.volume')

            ->where('dt_lpb.tally_id', '=', $id)
            // ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', 'hd_lpb.lokasi_kedatangan_id')
            ->join('dt_lpb', 'dt_lpb.hd_lpb_id', '=', 'hd_lpb.hd_lpb_id')
            ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
            ->orderBy('tally_table.tally_id')->get();

        if ($model === null) {
            abort('404');
        }

        // $query = DB::table('dt_isi_bandsaw')
        // ->select('dt_isi_bandsaw.*', 'tally.table.tally_no', 'mst_jenis_kayu.jenis_kayu', 'mst_deskripsi_tallysheet.nama_deskripsi')
        //     ->where('dt_lpb_id', '=', $id)
        //     ->where('dt_isi_bandsaw.status', '=', true)
        //     ->join('dt_lpb', 'dt_lpb.dt_lpb_id', '=', 'dt_isi_bandsaw.dt_lpb_id')
        //     ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
        //     ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
        //     ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_tally.deskripsi_id')
        //     ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
        //  ->orderBy('dt_isi_bandsaw.created_by');

        $query = DB::table('dt_isi_bandsaw')
            ->select('tally_table.tally_no', 'dt_isi_bandsaw.bandsaw_date', 'dt_isi_bandsaw.isi_bandsaw_id', 'dt_isi_bandsaw.jenis_kayu_id', 'dt_isi_bandsaw.deskripsi_id', 'mst_jenis_kayu.jenis_kayu', 'mst_deskripsi_tallysheet.nama_deskripsi', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'dt_isi_bandsaw.pcs', 'dt_isi_bandsaw.volume', 'dt_isi_bandsaw.created_date')
            ->where('dt_isi_bandsaw.status', '=', true)
            ->where('dt_isi_bandsaw.tally_id', '=', $id)
            // ->join('dt_lpb', 'dt_lpb.dt_lpb_id', '=', 'dt_isi_bandsaw.dt_lpb_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')
            ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')
            // ->orderBy('hd_lpb.created_by');
            ->orderBy('dt_isi_bandsaw.created_date');

        // dd($query);


        return view('produksi/rawmaterial/bandsaw.add', [
            "tal"         => $model,
            "models"         => $query->paginate(10),

            "mod"           => $mode,
            "woodOptions"   => WoodService::getActiveWood(),
            "desOptions"   => DesService::getActiveDes(),
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }



    public function destroy(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $isi = IsiBandsaw::find($id);
        $direct = $isi->isi_bandsaw_id;
        if ($isi === null) {
            abort('404');
        }
        // dd($isi->);

        // $isi =  LPBDetail::where('dt_lpb_id',$id)->delete();



        DB::beginTransaction();
        try {

            $isi->delete();
            DB::commit();



            $request->session()->flash(
                'successMessage',
                trans('message.delete-message', ['variable' => trans('fields.notally') . ' ' . $id])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.faildel-message', ['variable' => trans('fields.notally') . ' ' . $id])
            );
        }

        return redirect(route('bandsaw-add', ['id' => $direct]));
    }



    public function edit(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $model = Tally::find($id);

        if ($model === null) {
            abort('404');
        }


        $query = DB::table('dt_isi_bandsaw')
            ->select('tally_table.tally_no', 'dt_isi_bandsaw.bandsaw_date', 'dt_isi_bandsaw.isi_bandsaw_id', 'dt_isi_bandsaw.jenis_kayu_id', 'dt_isi_bandsaw.deskripsi_id', 'mst_jenis_kayu.jenis_kayu', 'mst_deskripsi_tallysheet.nama_deskripsi', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'dt_isi_bandsaw.pcs', 'dt_isi_bandsaw.volume', 'dt_isi_bandsaw.created_date')
            ->where('dt_isi_bandsaw.status', '=', true)
            ->where('dt_isi_bandsaw.tally_id', '=', $id)
            // ->join('dt_lpb', 'dt_lpb.dt_lpb_id', '=', 'dt_isi_bandsaw.dt_lpb_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')
            ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')
            // ->orderBy('hd_lpb.created_by');
            ->orderBy('dt_isi_bandsaw.created_date');

        return view('produksi/rawmaterial/bandsaw.add', [
            "model"         => $model,
            "models"         => $query->paginate(10),
            "woodOptions"   => WoodService::getActiveWood(),
            "desOptions"   => DesService::getActiveDes(),
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }



    public function save(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        $id     = intval($request->get('isi_bandsaw_id', 0));




        $this->validate($request, [
            'tally_id'            => 'required|integer',
            'jenis_kayu_id'       => 'required|integer',
            'deskripsi_id'        => 'required|integer',
            'tinggi'              => 'required|integer',
            'lebar'               => 'required|integer',
            'panjang'             => 'required|integer',
            'pcs'                 => 'required|integer',

        ]);

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            if (empty($id)) {
                $model  = new IsiBandsaw();
                $model->created_by      = \Auth::user()->id;
                $model->created_date    = $now;
            } else {
                $model  = IsiBandsaw::find($id);
                $model->modified_by   = \Auth::user()->id;
                $model->modified_date = $now;
            }

            $model->tally_id   = $request->get('tally_id');
            $model->jenis_kayu_id   = $request->get('jenis_kayu_id');
            $model->deskripsi_id   = $request->get('deskripsi_id');
            $model->tinggi   = $request->get('tinggi');
            $model->lebar   = $request->get('lebar');
            $model->panjang   = $request->get('panjang');
            $model->pcs   = $request->get('pcs');
            $model->volume   = ($request->get('tinggi') * $request->get('panjang') * $request->get('lebar') * $request->get('pcs')) / 1000000000;
            $model->keterangan          = 'Basah';
            $model->bandsaw_date        = $request->get('bandsaw_date');



            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('fields.add-bandsaw') . ' ' . ''])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('fields.add-bandsaw') . ' ' . ''])
            );
        }

        return redirect(route('bandsaw-add', ['id' => $request->get('tally_id')]));
    }
}
