<?php

namespace App\Http\Controllers\Produksi\Mutation_Wip;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Produksi\Tally;
use App\Model\Produksi\DtProsesStockMs;
use App\Model\Produksi\HdLpb;
use App\Model\Produksi\LPBDetail;
use App\Service\Master\WoodService;
use DB;

class ProsesMsController extends Controller
{
    const URL       = 'produksi/mutation_wip/proses-ms';
    const RESOURCE  = 'proses-ms';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }


        $query = DB::table('dt_proses_stockms')
            ->select('dt_proses_stockms.proses_stockms_id', 'dt_proses_stockms.proses_date', 'mst_jenis_kayu.jenis_kayu', 'dt_proses_stockms.tinggi', 'dt_proses_stockms.lebar', 'dt_proses_stockms.panjang', 'dt_proses_stockms.pcs', 'dt_proses_stockms.volume', 'dt_proses_stockms.keterangan')
            ->where('dt_proses_stockms.pcs', '!=', 0)
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_proses_stockms.jenis_kayu_id')
            ->orderBy('dt_proses_stockms.proses_date', 'desc');

        return view('produksi/mutation_wip/proses-ms.index', [
            "models"        => $query->paginate(10),

            // "filters"       => $filters,
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }
}
