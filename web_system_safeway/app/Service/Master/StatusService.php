<?php

namespace App\Service\Master;

class StatusService
{
    public static function getActiveStatus()
    {
        return \DB::table('mst_user_status')
            ->where('status', '=', true)
            ->orderBy('nama_status')
            ->get();
    }
}
