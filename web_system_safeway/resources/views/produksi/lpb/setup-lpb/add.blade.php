@extends('layouts.master')

@section('title', trans('menu.setup-lpb'))

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">{{ trans('menu.setup-lpb') }}</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="{{ route('dashboard') }}">{{ trans('menu.dashboard') }}</a>
                            </li>
                            <li>
                                <a>{{ trans('menu.produksi') }}</a>
                            </li>
                            <li class="active">
                                {{ trans('menu.setup-lpb') }}
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-border panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ trans('fields.add-lpb') }}</h3>
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal" role="form" method="post" action="{{ route('setup-lpb-save') }}">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="p-20">
                                            <input type="hidden" name="id" class="form-control" value="{{ count($errors) > 0 ? old('hd_lpb_id') : $model->hd_lpb_id }}">
                                             <div class="form-group {{ $errors->has('nomor_lpb') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.nomor_lpb') }}</label>
                                                <div class="col-md-5">
                                                    <input type="text" name="nomor_lpb" class="form-control" value="{{ count($errors) > 0 ? old('nomor_lpb') : $model->nomor_lpb }}">
                                                    @if($errors->has('nomor_lpb'))
                                                        <span class="help-block">{{ $errors->first('nomor_lpb') }}</span>
                                                    @endif
                                                </div>
                                            </div>

                                             <div class="form-group {{ $errors->has('tanggal_kedatangan') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.arrival_date') }}</label>
                                                <div class="col-md-5">
                                                    <input type="date" name="tanggal_kedatangan" class="form-control" value="{{ count($errors) > 0 ? old('tanggal_kedatangan') : $model->tanggal_kedatangan }}">
                                                    @if($errors->has('tanggal_kedatangan'))
                                                        <span class="help-block">{{ $errors->first('tanggal_kedatangan') }}</span>
                                                    @endif
                                                </div>
                                            </div>

                                             <div class="form-group {{ $errors->has('lokasi_kedatangan') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.lokasi_kedatangan') }}</label>
                                                <div class="col-md-5">
                                                    <select class=" form-control" id="lokasi_kedatangan_id" name="lokasi_kedatangan_id">
                                                        <?php $lokasiId = count($errors) > 0 ? old('lokasi_kedatangan') : $model->lokasi_kedatangan_id; ?>

                                                        <option value="">- {{ trans('fields.change') }} {{ trans('fields.lokasi_kedatangan') }} -</option>
                                                        @foreach($arrivallocationOptions as $arr)
                                                        <option value="{{ $arr->lokasi_kedatangan_id }}" = {{ $arr->lokasi_kedatangan_id == $lokasiId ? 'selected' : ''}} >{{ $arr->lokasi_kedatangan }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            

                                            <div class="form-group {{ $errors->has('supplier_name') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.supplier_name') }}</label>
                                                <div class="col-md-5">
                                                    <select class=" form-control" id="supplier_id" name="supplier_id">
                                                        <?php $supplierId = count($errors) > 0 ? old('supplier_name') : $model->supplier_id; ?>
                                                        <option value="">- {{ trans('fields.change') }} {{ trans('fields.supplier_name') }} -</option>
                                                        @foreach($supplierOptions as $supp)
                                                        <option value="{{ $supp->supplier_id }}" = {{ $supp->supplier_id == $supplierId ? 'selected' : ''}} >{{ $supp->supplier_name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                             <div class="form-group {{ $errors->has('nomor_po') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.nomor_po') }}</label>
                                                <div class="col-md-5">
                                                    <input type="text" name="nomor_po" class="form-control" value="{{ count($errors) > 0 ? old('nomor_po') : $model->nomor_po }}">
                                                    @if($errors->has('nomor_po'))
                                                        <span class="help-block">{{ $errors->first('nomor_po') }}</span>
                                                    @endif
                                                </div>
                                            </div>

                                             <div class="form-group {{ $errors->has('nomor_sj') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.nomor_afkir') }}</label>
                                                <div class="col-md-5">
                                                    <input type="text" name="nomor_sj" class="form-control" value="{{ count($errors) > 0 ? old('nomor_sj') : $model->nomor_sj }}">
                                                    @if($errors->has('nomor_sj'))
                                                        <span class="help-block">{{ $errors->first('nomor_sj') }}</span>
                                                    @endif
                                                </div>
                                            </div>

                                             <div class="form-group {{ $errors->has('plat_mobil') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.plat_no') }}</label>
                                                <div class="col-md-5">
                                                    <input type="text" name="plat_mobil" class="form-control" value="{{ count($errors) > 0 ? old('plat_mobil') : $model->plat_mobil }}">
                                                    @if($errors->has('plat_mobil'))
                                                        <span class="help-block">{{ $errors->first('plat_mobil') }}</span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group {{ $errors->has('tanggal_afkir') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.afkir_date') }}</label>
                                                <div class="col-md-5">
                                                    <input type="date" name="tanggal_afkir" class="form-control" value="{{ count($errors) > 0 ? old('tanggal_afkir') : $model->tanggal_afkir }}">
                                                    @if($errors->has('tanggal_afkir'))
                                                        <span class="help-block">{{ $errors->first('tanggal_afkir') }}</span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group {{ $errors->has('plat_afkir') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.plat_no_afkir') }}</label>
                                                <div class="col-md-5">
                                                    <input type="text" name="plat_afkir" class="form-control" value="{{ count($errors) > 0 ? old('plat_afkir') : $model->plat_afkir }}">
                                                    @if($errors->has('plat_afkir'))
                                                        <span class="help-block">{{ $errors->first('plat_afkir') }}</span>
                                                    @endif
                                                </div>
                                            </div>


                                            <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <div class="checkbox checkbox-primary">
                                                        <?php $status = count($errors) > 0 ? old('status') : $model->status; ?>
                                                        <input id="status" name="status" value="true" type="checkbox" {{ $status == true ? 'checked' : '' }}>
                                                        <label for="status">
                                                            {{ trans('fields.is-active') }}
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="col-sm-12 data-table-toolbar text-right" style="padding-top: 10px;">
                                        <div class="form-group">
                                            <a href="{{ url('produksi/setup-lpb') }}" class="btn btn-sm btn-warning"><i class="fa fa-reply"></i> {{ trans('fields.cancel') }}</a>
                                            <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i> {{ trans('fields.save') }}</button>
                                        </div>
                                    </div> 
                                </div>
                            </form>
                        </div>
                    </div>
                        <!-- end row -->
                </div> <!-- end card-box -->
            </div><!-- end col -->
        </div>
    </div>
</div>
@endsection