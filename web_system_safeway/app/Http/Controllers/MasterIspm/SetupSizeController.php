<?php

namespace App\Http\Controllers\MasterIspm;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\MasterIspm\Size;
use DB;

class SetupSizeController extends Controller
{
    const URL       = 'master-ispm/setup-size';
    const RESOURCE  = 'setup-size';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        if ($request->isMethod('post')) {
            $request->session()->put('filters', $request->all());
            return redirect(self::URL . '?page=1');
        } elseif (empty($request->get('page'))) {
            $request->session()->forget('filters');
        }
        $filters = $request->session()->get('filters');
        $query = \DB::table('mst_sizeispm')
            ->orderBy('mst_sizeispm.size');

        if (!empty($filters['status']) || !$request->session()->has('filters')) {
            $query->where('mst_sizeispm.status', '=', true);
        } else {
            $query->where('mst_sizeispm.status', '=', false);
        }

        if (!empty($filters['size'])) {
            $query->where('mst_sizeispm.size', 'like', '%' . $filters['size'] . '%');
        }

        return view('master-ispm/setup-size.index', [
            "models"        => $query->paginate(10),
            "filters"       => $filters,
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function add(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        $model           = new Size();
        $model->status   = true;

        return view('master-ispm/setup-size.add', [
            "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }

    public function edit(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $model = Size::find($id);

        if ($model === null) {
            abort('404');
        }

        return view('master-ispm/setup-size.add', [
            "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }

    public function delete(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $model =  Size::find($id);

        if ($model === null) {
            abort('404');
        }

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            $model->modified_by   = \Auth::user()->id;
            $model->modified_date = $now;
            $model->status   = false;

            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.delete-message', ['variable' => trans('fields.name_size') . ' ' . $id])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.faildel-message', ['variable' => trans('fields.name_size') . ' ' . $id])
            );
        }

        return redirect(route('setup-size-index'));
    }

    public function save(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add']) && !\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $id     = intval($request->get('id', 0));

        $this->validate($request, [
            'size' => 'required|max:255|unique:mst_size,size,' . $id . ',size_id',
        ]);

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            if (empty($id)) {
                $model  = new Size();
                $model->created_by      = \Auth::user()->id;
                $model->created_date    = $now;
            } else {
                $model  = Size::find($id);
                $model->modified_by   = \Auth::user()->id;
                $model->modified_date = $now;
            }

            $model->size   = $request->get('size');
            $model->status   = !empty($request->get('status')) ? true : false;

            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('menu.size') . ' ' . $request->get('size')])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('menu.size') . ' ' . $request->get('size')])
            );
        }

        return redirect(route('setup-sizeispm-index'));
    }
}
