<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHdSpkBandsawTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hd_spk_bandsaw', function (Blueprint $table) {
            $table->increments('hd_spk_bandsaw_id');
            $table->integer('spk_bandsaw_no')->nullable();
            $table->date('bandsaw_date');
            $table->string('keterangan');
            $table->boolean('catatan');
            $table->boolean('status')->default(true);
            $table->integer('created_by');
            $table->dateTime('created_date');
            $table->integer('modified_by')->nullable();
            $table->dateTime('modified_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hd_spk_bandsaw');
    }
}
