<?php

namespace App\Http\Controllers\MasterProduksi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\MasterProduksi\PalletName;
use DB;

class SetupPalletNameController extends Controller
{
    const URL       = 'master-produksi/pallet-name';
    const RESOURCE  = 'pallet-name';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        if ($request->isMethod('post')) {
            $request->session()->put('filters', $request->all());
            return redirect(self::URL . '?page=1');
        } elseif (empty($request->get('page'))) {
            $request->session()->forget('filters');
        }
        $filters = $request->session()->get('filters');
        $query = \DB::table('mst_pallet_name')
            ->orderBy('mst_pallet_name.pallet_name');

        if (!empty($filters['status']) || !$request->session()->has('filters')) {
            $query->where('mst_pallet_name.status', '=', true);
        } else {
            $query->where('mst_pallet_name.status', '=', false);
        }

        if (!empty($filters['pallet_name'])) {
            $query->where('mst_pallet_name.pallet_name', 'like', '%' . $filters['pallet_name'] . '%');
        }

        return view('master-produksi/pallet-name.index', [
            "models"        => $query->paginate(10),
            "filters"       => $filters,
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function add(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        $model           = new PalletName();
        $model->status   = true;

        return view('master-produksi/pallet-name.add', [
            "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }

    public function edit(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $model = PalletName::find($id);

        if ($model === null) {
            abort('404');
        }

        return view('master-produksi/pallet-name.add', [
            "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }



    public function save(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add']) && !\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $id     = intval($request->get('id', 0));

        $this->validate($request, [
            'pallet_name' => 'required|max:255|',
            'kode_pallet' => 'required|max:255|',
            'tinggi'              => 'required|integer',
            'lebar'               => 'required|integer',
            'panjang'             => 'required|integer',
        ]);

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            if (empty($id)) {
                $model  = new PalletName();
                $model->created_by      = \Auth::user()->id;
                $model->created_date    = $now;
            } else {
                $model  = PalletName::find($id);
                $model->modified_by   = \Auth::user()->id;
                $model->modified_date = $now;
            }

            $model->kode_pallet   = $request->get('kode_pallet');
            $model->pallet_name   = $request->get('pallet_name');
            $model->panjang   = $request->get('panjang');
            $model->lebar   = $request->get('lebar');
            $model->tinggi   = $request->get('tinggi');


            $model->status   = !empty($request->get('status')) ? true : false;

            $model->save();
            DB::commit();



            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('menu.setup-pallet-name') . ' ' . $request->get('pallet_name')])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('menu.setup-pallet-name') . ' ' . $request->get('pallet_name')])
            );
        }

        return redirect(route('setup-pallet-name-index'));
    }
}
