<?php

namespace App\Service\DivisionContainer;

class DepoLocationService
{
    public static function getActiveDepoLocation()
    {
        return \DB::table('mst_depolocation')
            ->where('status', '=', true)
            ->orderBy('nama_depolocation')
            ->get();
    }
}
