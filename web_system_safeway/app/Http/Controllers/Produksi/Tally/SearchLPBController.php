<?php

namespace App\Http\Controllers\Produksi\Tally;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Produksi\Tally;
use App\Model\Produksi\IsiTally;
use Elibyy\TCPDF\Facades\TCPDF;
use Maatwebsite\Excel\Facades\Excel;
use \Milon\Barcode\DNS1D;
use DB;

class SearchLPBController extends Controller
{
    const URL       = 'produksi/search-lpb';
    const RESOURCE  = 'search-lpb';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        if ($request->isMethod('post')) {
            $request->session()->put('filters', $request->all());
            return redirect(self::URL . '?page=1');
        } elseif (empty($request->get('page'))) {
            $request->session()->forget('filters');
        }


        $filters = $request->session()->get('filters');


        $query = DB::table('tally_table')
            ->select('hd_lpb.nomor_lpb', 'hd_lpb.tanggal_kedatangan', 'tally_table.tally_no', 'tally_table.date_in_kd', 'tally_table.date_out_kd', 'tally_table.date_mutation_wip', 'tally_table.created_date', 'tally_table.status', 'tally_table.tally_id', 'tally_table.status_mutation_wip')
            ->where('tally_table.status', '=', true)

            ->leftJoin('dt_lpb', 'dt_lpb.tally_id', '=', 'tally_table.tally_id')
            ->leftJoin('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
            ->groupBy('hd_lpb.nomor_lpb','hd_lpb.tanggal_kedatangan', 'tally_table.tally_no', 'tally_table.date_in_kd', 'tally_table.date_out_kd', 'tally_table.date_mutation_wip', 'tally_table.created_date', 'tally_table.status', 'tally_table.tally_id', 'tally_table.status_mutation_wip')
            ->orderBy('tally_table.tally_no');

        // dd($query);
        if (!empty($filters['tally_no'])) {
            $query->where('tally_table.tally_no', 'like', '%' . $filters['tally_no'] . '%');
        }
        return view('produksi/tally/search-lpb.index', [
            "models"        => $query->paginate(10),
            "filters"       => $filters,
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }
}
