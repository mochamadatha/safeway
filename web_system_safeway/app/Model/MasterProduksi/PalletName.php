<?php

namespace App\Model\MasterProduksi;

use Illuminate\Database\Eloquent\Model;

class PalletName extends Model
{
    protected $table        = 'mst_pallet_name';
    protected $primaryKey   = 'pallet_name_id';
    public $timestamps      = false;
}
