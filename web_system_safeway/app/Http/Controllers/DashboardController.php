<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
use DB;

class DashboardController extends Controller
{
    const URL       = 'dashboard';
    const RESOURCE  = 'dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        if ($request->isMethod('post')) {
            $request->session()->put('filters', $request->all());
            return redirect(self::URL . '?page=1');
        } elseif (empty($request->get('page'))) {
            $request->session()->forget('filters');
        }

        $page = Input::get('page', 1);
        $paginate = 10;

        $query = DB::table('dt_lpb')
            ->select('tally_table.tally_no', 'mst_jenis_kayu.jenis_kayu', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 'dt_isi_tally.panjang', 'dt_isi_tally.pcs', 'dt_isi_tally.volume', 'mst_lokasi_kedatangan.lokasi_kedatangan', 'dt_lpb.created_date')
            ->where('dt_lpb.status', '=', true)
            ->where('tally_table.date_in_kd', '=', null)
            ->whereNotIn('dt_lpb.tally_id',  DB::table('dt_isi_bandsaw')
                ->select('tally_id')
                ->groupBy('tally_id'))
            ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
            ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=', 'hd_lpb.lokasi_kedatangan_id')
            ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
            // ->orderBy('hd_lpb.created_by');
            ->orderBy('dt_lpb.created_date', 'desc');

        $query2 = DB::table('dt_isi_bandsaw')
            ->select('tally_table.tally_no', 'mst_jenis_kayu.jenis_kayu', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'dt_isi_bandsaw.pcs', 'dt_isi_bandsaw.volume', 'mst_lokasi_kedatangan.lokasi_kedatangan', 'dt_isi_bandsaw.created_date')
            ->where('dt_isi_bandsaw.status', '=', true)
            ->where('tally_table.date_in_kd', '=', null)
            // ->where('dt_isi_bandsaw.tally_id', '=', $id)
            ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'dt_isi_bandsaw.tally_id')
            ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
            ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=', 'hd_lpb.lokasi_kedatangan_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')
            ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')
            ->groupBy('tally_table.tally_no', 'mst_jenis_kayu.jenis_kayu', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'dt_isi_bandsaw.pcs', 'dt_isi_bandsaw.volume', 'mst_lokasi_kedatangan.lokasi_kedatangan', 'dt_isi_bandsaw.created_date')
            ->union($query)
            ->orderBy('created_date')->get();


        $info = DB::table('dt_lpb')
            ->select(
                'mst_jenis_kayu.jenis_kayu',
                'dt_isi_tally.tinggi',
                'dt_isi_tally.lebar',
                'dt_isi_tally.panjang',
                DB::raw('SUM(dt_isi_tally.pcs) as pcs'),
                DB::raw('SUM(dt_isi_tally.volume) as volume')
            )
            ->where('dt_lpb.status', '=', true)
            ->where('tally_table.date_in_kd', '=', null)
            ->whereNotIn('dt_lpb.tally_id',  DB::table('dt_isi_bandsaw')
                ->select('tally_id')
                ->groupBy('tally_id'))
            ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
            ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=', 'hd_lpb.lokasi_kedatangan_id')
            ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
            // ->orderBy('hd_lpb.created_by');
            ->groupBy('mst_jenis_kayu.jenis_kayu', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 'dt_isi_tally.panjang');

        $info2 = DB::table('dt_isi_bandsaw')
            ->select('mst_jenis_kayu.jenis_kayu', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', DB::raw('SUM(dt_isi_bandsaw.pcs) as pcs'), DB::raw('SUM(dt_isi_bandsaw.volume) as volume'))
            ->where('dt_isi_bandsaw.status', '=', true)
            ->where('tally_table.date_in_kd', '=', null)
            // ->where('dt_isi_bandsaw.tally_id', '=', $id)
            ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'dt_isi_bandsaw.tally_id')
            ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
            ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=', 'hd_lpb.lokasi_kedatangan_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')
            ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')
            ->groupBy('tally_table.tally_no', 'mst_jenis_kayu.jenis_kayu', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'dt_isi_bandsaw.pcs', 'dt_isi_bandsaw.volume', 'mst_lokasi_kedatangan.lokasi_kedatangan', 'dt_isi_bandsaw.created_date')
            ->union($info)
            ->orderBy('jenis_kayu')->get();


        $queryms = DB::table('jumlah_ms')
            ->select('jumlah_ms.jumlah_ms_id', 'mst_jenis_kayu.jenis_kayu', 'jumlah_ms.tinggi', 'jumlah_ms.lebar', 'jumlah_ms.panjang', 'jumlah_ms.pcs', 'jumlah_ms.volume')
            ->where('jumlah_ms.pcs', '!=', 0)
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'jumlah_ms.jenis_kayu_id')
            ->orderBy('jumlah_ms.pcs', 'desc');


        $infoms = DB::table('jumlah_ms')
            ->select(
                'mst_jenis_kayu.jenis_kayu',
                'jumlah_ms.tinggi',
                'jumlah_ms.lebar',
                'jumlah_ms.panjang',
                DB::raw('SUM(jumlah_ms.pcs) as pcs'),
                DB::raw('SUM(jumlah_ms.volume) as volume')
            )
            ->where('jumlah_ms.pcs', '!=', 0)
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'jumlah_ms.jenis_kayu_id')
            ->groupBy('mst_jenis_kayu.jenis_kayu', 'jumlah_ms.tinggi', 'jumlah_ms.lebar', 'jumlah_ms.panjang');

        $infoinkd = DB::table('hd_lpb')
            ->select('mst_jenis_kayu.jenis_kayu', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 'dt_isi_tally.panjang', 'mst_chamber.chamber_name', DB::raw('SUM(dt_isi_tally.pcs) as pcs'), DB::raw('SUM(dt_isi_tally.volume) as volume'))
            ->where('hd_lpb.status', '=', true)
            ->where('tally_table.date_in_kd', '!=', NULL)
            ->where('tally_table.date_out_kd', '=', NULL)
            ->whereNotIn('dt_lpb.tally_id',  DB::table('dt_isi_bandsaw')
                ->select('tally_id')
                ->groupBy('tally_id'))
            // ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', 'hd_lpb.lokasi_kedatangan_id')
            ->join('dt_lpb', 'dt_lpb.hd_lpb_id', '=', 'hd_lpb.hd_lpb_id')
            ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
            ->join('mst_chamber', 'mst_chamber.chamber_id', 'tally_table.chamber_id')
            ->groupBy('mst_jenis_kayu.jenis_kayu', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 'dt_isi_tally.panjang', 'mst_chamber.chamber_name');

        // ->orderBy('hd_lpb.created_by');

        // dd($info);

        $infoinkd2 = DB::table('dt_isi_bandsaw')
            ->select('mst_jenis_kayu.jenis_kayu', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'mst_chamber.chamber_name', DB::raw('SUM(dt_isi_bandsaw.pcs) as pcs'), DB::raw('SUM(dt_isi_bandsaw.volume) as volume'))
            ->where('dt_isi_bandsaw.status', '=', true)
            // ->where('tally_table.date_in_kd', '=', null)
            // ->where('dt_isi_bandsaw.tally_id', '=', $id)
            ->whereIn('dt_isi_bandsaw.tally_id', DB::table('tally_table')
                ->select('tally_table.tally_id')
                ->where('tally_table.date_in_kd', '!=', NULL)
                ->where('tally_table.date_out_kd', '=', NULL)
                ->join('dt_isi_bandsaw', 'dt_isi_bandsaw.tally_id', '=', 'tally_table.tally_id')
                ->groupBy('tally_id'))
            ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'dt_isi_bandsaw.tally_id')
            ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
            // ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=' ,'hd_lpb.lokasi_kedatangan_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')
            // ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')
            ->join('mst_chamber', 'mst_chamber.chamber_id', 'tally_table.chamber_id')
            ->groupBy('mst_jenis_kayu.jenis_kayu', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'mst_chamber.chamber_name')
            ->union($infoinkd)
            ->orderBy('jenis_kayu')->get();




        $infoinkddate = DB::table('hd_lpb')
            ->select(
                'tally_table.date_in_kd',
                'mst_chamber.chamber_name',
                DB::raw('SUM(dt_isi_tally.pcs) as pcs'),
                DB::raw('COUNT(DISTINCT tally_table.tally_no) as tally_no'),
                DB::raw('SUM(dt_isi_tally.volume) as volume'),
                DB::raw('"Non Bandsaw" status_data')
            )
            ->where('hd_lpb.status', '=', true)
            ->where('tally_table.date_in_kd', '!=', NULL)
            ->where('tally_table.date_out_kd', '=', NULL)
            ->whereNotIn('dt_lpb.tally_id',  DB::table('dt_isi_bandsaw')
                ->select('tally_id')
                ->groupBy('tally_id'))
            // ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', 'hd_lpb.lokasi_kedatangan_id')
            ->join('dt_lpb', 'dt_lpb.hd_lpb_id', '=', 'hd_lpb.hd_lpb_id')
            ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
            ->join('mst_chamber', 'mst_chamber.chamber_id', 'tally_table.chamber_id')
            ->groupBy('tally_table.date_in_kd', 'mst_chamber.chamber_name', 'status_data');



        $infoinkddate2 = DB::table('dt_isi_bandsaw')
            ->select('tally_table.date_in_kd', 'mst_chamber.chamber_name', DB::raw('SUM(dt_isi_bandsaw.pcs) as pcs'), DB::raw('COUNT(DISTINCT tally_table.tally_no) as tally_no'), DB::raw('SUM(dt_isi_bandsaw.volume) as volume'), DB::raw('"Bandsaw" status_data'))
            ->where('dt_isi_bandsaw.status', '=', true)
            // ->where('tally_table.date_in_kd', '=', null)
            // ->where('dt_isi_bandsaw.tally_id', '=', $id)
            ->whereIn('dt_isi_bandsaw.tally_id', DB::table('tally_table')
                ->select('tally_table.tally_id')
                ->where('tally_table.date_in_kd', '!=', NULL)
                ->where('tally_table.date_out_kd', '=', NULL)
                ->join('dt_isi_bandsaw', 'dt_isi_bandsaw.tally_id', '=', 'tally_table.tally_id')
                ->groupBy('tally_id'))
            // ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'dt_isi_bandsaw.tally_id')
            // ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
            // ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=' ,'hd_lpb.lokasi_kedatangan_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')
            // ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')
            ->join('mst_chamber', 'mst_chamber.chamber_id', 'tally_table.chamber_id')
            ->groupBy('tally_table.date_in_kd', 'mst_chamber.chamber_name', 'status_data')
            ->union($infoinkddate)
            ->orderBy('date_in_kd')->get();

        //dd($infoinkddate2);

        $infooutkd = DB::table('hd_lpb')
            ->select(
                'mst_jenis_kayu.jenis_kayu',
                'dt_isi_tally.tinggi',
                'dt_isi_tally.lebar',
                'dt_isi_tally.panjang',
                DB::raw('SUM(dt_isi_tally.pcs) as pcs'),
                DB::raw('SUM(dt_isi_tally.volume) as volume')
            )
            ->where('hd_lpb.status', '=', true)
            ->where('tally_table.date_in_kd', '!=', NULL)
            ->where('tally_table.date_out_kd', '!=', NULL)
            ->where('tally_table.date_mutation_wip', '=', NULL)
            ->whereNotIn('dt_lpb.tally_id',  DB::table('dt_isi_bandsaw')
                ->select('tally_id')
                ->groupBy('tally_id'))
            // ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', 'hd_lpb.lokasi_kedatangan_id')
            ->join('dt_lpb', 'dt_lpb.hd_lpb_id', '=', 'hd_lpb.hd_lpb_id')
            ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
            ->groupBy('mst_jenis_kayu.jenis_kayu', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 'dt_isi_tally.panjang');
        // ->join('mst_chamber', 'mst_chamber.chamber_id', 'tally_table.chamber_id')
        // ->orderBy('hd_lpb.created_by');

        // dd($info);

        $infooutkd2 = DB::table('dt_isi_bandsaw')
            ->select('mst_jenis_kayu.jenis_kayu', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', DB::raw('SUM(dt_isi_bandsaw.pcs) as pcs'), DB::raw('SUM(dt_isi_bandsaw.volume) as volume'))
            ->where('dt_isi_bandsaw.status', '=', true)
            // ->where('tally_table.date_in_kd', '=', null)
            // ->where('dt_isi_bandsaw.tally_id', '=', $id)
            ->whereIn('dt_isi_bandsaw.tally_id', DB::table('tally_table')
                ->select('tally_table.tally_id')
                ->where('tally_table.date_in_kd', '!=', NULL)
                ->where('tally_table.date_out_kd', '!=', NULL)
                ->where('tally_table.date_mutation_wip', '=', NULL)
                ->join('dt_isi_bandsaw', 'dt_isi_bandsaw.tally_id', '=', 'tally_table.tally_id')
                ->groupBy('tally_id'))
            ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'dt_isi_bandsaw.tally_id')
            ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
            // ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=' ,'hd_lpb.lokasi_kedatangan_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')
            // ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')
            // ->join('mst_chamber', 'mst_chamber.chamber_id', 'tally_table.chamber_id')
            ->groupBy('mst_jenis_kayu.jenis_kayu', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang')
            ->union($infooutkd)
            ->orderBy('jenis_kayu')->get();











        //  $jumlah = DB::table('dt_lpb')
        // ->select( 'mst_jenis_kayu.jenis_kayu', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 'dt_isi_tally.panjang',  DB::raw('sum(dt_isi_tally.pcs) as pcs, sum(dt_isi_tally.volume) as volume'))
        //     ->where('dt_lpb.status', '=', true)
        //     ->where('tally_table.date_in_kd', '=', null)
        //     ->whereNotIn('dt_lpb.tally_id',  DB::table('dt_isi_bandsaw')
        //             ->select('tally_id')
        //             ->groupBy('tally_id'))
        //     ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
        //     ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=' ,'hd_lpb.lokasi_kedatangan_id')
        //     ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
        //     ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
        //     ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
        //     // ->orderBy('hd_lpb.created_by');
        //     ->orderBy('dt_lpb.created_date', 'desc');

        // $jumlah2 = DB::table('dt_isi_bandsaw')
        // ->select('tally_table.tally_no', 'mst_jenis_kayu.jenis_kayu', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang',  DB::raw('sum(dt_isi_bandsaw.pcs) as pcs, sum(dt_isi_bandsaw.volume) as volume'))
        //     ->where('dt_isi_bandsaw.status', '=', true)
        //     ->where('tally_table.date_in_kd', '=', null)
        //     // ->where('dt_isi_bandsaw.tally_id', '=', $id)
        //     ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'dt_isi_bandsaw.tally_id')
        //     ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
        //     ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
        //     ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=' ,'hd_lpb.lokasi_kedatangan_id')
        //     ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')
        //     ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')
        //     ->groupBy( 'mst_jenis_kayu.jenis_kayu', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'dt_isi_bandsaw.pcs', 'dt_isi_bandsaw.volume')
        //     ->union($jumlah)
        //     ->orderBy('created_date')->get(); 
        // ->orderBy('created_date')->get(); 

        // $a = DB::table('dt_isi_bandsaw')
        //         ->select('tally_id')
        //         ->groupBy('tally_id')->get();
        //dd($jumlah2);  



        if (!empty($filters['status']) || !$request->session()->has('filters')) {
            $query->where('hd_lpb.status', '=', true);
        } else {
            $query->where('hd_lpb.status', '=', false);
        }

        $slice = array_slice($query2->toArray(), $paginate * ($page - 1), $paginate);
        $result = new LengthAwarePaginator($slice, count($query2), $paginate);
        // return View::make('produksi/mutation_wip/mutation.addtally');
        // dd(url()->current());
        $result = $result->setPath(url()->current());



        return view('dashboard', [

            "models"         => $result,
            "model"         => $query2,
            "modell"         => $queryms,
            "info"         => $info2,
            "infoinkd"         => $infoinkd2,
            "infooutkd"       =>  $infooutkd2,
            "infoinkddate"         => $infoinkddate2,
            "infoms"          => $infoms->paginate(10),
            // "modelk"         => $jumlah2,     

            // "models"         => $query2->paginate(10),
            // "tallyLPBOptions"   => TallyService::getTallyLPB(),
            // "desOptions"   => DesService::getActiveDes(),
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }
}
