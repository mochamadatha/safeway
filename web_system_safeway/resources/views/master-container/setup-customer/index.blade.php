@extends('layouts.master')

@section('title', trans('menu.setup-customer'))

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
        	<div class="row">
				<div class="col-xs-12">
					<div class="page-title-box">
                        <h4 class="page-title">{{ trans('menu.setup-customer') }}</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="{{ route('dashboard') }}">{{ trans('menu.dashboard') }}</a>
                            </li>
                            <li>
                                <a>{{ trans('menu.master-container') }}</a>
                            </li>
                            <li class="active">
                                {{ trans('menu.setup-customer') }}
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
				</div>
			</div>
            <!-- end row -->
            @if(Session::has('successMessage'))
            <div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-check-all"></i>
                <strong>{{ Session::get('successMessage') }}.</strong>
            </div>
            @endif

            @if(Session::has('errorMessage'))
            <div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-block-helper"></i>
                <strong>{{ Session::get('errorMessage') }}.</strong>
            </div>
            @endif

			<div class="row">
			    <div class="col-md-12">
			        <div class="panel panel-border panel-success">
			        	<div class="panel-heading">
                        </div>
                        <div class="panel-body">
				            <div class="row">
				                <form class="form-horizontal" role="form" method="post" action="">
				                    {{ csrf_field() }}
				                 
				                    <div class="col-md-12">
				                        <p class="text-muted font-14 m-b-20 pull-right">
				                            @can('access', [$resource, 'add'])
				                            <a href="{{ route('setup-customer-add') }}" class="btn btn-primary waves-effect w-md waves-light"><i class="fa fa-plus"></i> {{ trans('fields.add-new') }}</a>
				                            @endcan

				                        </p>
				                    </div>
				                </form>
				            </div>
				            <div class="row">
				                <table class="table table-bordered table-hover table-striped m-0">
				                    <thead>
				                        <tr>
				                            <th width="10%" class="text-center">{{ trans('fields.num') }}</th>
				                            <th width="60%" class="text-center">{{ trans('fields.customer_name') }}</th>
				                            <th width="30%" class="text-center">{{ trans('fields.action') }}</th>
				                        </tr>
				                    </thead>
				                    <tbody>
				                        <?php $no = ($models->currentPage() - 1) * $models->perPage() + 1; ?>
				                        @foreach($models as $model)
				                        <tr>
				                            <th scope="row" style="text-align: center;">{{ $no++ }}</th>
				                            <td>{{ $model->nama_customer }}</td>
				                            <td class="text-center">
				                            @can('access', [$resource, 'update'])
				                                <a href="{{ route('setup-customer-edit', ['id' => $model->customer_id]) }}" data-toggle="tooltip" class="btn btn-xs btn-warning" data-original-title="Edit">
				                                    <i class="fa fa-pencil"></i>
				                                </a>
				                            @endcan
				                            

				                        </tr>
				                        @endforeach
				                    </tbody>
				                </table>
				                <div class="data-table-toolbar">
				                    {!! $models->render() !!}
				                </div>
				            </div>
			            </div>
			        </div>
			    </div>
			</div>
		</div>
	</div>
</div>
@endsection