<?php

namespace App\Model\MasterHRD;

use Illuminate\Database\Eloquent\Model;

class Divisi extends Model
{
    protected $table        = 'mst_divisi';
    protected $primaryKey   = 'divisi_id';
    public $timestamps      = false;
}
