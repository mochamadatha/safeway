<?php

namespace App\Http\Controllers\MasterContainer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\MasterContainer\Agent;
use DB;

class SetupAgentController extends Controller
{
    const URL       = 'master-container/setup-agent';
    const RESOURCE  = 'setup-agent';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        if ($request->isMethod('post')) {
            $request->session()->put('filters', $request->all());
            return redirect(self::URL . '?page=1');
        } elseif (empty($request->get('page'))) {
            $request->session()->forget('filters');
        }
        $filters = $request->session()->get('filters');
        $query = \DB::table('mst_agent')
            ->orderBy('mst_agent.nama_agent');

        if (!empty($filters['status']) || !$request->session()->has('filters')) {
            $query->where('mst_agent.status', '=', true);
        } else {
            $query->where('mst_agent.status', '=', false);
        }

        if (!empty($filters['nama_agent'])) {
            $query->where('mst_agent.nama_agent', 'like', '%' . $filters['nama_agent'] . '%');
        }

        return view('master-container/setup-agent.index', [
            "models"        => $query->paginate(10),
            "filters"       => $filters,
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function add(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        $model           = new Agent();
        $model->status   = true;

        return view('master-container/setup-agent.add', [
            "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }

    public function edit(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $model = Agent::find($id);

        if ($model === null) {
            abort('404');
        }

        return view('master-container/setup-agent.add', [
            "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }

    public function delete(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $model =  Agent::find($id);

        if ($model === null) {
            abort('404');
        }

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            $model->modified_by   = \Auth::user()->id;
            $model->modified_date = $now;
            $model->status   = false;

            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.delete-message', ['variable' => trans('fields.agent_name') . ' ' . $id])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.faildel-message', ['variable' => trans('fields.agent_name') . ' ' . $id])
            );
        }

        return redirect(route('setup-agent-index'));
    }

    public function save(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add']) && !\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $id     = intval($request->get('id', 0));

        $this->validate($request, [
            'nama_agent' => 'required|max:255|unique:mst_agent,nama_agent,' . $id . ',agent_id',
        ]);

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            if (empty($id)) {
                $model  = new Agent();
                $model->created_by      = \Auth::user()->id;
                $model->created_date    = $now;
            } else {
                $model  = Agent::find($id);
                $model->modified_by   = \Auth::user()->id;
                $model->modified_date = $now;
            }

            $model->nama_agent   = $request->get('nama_agent');
            $model->status   = !empty($request->get('status')) ? true : false;

            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('menu.agent_name') . ' ' . $request->get('nama_agent')])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('menu.agent_name') . ' ' . $request->get('nama_agent')])
            );
        }

        return redirect(route('setup-agent-index'));
    }
}
