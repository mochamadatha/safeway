<?php

namespace App\Http\Controllers\MasterHRD;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\MasterHRD\Divisi;
use App\Model\MasterHRD\Cabang;
use App\Model\MasterHRD\Pegawai;
use App\Service\Master\DivisionService;
use App\Service\Master\BranchService;
use DB;

class SetupEmployeeController extends Controller
{
    const URL       = 'master-produksi/setup-employee';
    const RESOURCE  = 'setup-employee';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        if ($request->isMethod('post')) {
            $request->session()->put('filters', $request->all());
            return redirect(self::URL . '?page=1');
        } elseif (empty($request->get('page'))) {
            $request->session()->forget('filters');
        }
        $filters = $request->session()->get('filters');
        $query = \DB::table('mst_pegawai')
            // ->orderBy('mst_harga.harga');
            ->select('mst_pegawai.*', 'mst_divisi.nama_divisi', 'mst_cabang.nama_cabang')
            ->join('mst_divisi', 'mst_divisi.divisi_id', '=', 'mst_pegawai.divisi_id')
            ->join('mst_cabang', 'mst_cabang.cabang_id', '=', 'mst_pegawai.cabang_id')
            ->orderBy('mst_pegawai.created_by');

        if (!empty($filters['status']) || !$request->session()->has('filters')) {
            $query->where('mst_pegawai.status', '=', true);
        } else {
            $query->where('mst_pegawai.status', '=', false);
        }

        if (!empty($filters['nama_pegawai'])) {
            $query->where('mst_pegawai.nama_pegawai', 'like', '%' . $filters['nama_pegawai'] . '%');
        }

        return view('master-hrd/setup-employee.index', [
            "models"        => $query->paginate(10),
            "filters"       => $filters,
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function add(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }


        $model           = new Pegawai();
        $model->status   = true;

        if ($model === null) {
            abort('404');
        }

        $query = DB::table('mst_pegawai')
            // ->orderBy('mst_harga.harga');
            ->select('mst_pegawai.*', 'mst_divisi.nama_divisi', 'mst_cabang.nama_cabang')
            ->join('mst_divisi', 'mst_divisi.divisi_id', '=', 'mst_pegawai.divisi_id')
            ->join('mst_cabang', 'mst_cabang.cabang_id', '=', 'mst_pegawai.cabang_id')
            ->orderBy('mst_pegawai.created_by');

        return view('master-hrd/setup-employee.add', [
            "model"         => $model,
            "models"         => $query->paginate(10),
            "divisionOptions"   => DivisionService::getActiveDivision(),
            "branchOptions"     => BranchService::getActiveBranch(),
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function edit(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $model = Price::find($id);

        if ($model === null) {
            abort('404');
        }


        return view('master-hrd/setup-employee.add', [
            "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }

    public function save(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        $id     = intval($request->get('id', 0));

        $this->validate($request, [
            'nama_pegawai'            => 'required|max:255',
            'divisi_id'            => 'required|integer',
            'cabang_id'            => 'required|integer',
            'alamat_pegawai'            => 'required|max:255',
            'email'            => 'required|max:255',
            'phone'            => 'required|max:255',


        ]);

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            if (empty($id)) {
                $model  = new Pegawai();
                $model->created_by      = \Auth::user()->id;
                $model->created_date    = $now;
            } else {
                $model  = Pegawai::find($id);
                $model->modified_by   = \Auth::user()->id;
                $model->modified_date = $now;
            }

            $model->nama_pegawai   = $request->get('nama_pegawai');
            $model->divisi_id   = $request->get('divisi_id');
            $model->cabang_id   = $request->get('cabang_id');
            $model->alamat_pegawai   = $request->get('alamat_pegawai');
            $model->email   = $request->get('email');
            $model->phone   = $request->get('phone');





            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('fields.add-price') . ' ' . ''])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('fields.add-price') . ' ' . ''])
            );
        }

        return redirect(route('setup-price-index', ['id' => $request->get('price_id')]));
    }
}
