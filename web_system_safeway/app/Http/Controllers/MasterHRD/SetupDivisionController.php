<?php

namespace App\Http\Controllers\MasterHRD;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\MasterHRD\Divisi;
use DB;

class SetupDivisionController extends Controller
{
    const URL       = 'master-hrd/setup-division';
    const RESOURCE  = 'setup-division';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        if ($request->isMethod('post')) {
            $request->session()->put('filters', $request->all());
            return redirect(self::URL . '?page=1');
        } elseif (empty($request->get('page'))) {
            $request->session()->forget('filters');
        }
        $filters = $request->session()->get('filters');
        $query = \DB::table('mst_divisi')
            ->orderBy('mst_divisi.nama_divisi');

        if (!empty($filters['status']) || !$request->session()->has('filters')) {
            $query->where('mst_divisi.status', '=', true);
        } else {
            $query->where('mst_divisi.status', '=', false);
        }

        if (!empty($filters['nama_divisi'])) {
            $query->where('mst_divisi.nama_divisi', 'like', '%' . $filters['nama_divisi'] . '%');
        }

        return view('master-hrd/setup-division.index', [
            "models"        => $query->paginate(10),
            "filters"       => $filters,
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function add(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        $model           = new Divisi();
        $model->status   = true;

        return view('master-hrd/setup-division.add', [
            "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }

    public function edit(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $model = Divisi::find($id);

        if ($model === null) {
            abort('404');
        }

        return view('master-hrd/setup-division.add', [
            "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }

    public function save(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add']) && !\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $id     = intval($request->get('id', 0));

        $this->validate($request, [
            'nama_divisi' => 'required|max:255|unique:mst_divisi,nama_divisi,' . $id . ',divisi_id',
        ]);

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            if (empty($id)) {
                $model  = new Divisi();
                $model->created_by      = \Auth::user()->id;
                $model->created_date    = $now;
            } else {
                $model  = Cabang::find($id);
                $model->modified_by   = \Auth::user()->id;
                $model->modified_date = $now;
            }

            $model->nama_divisi   = $request->get('nama_divisi');
            $model->status   = !empty($request->get('status')) ? true : false;

            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('menu.division_name') . ' ' . $request->get('nama_divisi')])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('menu.division_name') . ' ' . $request->get('nama_divisi')])
            );
        }

        return redirect(route('setup-division-index'));
    }
}
