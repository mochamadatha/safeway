@extends('layouts.master')

@section('title', trans('menu.raw-material'))

@section('header')
        <!-- <link href="{{ asset('plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/> -->
        <!-- <link href="{{ asset('plugins/datatables/buttons.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/> -->
        <!-- <link href="{{ asset('plugins/datatables/fixedHeader.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/> -->
        <!-- <link href="{{ asset('plugins/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/> -->
        <!-- <link href="{{ asset('plugins/datatables/scroller.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/> -->
        <!-- <link href="{{ asset('plugins/datatables/dataTables.colVis.css') }}" rel="stylesheet" type="text/css"/> -->
        <link href="{{ asset('plugins/datatables/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
        <!-- <link href="{{ asset('plugins/datatables/fixedColumns.dataTables.min.css') }}" rel="stylesheet" type="text/css"/> -->
@endsection


@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">{{ trans('menu.raw-material') }}</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="{{ route('dashboard') }}">{{ trans('menu.dashboard') }}</a>
                            </li>
                            <li>
                                <a>{{ trans('menu.produksi') }}</a>
                            </li>
                            <li class="active">
                                {{ trans('menu.raw-material') }}
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            @if(Session::has('successMessage'))
            <div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-check-all"></i>
                <strong>{{ Session::get('successMessage') }}.</strong>
            </div>
            @endif

            @if(Session::has('errorMessage'))
            <div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-block-helper"></i>
                <strong>{{ Session::get('errorMessage') }}.</strong>
            </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-border panel-success">
                        <div class="panel-heading">
                        </div>

                        <div class="panel-body">
                            <div class="row">
                                <form class="form-horizontal" role="form" method="post" action="">
                                    {{ csrf_field() }}
                                    <div class="col-md-12">
                                        <p class="text-muted font-14 m-b-20 pull-right">
                                         @can('access', [$resource, 'add'])
                                         <a href="{{ route('bandsaw-index') }}" class="btn btn-primary waves-effect w-md waves-light"><i class="fa fa-plus"></i> {{ trans('fields.bandsaw-and-afkir') }}</a>
                                         @endcan

                                     

                                        @can('access', [$resource, 'add'])
                                            <a data-target="#infoms" data-toggle="modal"   class="btn btn-primary waves-effect w-md  waves-light" data-original-title="fa-plus">
                                            <i class="fa fa-info"></i> {{ trans('fields.info') }}</a>
                                            </a>
                                        @endcan

                                            @can('access', [$resource, 'add'])
                                         <a href="{{ route('raw-material-export-excel') }}" class="btn btn-success waves-effect w-md waves-light"> {{ trans('fields.export-excel') }}</a>
                                         @endcan

                                       


                                     
                                            @can('access', [$resource, 'add'])
                                            <a type="button" class="btn btn-primary waves-effect w-md waves-light" data-toggle="modal" data-target="#dtexcel" data-original-title="Edit"  method="post" enctype="multipart/form-data"></i> {{ trans('fields.import-mutation') }}</a>
                                            @endcan

                                            @can('access', [$resource, 'add'])
                                            <a type="button" class="btn btn-primary waves-effect w-md waves-light" data-toggle="modal" data-target="#dtsaldo" data-original-title="Edit"  method="post" enctype="multipart/form-data"></i> {{ trans('fields.import-saldo') }}</a>
                                            @endcan

                                            @can('access', [$resource, 'add'])
                                            <a type="button" class="btn btn-primary waves-effect w-md waves-light" data-toggle="modal" data-target="#dtlpb" data-original-title="Edit"  method="post" enctype="multipart/form-data"></i> {{ trans('fields.import-lpb') }}</a>
                                            @endcan


   
                                 
                                              <div class="col-md-2">
                                                <input type="text" name="tally_no" class="form-control" value="{{ !empty($filters['tally_no']) ? $filters['tally_no'] : '' }}">
                                                @if($errors->has('tally_no'))
                                                <span class="help-block">{{ $errors->first('tally_no') }}</span>
                                                @endif
                                            </div>

                                            

                                            <div class="col-md-3">
                                                <button type="submit" class="btn btn-info waves-effect w-xs waves-light"><i class="fa fa-search"></i> </button>
                                            </div>
                                    </p>

                                    
                                     
                                </div>
                            </form>
                        </div>


                        <div class="row">
                            <table class="table table-bordered table-hover table-striped m-0">
                                <thead>
                                    <tr>
                                        <th width="5%" class="text-center">{{ trans('fields.num') }}</th>                                        
                                        <th width="7%" class="text-center">{{ trans('fields.supplier_name') }}</th> 
                                        <th width="7%" class="text-center">{{ trans('fields.arrival_date') }}</th> 
                                         <th width="7%" class="text-center">{{ trans('fields.notally') }}</th>
                                        <th width="10%" class="text-center">{{ trans('fields.wood-type') }}</th>
                                        <th width="5%" class="text-center">{{ trans('fields.high') }}</th>
                                        <th width="5%" class="text-center">{{ trans('fields.width') }}</th>
                                        <th width="5%" class="text-center">{{ trans('fields.length') }}</th>
                                        <th width="5%" class="text-center">{{ trans('fields.pcs') }}</th>
                                        <th width="5%" class="text-center">{{ trans('fields.m3') }}</th>
                                        <th width="5%" class="text-center">{{ trans('fields.arrival-location') }}</th>
                                        <th width="5%" class="text-center">{{ trans('fields.status') }}</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = ($models->currentPage() - 1) * $models->perPage() + 1; ?>
                                    @foreach($models as $isi)
                                    <tr>
                                        <th scope="row" style="text-align: center;">{{ $no++ }}</th>
                                        <td class="text-center">{{ $isi->supplier_name }}</td>
                                        <td class="text-center">{{ date('d-m-Y', strtotime($isi->tanggal_kedatangan)) }}</td>
                                        <td class="text-center">{{ $isi->tally_no }}</td>
                                        <td class="text-center">{{ $isi->jenis_kayu }}</td>        
                                        <td class="text-center">{{ $isi->tinggi }}</td>
                                        <td class="text-center">{{ $isi->lebar }}</td>
                                        <td class="text-center">{{ $isi->panjang }}</td>
                                        <td class="text-center">{{ $isi->pcs }}</td>
                                        <td class="text-center">{{ $isi->volume }}</td>
                                        <td class="text-center">{{ $isi->lokasi_kedatangan }}</td>
                                        <td class="text-center">{{ $isi->status_data }}</td>


                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="data-table-toolbar pull-right">
                                {!! $models->render() !!}
                            </div>
                        </div>

                          <div id="infoms" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <!-- <form id="form-cancel" role="form" method="post" action="{{ route('raft-result-savematerialms') }}">
                                        {{ csrf_field() }} -->

                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title" id="cancel-text">{{ trans('fields.info') }}</h4>
                                        </div>

                                       


                                        
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="card-box table-responsive">
                                                        <table id="datatable" class="table table-striped table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th class="text-center">{{ trans('fields.num') }}</th>
                                                                <th class="text-center">{{ trans('fields.wood-type') }}</th>
                                                                <th class="text-center">{{ trans('fields.high') }}</th>
                                                                <th class="text-center">{{ trans('fields.width') }}</th>
                                                                <th class="text-center">{{ trans('fields.length') }}</th>
                                                                <th class="text-center">{{ trans('fields.pcs') }}</th>
                                                                <th class="text-center">{{ trans('fields.m3') }}</th>
                                           

                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php $no = 1;//($models->currentPage() - 1) * $models->perPage() + 1; ?>
                                                            @foreach($info as $in)
                                                            <tr>
                                                                <td class="text-center">{{ $no++ }}</td>
                                                                <td class="text-center">{{ $in->jenis_kayu }}</td>
                                                                <td class="text-center">{{ $in->tinggi }}</td>
                                                                <td class="text-center">{{ $in->lebar }}</td>
                                                                <td class="text-center">{{ $in->panjang }}</td>
                                                                <td class="text-center">{{ $in->pcs }}</td>
                                                                <td class="text-center">{{ $in->volume }}</td>
                                       
                                                            </tr>
                                                            @endforeach
                                                            </tbody>
                                                        </table>

                                                         <table>
                                                          <?php
                            
                                                                //$total_pcs = 0; 
                                                                //$total_volume = 0;
                                                            
                                                            ?>
                                                            {{-- @foreach($models as $view) --}}
                                                            <?php 
                                                                //$total_pcs += $view->pcs;
                                                                //$total_volume += $view->volume;
                                            
                                                                
                                                            ?>
                                                            {{-- @endforeach --}}


                                                            <tr>
                                                                <td></td>
                                                                <td>Total Pcs</td>
                                                                <td> : </td>
                                                                <td>{{ $t_pcs }}</td>
                                                            </tr>
                                                             <tr>
                                                                <td></td>
                                                                <td>Total Volume</td>
                                                                <td> : </td>
                                                                <td>{{ $t_volume }}</td>
                                                            </tr>  
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>  
                                        </div>
                                       
                                       <!--  <div class="modal-footer">
                                            !! $models->render() !!
                                        </div> -->
                                    <!-- </form> -->



                                </div>
                            </div>
                        </div>

                         <div id="dtexcel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form role="form" method="post" action="{{ route('raw-material-import-excel') }}" method="post" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">{{ trans('fields.import-mutation') }}</h4>
                                        </div>
                                 
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label for="file" class="col-md-3 control-label">Import</label>
                                                <div class="col-md-6">
                                                    <input type="file" id="file" name="file" class="form-control" autorequired>
                                                    <span class="help-block with-error"></span>
                                                </div>
                                            </div>
                                           
                                           
                                        </div><br>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                        </div>
                                      
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div id="dtsaldo" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form role="form" method="post" action="{{ route('raw-material-import-saldo') }}" method="post" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">{{ trans('fields.import-saldo') }}</h4>
                                        </div>
                                 
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label for="file" class="col-md-3 control-label">Import</label>
                                                <div class="col-md-6">
                                                    <input type="file" id="file" name="file" class="form-control" autorequired>
                                                    <span class="help-block with-error"></span>
                                                </div>
                                            </div>
                                           
                                           
                                        </div><br>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                        </div>
                                      
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div id="dtlpb" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form role="form" method="post" action="{{ route('raw-material-import-lpb') }}" method="post" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">{{ trans('fields.import-lpb') }}</h4>
                                        </div>
                                 
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label for="file" class="col-md-3 control-label">Import</label>
                                                <div class="col-md-6">
                                                    <input type="file" id="file" name="file" class="form-control" autorequired>
                                                    <span class="help-block with-error"></span>
                                                </div>
                                            </div>
                                           
                                           
                                        </div><br>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                        </div>
                                      
                                    </form>
                                </div>
                            </div>
                        </div>


                    </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection

    @section('script')
    <!-- jQuery  -->
        <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables/dataTables.bootstrap.js') }}"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#datatable').dataTable();
            });
        </script>
@endsection
