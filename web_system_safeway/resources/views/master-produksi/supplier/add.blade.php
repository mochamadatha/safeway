@extends('layouts.master')

@section('title', trans('menu.setup-supplier'))

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
        	<div class="row">
				<div class="col-xs-12">
					<div class="page-title-box">
                        <h4 class="page-title">{{ trans('menu.setup-supplier') }}</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="{{ route('dashboard') }}">{{ trans('menu.dashboard') }}</a>
                            </li>
                            <li>
                                <a>{{ trans('menu.master-produksi') }}</a>
                            </li>
                            <li class="active">
                                {{ trans('menu.setup-supplier') }}
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
				</div>
			</div>
            <!-- end row -->
            
			<div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-border panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ trans('fields.add-supplier') }}</h3>
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal" role="form" method="post" action="{{ route('setup-supplier-save') }}">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="p-20">
                                            <input type="hidden" name="id" class="form-control" value="{{ count($errors) > 0 ? old('id') : $model->supplier_id }}">

                                            <div class="form-group {{ $errors->has('supplier_name') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.supplier_name') }}</label>
                                                <div class="col-md-5">
                                                    <input type="text" name="supplier_name" class="form-control" value="{{ count($errors) > 0 ? old('supplier_name') : $model->supplier_name }}">
                                                    @if($errors->has('supplier_name'))
                                                        <span class="help-block">{{ $errors->first('supplier_name') }}</span>
                                                    @endif
                                                </div>
                                            </div>

                                         <div class="form-group {{ $errors->has('supplier_alamat') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.supplier_alamat') }}</label>
                                                <div class="col-md-5">
                                                    <input type="text" name="supplier_alamat" class="form-control" value="{{ count($errors) > 0 ? old('supplier_alamat') : $model->supplier_alamat }}">
                                                    @if($errors->has('supplier_alamat'))
                                                        <span class="help-block">{{ $errors->first('supplier_alamat') }}</span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group {{ $errors->has('pic') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.supplier_pic') }}</label>
                                                <div class="col-md-5">
                                                    <input type="text" name="pic" class="form-control" value="{{ count($errors) > 0 ? old('pic') : $model->pic }}">
                                                    @if($errors->has('pic'))
                                                        <span class="help-block">{{ $errors->first('pic') }}</span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.phone') }}</label>
                                                <div class="col-md-5">
                                                    <input type="text" name="phone" class="form-control" value="{{ count($errors) > 0 ? old('phone') : $model->phone }}">
                                                    @if($errors->has('phone'))
                                                        <span class="help-block">{{ $errors->first('phone') }}</span>
                                                    @endif
                                                </div>
                                            </div>

                                               <div class="form-group {{ $errors->has('note') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.note') }}</label>
                                                <div class="col-md-5">
                                                    <input type="text" name="note" class="form-control" value="{{ count($errors) > 0 ? old('note') : $model->note }}">
                                                    @if($errors->has('note'))
                                                        <span class="help-block">{{ $errors->first('note') }}</span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <div class="checkbox checkbox-primary">
                                                        <?php $status = count($errors) > 0 ? old('status') : $model->status; ?>
                                                        <input id="status" name="status" value="true" type="checkbox" {{ $status == true ? 'checked' : '' }}>
                                                        <label for="status">
                                                            {{ trans('fields.is-active') }}
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="col-sm-12 data-table-toolbar text-right" style="padding-top: 10px;">
                                        <div class="form-group">
                                            <a href="{{ url('master-produksi/setup-supplier') }}" class="btn btn-sm btn-warning"><i class="fa fa-reply"></i> {{ trans('fields.cancel') }}</a>
                                            <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i> {{ trans('fields.save') }}</button>
                                        </div>
                                    </div> 
                                </div>
                            </form>
                        </div>
                    </div>
                        <!-- end row -->
                </div> <!-- end card-box -->
            </div><!-- end col -->
        </div>
    </div>
</div>
@endsection