<?php

namespace App\Service\Master;

class DivisionService
{
    public static function getActiveDivision()
    {
        return \DB::table('mst_divisi')
            ->where('status', '=', true)
            ->orderBy('nama_divisi')
            ->get();
    }
}
