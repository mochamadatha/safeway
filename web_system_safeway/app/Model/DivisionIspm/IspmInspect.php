<?php

namespace App\Model\DivisionIspm;

use Illuminate\Database\Eloquent\Model;

class IspmInspect extends Model
{
    protected $table        = 'dt_ispminspect';
    protected $primaryKey   = 'ispminspect_id';
    public $timestamps      = false;
}
