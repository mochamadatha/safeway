<?php

namespace App\Model\MasterHRD;

use Illuminate\Database\Eloquent\Model;

class Cabang extends Model
{
    protected $table        = 'mst_cabang';
    protected $primaryKey   = 'cabang_id';
    public $timestamps      = false;
}
