<?php

namespace App\Model\Produksi;

use Illuminate\Database\Eloquent\Model;

class IsiAfkir extends Model
{
    protected $table        = 'dt_isi_afkir';
    protected $primaryKey   = 'isi_afkir_id';
    public $timestamps      = false;
}
