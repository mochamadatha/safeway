<?php 
// use App\Service\Master\SupplierService; 
// use App\Model\MasterProduksi\Supplier;
// use App\Model\Produksi\HdLpb;


list($titlefooter, $date) = explode("#", $title);
?>
<br/><br/>
<h3 style="text-align: center">Surat Perintah Kerja Bandsaw</h3>
<br/><br/>

<table id="header" width="100%" height="100%" border="1px" bgcolor="#20a0c5">
	
    <tr>
		<td width="10%" rowspan="2" style="font-size: 9px; font-weight: bold; text-align:center" >
			<table>
				<tr>
					<td > TallySheet</td>
					
				</tr>
			</table>
		</td>
		<td width="45%" style="font-size: 9px; font-weight: bold; text-align:center" >
			<table>
				<tr>
					<td align-text="center"> Ukuran Bahan Awal</td>
					
				</tr>
			</table>
		</td>
		<td width="45%"  height="15" style="font-size: 9px; font-weight: bold; text-align:center" >
			<table>
				<tr>
                    <td align-text="center"> Rencana Ukuran Belah</td>
					
				</tr>
			</table>
		</td>
    </tr>
    
    <tr>
	
		<td width="45%"  style="font-size: 9px; font-weight: bold; text-align:center" >
			<table border="1px" >
				<tr>
					<td  height="15" > P</td>
                    <td  height="15" > L</td>
                    <td  height="15" > T</td>
                    <td  height="15" > PCS</td>
                    <td  height="15" > M3</td>
				</tr>
			</table>
		</td>
		<td width="45%"  height="15" style="font-size: 9px; font-weight: bold; text-align:center" >
			<table border="1px" >
				<tr>
					<td height="15" > P</td>
                    <td height="15" > L</td>
                    <td height="15" > T</td>
                    <td height="15" > PCS</td>
                    <td height="15" > M3</td>
				</tr>
			</table>
		</td>
	</tr>

	
    
    
   
</table>


<hr/>