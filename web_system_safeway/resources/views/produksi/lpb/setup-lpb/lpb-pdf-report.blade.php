	<br>
	<table id="filters" width="100%" cellpadding="0" cellspacing="0" style="font-size: 11px" border="1px" align="center">
		
		<?php
			$no = 1;
			$total_pcs = 0; 
			$total_volume = 0; 
			$total_harga = 0; 
		?>
		@foreach($isi as $view)
		<tr>
			<td width="4%">{{ $no++ }}</td>
			<td width="16%"> {{ $view->jenis_kayu }}</td>
			<td width="10%">{{ $view->tally_no }}</td>
            <td width="6%">{{ $view->tinggi }}</td>
            <td width="6%">{{ $view->lebar }}</td>
            <td width="6%">{{ $view->panjang }}</td>
            <td width="9%">{{ $view->pcs }}</td>
			<td width="9%">{{ number_format( $view->volume , 4 , ',' , ',' ) }}</td>
			 	<?php if ($view->harga_satuan != ''): ?>
			<td width="17%">
				<table>
					<tr>
						<td align="center" width="25%">Rp.</td>


						<td align="right" width="70%">{{ number_format( $view->harga_satuan , 0 , '' , '.' ) . ",-" }}</td>
						<td width="5%"> </td>
					</tr>
				</table>
			</td>
			<td width="17%">
				<table>
					<tr>
						<td align="center" width="25%">Rp.</td>
						<td align="right" width="70%">{{ number_format( $view->total_harga, 0 , '' , '.' ) . ",-" }}</td>
						<td width="5%"> </td>
					</tr>
				</table>
			</td>

			<?php else: ?>


			<td width="17%">
				<table>
					<tr>
						<td align="center" width="25%">Rp.</td>


						<td align="right" width="70%">{{ number_format( $view->harga , 0 , '' , '.' ) . ",-" }}</td>
						<td width="5%"> </td>
					</tr>
				</table>
			</td>
			<td width="17%">
				<table>
					<tr>
						<td align="center" width="25%">Rp.</td>
						<td align="right" width="70%">{{ number_format( $view->total_harga , 0 , '' , '.' ) . ",-" }}</td>
						<td width="5%"> </td>
					</tr>
				</table>
			</td>

			<?php endif ?>
		</tr>
		<?php 
			$total_pcs += $view->pcs;
			$total_volume += $view->volume;
			if ($view->harga_satuan != '') {
				$total_harga += $view->total_harga;
			} else {
				$total_harga += $view->total_harga;
			}
			
		?>
		@endforeach
		
		<tr>
			<td colspan="6">Total Baik</td>
			<td>{{ $total_pcs }}</td>
			<td>{{ number_format( $total_volume , 4 , ',' , ',' ) }}</td>

			<td align="right" colspan="2">
				<table>
					<tr>
						<td align="center" width="12.5%" >Rp. </td>
						<td align="right" width="85.5%">{{ number_format( $total_harga , 0 , '' , '.' ) . ",-" }}</td>
						<td width="2%"> </td>
					</tr>
				</table>
			</td>		
		</tr>


		<tr>
			<td colspan="10"> </td>	
		</tr>
		<?php
			$num = 1;
			$all_pcs = 0; 
			$all_volume = 0; 
		?>
		@foreach($jumlah as $look)
	
		
		
		<!-- deskripsi -->
		<tr>
			<td>{{ $num++ }}</td>
			<td align="left" colspan="2"> {{ $look->jenis_kayu }}</td>
			<td align="center" colspan="3">{{ $look->nama_deskripsi }}</td>
			<td>{{ $look->pcs }}</td>
			<td>{{ number_format( $look->volume , 4 , ',' , ',' ) }}</td>
			<td align="right" colspan="2">
				<table>
					<tr>
						<td align="center" width="12.5%" >Rp. </td>
						<td align="right" width="85.5%">{{ number_format(  $look->total_harga , 0 , '' , '.' ) . ",-" }}</td>
						<td width="2%"> </td>
					</tr>
				</table>
			</td>		
			
		</tr>

		<?php 
			$all_pcs += $look->pcs;
			$all_volume += $look->volume;
		?>
		@endforeach

		
		
		<tr>
			<td colspan="10"> </td>	
		</tr>
		<?php
			$num = 1;
			$all_pcs = 0; 
			$all_volume = 0; 
		?>
		@foreach($afkir as $see)


		
		<!-- Afkir -->
		<tr>
			<td>{{ $num++ }}</td>
			<td align="left" colspan="2"> {{ $see->jenis_kayu }}</td>
	
			<td>{{ $see->tinggi }}</td>
			<td>{{ $see->lebar }}</td>
			<td>{{ $see->panjang }}</td>
			<td>{{ $see->pcs }}</td>
			<td>{{ number_format( $see->volume , 4 , ',' , ',' ) }}</td>
			
		</tr>

		<?php 
			$all_pcs += $see->pcs;
			$all_volume += $see->volume;
		?>
		@endforeach

		<tr>
			<td colspan="6">Total Afkir</td>
			<td>{{ $all_pcs }}</td>
			<td>{{ number_format( $all_volume , 4 , ',' , ',' ) }}</td>
				
		</tr>

		
	
		<tr bgcolor="#808B96" style="color: white; font-weight: bold;">
			<td colspan="6">Grand Total</td>
			<td><?php echo ($total_pcs+$all_pcs); ?></td>
			<td><?php echo  number_format( ($total_volume+$all_volume) , 4 , ',' , '.' ) ; ?></td>



			<td align="right" colspan="2">
				<table>
					<tr>
						<td align="center" width="12.5%" >Rp. </td>
						<td align="right" width="85.5%">{{ number_format( $total_harga , 0 , '' , '.' ) . ",-" }}</td>
						<td width="2%"> </td>
					</tr>
				</table>
			</td>		
			
		</tr>
		<tr style="font-size: 10px">
			<td colspan="2" border="0px">
				<table>
					<tr>
						<td>Administrasi Hutang</td>
					</tr>
					<tr>
						<td height="45"> </td>
					</tr>
					<tr>
						<td>Wiwik</td>
					</tr>
				</table>
			</td>
			<td colspan="4" border="0px">
				<table>
					<tr>
						<td>Head Of Division</td>
					</tr>
					<tr>
						<td height="45"> </td>
					</tr>
					<tr>
						<td>Harry Wijayanto</td>
					</tr>
				</table>
			</td>
			<td colspan="2" border="0px">
				<table>
					<tr>
						<td>Plant Manager</td>
					</tr>
					<tr>
						<td height="45"> </td>
					</tr>
					<tr>
						<td>Mustofa</td>
					</tr>
				</table>
			</td>
			<td colspan="2" border="0px">
				<table>
					<tr>
						<td>Raw Material Staff</td>
					</tr>
					<tr>
						<td height="45"> </td>
					</tr>
					<tr>
						<td>Rifa'i</td>
					</tr>
				</table>
			</td>
		</tr>

	</table>