<?php

namespace App\Model\MasterContainer;

use Illuminate\Database\Eloquent\Model;

class Inspect extends Model
{
    protected $table        = 'mst_inspect';
    protected $primaryKey   = 'inspect_id';
    public $timestamps      = false;
}
