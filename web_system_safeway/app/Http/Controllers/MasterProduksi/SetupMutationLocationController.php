<?php

namespace App\Http\Controllers\MasterProduksi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\MasterProduksi\LokasiMutasi;
//use Maatwebsite\Excel\Excel;

use Maatwebsite\Excel\Facades\Excel;
use DB;

class SetupMutationLocationController extends Controller
{
    const URL       = 'master-produksi/setup-mutation-location';
    const RESOURCE  = 'setup-mutation-location';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        } 

        if ($request->isMethod('post')) {
            $request->session()->put('filters', $request->all());
            return redirect(self::URL.'?page=1');
        } elseif (empty($request->get('page'))) {
            $request->session()->forget('filters');
        }
        $filters = $request->session()->get('filters');
        $query = \DB::table('mst_lokasi_mutasi')
                    ->orderBy('mst_lokasi_mutasi.lokasi_mutasi');

        if (!empty($filters['status']) || !$request->session()->has('filters')) {
            $query->where('mst_lokasi_mutasi.status', '=', true);
        } else {
            $query->where('mst_lokasi_mutasi.status', '=', false);
        }

        if (!empty($filters['lokasi_mutasi'])) {
            $query->where('mst_lokasi_mutasi.lokasi_mutasi', 'ilike', '%'.$filters['lokasi_mutasi'].'%');
        }

        return view('master-produksi/mutation-location.index',[
            "models"        => $query->paginate(10),
            "filters"       => $filters,
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function add(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        } 

        $model           = new LokasiMutasi();
        $model->status   = true;
        
        return view('master-produksi/mutation-location.add',[
            "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }

    public function edit(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        } 

        $model = LokasiMutasi::find($id);

        if ($model===null) {
            abort('404');
        }
        
        return view('master-produksi/mutation-location.add',[
            "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }

    public function importexcel (Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        } 

        //if ($request->Request('file')) {
             $model  = new LokasiMutasi();
             $now    = new \DateTime();
               
            $model->created_date    = $now;
       
            //$model->modified_date = $now;
            $path = $request->file('file')->getRealPath();
            $data = EXCEL::load($path, function($reader){})->get();
          
            if (!empty($data) && $data->count()) {
                foreach ($data as $key => $value) {
                    $modal = new LokasiMutasi();
                    $modal->lokasi_mutasi = $value->lokasi_mutasi;
                    $modal->created_by = \Auth::user()->id;
                    $modal->created_date    =  $now;




                  
                    # code...
                }


    
             

               

            } 
            
        $modal->save();
        return back ();

    }



    public function downloadExcel($type)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        } 
        $data = LokasiMutasi::get()->toArray();
        return Excel::create('itsolutionstuff_example', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download($type);
    }
    

    public function save(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add']) && !\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        } 

        $id     = intval($request->get('id', 0));

        $this->validate($request, [
            'lokasi_mutasi' => 'required|max:255|unique:mst_lokasi_mutasi,lokasi_mutasi,'.$id.',lokasi_mutasi_id',
        ]);

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            if(empty($id)){
                $model  = new LokasiMutasi();
                $model->created_by      = \Auth::user()->id;
                $model->created_date    = $now;
            }else{
                $model  = LokasiMutasi::find($id);
                $model->modified_by   = \Auth::user()->id;
                $model->modified_date = $now;
            }

            $model->lokasi_mutasi   = $request->get('lokasi_mutasi');
            $model->status   = !empty($request->get('status')) ? true : false;
            
            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('menu.mutation-location').' '.$request->get('lokasi_mutasi')])
                );

        } catch (\Exception $e) {
             DB::rollback();
             $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('menu.mutation-location').' '.$request->get('lokasi_mutasi')])
                );
         }

        return redirect(route('setup-mutation-location-index'));
    }
}
