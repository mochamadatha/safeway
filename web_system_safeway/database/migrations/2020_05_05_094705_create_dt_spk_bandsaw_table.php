<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDtSpkBandsawTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dt_spk_bandsaw', function (Blueprint $table) {
            $table->increments('dt_spk_bandsaw_id');
            $table->integer('hd_spk_bandsaw_id');
            $table->integer('tally_id');
            // $table->integer('isi_tally_id');
            $table->boolean('status');
            $table->integer('created_by');
            $table->dateTime('created_date');
            $table->integer('modified_by')->nullable();
            $table->dateTime('modified_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dt_spk_bandsaw');
    }
}
