<?php

namespace App\Service\Master;

class ArrivalLocationService
{
    public static function getActiveArrivalLocation()
    {
        return \DB::table('mst_lokasi_kedatangan')
            ->where('status', '=', true)
            ->orderBy('lokasi_kedatangan')
            ->get();
    }
}
