<?php

namespace App\Service\Master;

class DesService
{
    public static function getActiveDes()
    {
        return \DB::table('mst_deskripsi_tallysheet')
            ->where('status', '=', true)
            ->orderBy('nama_deskripsi')
            ->get();
    }
}
