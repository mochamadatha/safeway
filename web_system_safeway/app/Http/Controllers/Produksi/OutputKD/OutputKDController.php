<?php

namespace App\Http\Controllers\Produksi\OutputKD;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Produksi\LPB;
use App\Model\Produksi\LPBDetail;
use App\Service\Master\TallyService;
use App\Model\MasterProduksi\Supplier;
use App\Service\Master\LocationOutputKDService;
use App\Model\Produksi\HdLpb;
use Elibyy\TCPDF\Facades\TCPDF;
use App\Model\Produksi\IsiBandsaw;
use App\Model\Produksi\IsiAfkir;
use App\Service\Master\ChamberService;
use Illuminate\Support\Facades\Input;
use App\Model\MasterProduksi\LokasiKedatangan;
use App\Model\MasterProduksi\LokasiOutputKD;
use App\Service\Master\ArrivalLocationService;
use Illuminate\Pagination\LengthAwarePaginator;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class OutputKDController extends Controller
{
    const URL       = 'produksi/output-kd';
    const RESOURCE  = 'output-kd';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        if ($request->isMethod('post')) {
            $request->session()->put('filters', $request->all());
            return redirect(self::URL . '?page=1');
        } elseif (empty($request->get('page'))) {
            $request->session()->forget('filters');
        }

        $page = Input::get('page', 1);
        $paginate = 10;


        // $info = DB::table('hd_lpb')
        //     ->select(
        //         'tally_table.tally_no',
        //         'tally_table.date_in_kd',
        //         'mst_jenis_kayu.jenis_kayu',
        //         'dt_isi_tally.tinggi',
        //         'dt_isi_tally.lebar',
        //         'dt_isi_tally.panjang',
        //         'dt_isi_tally.pcs',
        //         'dt_isi_tally.volume',
              
            
        //         'mst_lokasi_outputkd.lokasi_outputkd'
        //     )
        //     ->where('hd_lpb.status', '=', true)
        //     ->where('tally_table.date_in_kd', '!=', NULL)
        //     ->where('tally_table.date_out_kd', '!=', NULL)
        //     ->where('tally_table.date_mutation_wip', '=', NULL)
        //     ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')
        //     ->whereNotIn('dt_lpb.tally_id',  DB::table('dt_isi_bandsaw')
        //         ->select('tally_id')
        //         ->groupBy('tally_id'))
         
        //     ->join('dt_lpb', 'dt_lpb.hd_lpb_id', '=', 'hd_lpb.hd_lpb_id')
        //     ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
        //     ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
        //     ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')

        //     ->join('mst_lokasi_outputkd', 'mst_lokasi_outputkd.lokasi_outputkd_id', '=', 'tally_table.lokasi_outputkd_id')
        //     ->orderBy('hd_lpb.created_by');

        // //dd ($query);

        // $info2 = DB::table('dt_isi_bandsaw')
        //     ->select('tally_table.tally_no',
        //      'tally_table.date_out_kd', 
        //      'mst_jenis_kayu.jenis_kayu', 
        //      'dt_isi_bandsaw.tinggi', 
        //      'dt_isi_bandsaw.lebar', 
        //      'dt_isi_bandsaw.panjang', 
        //      'dt_isi_bandsaw.pcs',
        //       'dt_isi_bandsaw.volume', 
        //       'mst_lokasi_outputkd.lokasi_outputkd')
        //     ->where('dt_isi_bandsaw.status', '=', true)
        //     // ->where('tally_table.date_in_kd', '=', null)
        //     // ->where('dt_isi_bandsaw.tally_id', '=', $id)
        //     ->whereIn('dt_isi_bandsaw.tally_id', DB::table('tally_table')
        //         ->select('tally_table.tally_id')
        //         ->where('tally_table.date_in_kd', '!=', NULL)
        //         ->where('tally_table.date_out_kd', '!=', NULL)
        //         ->where('tally_table.date_mutation_wip', '=', NULL)
        //         ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')
        //         ->join('dt_isi_bandsaw', 'dt_isi_bandsaw.tally_id', '=', 'tally_table.tally_id')
        //         ->groupBy('tally_id'))
        //     ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'dt_isi_bandsaw.tally_id')
        //     ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
        //     ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
          
        //     ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')

        //     // ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')

        //     ->join('mst_lokasi_outputkd', 'mst_lokasi_outputkd.lokasi_outputkd_id', '=', 'tally_table.lokasi_outputkd_id')
        //     ->groupBy('tally_table.tally_no', 'tally_table.date_out_kd', 'mst_jenis_kayu.jenis_kayu', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'dt_isi_bandsaw.pcs', 'dt_isi_bandsaw.volume', 'mst_lokasi_outputkd.lokasi_outputkd')
        //     ->union($info)
        //     ->orderBy('tally_no')->get();

        $info = DB::table('hd_lpb')
        ->select(
            'mst_jenis_kayu.jenis_kayu',
            'dt_isi_tally.tinggi',
            'dt_isi_tally.lebar',
            'dt_isi_tally.panjang', 'mst_lokasi_outputkd.lokasi_outputkd',
            DB::raw('SUM(dt_isi_tally.pcs) as pcs'),
            DB::raw('SUM(dt_isi_tally.volume) as volume')
        )
        ->where('hd_lpb.status', '=', true)
        ->where('tally_table.date_in_kd', '!=', NULL)
        ->where('tally_table.date_out_kd', '!=', NULL)
        ->where('dt_isi_tally.status', '=', true)
        ->where('tally_table.date_mutation_wip', '=', NULL)
        ->whereNotIn('dt_lpb.tally_id',  DB::table('dt_isi_bandsaw')
            ->select('tally_id')
            ->groupBy('tally_id'))
        // ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', 'hd_lpb.lokasi_kedatangan_id')
        ->join('dt_lpb', 'dt_lpb.hd_lpb_id', '=', 'hd_lpb.hd_lpb_id')
        ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
        ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
        ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
        ->join('mst_lokasi_outputkd', 'mst_lokasi_outputkd.lokasi_outputkd_id', '=', 'tally_table.lokasi_outputkd_id')
        ->groupBy('mst_jenis_kayu.jenis_kayu', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 'dt_isi_tally.panjang',  'mst_lokasi_outputkd.lokasi_outputkd');



    $info2 = DB::table('dt_isi_bandsaw')
        ->select('mst_jenis_kayu.jenis_kayu', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'mst_lokasi_outputkd.lokasi_outputkd', DB::raw('SUM(dt_isi_bandsaw.pcs) as pcs'), DB::raw('SUM(dt_isi_bandsaw.volume) as volume'))
        ->where('dt_isi_bandsaw.status', '=', true)
        ->where('tally_table.date_in_kd', '!=', NULL)
        ->where('tally_table.date_out_kd', '!=', NULL)
        ->where('tally_table.date_mutation_wip', '=', NULL)
        // ->where('tally_table.date_in_kd', '=', null)
        // ->where('dt_isi_bandsaw.tally_id', '=', $id)
        ->whereIn('dt_isi_bandsaw.tally_id', DB::table('tally_table')
            ->select('tally_table.tally_id')
            ->where('tally_table.date_in_kd', '=', NULL)

            ->join('dt_isi_bandsaw', 'dt_isi_bandsaw.tally_id', '=', 'tally_table.tally_id')
            ->groupBy('tally_id'))
        // ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'dt_isi_bandsaw.tally_id')
        // ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
        ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
        // ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=' ,'hd_lpb.lokasi_kedatangan_id')
        ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')
        // ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')
        ->join('mst_lokasi_outputkd', 'mst_lokasi_outputkd.lokasi_outputkd_id', '=', 'tally_table.lokasi_outputkd_id')
        ->groupBy('mst_jenis_kayu.jenis_kayu', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'mst_lokasi_outputkd.lokasi_outputkd')
        ->union($info)
        ->orderBy('jenis_kayu')->get();





        // dd($info2);  




        $filters = $request->session()->get('filters');


        $query = DB::table('hd_lpb')
            ->select(
                'tally_table.tally_no',
                'tally_table.date_out_kd',
                'mst_jenis_kayu.jenis_kayu',
                'dt_isi_tally.deskripsi_id',
                'dt_isi_tally.tinggi',
                'dt_isi_tally.lebar',
                'dt_isi_tally.panjang',
                'dt_isi_tally.pcs',
                'dt_isi_tally.volume',
                'hd_lpb.tanggal_kedatangan',
                'mst_supplier.supplier_name',
                'dt_isi_tally.created_date',
                'mst_lokasi_outputkd.lokasi_outputkd',
                DB::raw('"Non BS" status_data')
            )
            ->where('hd_lpb.status', '=', true)
            ->where('tally_table.date_in_kd', '!=', NULL)
            ->where('tally_table.date_out_kd', '!=', NULL)
            ->where('tally_table.date_mutation_wip', '=', NULL)
            ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')
            ->where('tally_table.tally_no', 'like', '%' . $filters['tally_no'] . '%')
            ->whereNotIn('dt_lpb.tally_id',  DB::table('dt_isi_bandsaw')
                ->select('tally_id')
                ->groupBy('tally_id'))
            ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', 'hd_lpb.lokasi_kedatangan_id')
            ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_lpb.supplier_id')
            ->join('dt_lpb', 'dt_lpb.hd_lpb_id', '=', 'hd_lpb.hd_lpb_id')
            ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')

            ->join('mst_lokasi_outputkd', 'mst_lokasi_outputkd.lokasi_outputkd_id', '=', 'tally_table.lokasi_outputkd_id')
            ->orderBy('hd_lpb.created_by');


        //dd ($query);

        $query2 = DB::table('dt_isi_bandsaw')
            ->select(
                'tally_table.tally_no',
                'tally_table.date_out_kd',
                'mst_jenis_kayu.jenis_kayu',
                'dt_isi_bandsaw.deskripsi_id',
                'dt_isi_bandsaw.tinggi',
                'dt_isi_bandsaw.lebar',
                'dt_isi_bandsaw.panjang',
                'dt_isi_bandsaw.pcs',
                'dt_isi_bandsaw.volume',
                'hd_lpb.tanggal_kedatangan',
                'mst_supplier.supplier_name',
                'dt_isi_bandsaw.created_date',
                'mst_lokasi_outputkd.lokasi_outputkd',
                DB::raw('"BS" status_data')
            )
            ->where('dt_isi_bandsaw.status', '=', true)
            ->where('tally_table.tally_no', 'like', '%' . $filters['tally_no'] . '%')
            ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')
            // ->where('tally_table.date_in_kd', '=', null)
            // ->where('dt_isi_bandsaw.tally_id', '=', $id)
            ->whereIn('dt_isi_bandsaw.tally_id', DB::table('tally_table')
                ->select('tally_table.tally_id')
                ->where('tally_table.date_in_kd', '!=', NULL)
                ->where('tally_table.date_out_kd', '!=', NULL)
                ->where('tally_table.date_mutation_wip', '=', NULL)
                ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')
                ->join('dt_isi_bandsaw', 'dt_isi_bandsaw.tally_id', '=', 'tally_table.tally_id')
                ->groupBy('tally_id'))
            ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'dt_isi_bandsaw.tally_id')
            ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
            ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=', 'hd_lpb.lokasi_kedatangan_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')
            ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_lpb.supplier_id')

            // ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')

            ->join('mst_lokasi_outputkd', 'mst_lokasi_outputkd.lokasi_outputkd_id', '=', 'tally_table.lokasi_outputkd_id')
            ->groupBy('tally_table.tally_no', 'tally_table.date_out_kd', 'dt_isi_bandsaw.deskripsi_id','mst_jenis_kayu.jenis_kayu', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'dt_isi_bandsaw.pcs', 'dt_isi_bandsaw.volume', 'dt_isi_bandsaw.created_date', 'mst_lokasi_outputkd.lokasi_outputkd',  'hd_lpb.tanggal_kedatangan', 'mst_supplier.supplier_name')
            ->union($query)
            ->orderBy('created_date')->get();

        $t_pcs = 0;
        $t_volume = 0;
        foreach ($query2 as $i) {
            $t_pcs += $i->pcs;
            $t_volume += $i->volume;
        }


        // dd($query2);


        if (!empty($filters['status']) || !$request->session()->has('filters')) {
            $query->where('hd_lpb.status', '=', true);
        } else {
            $query->where('hd_lpb.status', '=', false);
        }


        $slice = array_slice($query2->toArray(), $paginate * ($page - 1), $paginate);
        $result = new LengthAwarePaginator($slice, count($query2), $paginate);
        // return View::make('produksi/mutation_wip/mutation.addtally');
        // dd(url()->current());
        $result = $result->setPath(url()->current());



        return view('produksi/output-kd.index', [

            // "models"         => $query->paginate(10),
            "models"         => $result,
            "info"         => $info2,
            "LocationOutputKDOptions"   => LocationOutputKDService::getActiveLocationOutputKD(),
            "t_pcs"      => $t_pcs,
            "t_volume"      => $t_volume,
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }


    public function exportdataoutkd()
    {

        // dd('test');

        ob_end_clean();
        ob_start();
        Excel::create('Data All Output KD  (history)', function ($excel) {

            $excel->sheet('Sheet 1', function ($sheet) {

        $query = DB::table('hd_lpb')
                    ->select(
                        'tally_table.tally_no',
                        'tally_table.date_out_kd',
                        'mst_jenis_kayu.jenis_kayu',
                        'dt_isi_tally.deskripsi_id',
                        'dt_isi_tally.tinggi',
                        'dt_isi_tally.lebar',
                        'dt_isi_tally.panjang',
                        'dt_isi_tally.pcs',
                        'dt_isi_tally.volume',
                        'mst_lokasi_kedatangan.lokasi_kedatangan',
                        'dt_isi_tally.created_date',
                        'mst_chamber.chamber_name',
                        DB::raw('"Non BS" status_data')
                    )
                    ->where('hd_lpb.status', '=', true)
                    ->where('tally_table.date_out_kd', '!=', NULL)
                    ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')

                    ->whereNotIn('dt_lpb.tally_id',  DB::table('dt_isi_bandsaw')
                        ->select('tally_id')
                        ->groupBy('tally_id'))
                    ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', 'hd_lpb.lokasi_kedatangan_id')
                    ->join('dt_lpb', 'dt_lpb.hd_lpb_id', '=', 'hd_lpb.hd_lpb_id')
                    ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
                    ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
                    ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
                    ->join('mst_chamber', 'mst_chamber.chamber_id', 'tally_table.chamber_id')
                    ->orderBy('hd_lpb.created_by');


                $query2 = DB::table('dt_isi_bandsaw')
                    ->select(
                        'tally_table.tally_no',
                        'tally_table.date_out_kd',
                        'mst_jenis_kayu.jenis_kayu',
                        'dt_isi_bandsaw.deskripsi_id',
                        'dt_isi_bandsaw.tinggi',
                        'dt_isi_bandsaw.lebar',
                        'dt_isi_bandsaw.panjang',
                        'dt_isi_bandsaw.pcs',
                        'dt_isi_bandsaw.volume',
                        'mst_lokasi_kedatangan.lokasi_kedatangan',
                        'dt_isi_bandsaw.created_date',
                        'mst_chamber.chamber_name',
                        DB::raw('"BS" status_data')
                    )
                    ->where('dt_isi_bandsaw.status', '=', true)
                    ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')
                    

                    ->whereIn('dt_isi_bandsaw.tally_id', DB::table('tally_table')
                        ->select('tally_table.tally_id')
                        ->where('tally_table.date_out_kd', '!=', NULL)
                        ->join('dt_isi_bandsaw', 'dt_isi_bandsaw.tally_id', '=', 'tally_table.tally_id')
                        ->groupBy('tally_id'))
                    ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'dt_isi_bandsaw.tally_id')
                    ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
                    ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
                    ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=', 'hd_lpb.lokasi_kedatangan_id')
                    ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')
                    // ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')
                    ->join('mst_chamber', 'mst_chamber.chamber_id', 'tally_table.chamber_id')
                    ->groupBy('tally_table.tally_no', 'tally_table.date_out_kd','dt_isi_bandsaw.deskripsi_id', 'mst_jenis_kayu.jenis_kayu', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'dt_isi_bandsaw.pcs', 'dt_isi_bandsaw.volume', 'mst_lokasi_kedatangan.lokasi_kedatangan', 'dt_isi_bandsaw.created_date', 'mst_chamber.chamber_name')
                    ->union($query)
                    ->orderBy('created_date')->get();
    
                    
 


                    // dd($query2);


                foreach ($query2 as $product) {
                    $data[] = array(
                        // $product->isi_tally_id,
                        $product->tally_no,
                        $product->jenis_kayu,
                        $product->date_out_kd,
                        // $product->date_out_kd,
                        // $product->date_mutation_wip,
                        $product->tinggi,
                        $product->lebar,
                        $product->panjang,
                        $product->pcs,
                        $product->volume,
                        $product->lokasi_kedatangan,
                        // $product->chamber_name,
                        $product->status_data


                    );
                }

                $sheet->fromArray($data, null, 'A1', false, false);
                $headings = array(
                    'Nomor Tally','Jenis Kayu',  'Tanggal Out KD', 'Tinggi', 'Lebar', 'Panjang', 'Pcs', 'Volume','Lokasi Kedatangan', 'Status'
                    
                );
                $sheet->prependRow(1, $headings);
            });
        })->export('xlsx');
    }
    public function exportexcel()
    {

        ob_end_clean();
        ob_start();
        Excel::create('Data Output KD', function ($excel) {

            $excel->sheet('Sheet 1', function ($sheet) {

                $query = DB::table('hd_lpb')
                    ->select(
                        'tally_table.tally_no',
                        'tally_table.date_out_kd',
                        'mst_jenis_kayu.jenis_kayu',
                        'dt_isi_tally.deskripsi_id',
                        'dt_isi_tally.tinggi',
                        'dt_isi_tally.lebar',
                        'dt_isi_tally.panjang',
                        'dt_isi_tally.pcs',
                        'dt_isi_tally.volume',
                        'hd_lpb.tanggal_kedatangan',
                        'mst_supplier.supplier_name',
                        'dt_isi_tally.created_date',
                        'mst_lokasi_outputkd.lokasi_outputkd',
                        DB::raw('"Non BS" status_data')
                    )
                    ->where('hd_lpb.status', '=', true)
                    ->where('tally_table.date_in_kd', '!=', NULL)
                    ->where('tally_table.date_out_kd', '!=', NULL)
                    ->where('tally_table.date_mutation_wip', '=', NULL)
                    ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')

                    ->whereNotIn('dt_lpb.tally_id',  DB::table('dt_isi_bandsaw')
                        ->select('tally_id')
                        ->groupBy('tally_id'))
                    ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', 'hd_lpb.lokasi_kedatangan_id')
                    ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_lpb.supplier_id')
                    ->join('dt_lpb', 'dt_lpb.hd_lpb_id', '=', 'hd_lpb.hd_lpb_id')
                    ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
                    ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
                    ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')

                    ->join('mst_lokasi_outputkd', 'mst_lokasi_outputkd.lokasi_outputkd_id', '=', 'tally_table.lokasi_outputkd_id')
                    ->orderBy('hd_lpb.created_by');


                //dd ($query);

                $query2 = DB::table('dt_isi_bandsaw')
                    ->select(
                        'tally_table.tally_no',
                        'tally_table.date_out_kd',
                        'mst_jenis_kayu.jenis_kayu',
                        'dt_isi_bandsaw.deskripsi_id',
                        'dt_isi_bandsaw.tinggi',
                        'dt_isi_bandsaw.lebar',
                        'dt_isi_bandsaw.panjang',
                        'dt_isi_bandsaw.pcs',
                        'dt_isi_bandsaw.volume',
                        'hd_lpb.tanggal_kedatangan',
                        'mst_supplier.supplier_name',
                        'dt_isi_bandsaw.created_date',
                        'mst_lokasi_outputkd.lokasi_outputkd',
                        DB::raw('"BS" status_data')
                    )
                    ->where('dt_isi_bandsaw.status', '=', true)

                    ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')
                    // ->where('tally_table.date_in_kd', '=', null)
                    // ->where('dt_isi_bandsaw.tally_id', '=', $id)
                    ->whereIn('dt_isi_bandsaw.tally_id', DB::table('tally_table')
                        ->select('tally_table.tally_id')
                        ->where('tally_table.date_in_kd', '!=', NULL)
                        ->where('tally_table.date_out_kd', '!=', NULL)
                        ->where('tally_table.date_mutation_wip', '=', NULL)
                        ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')
                        ->join('dt_isi_bandsaw', 'dt_isi_bandsaw.tally_id', '=', 'tally_table.tally_id')
                        ->groupBy('tally_id'))
                    ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'dt_isi_bandsaw.tally_id')
                    ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
                    ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
                    ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=', 'hd_lpb.lokasi_kedatangan_id')
                    ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')
                    ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_lpb.supplier_id')

                    // ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')

                    ->join('mst_lokasi_outputkd', 'mst_lokasi_outputkd.lokasi_outputkd_id', '=', 'tally_table.lokasi_outputkd_id')
                    ->groupBy('tally_table.tally_no', 'tally_table.date_out_kd','dt_isi_bandsaw.deskripsi_id', 'mst_jenis_kayu.jenis_kayu', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'dt_isi_bandsaw.pcs', 'dt_isi_bandsaw.volume', 'dt_isi_bandsaw.created_date', 'mst_lokasi_outputkd.lokasi_outputkd',  'hd_lpb.tanggal_kedatangan', 'mst_supplier.supplier_name')
                    ->union($query)
                    ->orderBy('created_date')->get();

                foreach ($query2 as $product) {
                    $data[] = array(
                        $product->supplier_name,
                        $product->tanggal_kedatangan,
                        $product->date_out_kd,
                        $product->tally_no,
                        $product->jenis_kayu,
                        $product->tinggi,
                        $product->lebar,
                        $product->panjang,
                        $product->pcs,
                        $product->volume,
                        $product->lokasi_outputkd,
                        $product->status_data,


                    );
                }

                $sheet->fromArray($data, null, 'A1', false, false);
                $headings = array(
                    'Nama Supplier', 'Tanggal Kedatangan', 'Tanggal Output KD', 'Nomor Tally', 'Jenis Kayu', 'Tinggi', 'Lebar',
                    'Panjang', 'Pcs', 'Volume', 'Lokasi Output KD', 'Status'
                );
                $sheet->prependRow(1, $headings);
            });
        })->export('xlsx');
    }


    public function save(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        $this->validate($request, [
            'date_out_kd'    => 'required|date',
            'tally_no'      => 'required',
            'lokasi_outputkd_id'    => 'required|integer',
        ]);

        DB::beginTransaction();
        try {

            // $now    = new \DateTime();
            $tally_split = str_replace("\r\n", ',', $request->get('tally_no'));
            $tally_split = explode(",", $tally_split);
            $query = DB::table('tally_table')
                ->whereIn('tally_no', $tally_split)
                ->where('date_in_kd', '!=', NULL)
                ->where('date_out_kd', '=', NULL)
                ->update(['lokasi_outputkd_id' => $request->get('lokasi_outputkd_id'), 'date_out_kd' => $request->get('date_out_kd')]);

            // dd($query);

            // $query->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('fields.add-output-kd') . ' ' . ''])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('fields.add-input-kd') . ' ' . ''])
            );
        }

        return redirect(route('output-kd-index'));

        // $query = DB::table('hd_lpb')
        // ->select('hd_lpb.*', 'tally_table.tally_no')
        //         ->join('tally_table', 'tally_table.tally_id', '=', 'hd_lpb.tally_id')
        //      //   ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_lpb.supplier_id')
        // ->orderBy('hd_lpb.created_by');

        // return view('produksi/lpb/setup-lpb.add',[
        //     "model"         => $model,
        //     // "models"         => $query->paginate(10),
        //    // "tallyOptions"   => TallyService::getActiveTally(),
        //     "supplierOptions" => SupplierService::getActiveSupplier(),
        //     "arrivallocationOptions"    => ArrivalLocationService::getActiveArrivalLocation(),
        //     "url"           => self::URL,
        //     "resource"      => self::RESOURCE,
        // ]);

        // $b = htmlspecialchars_decode($request->get('tally_no'));
        // $a = $request->get('tally_no');
        // $text = str_replace(PHP_EOL, ',',$a);
        // $text = explode(",", $text);
        // // echo($text);
        // // $text = "[". $text . "]";
        // $query = DB::table('tally_table')
        //         ->whereIn('tally_no', $text)->get();

        // dd($query);
        // dd($text);
    }
}
