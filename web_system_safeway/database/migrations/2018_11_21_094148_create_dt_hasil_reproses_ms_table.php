<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDtHasilReprosesMsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dt_hasil_reproses_ms', function (Blueprint $table) {
            $table->increments('hasil_reproses_ms_id');
            $table->integer('ambil_stockms_id');
            $table->integer('jenis_kayu_id');
            $table->integer('tinggi');
            $table->integer('lebar');
            $table->integer('panjang');
            $table->integer('pcs');
            $table->double('volume');
            $table->boolean('status')->default(true);
            $table->date('tanggal_reproses');
            $table->integer('created_by');
            $table->dateTime('created_date');
            $table->integer('modified_by')->nullable();
            $table->dateTime('modified_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dt_hasil_reproses_ms');
    }
}
