<?php

namespace App\Http\Controllers\Produksi\Dashboard;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Model\Produksi\LPB;
use App\Model\Produksi\LPBDetail;
use App\Service\Master\TallyService;
use App\Model\MasterProduksi\Supplier;
use App\Service\Master\SupplierService;
use App\Model\Produksi\HdLpb;
use App\Model\Produksi\IsiBandsaw;
use Elibyy\TCPDF\Facades\TCPDF;
use App\Model\Produksi\IsiAfkir;
use App\Service\Master\WoodService;
use App\Model\MasterProduksi\LokasiKedatangan;
use App\Service\Master\ArrivalLocationService;
use DB;

class DashboardController extends Controller
{
    const URL       = 'produksi/dashboard';
    const RESOURCE  = 'dashboard';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        if ($request->isMethod('post')) {
            $request->session()->put('filters', $request->all());
            return redirect(self::URL . '?page=1');
        } elseif (empty($request->get('page'))) {
            $request->session()->forget('filters');
        }

        $page = Input::get('page', 1);
        $paginate = 10;

        $query = DB::table('dt_lpb')
            ->select('tally_table.tally_no', 'mst_jenis_kayu.jenis_kayu', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 'dt_isi_tally.panjang', 'dt_isi_tally.pcs', 'dt_isi_tally.volume', 'mst_lokasi_kedatangan.lokasi_kedatangan', 'dt_lpb.created_date')
            ->where('dt_lpb.status', '=', true)
            ->where('tally_table.date_in_kd', '=', null)
            ->whereNotIn('dt_lpb.tally_id',  DB::table('dt_isi_bandsaw')
                ->select('tally_id')
                ->groupBy('tally_id'))
            ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
            ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=', 'hd_lpb.lokasi_kedatangan_id')
            ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
            // ->orderBy('hd_lpb.created_by');
            ->orderBy('dt_lpb.created_date', 'desc');

        $query2 = DB::table('dt_isi_bandsaw')
            ->select('tally_table.tally_no', 'mst_jenis_kayu.jenis_kayu', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'dt_isi_bandsaw.pcs', 'dt_isi_bandsaw.volume', 'mst_lokasi_kedatangan.lokasi_kedatangan', 'dt_isi_bandsaw.created_date')
            ->where('dt_isi_bandsaw.status', '=', true)
            ->where('tally_table.date_in_kd', '=', null)
            // ->where('dt_isi_bandsaw.tally_id', '=', $id)
            ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'dt_isi_bandsaw.tally_id')
            ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
            ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=', 'hd_lpb.lokasi_kedatangan_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')
            ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')
            ->groupBy('tally_table.tally_no', 'mst_jenis_kayu.jenis_kayu', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'dt_isi_bandsaw.pcs', 'dt_isi_bandsaw.volume', 'mst_lokasi_kedatangan.lokasi_kedatangan', 'dt_isi_bandsaw.created_date')
            ->union($query)
            ->orderBy('created_date')->get();


        $info = DB::table('dt_lpb')
            ->select(
                'mst_jenis_kayu.jenis_kayu',
                'dt_isi_tally.tinggi',
                'dt_isi_tally.lebar',
                'dt_isi_tally.panjang',
                DB::raw('SUM(dt_isi_tally.pcs) as pcs'),
                DB::raw('SUM(dt_isi_tally.volume) as volume')
            )
            ->where('dt_lpb.status', '=', true)
            ->where('tally_table.date_in_kd', '=', null)
            ->whereNotIn('dt_lpb.tally_id',  DB::table('dt_isi_bandsaw')
                ->select('tally_id')
                ->groupBy('tally_id'))
            ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
            ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=', 'hd_lpb.lokasi_kedatangan_id')
            ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
            // ->orderBy('hd_lpb.created_by');
            ->groupBy('mst_jenis_kayu.jenis_kayu', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 'dt_isi_tally.panjang');

        $info2 = DB::table('dt_isi_bandsaw')
            ->select('mst_jenis_kayu.jenis_kayu', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', DB::raw('SUM(dt_isi_bandsaw.pcs) as pcs'), DB::raw('SUM(dt_isi_bandsaw.volume) as volume'))
            ->where('dt_isi_bandsaw.status', '=', true)
            ->where('tally_table.date_in_kd', '=', null)
            // ->where('dt_isi_bandsaw.tally_id', '=', $id)
            ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'dt_isi_bandsaw.tally_id')
            ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
            ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=', 'hd_lpb.lokasi_kedatangan_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')
            ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')
            ->groupBy('tally_table.tally_no', 'mst_jenis_kayu.jenis_kayu', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'dt_isi_bandsaw.pcs', 'dt_isi_bandsaw.volume', 'mst_lokasi_kedatangan.lokasi_kedatangan', 'dt_isi_bandsaw.created_date')
            ->union($info)
            ->orderBy('jenis_kayu')->get();








        if (!empty($filters['status']) || !$request->session()->has('filters')) {
            $query->where('hd_lpb.status', '=', true);
        } else {
            $query->where('hd_lpb.status', '=', false);
        }

        $slice = array_slice($query2->toArray(), $paginate * ($page - 1), $paginate);
        $result = new LengthAwarePaginator($slice, count($query2), $paginate);
        // return View::make('produksi/mutation_wip/mutation.addtally');
        // dd(url()->current());
        $result = $result->setPath(url()->current());



        return view('dashboard', [

            "models"         => $result,
            "model"         => $query2,
            "info"         => $info2,
            // "modelk"         => $jumlah2,     

            // "models"         => $query2->paginate(10),
            // "tallyLPBOptions"   => TallyService::getTallyLPB(),
            // "desOptions"   => DesService::getActiveDes(),
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }
}
