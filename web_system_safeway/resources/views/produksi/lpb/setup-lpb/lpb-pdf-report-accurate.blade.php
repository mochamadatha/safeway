<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<br><br><br>
	<table id="filters" width="100%" cellpadding="0" cellspacing="0" style="font-size: 11px" border="1px" align="center">
		<tr bgcolor="#008d95" style="color: white; font-weight: bold;"  >
			<td rowspan="2" width="4%" height="22">No</td>
			<td rowspan="2" width="16%" height="22">jenis kayu</td>
			<td rowspan="2" width="10%" height="22">No. Tally Sheet</td>
			<td colspan="3" width="18%" height="22">Ukuran</td>
			<td colspan="2" width="18%" height="22">Jumlah Diterima</td>
			<td rowspan="2" width="17%" height="22">Harga Satuan</td>
			<td rowspan="2" width="17%" height="22">Total Harga</td>
		</tr>
		<tr bgcolor="#008d95" style="color: white; font-weight: bold; font-size: 11px;" >
			<td height="22">T</td>
			<td height="22">L</td>
			<td height="22">P</td>
			<td height="22">PCS</td>
			<td height="22">m3</td>
		</tr>
		
		<!-- Total Balok -->
		<?php
			$no = 1;
			$total_pcs = 0; 
			$total_volume = 0; 
			$total_harga = 0; 
		?>

		@foreach($total as $view)
		<tr>
			<td>{{ $no++ }}</td>
			<td colspan="2" align="left"> {{ $view->jenis_kayu }}</td>
            <td>{{ $view->tinggi }}</td>
            <td>{{ $view->lebar }}</td>
            <td>{{ $view->panjang }}</td>
            <td>{{ $view->pcs }}</td>
			<td>{{ $view->volume }}</td>
			
			 <?php if ($view->harga_satuan != ''): ?>
			<td width="17%">
				<table>
					<tr>
						<td align="center" width="25%">Rp.</td>


						<td align="right" width="70%">{{ number_format( $view->harga_satuan , 0 , '' , '.' ) . ",-" }}</td>
						<td width="5%"> </td>
					</tr>
				</table>
			</td>
			

			<?php else: ?>


			<td width="17%">
				<table>
					<tr>
						<td align="center" width="25%">Rp.</td>


						<td align="right" width="70%">{{ number_format( $view->harga , 0 , '' , '.' ) . ",-" }}</td>
						<td width="5%"> </td>
					</tr>
				</table>
			</td>
			
			<?php endif ?>
			<td width="17%">
				<table>
					<tr>
						<td align="center" width="25%">Rp.</td>


						<td align="right" width="70%">{{ number_format(  $view->total_harga , 0 , '' , '.' ) . ",-" }}</td>
						<td width="5%"> </td>
					</tr>
				</table>
			</td>
			
			
		</tr>

		<?php 
			$total_pcs += $view->pcs;
			$total_volume += $view->volume;
			if ($view->harga_satuan != '') {
				$total_harga += $view->total_harga;
			} else {
				$total_harga += $view->total_harga;
			}
			
		?>
			
		@endforeach
		<tr>
			<td colspan="6">Total Baik</td>
			<td>{{ $total_pcs }}</td>
			<td>{{ number_format( $total_volume , 4 , ',' , ',' ) }}</td>

			<td align="right" colspan="2">
				<table>
					<tr>
						<td align="center" width="12.5%" >Rp. </td>
						<td align="right" width="85.5%">{{ number_format( $total_harga , 0 , '' , '.' ) . ",-" }}</td>
						<td width="2%"> </td>
					</tr>
				</table>
			</td>		
		</tr>

	</table>

	
</body>
</html>