<?php

namespace App\Service\DivisionContainer;

class InspectService
{
    public static function getActiveInspect()
    {
        return \DB::table('mst_inspect')
            ->where('status', '=', true)
            ->orderBy('nama_inspect')
            ->get();
    }
}
