Cara Instalasi :

1. Git clone
2. composer install (opsional jika langsung copy vendor)
3. Buat database
4. Copy .env.example dan rename jadi .env -> sesuaikan config db sesuai dengan db yang dibuat
5. Copy {file system}/server.php.example dan rename jadi server.php -> setting line 17 
dan 21 sesuai dengan path file untuk public (/../{directory_public})
6. Copy {file public}/index.php.example dan rename jadi index.php -> setting line 24 dan 38 sesuai dengan path file untuk system (/../{directory_system})
7. edit vendor/laravel/framework/src/Illuminate/Foundation/Console/ServeCommand.php line 35 chdir(public_path()); -> chdir('/');
8. Optional => php artisan key:generate
9. jalankan localhost/{path_public} / php artisan serve