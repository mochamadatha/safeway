<?php

namespace App\Http\Controllers\Produksi\Tally;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Produksi\Tally;
use App\Model\Produksi\IsiTally;
use Elibyy\TCPDF\Facades\TCPDF;
use Maatwebsite\Excel\Facades\Excel;
use \Milon\Barcode\DNS1D;
use DB;

class GenerateNoTallyController extends Controller
{
    const URL       = 'produksi/generate-notally';
    const RESOURCE  = 'generate-notally';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        if ($request->isMethod('post')) {
            $request->session()->put('filters', $request->all());
            return redirect(self::URL . '?page=1');
        } elseif (empty($request->get('page'))) {
            $request->session()->forget('filters');
        }


        $filters = $request->session()->get('filters');
        $query = \DB::table('tally_table')
            ->where('tally_table.status', '=', true)
            ->orderBy('tally_table.tally_no', 'desc');

        // $query = DB::table('dt_lpb')
        // ->select ('hd_lpb.nomor_lpb','tally_table.tally_no','tally_table.date_in_kd', 'tally_table.date_out_kd', 'tally_table.date_mutation_wip', 'tally_table.created_date', 'tally_table.status' , 'tally_table.tally_id', 'tally_table.status_mutation_wip')
        //         ->where('tally_table.status', '=', true)

        //         ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
        //          ->join('tally_table', 'tally_table.tally_id', '=', 'dt_lpb.tally_id')
        //           ->groupBy('hd_lpb.nomor_lpb','tally_table.tally_no','tally_table.date_in_kd', 'tally_table.date_out_kd', 'tally_table.date_mutation_wip', 'tally_table.created_date', 'tally_table.status', 'tally_table.tally_id', 'tally_table.status_mutation_wip')
        // ->orderBy('tally_table.tally_no');

        // $query = DB::table('tally_table')
        //     ->select(
        //         'hd_lpb.nomor_lpb',
        //         'tally_table.tally_no',
        //         'tally_table.date_in_kd',
        //         'tally_table.date_out_kd',
        //         'tally_table.date_mutation_wip',
        //         'tally_table.created_date',
        //         'tally_table.status',
        //         'tally_table.tally_id',
        //         'tally_table.status_mutation_wip'
        //     )
        //     ->where('tally_table.status', '=', true)

        //     ->leftJoin('dt_lpb', 'dt_lpb.tally_id', '=', 'tally_table.tally_id')
        //     ->leftJoin('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
        //     ->groupBy('hd_lpb.nomor_lpb', 'tally_table.tally_no', 'tally_table.date_in_kd', 'tally_table.date_out_kd', 'tally_table.date_mutation_wip', 'tally_table.created_date', 'tally_table.status', 'tally_table.tally_id', 'tally_table.status_mutation_wip')
        //     ->orderBy('tally_table.tally_no');

        // dd($query);
        if (!empty($filters['tally_no'])) {
            $query->where('tally_table.tally_no', 'like', '%' . $filters['tally_no'] . '%');
        }

        // if (!empty($filters['status']) || !$request->session()->has('filters')) {
        //     $query->where('tally_table.status', '=', true);
        // } else {
        //     $query->where('tally_table.status', '=', false);
        // }

        // $filters = $request->session()->get('filters');
        // // $query = \DB::table('tally_table')
        // // ->where('tally_table.status', '=', true)
        // //             ->orderBy('tally_table.tally_no');

        // //   $query = DB::table('dt_lpb')
        // // ->select ('hd_lpb.nomor_lpb','tally_table.tally_no','tally_table.date_in_kd', 'tally_table.date_out_kd', 'tally_table.date_mutation_wip', 'tally_table.created_date', 'tally_table.status','tally_table.tally_id' ,'tally_table.status_mutation_wip')
        // //         ->where('tally_table.status', '=', true)

        // //         ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
        // //          ->join('tally_table', 'tally_table.tally_id', '=', 'dt_lpb.tally_id')
        // //           ->groupBy('hd_lpb.nomor_lpb','tally_table.tally_no','tally_table.date_in_kd', 'tally_table.date_out_kd', 'tally_table.date_mutation_wip', 'tally_table.created_date', 'tally_table.status' ,'tally_table.tally_id', 'tally_table.status_mutation_wip')
        // // ->orderBy('tally_table.tally_no');

        // $query = DB::table('tally_table')
        // ->select ('hd_lpb.nomor_lpb','tally_table.tally_no','tally_table.date_in_kd', 'tally_table.date_out_kd', 'tally_table.date_mutation_wip', 'tally_table.created_date', 'tally_table.status','tally_table.tally_id' ,'tally_table.status_mutation_wip')
        //         ->where('tally_table.status', '=', true)
        //         ->leftJoin('dt_lpb', 'dt_lpb.tally_id', '=', 'tally_table.tally_id')
        //          ->leftJoin('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
        //           ->groupBy('hd_lpb.nomor_lpb','tally_table.tally_no','tally_table.date_in_kd', 'tally_table.date_out_kd', 'tally_table.date_mutation_wip', 'tally_table.created_date', 'tally_table.status' ,'tally_table.tally_id', 'tally_table.status_mutation_wip')
        // ->orderBy('tally_table.tally_no');
        // dd($query);



        return view('produksi/tally/generatenotally.index', [
            "models"        => $query->paginate(10),
            "filters"       => $filters,
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function add(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        $model           = new Tally();
        // $model->status   = true;

        return view('produksi/tally/generatenotally.add', [
            "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }

    public function printf(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        // $model           = new Tally();
        // $model->status   = true;

        return view('produksi/tally/generatenotally.print-form', [
            // "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }

    public function Status(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $model = Status::find($id);

        if ($model === null) {
            abort('404');
        }

        return view('produksi/tally/generatenotally.add', [
            "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }

    public function edit(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $model =  Tally::find($id);

        if ($model === null) {
            abort('404');
        }

        return view('produksi/tally/generatenotally.edit', [
            "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }

    public function delete(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $model =  Tally::find($id);

        if ($model === null) {
            abort('404');
        }

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            $model->modified_by   = \Auth::user()->id;
            $model->modified_date = $now;
            $model->status   = false;

            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.delete-message', ['variable' => trans('fields.notally') . ' ' . $id])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.faildel-message', ['variable' => trans('fields.notally') . ' ' . $id])
            );
        }

        return redirect(route('generate-notally-index'));
    }

    public function print(request $request)
    {

        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        $this->validate($request, [
            'first_tally'            => 'required|integer',
            'end_tally'            => 'required|integer',
        ]);

        // $id     = intval($request->get('id', 0));
        $first  = $request->get('first_tally');
        $end    = $request->get('end_tally');
        if ($first > $end) {
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-generate')
            );

            return redirect(route('generate-notally-printf'));
            exit();
        }

        // $model = \DB::table('tally_table')
        //             ->select('tally_table.tally_no')
        //             ->where('tally_table.status', '=', true)
        //             ->whereBetween('tally_table.tally_no', [$first, $end])
        //             ->orderBy('tally_table.tally_no')->get();

        for ($i = $first; $i <= $end; $i++) {
            //  $z[] = $this->complete_zero($i,3).'-'.date("d").$this->convbulan(date("m")).date("y").$model->lokasi_kedatangan_id.$this->complete_zero($model->pallet_name_id,3);
            $z[] = $i;
        }
        $max = count($z);
        $data    = [];
        $dataSem = [];
        foreach ($z as $key => $value) {
            $dataSem[] = $value;
            if (($key + 1) % 3 == 0) {
                $data[] = $dataSem;
                $dataSem = [];
            } elseif (($key + 1) == $max) {
                $data[] = $dataSem;
                $dataSem = [];
            }
        }
        // dd(count($model));
        // dd($data);

        $now    = new \DateTime();
        $now = date_format($now, 'd/m/Y');
        // $model =  HdLpb::find($id);


        // $header = view('produksi/tally/generatenotally.print-pdf', [
        //     'title' => trans('menu.lpb-pdf-report'),
        //     'model' => $data,
        //    // 'isi'   => $isi,
        // ])->render();

        // \PDF::setHeaderCallback(function($pdf) use ($header) {
        //     $pdf->writeHTML($header);
        // });

        // $html = view('produksi/tally/generatenotally/', [
        //   "model"         => $model,
        //   "model"         => $model,
        //   "supplierOptions" => SupplierService::getActiveSupplier(),


        //])->render();

        $pdf = new TCPDF();
        $pdf::SetTitle('Report Afkir');


        // Custom Footer
        // \PDF::setFooterCallback(function($pdf) {

        //         // Position at 15 mm from bottom
        //         $pdf->SetY(-15);
        //         // Set font
        //         $pdf->SetFont('helvetica', 'I', 8);
        //         // Page number
        //         $pdf->Cell(0, 10, 'Page '.$pdf->getAliasNumPage().'/'.$pdf->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');

        // });

        // $pdf::SetTitle('Afkir-');
        //  $resolution= array(187, 143);
        // $resolution= array(143, 187);
        $pdf::SetMargins(1, 1, 1); //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf::AddPage('', array(160,  195));
        $pdf::SetAutoPageBreak(TRUE, 0);



        // $d = new DNS1D();
        // $d->setStorPath(__DIR__."/cache/");
        // echo $d->getBarcodeHTML("9780691147727", "EAN13");
        // exit();

        // $pdf::Cell(24,4,'Nomor :',0,0, 'R');

        $style = array(
            'position' => '',
            'align' => 'C',
            'stretch' => true,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0, 0, 0),
            'bgcolor' => false, //array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 9,
            'stretchtext' => 3,


        );

        $x = 3;
        $y = 2;
        $count = 1;
        $tempbatas = 31;

        foreach ($data as $row) {

            foreach ($row as $m) {
                // $pdf::Cell(50, 20, $m->tally_no, 1, 0);
                $pdf::write1DBarcode($m, 'C39', $x, $y, 50, 18, 0.4, $style, 'N');
                $x += 51;
                $count++;
            }
            if ($count == $tempbatas) {
                $pdf::AddPage('', array(160,  195));
                $x = 3;
                $y = 2;
                $tempbatas += 30;
            } else {
                $x = 3;
                $y += 19;
            }
        }


        // $pdf::Cell(50, 20, 'hh', 1, 0);
        // $pdf::Cell(1, 20, 'hh', 1, 0);
        // $pdf::Cell(50, 20, 'hh', 1, 1);
        // $pdf::write1DBarcode('CODE 39', 'C39', '', '', '', 18, 0.4, $style, 'N');
        // $pdf::write1DBarcode('CODE 39', 'C39', 3, 2, 50, 20, 0.4, $style, 'N');
        // $pdf::write1DBarcode('CODE 39', 'C39', 54, 2, 50, 20, 0.4, $style, 'N');
        // $pdf::write1DBarcode('CODE 39', 'C39', 105, 2, 50, 20, 0.4, $style, 'N');

        // $pdf::write1DBarcode('CODE 39', 'C39', 3, 23, 50, 20, 0.4, $style, 'N');
        // $pdf::write1DBarcode('CODE 39', 'C39', 54, 23, 50, 20, 0.4, $style, 'N');
        // $pdf::write1DBarcode('CODE 39', 'C39', 105, 23, 50, 20, 0.4, $style, 'N');

        // $pdf::write1DBarcode('CODE 39', 'C39', '', '', 50, 20, 0.4, $style, 'N');
        // $pdf::write1DBarcode('CODE 39', 'C39', '', '', 50, 20, 0.4, $style, 'N');
        // $pdf::write1DBarcode('CODE 39', 'C39', '', '', 50, 20, 0.4, $style, 'N');
        // $pdf::write1DBarcode('CODE 39', 'C39', '', '', 50, 20, 0.4, $style, 'N');
        // $pdf::write1DBarcode('1909001 asdasd', 'C39', '', '', 50, 20, 0.4, $style, 'N');
        // $pdf::write1DBarcode('cccc', 'C39', '', '', 50, 20, 0.4, $style, 'N');
        // $pdf::write1DBarcode('cccc', 'C39', '', '', 50, 20, 0.4, $style, 'N');
        // $pdf::write1DBarcode('cccc', 'C39', '', '', 50, 20, 0.4, $style, 'N');
        // $pdf::write1DBarcode('cccc', 'C39', '', '', 50, 20, 0.4, $style, 'N');
        // $pdf::write1DBarcode('cccc', 'C39', '', '', 50, 20, 0.4, $style, 'N');
        // $pdf::write1DBarcode('cccc', 'C39', '', '', 50, 20, 0.4, $style, 'N');
        // $pdf::write1DBarcode('cccc', 'C39', '', '', 50, 20, 0.4, $style, 'N');

        // $pdf::AddPage('', array(  160,  215));
        //  $pdf::AddPage('P', $resolution);
        // $pdf::writeHTML($html, true, false, true, false, '');
        ob_end_clean();

        $pdf::Output('No Tally' . '.pdf');
        //  // $pdf::Output($query->container_number.' '.$query->created_date.'.pdf');

        // $pdf->SetFont('helveticaBI', '', 30);
        // $pdf->SetFont(times, 'BI', 6,'', 'false');
        // $pdf->SetMargins(56.7, 56.7);


    }

    public function exportexcel()
    {

        ob_end_clean();
        ob_start();
        Excel::create('Export data', function ($excel) {

            $excel->sheet('Test', function ($sheet) {



                // $querys = DB::table('dt_isi_tally')
                //     ->select('tally_table.tally_id', 'dt_isi_tally.isi_tally_id', 'tally_table.tally_no', 'hd_lbp.tanggal_kedatangan', 'mst_jenis_kayu.jenis_kayu', 'mst_deskripsi_tallysheet.nama_deskripsi', 'dt_isi_tally.panjang', 'dt_isi_tally.lebar', 'dt_isi_tally.tinggi')
                //     ->where('dt_isi_tally.status', '=', true)
                //     ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
                //     ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_tally.deskripsi_id')
                //     ->join('hd_lpb', 'hd_lpb.tally_id', '=', 'tally_table.tally_id')
                //     ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
                //     ->get();
                //     dd($query);

                $querys = DB::table('dt_lpb')
                    ->select(
                        'dt_lpb.dt_lpb_id',
                        'mst_jenis_kayu.jenis_kayu',
                        'tally_table.tally_no',
                        'tally_table.date_in_kd',
                        'tally_table.date_out_kd',
                        'tally_table.date_mutation_wip',
                        'mst_deskripsi_tallysheet.nama_deskripsi',
                        'mst_lokasi_kedatangan.lokasi_kedatangan',
                        'dt_lpb.tally_id',
                        'dt_isi_tally.isi_tally_id',
                        'dt_isi_tally.tinggi',
                        'dt_isi_tally.lebar',
                        'dt_isi_tally.panjang',
                        'dt_isi_tally.pcs',
                        'dt_isi_tally.volume',
                        'dt_lpb.note',
                        'hd_lpb.tanggal_kedatangan'
                    )
              
                    ->where('dt_lpb.status', '=', true)
                    ->where('dt_isi_tally.status', '=', true)
                    ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
                    ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
                    ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
                    ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_tally.deskripsi_id')

                    ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
                    ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', 'hd_lpb.lokasi_kedatangan_id')
                    ->orderBy('tally_table.tally_no')->get();
                // $querys = DB::table('hd_lpb')
                //     ->select(
                //         'tally_table.tally_no',
                //         'tally_table.date_in_kd',
                //         'hd_lpb.tanggal_kedatangan',
                //         'tally_table.date_out_kd',
                //         'dt_isi_tally.isi_tally_id',
                //         'mst_jenis_kayu.jenis_kayu',
                //         'dt_isi_tally.tinggi',
                //         'dt_isi_tally.lebar',
                //         'dt_isi_tally.panjang',
                //         'dt_isi_tally.pcs',
                //         'dt_isi_tally.volume',
             
                       
                //     )
                //     ->where('tanggal_kedatangan','=', '2020-06-16')
                //     ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')
                //     ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
                //     ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
                //     ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
                //     ->orderBy('hd_lpb.created_by')->get();

                        // dd($querys);
               
                  



                foreach ($querys as $query) {
                    $data[] = array(
     
                        $query->isi_tally_id,
                        $query->tally_no,
                        $query->jenis_kayu,
                        $query->tanggal_kedatangan,
                        $query->date_in_kd,
                        $query->date_out_kd,
                        $query->date_mutation_wip,
                        // $query->nama_deskripsi,
                        $query->panjang,
                        $query->lebar,
                        $query->tinggi,
                        $query->pcs,
                        $query->volume,
                        $query->lokasi_kedatangan

                    );
                }

                $sheet->fromArray($data, null, 'A1', false, false);
                $headings = array('tally id','tally no','jenis kayu', 'tanggal kedatangan','date in kd','date_out_kd','date mutation wip', 'panjang', 'lebar', 'tinggi', 'pcs', 'volume');
                $sheet->prependRow(1, $headings);
            });
        })->export('xlsx');
    }


    public function importexcel(Request $request)
    {

        //if ($request->Request('file')) {
        $model  = new Tally();
        $now    = new \DateTime();



        //$model->modified_date = $now;
        $path = $request->file('file')->getRealPath();
        $data = EXCEL::load($path, function ($reader) { })->get();

        if (!empty($data) && $data->count()) {
            foreach ($data as $key => $value) {
                $modal = new Tally();
                $modal->tally_no = $value->tally_no;
                $modal->created_by = $value->created_by;
                $modal->created_date    =  $value->created_date;
                $modal->save();
            }
        }

        return back();
    }

    public function importexcelisi(Request $request)
    {

        //if ($request->Request('file')) {
        $model  = new IsiTally();
        $now    = new \DateTime();



        //$model->modified_date = $now;
        $path = $request->file('file')->getRealPath();
        $data = EXCEL::load($path, function ($reader) { })->get();

        if (!empty($data) && $data->count()) {
            foreach ($data as $key => $value) {
                $modal = new IsiTally();
                $modal->tally_id = $value->tally_id;
                $modal->jenis_kayu_id = $value->jenis_kayu_id;
                $modal->deskripsi_id = $value->deskripsi_id;
                $modal->tinggi = $value->tinggi;
                $modal->lebar = $value->lebar;
                $modal->panjang = $value->panjang;
                $modal->pcs = $value->pcs;
                $modal->volume = $value->volume;
                $modal->created_by = $value->created_by;
                $modal->created_date    =  $value->created_date;
                $modal->save();
            }
        }

        return back();
    }






    public function saveadd(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        $this->validate($request, [
            'first_tally'            => 'required|integer',
            'end_tally'            => 'required|integer',
        ]);

        // $id     = intval($request->get('id', 0));
        $first  = $request->get('first_tally');
        $end    = $request->get('end_tally');
        if ($first > $end) {
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-generate')
            );

            return redirect(route('generate-notally-add'));
            exit();
        }

        $c = 0;
        $now    = new \DateTime();
        for ($i = $first; $i <= $end; $i++) {
            // if(empty($id)){
            if (DB::table('tally_table')->where('tally_id', $i)->count() == 0) {
                # code...
                $model  = new Tally();
                $model->tally_no        = $i;
                $model->created_by      = \Auth::user()->id;
                $model->created_date    = $now;

                $model->save();
                $c++;
            }
        }

        $request->session()->flash(
            'successMessage',
            trans('message.generate-message', ['variable' => $c . ' ' . trans('fields.notally')])
        );



        return redirect(route('generate-notally-index'));
    }

    public function save(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add']) && !\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $id     = intval($request->get('id', 0));

        $this->validate($request, [
            'tally_no' => 'required|max:255|unique:tally_table,tally_no,' . $id . ',tally_id',
        ]);

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            if (empty($id)) {
                $model  = new Tally();
                $model->created_by      = \Auth::user()->id;
                $model->created_date    = $now;
            } else {
                $model  = Tally::find($id);
                $model->modified_by   = \Auth::user()->id;
                $model->modified_date = $now;
            }

            $model->tally_no   = $request->get('tally_no');
            $model->status   = !empty($request->get('status')) ? true : false;

            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('menu.notally') . ' ' . $request->get('tally_no')])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('menu.notally') . ' ' . $request->get('tally_no')])
            );
        }

        return redirect(route('generate-notally-index'));
    }
}
