<?php

namespace App\Model\MasterHRD;

use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    protected $table        = 'mst_pegawai';
    protected $primaryKey   = 'pegawai_id';
    public $timestamps      = false;
}
