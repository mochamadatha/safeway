@extends('layouts.master')

@section('title', trans('menu.tallysheet'))

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">{{ trans('menu.tallysheet') }}</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="{{ route('dashboard') }}">{{ trans('menu.dashboard') }}</a>
                            </li>
                            <li>
                                <a>{{ trans('menu.produksi') }}</a>
                            </li>
                            <li class="active">
                                {{ trans('menu.tallysheet') }}
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            @if(Session::has('successMessage'))
            <div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-check-all"></i>
                <strong>{{ Session::get('successMessage') }}.</strong>
            </div>
            @endif

            @if(Session::has('errorMessage'))
            <div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-block-helper"></i>
                <strong>{{ Session::get('errorMessage') }}.</strong>
            </div>
            @endif
            
            <div class="row">
                <div class=" col-md-12">
                    <div class="panel panel-border panel-success">
                        <div class="panel-heading">
                    
                            <h3 class="panel-title">No Tally     : {{ $model->tally_no }} </h3> <br>

                        <?php
                          
                            $total_pcs = 0; 
                            $total_volume = 0; 
                            $total_harga = 0; 
                        ?>
                        @foreach($models as $model)
                        <?php 
                            $total_pcs += $model->pcs;
                            $total_volume += $model->volume;
                            
                        ?>
                        @endforeach
                            <h4 class="panel-title">Total Pcs    : {{ $total_pcs }} </h4>
                            <h4 class="panel-title">Total Volume : {{ number_format( $total_volume , 4 , ',' , ',' ) }} </h4>
                        </div>
                        
                        

                        <tr>
                      
                            <td></td>
                            <td></td>
 
                        </tr>


                        <div class="panel-body">
                            <div class="row">
                                <form class="form-horizontal" role="form" method="post" action="{{ route('tallysheet-save') }}">
                                    {{ csrf_field() }}
                                    <div class="col-sm-12 data-table-toolbar text-right">
                                        <div class="form-group">
                                            <button type="button" class="btn btn-primary waves-effect waves-light btn-sm" data-toggle="modal" data-target="#dttally"><i class="fa fa-plus-circle"></i> {{ trans('fields.add-tally-sheet') }} </button>
                                            <a id="clear-details" href="{{ url('produksi/generate-notally') }}"  class="btn btn-sm btn-success">
                                                <i class="fa fa-reply"></i> {{ trans('fields.back') }} 
                                            </a>
                                        </div>
                                        <div class="row">
                                            <table class="table table-bordered table-hover table-striped m-0">
                                                <thead>
                                                    <tr>
                                                        <th width="10%" class="text-center">{{ trans('fields.num') }}</th>
                                                        <th width="17%" class="text-center">{{ trans('fields.wood-type') }}</th>
                                                        <th width="18%" class="text-center">{{ trans('fields.description') }}</th>
                                                        <th width="10%" class="text-center">{{ trans('fields.high') }}</th>
                                                        <th width="10%" class="text-center">{{ trans('fields.width') }}</th>
                                                        <th width="10%" class="text-center">{{ trans('fields.length') }}</th>
                                                        <th width="10%" class="text-center">{{ trans('fields.pcs') }}</th>
                                                        <th width="10%" class="text-center">{{ trans('fields.m3') }}</th>
                                                        <th width="5%" class="text-center">{{ trans('fields.action') }}</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $no = ($models->currentPage() - 1) * $models->perPage() + 1; ?>


                                                    @foreach($models as $model)
                                                    <tr>
                                                        <th scope="row" style="text-align: center;">{{ $no++ }}</th>
                                                       
                                                        <td>{{ $model->jenis_kayu }}</td>
                                                        <td>{{ $model->nama_deskripsi }}</td>
                                                        <td>{{ $model->tinggi }}</td>
                                                        <td>{{ $model->lebar }}</td>
                                                        <td>{{ $model->panjang }}</td>
                                                        <td>{{ $model->pcs }}</td>
                                                        <td>{{ $model->volume }}</td>
                                                        <td class="text-center">
                                                            @can('access', [$resource, 'update'])
                                                            <a data-target="#dttally{{ $model->isi_tally_id }}" data-toggle="modal"  class="btn btn-xs btn-warning" data-original-title="Edit">
                                                                <i class="fa fa-pencil"></i>
                                                            </a>
                                                            @endcan
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            <div class="data-table-toolbar">
                                                {!! $models->render() !!}
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div> 
                        </div>

                        @foreach($models as $model)
                        @can('access', [$resource, 'update'])
                        <div id="dttally{{ $model->isi_tally_id }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form role="form" method="post" action="{{ route('tallysheet-save') }}">
                                        {{ csrf_field() }}
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">{{ trans('fields.add-tally-sheet') }}</h4>
                                        </div>
                                        <div class="modal-body">

                                            <input type="hidden" name="tally_id" class="form-control" value="{{ $model->tally_id }}">
                                            <input type="hidden" name="isi_tally_id" class="form-control" value="{{ count($errors) > 0 ? old('isi_tally_id') : $model->isi_tally_id }}">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="field-1" class="control-label">{{ trans('fields.wood-type') }}</label>
                                                        <select class="form-control" id="jenis_kayu_id" name="jenis_kayu_id">
                                                            <?php $kayuId = count($errors) > 0 ? old('jenis_kayu_id') : $model->jenis_kayu_id; ?>
                                                            <option value="">- {{ trans('fields.change') }} {{ trans('fields.wood-type') }} -</option>
                                                            @foreach($woodOptions as $wood)
                                                            <option value="{{ $wood->jenis_kayu_id }}" {{ $wood->jenis_kayu_id == $kayuId ? 'selected' : '' }}>{{ $wood->jenis_kayu }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="field-2" class="control-label">{{ trans('fields.description') }}</label>
                                                        <select class="form-control" id="deskripsi_id" name="deskripsi_id">
                                                            <?php $deskripsiId = count($errors) > 0 ? old('deskripsi_id') : $model->deskripsi_id; ?>
                                                            <option value="">- {{ trans('fields.change') }} {{ trans('fields.description') }} -</option>
                                                            @foreach($desOptions as $des)
                                                            <option value="{{ $des->deskripsi_id }}" {{ $des->deskripsi_id == $deskripsiId ? 'selected' : '' }}>{{ $des->nama_deskripsi }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="tinggi" class="control-label">{{ trans('fields.high') }}</label>
                                                        <input type="number" class="form-control" id="tinggi" name="tinggi" value="{{ count($errors) > 0 ? old('tinggi') : $model->tinggi }}">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="lebar" class="control-label">{{ trans('fields.width') }}</label>
                                                        <input type="number" class="form-control" id="lebar" name="lebar" value="{{ count($errors) > 0 ? old('lebar') : $model->lebar }}">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="panjang" class="control-label">{{ trans('fields.length') }}</label>
                                                        <input type="number" class="form-control" id="panjang" name="panjang" value="{{ count($errors) > 0 ? old('panjang') : $model->panjang }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="pcs" class="control-label">{{ trans('fields.pcs') }}</label>
                                                        <input type="number" class="form-control" id="pcs" name="pcs" value="{{ count($errors) > 0 ? old('pcs') : $model->pcs }}">
                                                    </div>
                                                </div>
                                                
                                            </div>

                                            
                                            
                                            <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <div class="checkbox checkbox-primary">
                                                        <?php $status = count($errors) > 0 ? old('status') : $model->status; ?>
                                                        <input id="status" name="status" value="true" type="checkbox" {{ $status == true ? 'checked' : '' }}>
                                                        <label for="status">
                                                            {{ trans('fields.is-active') }}
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-info waves-effect waves-light">Save changes</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div><!-- /.modal -->
                        @endcan
                        @endforeach

                        <div id="dttally" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form role="form" method="post" action="{{ route('tallysheet-save') }}">
                                        {{ csrf_field() }}
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">{{ trans('fields.add-tally-sheet') }}</h4>
                                        </div>
                                        <div class="modal-body">
                                      
                                            <input type="hidden" name="tally_id" class="form-control" value="{{ $model->tally_id }}">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="field-1" class="control-label">{{ trans('fields.wood-type') }}</label>
                                                        <select class="form-control" id="jenis_kayu_id" name="jenis_kayu_id">
                                                            <option value="">- {{ trans('fields.change') }} {{ trans('fields.wood-type') }} -</option>
                                                            @foreach($woodOptions as $wood)
                                                            <option value="{{ $wood->jenis_kayu_id }}" {{ !empty($filters['wood']) && $filters['wood'] == $wood->jenis_kayu_id ? 'selected' : ''}} >{{ $wood->jenis_kayu }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="field-2" class="control-label">{{ trans('fields.description') }}</label>
                                                        <select class="form-control" id="deskripsi_id" name="deskripsi_id">
                                                            <option value="">- {{ trans('fields.change') }} {{ trans('fields.description') }} -</option>
                                                            @foreach($desOptions as $des)
                                                            <option value="{{ $des->deskripsi_id }}" {{ !empty($filters['description']) && $filters['description'] == $des->deskripsi_id ? 'selected' : ''}} >{{ $des->nama_deskripsi }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="tinggi" class="control-label">{{ trans('fields.high') }}</label>
                                                        <input type="number" class="form-control" id="tinggi" name="tinggi">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="lebar" class="control-label">{{ trans('fields.width') }}</label>
                                                        <input type="number" class="form-control" id="lebar" name="lebar">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="panjang" class="control-label">{{ trans('fields.length') }}</label>
                                                        <input type="number" class="form-control" id="panjang" name="panjang">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="pcs" class="control-label">{{ trans('fields.pcs') }}</label>
                                                        <input type="number" class="form-control" id="pcs" name="pcs">
                                                    </div>
                                                </div>
                                                
                                            </div>

                                            
                                            <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <div class="checkbox checkbox-primary">
                                                        <?php $status = count($errors) > 0 ? old('status') : $model->status; ?>
                                                        <input id="status" name="status" value="true" type="checkbox" {{ $status == true ? 'checked' : '' }}>
                                                        <label for="status">
                                                            {{ trans('fields.is-active') }}
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div><!-- /.modal -->
                    </div>
                </div>

            </div>


        </div>
    </div>
</div>
@endsection