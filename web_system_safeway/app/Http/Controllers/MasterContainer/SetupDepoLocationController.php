<?php

namespace App\Http\Controllers\MasterContainer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\MasterContainer\DepoLocation;
use DB;

class SetupDepoLocationController extends Controller
{
    const URL       = 'master-container/setup-depolocation';
    const RESOURCE  = 'setup-depolocation';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        if ($request->isMethod('post')) {
            $request->session()->put('filters', $request->all());
            return redirect(self::URL . '?page=1');
        } elseif (empty($request->get('page'))) {
            $request->session()->forget('filters');
        }
        $filters = $request->session()->get('filters');
        $query = \DB::table('mst_depolocation')
            ->orderBy('mst_depolocation.nama_depolocation');

        if (!empty($filters['status']) || !$request->session()->has('filters')) {
            $query->where('mst_depolocation.status', '=', true);
        } else {
            $query->where('mst_depolocation.status', '=', false);
        }

        if (!empty($filters['nama_depolocation'])) {
            $query->where('mst_depolocation.nama_depolocation', 'like', '%' . $filters['nama_depolocation'] . '%');
        }

        return view('master-container/setup-depolocation.index', [
            "models"        => $query->paginate(10),
            "filters"       => $filters,
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function add(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        $model           = new DepoLocation();
        $model->status   = true;

        return view('master-container/setup-depolocation.add', [
            "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }

    public function edit(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $model = DepoLocation::find($id);

        if ($model === null) {
            abort('404');
        }

        return view('master-container/setup-depolocation.add', [
            "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }

    public function delete(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $model =  DepoLocation::find($id);

        if ($model === null) {
            abort('404');
        }

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            $model->modified_by   = \Auth::user()->id;
            $model->modified_date = $now;
            $model->status   = false;

            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.delete-message', ['variable' => trans('fields.name_depolocation') . ' ' . $id])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.faildel-message', ['variable' => trans('fields.name_depolocation') . ' ' . $id])
            );
        }

        return redirect(route('setup-depolocation-index'));
    }

    public function save(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add']) && !\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $id     = intval($request->get('id', 0));

        $this->validate($request, [
            'nama_depolocation' => 'required|max:255|unique:mst_depolocation,nama_depolocation,' . $id . ',depolocation_id',
        ]);

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            if (empty($id)) {
                $model  = new DepoLocation();
                $model->created_by      = \Auth::user()->id;
                $model->created_date    = $now;
            } else {
                $model  = DepoLocation::find($id);
                $model->modified_by   = \Auth::user()->id;
                $model->modified_date = $now;
            }

            $model->nama_depolocation   = $request->get('nama_depolocation');
            $model->status   = !empty($request->get('status')) ? true : false;

            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('menu.name_depolocation') . ' ' . $request->get('name_depolocation')])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('menu.name_depolocation') . ' ' . $request->get('nama_depolocation')])
            );
        }

        return redirect(route('setup-depolocation-index'));
    }
}
