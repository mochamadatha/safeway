<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDtIspminspectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dt_ispminspect', function (Blueprint $table) {
           $table->increments('ispminspect_id');
            $table->integer('sizeispm_id');
            $table->string('container_number')->unique();
            $table->integer('assignment_letter');
            $table->dateTime('inspect_date');
            $table->string('empty');
            $table->string('one_cargo');
            $table->string('half_cargo');
            $table->string('full_cargo');
            $table->string('cargo_label');
            $table->string('one_door_closed');
            $table->string('door_closed');
            $table->string('agent_seal');
            $table->string('customer_seal');
            $table->boolean('status')->default(true);
            $table->integer('created_by');
            $table->dateTime('created_date');
            $table->integer('modified_by')->nullable();
            $table->dateTime('modified_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dt_ispminspect');
    }
}
