<?php

namespace App\Model\MasterIspm;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    protected $table        = 'mst_sizeispm';
    protected $primaryKey   = 'sizeispm_id';
    public $timestamps      = false;
}
