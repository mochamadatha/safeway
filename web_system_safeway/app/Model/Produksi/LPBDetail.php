<?php

namespace App\Model\Produksi;

use Illuminate\Database\Eloquent\Model;

class LPBDetail extends Model
{
    protected $table        = 'dt_lpb';
    protected $primaryKey   = 'dt_lpb_id';
    public $timestamps      = false;
}
