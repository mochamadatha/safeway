<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table        = 'mst_user_status';
    protected $primaryKey   = 'status_id';
    public $timestamps      = false;
}
