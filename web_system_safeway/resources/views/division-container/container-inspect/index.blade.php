@extends('layouts.master')

@section('title', trans('menu.container-inspect'))

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
        	<div class="row">
				<div class="col-xs-12">
					<div class="page-title-box">
                        <h4 class="page-title">{{ trans('menu.container-inspect') }}</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="{{ route('dashboard') }}">{{ trans('menu.dashboard') }}</a>
                            </li>
                            <li>
                                <a>{{ trans('menu.division-container') }}</a>
                            </li>
                            <li class="active">
                                {{ trans('menu.container-inspect') }}
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
				</div>
			</div>
            <!-- end row -->
            @if(Session::has('successMessage'))
            <div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-check-all"></i>
                <strong>{{ Session::get('successMessage') }}.</strong>
            </div>
            @endif

            @if(Session::has('errorMessage'))
            <div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-block-helper"></i>
                <strong>{{ Session::get('errorMessage') }}.</strong>
            </div>
            @endif

			<div class="row">
			    <div class="col-md-12">
			        <div class="panel panel-border panel-success">
			        	<div class="panel-heading">
                        </div>
                        <div class="panel-body">
				            <div class="row">
				                <form class="form-horizontal" role="form" method="post" action="">
				                    {{ csrf_field() }}
				                     <div class="col-md-6">
				                        <div class="form-group {{ $errors->has('container_number') ? 'has-error' : '' }}">
				                            <label class="col-md-4 control-label">{{ trans('fields.container_number') }}</label>
				                            <div class="col-md-8">
				                                <input type="text" name="container_number" class="form-control" value="{{ !empty($filters['container_number']) ? $filters['container_number'] : '' }}">
				                                @if($errors->has('container_number'))
				                                    <span class="help-block">{{ $errors->first('container_number') }}</span>
				                                @endif
				                            </div>
				                        </div>
				                    <!--     <div class="form-group {{ $errors->has('created_date') ? 'has-error' : '' }}">
				                            <label class="col-md-4 control-label">{{ trans('fields.container_number') }}</label>
				                            <div class="col-md-8">
				                                <input type="date" name="created_date" class="form-control" value="{{ !empty($filters['created_date']) ? $filters['created_date'] : '' }}">
				                                @if($errors->has('created_date'))
				                                    <span class="help-block">{{ $errors->first('created_date') }}</span>
				                                @endif
				                            </div>
				                        </div> -->
				                        <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
				                            <div class="col-sm-offset-4 col-sm-8">
				                                <div class="checkbox checkbox-primary">
				                                    <?php $status = !empty($filters['status']) || !Session::has('filters'); ?>
				                                    <input id="status" name="status" value="true" type="checkbox" {{ $status == true ? 'checked' : '' }}>
				                                    <label for="status">
				                                        {{ trans('fields.is-active') }}
				                                    </label>
				                                </div>
				                            </div>
				                        </div>
				                    </div>
				                    <div class="col-md-12">
				                        <p class="text-muted font-14 m-b-20 pull-right">
				                            @can('access', [$resource, 'add'])
				                            <a href="{{ route('container-inspect-add') }}" class="btn btn-primary waves-effect w-md waves-light"><i class="fa fa-plus"></i> {{ trans('fields.add-new') }}</a>
				                            @endcan
				                            <button type="submit" class="btn btn-info waves-effect w-md waves-light"><i class="fa fa-search"></i> {{ trans('fields.search') }}</button>
				                        </p>
				                    </div>
				                </form>
				            </div>
				            <div class="row">
				                <table class="table table-bordered table-hover table-striped m-0">
				                    <thead>
				                        <tr>
				                            <th width="3%" class="text-center">{{ trans('fields.num') }}</th>
				                            <th width="10%" class="text-center">{{ trans('fields.container_number') }}</th>  
				                            <th width="10%" class="text-center">{{ trans('fields.nama_customer') }}</th>
				                            <th width="8%" class="text-center">{{ trans('fields.nama_depolocation') }}</th>
				                            <th width="8%" class="text-center">{{ trans('fields.safeway_sn') }}</th>
				                            <th width="8%" class="text-center">{{ trans('fields.nama_inspect') }}</th>
				                            <th width="10%" class="text-center">{{ trans('fields.dategenerate') }}</th>
				                            <th width="11%" class="text-center">{{ trans('fields.action') }}</th>
				                        </tr>
				                    </thead>
				                    <tbody>
				                        <?php $no = ($models->currentPage() - 1) * $models->perPage() + 1; ?>
				                        @foreach($models as $model)
				                        <tr>
				                            <th scope="row" style="text-align: center;">{{ $no++ }}</th>
				                            <td>{{ $model->container_number }}</td>
				                            <td>{{ $model->nama_customer }}</td>
				                            <td>{{ $model->nama_depolocation }}</td>
				                            <td>{{ $model->safeway_sn }}</td>
				                        	<td>{{ $model->nama_inspect }}</td>
				                        	<td>{{ date(' d-m-Y ', strtotime($model->created_date)) }}</td>
				                            <td class="text-center">
				                            @can('access', [$resource, 'update'])
				                                <a href="{{ route('container-inspect-edit', ['id' => $model->containerinspect_id]) }}" data-toggle="tooltip" class="btn btn-xs btn-warning" data-original-title="Edit">
				                                    <i class="fa fa-pencil"></i>
				                                </a>

				                                <!--<a href="{{ route('container-inspect-edit', ['id' => $model->containerinspect_id]) }}" data-toggle="tooltip" class="btn btn-xs  btn-danger waves-light" data-original-title="remove">
				                                    <i class="fa fa-remove"></i>
				                                </a> -->
				                                

				                                <a href="{{ route('container-inspect-pdf', ['id' => $model->containerinspect_id]) }}" data-toggle="tooltip" class="btn btn-xs  btn-success waves-light" data-original-title="print" target="_blank">
				                                    <i class="fa fa-print"></i>
				                                </a>

				                            @endcan
				                        </tr>
				                        @endforeach
				                    </tbody>
				                </table>
				                <div class="data-table-toolbar">
				                    {!! $models->render() !!}
				                </div>
				            </div>
			            </div>
			        </div>
			    </div>
			</div>
		</div>
	</div>
</div>
@endsection