<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTallyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tally_table', function (Blueprint $table) {
            $table->increments('tally_id');
            $table->integer('chamber_id')->nullable();
            $table->integer('lokasi_outputkd_id')->nullable();
            $table->string('tally_no')->unique();
            $table->dateTime('date_in_kd')->nullable();
            $table->dateTime('date_out_kd')->nullable();
            $table->dateTime('date_mutation_wip')->nullable();
            $table->integer('status_mutation_wip')->nullable();
            $table->boolean('status')->default(true);
            $table->integer('created_by');
            $table->dateTime('created_date');
            $table->integer('modified_by')->nullable();
            $table->dateTime('modified_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('taly_table');
    }
}
