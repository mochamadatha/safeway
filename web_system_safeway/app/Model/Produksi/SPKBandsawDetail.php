<?php

namespace App\Model\Produksi;

use Illuminate\Database\Eloquent\Model;

class SPKBandsawDetail extends Model
{
    protected $table        = 'dt_spk_bandsaw';
    protected $primaryKey   = 'dt_spk_bandsaw_id';
    public $timestamps      = false;
}