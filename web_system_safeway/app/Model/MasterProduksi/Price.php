<?php

namespace App\Model\MasterProduksi;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $table        = 'mst_harga';
    protected $primaryKey   = 'harga_id';
    public $timestamps      = false;
}
