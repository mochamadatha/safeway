@extends('layouts.master')

@section('title', trans('menu.ispm-inspect'))

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="ispm">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">{{ trans('menu.ispm-inspect') }}</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="{{ route('dashboard') }}">{{ trans('menu.dashboard') }}</a>
                            </li>
                            <li>
                                <a>{{ trans('menu.division-ispm') }}</a>
                            </li>
                            <li class="active">
                                {{ trans('menu.ispm-inspect') }}
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-border panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ trans('fields.add-inspect') }}</h3>
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal" role="form" method="post" action="{{ route('ispm-inspect-save') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="hidden" name="id" class="form-control" value="{{ count($errors) > 0 ? old('id') : $model->ispminspect_id }}">

                                        <div class="form-group {{ $errors->has('assignment_letter') ? 'has-error' : '' }}">
                                            <label class="col-md-4 control-label">{{ trans('fields.assignment_letter') }}</label>
                                            <div class="col-md-6"> 
                                                <input type="text" name="assignment_letter" class="form-control" value="{{ count($errors) > 0 ? old('assignment_letter') : $model->assignment_letter }}">
                                                @if($errors->has('assignment_letter'))
                                                <span class="help-block">{{ $errors->first('assignment_letter') }}</span>
                                                    @endif
                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('container_number') ? 'has-error' : '' }}">
                                            <label class="col-md-4 control-label">{{ trans('fields.container_number') }}</label>
                                            <div class="col-md-6"> 
                                                <input type="text" name="container_number" class="form-control" value="{{ count($errors) > 0 ? old('container_number') : $model->container_number }}">
                                                @if($errors->has('container_number'))
                                                <span class="help-block">{{ $errors->first('container_number') }}</span>
                                                    @endif
                                            </div>
                                        </div>

                             

                                   

                           


                                    </div>

                                      <div class="col-md-6">
                                        <input type="hidden" name="id" class="form-control" value="{{ count($errors) > 0 ? old('id') : $model->ispminspect_id }}">
                                        
                             

                                        <div class="form-group {{ $errors->has('size') ? 'has-error' : '' }}">
                                            <label class="col-md-4 control-label">{{ trans('fields.size') }}</label>
                                            <div class="col-md-6">
                                                <select class="form-control" id="size" name="size">
                                                    <?php $sizeId = count($errors) > 0 ? old('size') : $model->sizeispm_id; ?>
                                                        <option value="">{{ trans('fields.please-select') }}</option>
                                                    @foreach($sizeOptions as $div)
                                                        <option value="{{ $div->sizeispm_id }}" {{ $div->sizeispm_id == $sizeId ? 'selected' : '' }}>{{ $div->size }}</option>
                                                    @endforeach
                                                </select>
                                                    @if($errors->has('size'))
                                                        <span class="help-block">{{ $errors->first('size') }}</span>
                                                    @endif
                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('assignment_letter') ? 'has-error' : '' }}">
                                            <label class="col-md-4 control-label">{{ trans('fields.inspect_date') }}</label>   
                                                <div class="col-md-6">
                                                     <input type="date" class="form-control" placeholder="mm/dd/yyyy" id="datepicker-autoclose" name="inspect_date">
                            
                                                </div><!-- input-group -->
                                             
                                        </div>
  
                                    </div>
                                    


                                 <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group {{ $errors->has('empty') ? 'has-error' : '' }}">
                                                    <label class="col-md-4 control-label">{{ trans('fields.empty') }}</label>
                                                    <div class="col-md-6">
                                                        <input type="file" id="empty" name="empty" style="display:none">
                                                        <div class="btn btn-photo well text-center" style="padding: 5px; margin: 0px;">
                                                    @if(!empty($model->empty))
                                                    <img height="150" width="150"src="{{ asset(Config::get('app.paths.foto-ispm').'/'.$model->empty) }}"/><span></span>
                                                    @else
                                                    <img height="150" height="150" hidden/><span>{{ trans('fields.choose-file') }}</span>
                                                    @endif
                                                </div>
                                                @if($errors->has('empty'))
                                                <span class="help-block">{{ $errors->first('empty') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group {{ $errors->has('full_cargo') ? 'has-error' : '' }}">
                                                    <label class="col-md-4 control-label">{{ trans('fields.full_cargo') }}</label>
                                                    <div class="col-md-6">
                                                        <input type="file" id="full_cargo" name="full_cargo" style="display:none">
                                                        <div class="btn btn-photo well text-center" style="padding: 5px; margin: 0px;">
                                                    @if(!empty($model->full_cargo))
                                                    <img height="150"  width="150" src="{{ asset(Config::get('app.paths.foto-ispm').'/'.$model->full_cargo) }}"/><span></span>
                                                    @else
                                                    <img height="150" width="150" hidden/><span>{{ trans('fields.choose-file') }}</span>
                                                    @endif
                                                </div>
                                                @if($errors->has('full_cargo'))
                                                <span class="help-block">{{ $errors->first('full_cargo') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group {{ $errors->has('door_closed') ? 'has-error' : '' }}">
                                                    <label class="col-md-4 control-label">{{ trans('fields.door_closed') }}</label>
                                                    <div class="col-md-6">
                                                        <input type="file" id="door_closed" name="door_closed" style="display:none">
                                                        <div class="btn btn-photo well text-center" style="padding: 5px; margin: 0px;">
                                                    @if(!empty($model->door_closed))
                                                    <img height="150"  width="150" src="{{ asset(Config::get('app.paths.foto-ispm').'/'.$model->door_closed) }}"/><span></span>
                                                    @else
                                                    <img height="150" width="150" hidden/><span>{{ trans('fields.choose-file') }}</span>
                                                    @endif
                                                </div>
                                                @if($errors->has('door_closed'))
                                                <span class="help-block">{{ $errors->first('door_closed') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{ $errors->has('one_cargo') ? 'has-error' : '' }}">
                                                    <label class="col-md-4 control-label">{{ trans('fields.one_cargo') }}</label>
                                                    <div class="col-md-6">
                                                        <input type="file" id="one_cargo" name="one_cargo" style="display:none">
                                                        <div class="btn btn-photo well text-center" style="padding: 5px; margin: 0px;">
                                                    @if(!empty($model->one_cargo))
                                                    <img height="150"  width="150" src="{{ asset(Config::get('app.paths.foto-ispm').'/'.$model->one_cargo) }}"/><span></span>
                                                    @else
                                                    <img height="150" width="150" hidden/><span>{{ trans('fields.choose-file') }}</span>
                                                    @endif
                                                </div>
                                                @if($errors->has('one_cargo'))
                                                <span class="help-block">{{ $errors->first('one_cargo') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group {{ $errors->has('cargo_label') ? 'has-error' : '' }}">
                                                    <label class="col-md-4 control-label">{{ trans('fields.cargo_label') }}</label>
                                                    <div class="col-md-6">
                                                        <input type="file" id="cargo_label" name="cargo_label" style="display:none">
                                                        <div class="btn btn-photo well text-center" style="padding: 5px; margin: 0px;">
                                                    @if(!empty($model->cargo_label))
                                                    <img height="150"  width="150" src="{{ asset(Config::get('app.paths.foto-ispm').'/'.$model->cargo_label) }}"/><span></span>
                                                    @else
                                                    <img height="150" width="150" hidden/><span>{{ trans('fields.choose-file') }}</span>
                                                    @endif
                                                </div>
                                                @if($errors->has('cargo_label'))
                                                <span class="help-block">{{ $errors->first('cargo_label') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group {{ $errors->has('agent_seal') ? 'has-error' : '' }}">
                                                    <label class="col-md-4 control-label">{{ trans('fields.agent_seal') }}</label>
                                                    <div class="col-md-6">
                                                        <input type="file" id="agent_seal" name="agent_seal" style="display:none">
                                                        <div class="btn btn-photo well text-center" style="padding: 5px; margin: 0px;">
                                                    @if(!empty($model->agent_seal))
                                                    <img height="150"  width="150" src="{{ asset(Config::get('app.paths.foto-ispm').'/'.$model->agent_seal) }}"/><span></span>
                                                    @else
                                                    <img height="150" width="150" hidden/><span>{{ trans('fields.choose-file') }}</span>
                                                    @endif
                                                </div>
                                                @if($errors->has('agent_seal'))
                                                <span class="help-block">{{ $errors->first('agent_seal') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        
                                        <div class="form-group {{ $errors->has('half_cargo') ? 'has-error' : '' }}">
                                                    <label class="col-md-4 control-label">{{ trans('fields.half_cargo') }}</label>
                                                    <div class="col-md-6">
                                                        <input type="file" id="half_cargo" name="half_cargo" style="display:none">
                                                        <div class="btn btn-photo well text-center" style="padding: 5px; margin: 0px;">
                                                    @if(!empty($model->half_cargo))
                                                    <img height="150"  width="150" src="{{ asset(Config::get('app.paths.foto-ispm').'/'.$model->half_cargo) }}"/><span></span>
                                                    @else
                                                    <img height="150" width="150" hidden/><span>{{ trans('fields.choose-file') }}</span>
                                                    @endif
                                                </div>
                                                @if($errors->has('half_cargo'))
                                                <span class="help-block">{{ $errors->first('half_cargo') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group {{ $errors->has('one_door_closed') ? 'has-error' : '' }}">
                                                    <label class="col-md-4 control-label">{{ trans('fields.one_door_closed') }}</label>
                                                    <div class="col-md-6">
                                                        <input type="file" id="one_door_closed" name="one_door_closed" style="display:none">
                                                        <div class="btn btn-photo well text-center" style="padding: 5px; margin: 0px;">
                                                    @if(!empty($model->one_door_closed))
                                                    <img height="150"  width="150" src="{{ asset(Config::get('app.paths.foto-ispm').'/'.$model->one_door_closed) }}"/><span></span>
                                                    @else
                                                    <img height="150" width="150" hidden/><span>{{ trans('fields.choose-file') }}</span>
                                                    @endif
                                                </div>
                                                @if($errors->has('one_door_closed'))
                                                <span class="help-block">{{ $errors->first('one_door_closed') }}</span>
                                                @endif

                                            </div>
                                        </div>

                                  

                                        <div class="form-group {{ $errors->has('customer_seal') ? 'has-error' : '' }}">
                                                    <label class="col-md-4 control-label">{{ trans('fields.customer_seal') }}</label>
                                                    <div class="col-md-6">
                                                        <input type="file" id="customer_seal" name="customer_seal" style="display:none">
                                                        <div class="btn btn-photo well text-center" style="padding: 5px; margin: 0px;">
                                                    @if(!empty($model->customer_seal))
                                                    <img height="150"  width="150" src="{{ asset(Config::get('app.paths.foto-ispm').'/'.$model->customer_seal) }}"/><span></span>
                                                    @else
                                                    <img height="150" width="150" hidden/><span>{{ trans('fields.choose-file') }}</span>
                                                    @endif
                                                </div>
                                                @if($errors->has('customer_seal'))
                                                <span class="help-block">{{ $errors->first('customer_seal') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    <div class="col-sm-12 data-table-toolbar text-right">
                                    <div class="form-group">
                                        <a href="{{ url('division-ispm/ispm-inspect') }}" class="btn btn-sm btn-warning"><i class="fa fa-reply"></i> {{ trans('fields.cancel') }}</a>
                                        <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i> {{ trans('fields.save') }}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- end row -->
                    </div> <!-- end card-box -->
                </div><!-- end col -->
            </div>
       </div>
    </div>
</div>
@endsection


@section('script')
@parent
<script type="text/javascript">
$(document).on('ready', function() {
    $('.btn-photo').on('click', function(){
        $(this).parent().find('input[type="file"]').click();
    });

    $("#empty").on('change', function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            var $img    = $(this).parent().find('img');
            var $span   = $(this).parent().find('span');
            reader.onload = function (e) {
                $img.attr('src', e.target.result);
                $img.show();
                $span.hide();
            }
            reader.readAsDataURL(this.files[0]);
        }
    });
    $("#full_cargo").on('change', function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            var $img    = $(this).parent().find('img');
            var $span   = $(this).parent().find('span');
            reader.onload = function (e) {
                $img.attr('src', e.target.result);
                $img.show();
                $span.hide();
            }
            reader.readAsDataURL(this.files[0]);
        }
    });
    $("#door_closed").on('change', function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            var $img    = $(this).parent().find('img');
            var $span   = $(this).parent().find('span');
            reader.onload = function (e) {
                $img.attr('src', e.target.result);
                $img.show();
                $span.hide();
            }
            reader.readAsDataURL(this.files[0]);
        }
    });
    $("#one_cargo").on('change', function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            var $img    = $(this).parent().find('img');
            var $span   = $(this).parent().find('span');
            reader.onload = function (e) {
                $img.attr('src', e.target.result);
                $img.show();
                $span.hide();
            }
            reader.readAsDataURL(this.files[0]);
        }
    });
    $("#cargo_label").on('change', function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            var $img    = $(this).parent().find('img');
            var $span   = $(this).parent().find('span');
            reader.onload = function (e) {
                $img.attr('src', e.target.result);
                $img.show();
                $span.hide();
            }
            reader.readAsDataURL(this.files[0]);
        }
    });
    $("#agent_seal").on('change', function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            var $img    = $(this).parent().find('img');
            var $span   = $(this).parent().find('span');
            reader.onload = function (e) {
                $img.attr('src', e.target.result);
                $img.show();
                $span.hide();
            }
            reader.readAsDataURL(this.files[0]);
        }
    });
    $("#half_cargo").on('change', function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            var $img    = $(this).parent().find('img');
            var $span   = $(this).parent().find('span');
            reader.onload = function (e) {
                $img.attr('src', e.target.result);
                $img.show();
                $span.hide();
            }
            reader.readAsDataURL(this.files[0]);
        }
    });
    $("#one_door_closed").on('change', function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            var $img    = $(this).parent().find('img');
            var $span   = $(this).parent().find('span');
            reader.onload = function (e) {
                $img.attr('src', e.target.result);
                $img.show();
                $span.hide();
            }
            reader.readAsDataURL(this.files[0]);
        }
    });
    $("#customer_seal").on('change', function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            var $img    = $(this).parent().find('img');
            var $span   = $(this).parent().find('span');
            reader.onload = function (e) {
                $img.attr('src', e.target.result);
                $img.show();
                $span.hide();
            }
            reader.readAsDataURL(this.files[0]);
        }
    });
      

});
</script>
@endsection





