<?php

namespace App\Http\Controllers\Produksi\InputKD;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Produksi\LPB;
use App\Model\Produksi\LPBDetail;
use App\Service\Master\TallyService;
use App\Model\MasterProduksi\Supplier;
use App\Service\Master\SupplierService;
use App\Model\Produksi\HdLpb;
use Elibyy\TCPDF\Facades\TCPDF;
use App\Model\Produksi\IsiBandsaw;
use App\Model\Produksi\IsiAfkir;
use App\Service\Master\WoodService;
use App\Service\Master\ChamberService;
use App\Model\MasterProduksi\LokasiKedatangan;
use App\Service\Master\ArrivalLocationService;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class InputKDController extends Controller
{
    const URL       = 'produksi/input-kd';
    const RESOURCE  = 'input-kd';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        if ($request->isMethod('post')) {
            $request->session()->put('filters', $request->all());
            return redirect(self::URL . '?page=1');
        } elseif (empty($request->get('page'))) {
            $request->session()->forget('filters');
        }

        $page = Input::get('page', 1);
        $paginate = 10;


        $info = DB::table('hd_lpb')
            ->select(
                'mst_jenis_kayu.jenis_kayu',
                'mst_chamber.chamber_name',
                
                'dt_isi_tally.tinggi',
                'dt_isi_tally.lebar',
                'dt_isi_tally.panjang',
                DB::raw('SUM(dt_isi_tally.pcs) as pcs'),
                DB::raw('SUM(dt_isi_tally.volume) as volume')
            )
            ->where('hd_lpb.status', '=', true)
            ->where('tally_table.date_in_kd', '!=', NULL)
            ->where('tally_table.date_out_kd', '=', NULL)
            ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')
            ->whereNotIn('dt_lpb.tally_id',  DB::table('dt_isi_bandsaw')
                ->select('tally_id')
                ->groupBy('tally_id'))
            // ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', 'hd_lpb.lokasi_kedatangan_id')
            ->join('dt_lpb', 'dt_lpb.hd_lpb_id', '=', 'hd_lpb.hd_lpb_id')
            ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
            ->join('mst_chamber', 'mst_chamber.chamber_id', 'tally_table.chamber_id')
            ->groupBy('mst_jenis_kayu.jenis_kayu', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 'mst_chamber.chamber_name', 'dt_isi_tally.panjang');


        $info2 = DB::table('dt_isi_bandsaw')
            ->select('mst_jenis_kayu.jenis_kayu', 'mst_chamber.chamber_name', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', DB::raw('SUM(dt_isi_bandsaw.pcs) as pcs'), DB::raw('SUM(dt_isi_bandsaw.volume) as volume'))
            ->where('dt_isi_bandsaw.status', '=', true)
            ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')
            // ->where('tally_table.date_in_kd', '=', null)
            // ->where('dt_isi_bandsaw.tally_id', '=', $id)
            ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')
            ->whereIn('dt_isi_bandsaw.tally_id', DB::table('tally_table')
                ->select('tally_table.tally_id')
                ->where('tally_table.date_in_kd', '!=', NULL)
                ->where('tally_table.date_out_kd', '=', NULL)
                ->join('dt_isi_bandsaw', 'dt_isi_bandsaw.tally_id', '=', 'tally_table.tally_id')
                ->groupBy('tally_id'))
            // ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'dt_isi_bandsaw.tally_id')
            // ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
            // ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=' ,'hd_lpb.lokasi_kedatangan_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')
            // ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')
            ->join('mst_chamber', 'mst_chamber.chamber_id', 'tally_table.chamber_id')
            ->groupBy('mst_jenis_kayu.jenis_kayu', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'mst_chamber.chamber_name')
            ->union($info)
            ->orderBy('jenis_kayu')->get();



        //      $info = DB::table('hd_lpb')
        // ->select('tally_table.tally_no', 'tally_table.date_in_kd', 'mst_jenis_kayu.jenis_kayu', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 'dt_isi_tally.panjang', 'dt_isi_tally.pcs', 'dt_isi_tally.volume', 'mst_lokasi_kedatangan.lokasi_kedatangan', 'dt_isi_tally.created_date', 'mst_chamber.chamber_name')
        //     ->where('hd_lpb.status', '=', true)
        //     ->where('tally_table.date_in_kd', '!=', NULL)
        //     ->where('tally_table.date_out_kd', '=', NULL)
        //     ->whereNotIn('dt_lpb.tally_id',  DB::table('dt_isi_bandsaw')
        //             ->select('tally_id')
        //             ->groupBy('tally_id')) 
        //     ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', 'hd_lpb.lokasi_kedatangan_id')
        //     ->join('dt_lpb', 'dt_lpb.hd_lpb_id', '=', 'hd_lpb.hd_lpb_id')
        //     ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
        //     ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
        //     ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
        //     ->join('mst_chamber', 'mst_chamber.chamber_id', 'tally_table.chamber_id')
        //     ->orderBy('hd_lpb.created_by');

        // $info2 = DB::table('dt_isi_bandsaw')
        // ->select('tally_table.tally_no', 'tally_table.date_in_kd', 'mst_jenis_kayu.jenis_kayu', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'dt_isi_bandsaw.pcs', 'dt_isi_bandsaw.volume', 'mst_lokasi_kedatangan.lokasi_kedatangan', 'dt_isi_bandsaw.created_date', 'mst_chamber.chamber_name')
        //     ->where('dt_isi_bandsaw.status', '=', true)
        //     // ->where('tally_table.date_in_kd', '=', null)
        //     // ->where('dt_isi_bandsaw.tally_id', '=', $id)
        //     ->whereIn('dt_isi_bandsaw.tally_id', DB::table('tally_table')
        //             ->select('tally_table.tally_id')
        //             ->where('tally_table.date_in_kd', '!=', NULL)
        //             ->where('tally_table.date_out_kd', '=', NULL)
        //             ->join('dt_isi_bandsaw','dt_isi_bandsaw.tally_id', '=', 'tally_table.tally_id')
        //             ->groupBy('tally_id'))
        //     ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'dt_isi_bandsaw.tally_id')
        //     ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
        //     ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
        //     ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=' ,'hd_lpb.lokasi_kedatangan_id')
        //     ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')
        //     // ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')
        //     ->join('mst_chamber', 'mst_chamber.chamber_id', 'tally_table.chamber_id')
        //     ->groupBy('tally_table.tally_no', 'tally_table.date_in_kd', 'mst_jenis_kayu.jenis_kayu', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'dt_isi_bandsaw.pcs', 'dt_isi_bandsaw.volume', 'mst_lokasi_kedatangan.lokasi_kedatangan', 'dt_isi_bandsaw.created_date', 'mst_chamber.chamber_name')
        //     ->union($info)
        //     ->orderBy('created_date')->get();




        // dd($ab." ".$bb);




        // dd($info2);
        $filters = $request->session()->get('filters');


   


        $query = DB::table('hd_lpb')
            ->select(
                'tally_table.tally_no',
                'tally_table.date_in_kd',
                'mst_jenis_kayu.jenis_kayu',
                'dt_isi_tally.deskripsi_id',
                'dt_isi_tally.tinggi',
                'dt_isi_tally.lebar',
                'dt_isi_tally.panjang',
                'dt_isi_tally.pcs',
                'dt_isi_tally.volume',
                'mst_lokasi_kedatangan.lokasi_kedatangan',
                'dt_isi_tally.created_date',
                'mst_chamber.chamber_name',
                DB::raw('"Non BS" status_data')
            )
            ->where('hd_lpb.status', '=', true)
            ->where('tally_table.date_in_kd', '!=', NULL)
            ->where('tally_table.date_out_kd', '=', NULL)
            ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')
            ->where('tally_table.tally_no', 'like', '%' . $filters['tally_no'] . '%')
            ->whereNotIn('dt_lpb.tally_id',  DB::table('dt_isi_bandsaw')
                ->select('tally_id')
                ->groupBy('tally_id'))
            ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', 'hd_lpb.lokasi_kedatangan_id')
            ->join('dt_lpb', 'dt_lpb.hd_lpb_id', '=', 'hd_lpb.hd_lpb_id')
            ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
            ->join('mst_chamber', 'mst_chamber.chamber_id', 'tally_table.chamber_id')
            ->orderBy('hd_lpb.created_by');

        $filters = $request->session()->get('filters');
        if (!empty($filters['tally_no'])) {
            $query->where('tally_table.tally_no', 'like', '%' . $filters['tally_no'] . '%');
        }


        $query2 = DB::table('dt_isi_bandsaw')
            ->select(
                'tally_table.tally_no',
                'tally_table.date_in_kd',
                'mst_jenis_kayu.jenis_kayu',
                'dt_isi_bandsaw.deskripsi_id',
                'dt_isi_bandsaw.tinggi',
                'dt_isi_bandsaw.lebar',
                'dt_isi_bandsaw.panjang',
                'dt_isi_bandsaw.pcs',
                'dt_isi_bandsaw.volume',
                'mst_lokasi_kedatangan.lokasi_kedatangan',
                'dt_isi_bandsaw.created_date',
                'mst_chamber.chamber_name',
                DB::raw('"BS" status_data')
            )
            ->where('dt_isi_bandsaw.status', '=', true)
            ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')
            ->where('tally_table.tally_no', 'like', '%' . $filters['tally_no'] . '%')
            // ->where('tally_table.date_in_kd', '=', null)
            // ->where('dt_isi_bandsaw.tally_id', '=', $id)
            ->whereIn('dt_isi_bandsaw.tally_id', DB::table('tally_table')
                ->select('tally_table.tally_id')
                ->where('tally_table.date_in_kd', '!=', NULL)
                ->where('tally_table.date_out_kd', '=', NULL)
                ->join('dt_isi_bandsaw', 'dt_isi_bandsaw.tally_id', '=', 'tally_table.tally_id')
                ->groupBy('tally_id'))
            ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'dt_isi_bandsaw.tally_id')
            ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
            ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=', 'hd_lpb.lokasi_kedatangan_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')
            // ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')
            ->join('mst_chamber', 'mst_chamber.chamber_id', 'tally_table.chamber_id')
            ->groupBy('tally_table.tally_no', 'tally_table.date_in_kd','dt_isi_bandsaw.deskripsi_id', 'mst_jenis_kayu.jenis_kayu', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'dt_isi_bandsaw.pcs', 'dt_isi_bandsaw.volume', 'mst_lokasi_kedatangan.lokasi_kedatangan', 'dt_isi_bandsaw.created_date', 'mst_chamber.chamber_name')
            ->union($query)
            ->orderBy('created_date')->get();

        $t_pcs = 0;
        $t_volume = 0;
        foreach ($query2 as $i) {
            $t_pcs += $i->pcs;
            $t_volume += $i->volume;
        }



        if (!empty($filters['status']) || !$request->session()->has('filters')) {
            $query->where('hd_lpb.status', '=', true);
        } else {
            $query->where('hd_lpb.status', '=', false);
        }

        $slice = array_slice($query2->toArray(), $paginate * ($page - 1), $paginate);
        $result = new LengthAwarePaginator($slice, count($query2), $paginate);
        // return View::make('produksi/mutation_wip/mutation.addtally');
        // dd(url()->current());
        $result = $result->setPath(url()->current());

        return view('produksi/input-kd.index', [

            // "models"         => $query2->paginate(10),
            // "models"         => $query2,
            "models"         => $result,
            "info"         => $info2,
            "ChamberOptions"   => ChamberService::getActiveChamber(),
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
            "t_pcs"      => $t_pcs,
            "t_volume"      => $t_volume,
        ]);
    }


    public function pdfchamber1(Request $request)
    {

        $info = DB::table('hd_lpb')
            ->select(
                'mst_jenis_kayu.jenis_kayu',
                'mst_chamber.chamber_name',
                'tally_table.date_in_kd',
                'dt_isi_tally.tinggi',
                'dt_isi_tally.lebar',
                'dt_isi_tally.panjang',
                DB::raw('SUM(dt_isi_tally.pcs) as pcs'),
                DB::raw('SUM(dt_isi_tally.volume) as volume')
            )
            ->where('mst_chamber.chamber_name', '=', 1)
            ->where('hd_lpb.status', '=', true)
            ->where('tally_table.date_in_kd', '!=', NULL)
            ->where('tally_table.date_out_kd', '=', NULL)
            ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')
            ->whereNotIn('dt_lpb.tally_id',  DB::table('dt_isi_bandsaw')
                ->select('tally_id')
                ->groupBy('tally_id'))
            // ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', 'hd_lpb.lokasi_kedatangan_id')
            ->join('dt_lpb', 'dt_lpb.hd_lpb_id', '=', 'hd_lpb.hd_lpb_id')
            ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
            ->join('mst_chamber', 'mst_chamber.chamber_id', 'tally_table.chamber_id')
            ->groupBy('mst_jenis_kayu.jenis_kayu', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 'mst_chamber.chamber_name', 'dt_isi_tally.panjang', 'tally_table.date_in_kd');


        $info2 = DB::table('dt_isi_bandsaw')
            ->select('mst_jenis_kayu.jenis_kayu', 'mst_chamber.chamber_name', 'tally_table.date_in_kd', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', DB::raw('SUM(dt_isi_bandsaw.pcs) as pcs'), DB::raw('SUM(dt_isi_bandsaw.volume) as volume'))
            ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')
            ->where('mst_chamber.chamber_name', '=', 1)
            ->where('dt_isi_bandsaw.status', '=', true)

            ->whereIn('dt_isi_bandsaw.tally_id', DB::table('tally_table')
                ->select('tally_table.tally_id')
                ->where('tally_table.date_in_kd', '!=', NULL)
                ->where('tally_table.date_out_kd', '=', NULL)
                ->join('dt_isi_bandsaw', 'dt_isi_bandsaw.tally_id', '=', 'tally_table.tally_id')
                ->groupBy('tally_id'))
            // ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'dt_isi_bandsaw.tally_id')
            // ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
            // ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=' ,'hd_lpb.lokasi_kedatangan_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')
            // ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')
            ->join('mst_chamber', 'mst_chamber.chamber_id', 'tally_table.chamber_id')
            ->groupBy('mst_jenis_kayu.jenis_kayu', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'mst_chamber.chamber_name', 'tally_table.date_in_kd')
            ->union($info)
            ->orderBy('jenis_kayu')->get();


        // dd($model);

        $now    = new \DateTime();
        $now = date_format($now, 'd/m/Y h:i:s');
        // $model =  HdLpb::find($id);


        $header = view('produksi/input-kd.header', [
            'title' => trans('menu.lpb-pdf-report') . "#" . $now,

            // "supplierOptions" => SupplierService::getActiveSupplier(),
        ])->render();
        \PDF::setHeaderCallback(function ($pdf) use ($header) {
            $pdf->writeHTML($header);
        });


        // $footer = view('produksi/lpb/setup-lpb.footer-pdf', [
        //     'title' => trans('menu.lpb-pdf-report')."#".$now,
        //     "model"         => $model,
        //     // "supplierOptions" => SupplierService::getActiveSupplier(),
        // ])->render();
        // \PDF::setFooterCallback(function($pdf) use ($footer) {
        //     $pdf->writeHTML($footer);
        // });

        // Custom Footer
        \PDF::setFooterCallback(function ($pdf) {

            // Position at 15 mm from bottom
            $pdf->SetY(-15);
            // Set font
            $pdf->SetFont('helvetica', 'I', 8);
            // Page number
            $pdf->Cell(0, 5, ' ' . $pdf->getAliasNumPage() . '/' . $pdf->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
        });

        $html = view('produksi/input-kd.reportchamber1', [
            "isi"         => $info2,

            "supplierOptions" => SupplierService::getActiveSupplier(),
            "woodOptions"   => WoodService::getActiveWood(),


        ])->render();

        $pdf = new TCPDF();
        $pdf::SetTitle('Data KD' . '1' . $now);
        ob_end_clean();

        $pdf::SetTopMargin(26);



        $pdf::AddPage();
        $pdf::writeHTML($html, true, false, true, false, '');
        $pdf::Output('Data KD' . '1' . '.pdf');
        // $pdf::Output($query->container_number.' '.$query->created_date.'.pdf');
        // $pdf->SetFont('Helvetica', '', 9);



    }

    public function pdfchamber2(Request $request)
    {

        $info = DB::table('hd_lpb')
            ->select(
                'mst_jenis_kayu.jenis_kayu',
                'mst_chamber.chamber_name',
                'tally_table.date_in_kd',
                'dt_isi_tally.tinggi',
                'dt_isi_tally.lebar',
                'dt_isi_tally.panjang',
                DB::raw('SUM(dt_isi_tally.pcs) as pcs'),
                DB::raw('SUM(dt_isi_tally.volume) as volume')
            )
            ->where('mst_chamber.chamber_name', '=', 2)
            ->where('hd_lpb.status', '=', true)
            ->where('tally_table.date_in_kd', '!=', NULL)
            ->where('tally_table.date_out_kd', '=', NULL)
            ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')
            ->whereNotIn('dt_lpb.tally_id',  DB::table('dt_isi_bandsaw')
                ->select('tally_id')
                ->groupBy('tally_id'))
            // ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', 'hd_lpb.lokasi_kedatangan_id')
            ->join('dt_lpb', 'dt_lpb.hd_lpb_id', '=', 'hd_lpb.hd_lpb_id')
            ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
            ->join('mst_chamber', 'mst_chamber.chamber_id', 'tally_table.chamber_id')
            ->groupBy('mst_jenis_kayu.jenis_kayu', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 'mst_chamber.chamber_name', 'dt_isi_tally.panjang', 'tally_table.date_in_kd');


        $info2 = DB::table('dt_isi_bandsaw')
            ->select('mst_jenis_kayu.jenis_kayu', 'mst_chamber.chamber_name', 'tally_table.date_in_kd', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', DB::raw('SUM(dt_isi_bandsaw.pcs) as pcs'), DB::raw('SUM(dt_isi_bandsaw.volume) as volume'))
            ->where('mst_chamber.chamber_name', '=', 2)
            ->where('dt_isi_bandsaw.status', '=', true)
            ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')
            // ->where('tally_table.date_in_kd', '=', null)
            // ->where('dt_isi_bandsaw.tally_id', '=', $id)
            ->whereIn('dt_isi_bandsaw.tally_id', DB::table('tally_table')
                ->select('tally_table.tally_id')
                ->where('tally_table.date_in_kd', '!=', NULL)
                ->where('tally_table.date_out_kd', '=', NULL)
                ->join('dt_isi_bandsaw', 'dt_isi_bandsaw.tally_id', '=', 'tally_table.tally_id')
                ->groupBy('tally_id'))
            // ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'dt_isi_bandsaw.tally_id')
            // ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
            // ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=' ,'hd_lpb.lokasi_kedatangan_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')
            // ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')
            ->join('mst_chamber', 'mst_chamber.chamber_id', 'tally_table.chamber_id')
            ->groupBy('mst_jenis_kayu.jenis_kayu', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'mst_chamber.chamber_name', 'tally_table.date_in_kd')
            ->union($info)
            ->orderBy('jenis_kayu')->get();


        // dd($model);

        $now    = new \DateTime();
        $now = date_format($now, 'd/m/Y h:i:s');
        // $model =  HdLpb::find($id);


        $header = view('produksi/input-kd.header', [
            'title' => trans('menu.lpb-pdf-report') . "#" . $now,

            // "supplierOptions" => SupplierService::getActiveSupplier(),
        ])->render();
        \PDF::setHeaderCallback(function ($pdf) use ($header) {
            $pdf->writeHTML($header);
        });


        // $footer = view('produksi/lpb/setup-lpb.footer-pdf', [
        //     'title' => trans('menu.lpb-pdf-report')."#".$now,
        //     "model"         => $model,
        //     // "supplierOptions" => SupplierService::getActiveSupplier(),
        // ])->render();
        // \PDF::setFooterCallback(function($pdf) use ($footer) {
        //     $pdf->writeHTML($footer);
        // });

        // Custom Footer
        \PDF::setFooterCallback(function ($pdf) {

            // Position at 15 mm from bottom
            $pdf->SetY(-15);
            // Set font
            $pdf->SetFont('helvetica', 'I', 8);
            // Page number
            $pdf->Cell(0, 5, ' ' . $pdf->getAliasNumPage() . '/' . $pdf->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
        });

        $html = view('produksi/input-kd.reportchamber1', [
            "isi"         => $info2,

            "supplierOptions" => SupplierService::getActiveSupplier(),
            "woodOptions"   => WoodService::getActiveWood(),


        ])->render();

        $pdf = new TCPDF();
        $pdf::SetTitle('Data KD' . '1' . $now);
        ob_end_clean();

        $pdf::SetTopMargin(26);



        $pdf::AddPage();
        $pdf::writeHTML($html, true, false, true, false, '');
        $pdf::Output('Data KD' . '1' . '.pdf');
        // $pdf::Output($query->container_number.' '.$query->created_date.'.pdf');
        // $pdf->SetFont('Helvetica', '', 9);



    }

    public function exportdatakd()
    {

        ob_end_clean();
        ob_start();
        Excel::create('Data All In KD  (history)', function ($excel) {

            $excel->sheet('Sheet 1', function ($sheet) {


                $query = DB::table('hd_lpb')
                    ->select(
                        'tally_table.tally_no',
                        'tally_table.date_in_kd',
                        'mst_jenis_kayu.jenis_kayu',
                        'dt_isi_tally.deskripsi_id',
                        'dt_isi_tally.tinggi',
                        'dt_isi_tally.lebar',
                        'dt_isi_tally.panjang',
                        'dt_isi_tally.pcs',
                        'dt_isi_tally.volume',
                        'mst_lokasi_kedatangan.lokasi_kedatangan',
                        'dt_isi_tally.created_date',
                        'mst_chamber.chamber_name',
                        DB::raw('"Non BS" status_data')
                    )
                    ->where('hd_lpb.status', '=', true)
                    ->where('tally_table.date_in_kd', '!=', NULL)
                    ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')

                    ->whereNotIn('dt_lpb.tally_id',  DB::table('dt_isi_bandsaw')
                        ->select('tally_id')
                        ->groupBy('tally_id'))
                    ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', 'hd_lpb.lokasi_kedatangan_id')
                    ->join('dt_lpb', 'dt_lpb.hd_lpb_id', '=', 'hd_lpb.hd_lpb_id')
                    ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
                    ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
                    ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
                    ->join('mst_chamber', 'mst_chamber.chamber_id', 'tally_table.chamber_id')
                    ->orderBy('hd_lpb.created_by');


                $query2 = DB::table('dt_isi_bandsaw')
                    ->select(
                        'tally_table.tally_no',
                        'tally_table.date_in_kd',
                        'mst_jenis_kayu.jenis_kayu',
                        'dt_isi_bandsaw.deskripsi_id',
                        'dt_isi_bandsaw.tinggi',
                        'dt_isi_bandsaw.lebar',
                        'dt_isi_bandsaw.panjang',
                        'dt_isi_bandsaw.pcs',
                        'dt_isi_bandsaw.volume',
                        'mst_lokasi_kedatangan.lokasi_kedatangan',
                        'dt_isi_bandsaw.created_date',
                        'mst_chamber.chamber_name',
                        DB::raw('"BS" status_data')
                    )
                    ->where('dt_isi_bandsaw.status', '=', true)
                    ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')
                    

                    // ->where('tally_table.date_in_kd', '=', null)
                    // ->where('dt_isi_bandsaw.tally_id', '=', $id)
                    ->whereIn('dt_isi_bandsaw.tally_id', DB::table('tally_table')
                        ->select('tally_table.tally_id')
                        ->where('tally_table.date_in_kd', '!=', NULL)
                        ->join('dt_isi_bandsaw', 'dt_isi_bandsaw.tally_id', '=', 'tally_table.tally_id')
                        ->groupBy('tally_id'))
                    ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'dt_isi_bandsaw.tally_id')
                    ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
                    ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
                    ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=', 'hd_lpb.lokasi_kedatangan_id')
                    ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')
                    // ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')
                    ->join('mst_chamber', 'mst_chamber.chamber_id', 'tally_table.chamber_id')
                    ->groupBy('tally_table.tally_no', 'tally_table.date_in_kd','dt_isi_bandsaw.deskripsi_id', 'mst_jenis_kayu.jenis_kayu', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'dt_isi_bandsaw.pcs', 'dt_isi_bandsaw.volume', 'mst_lokasi_kedatangan.lokasi_kedatangan', 'dt_isi_bandsaw.created_date', 'mst_chamber.chamber_name')
                    ->union($query)
                    ->orderBy('created_date')->get();
    
                    
 





                foreach ($query2 as $product) {
                    $data[] = array(
                        // $product->isi_tally_id,
                        $product->tally_no,
                        $product->jenis_kayu,
                        $product->date_in_kd,
                        // $product->date_out_kd,
                        // $product->date_mutation_wip,
                        $product->tinggi,
                        $product->lebar,
                        $product->panjang,
                        $product->pcs,
                        $product->volume,
                        $product->lokasi_kedatangan,
                        // $product->chamber_name,
                        $product->status_data


                    );
                }

                $sheet->fromArray($data, null, 'A1', false, false);
                $headings = array(
                    'Nomor Tally','Jenis Kayu',  'Tanggal Masuk KD', 'Tinggi', 'Lebar', 'Panjang', 'Pcs', 'Volume','Lokasi Kedatangan', 'Status'
                    
                );
                $sheet->prependRow(1, $headings);
            });
        })->export('xlsx');
    }

    public function pdfchamber3(Request $request)
    {

        $info = DB::table('hd_lpb')
            ->select(
                'mst_jenis_kayu.jenis_kayu',
                'mst_chamber.chamber_name',
                'tally_table.date_in_kd',
                'dt_isi_tally.tinggi',
                'dt_isi_tally.lebar',
                'dt_isi_tally.panjang',
                DB::raw('SUM(dt_isi_tally.pcs) as pcs'),
                DB::raw('SUM(dt_isi_tally.volume) as volume')
            )
            ->where('mst_chamber.chamber_name', '=', 3)
            ->where('hd_lpb.status', '=', true)

            ->where('tally_table.date_in_kd', '!=', NULL)
            ->where('tally_table.date_out_kd', '=', NULL)
            ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')
            ->whereNotIn('dt_lpb.tally_id',  DB::table('dt_isi_bandsaw')
                ->select('tally_id')
                ->groupBy('tally_id'))
            // ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', 'hd_lpb.lokasi_kedatangan_id')
            ->join('dt_lpb', 'dt_lpb.hd_lpb_id', '=', 'hd_lpb.hd_lpb_id')
            ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
            ->join('mst_chamber', 'mst_chamber.chamber_id', 'tally_table.chamber_id')
            ->groupBy('mst_jenis_kayu.jenis_kayu', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 'mst_chamber.chamber_name', 'dt_isi_tally.panjang', 'tally_table.date_in_kd');


        $info2 = DB::table('dt_isi_bandsaw')
            ->select('mst_jenis_kayu.jenis_kayu', 'mst_chamber.chamber_name', 'tally_table.date_in_kd', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', DB::raw('SUM(dt_isi_bandsaw.pcs) as pcs'), DB::raw('SUM(dt_isi_bandsaw.volume) as volume'))
            ->where('mst_chamber.chamber_name', '=', 3)
            ->where('dt_isi_bandsaw.status', '=', true)
            // ->where('tally_table.date_in_kd', '=', null)
            // ->where('dt_isi_bandsaw.tally_id', '=', $id)
            ->whereIn('dt_isi_bandsaw.tally_id', DB::table('tally_table')
                ->select('tally_table.tally_id')
                ->where('tally_table.date_in_kd', '!=', NULL)
                ->where('tally_table.date_out_kd', '=', NULL)
                ->join('dt_isi_bandsaw', 'dt_isi_bandsaw.tally_id', '=', 'tally_table.tally_id')
                ->groupBy('tally_id'))
            // ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'dt_isi_bandsaw.tally_id')
            // ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
            // ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=' ,'hd_lpb.lokasi_kedatangan_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')
            // ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')
            ->join('mst_chamber', 'mst_chamber.chamber_id', 'tally_table.chamber_id')
            ->groupBy('mst_jenis_kayu.jenis_kayu', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'mst_chamber.chamber_name', 'tally_table.date_in_kd')
            ->union($info)
            ->orderBy('jenis_kayu')->get();

        // dd($model);

        $now    = new \DateTime();
        $now = date_format($now, 'd/m/Y h:i:s');
        // $model =  HdLpb::find($id);


        $header = view('produksi/input-kd.header', [
            'title' => trans('menu.lpb-pdf-report') . "#" . $now,

            // "supplierOptions" => SupplierService::getActiveSupplier(),
        ])->render();
        \PDF::setHeaderCallback(function ($pdf) use ($header) {
            $pdf->writeHTML($header);
        });


        // $footer = view('produksi/lpb/setup-lpb.footer-pdf', [
        //     'title' => trans('menu.lpb-pdf-report')."#".$now,
        //     "model"         => $model,
        //     // "supplierOptions" => SupplierService::getActiveSupplier(),
        // ])->render();
        // \PDF::setFooterCallback(function($pdf) use ($footer) {
        //     $pdf->writeHTML($footer);
        // });

        // Custom Footer
        \PDF::setFooterCallback(function ($pdf) {

            // Position at 15 mm from bottom
            $pdf->SetY(-15);
            // Set font
            $pdf->SetFont('helvetica', 'I', 8);
            // Page number
            $pdf->Cell(0, 5, ' ' . $pdf->getAliasNumPage() . '/' . $pdf->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
        });

        $html = view('produksi/input-kd.reportchamber1', [
            "isi"         => $info2,

            "supplierOptions" => SupplierService::getActiveSupplier(),
            "woodOptions"   => WoodService::getActiveWood(),


        ])->render();

        $pdf = new TCPDF();
        $pdf::SetTitle('Data KD' . '1' . $now);
        ob_end_clean();

        $pdf::SetTopMargin(26);



        $pdf::AddPage();
        $pdf::writeHTML($html, true, false, true, false, '');
        $pdf::Output('Data KD' . '1' . '.pdf');
        // $pdf::Output($query->container_number.' '.$query->created_date.'.pdf');
        // $pdf->SetFont('Helvetica', '', 9);



    }


    public function exportexcel()
    {

        ob_end_clean();
        ob_start();
        Excel::create('Export data', function ($excel) {

            $excel->sheet('Sheet 1', function ($sheet) {

                //     $query = DB::table('hd_lpb')
                // ->select('tally_table.tally_no', 'tally_table.date_in_kd', 'mst_jenis_kayu.jenis_kayu', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 'dt_isi_tally.panjang', 'dt_isi_tally.pcs', 'dt_isi_tally.volume', 'mst_lokasi_kedatangan.lokasi_kedatangan', 'dt_isi_tally.created_date', 'mst_chamber.chamber_name')
                //     ->where('hd_lpb.status', '=', true)
                //     ->where('tally_table.date_in_kd', '!=', NULL)
                //     ->where('tally_table.date_out_kd', '=', NULL)
                //      ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')
                //     ->whereNotIn('dt_lpb.tally_id',  DB::table('dt_isi_bandsaw')
                //             ->select('tally_id')
                //             ->groupBy('tally_id')) 
                //     ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', 'hd_lpb.lokasi_kedatangan_id')
                //     ->join('dt_lpb', 'dt_lpb.hd_lpb_id', '=', 'hd_lpb.hd_lpb_id')
                //     ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
                //     ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
                //     ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
                //     ->join('mst_chamber', 'mst_chamber.chamber_id', 'tally_table.chamber_id')
                //     ->orderBy('hd_lpb.created_by');

                // $query2 = DB::table('dt_isi_bandsaw')
                // ->select('tally_table.tally_no', 'tally_table.date_in_kd', 'mst_jenis_kayu.jenis_kayu', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'dt_isi_bandsaw.pcs', 'dt_isi_bandsaw.volume', 'mst_lokasi_kedatangan.lokasi_kedatangan', 'dt_isi_bandsaw.created_date', 'mst_chamber.chamber_name')
                //     ->where('dt_isi_bandsaw.status', '=', true)
                //     // ->where('tally_table.date_in_kd', '=', null)
                //     // ->where('dt_isi_bandsaw.tally_id', '=', $id)
                //     ->whereIn('dt_isi_bandsaw.tally_id', DB::table('tally_table')
                //             ->select('tally_table.tally_id')
                //             ->where('tally_table.date_in_kd', '!=', NULL)
                //             ->where('tally_table.date_out_kd', '=', NULL)
                //               ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')
                //             ->join('dt_isi_bandsaw','dt_isi_bandsaw.tally_id', '=', 'tally_table.tally_id')
                //             ->groupBy('tally_id'))
                //     ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'dt_isi_bandsaw.tally_id')
                //     ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
                //     ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
                //     ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=' ,'hd_lpb.lokasi_kedatangan_id')
                //     ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')
                //     // ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')
                //     ->join('mst_chamber', 'mst_chamber.chamber_id', 'tally_table.chamber_id')
                //     ->groupBy('tally_table.tally_no', 'tally_table.date_in_kd', 'mst_jenis_kayu.jenis_kayu', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'dt_isi_bandsaw.pcs', 'dt_isi_bandsaw.volume', 'mst_lokasi_kedatangan.lokasi_kedatangan', 'dt_isi_bandsaw.created_date', 'mst_chamber.chamber_name')
                //     ->union($query)
                //     ->orderBy('created_date')->get();

//asli
                $query = DB::table('hd_lpb')
                    ->select(
                        'tally_table.tally_no',
                        'tally_table.date_in_kd',
                        'mst_jenis_kayu.jenis_kayu',
                        'dt_isi_tally.deskripsi_id',
                        'dt_isi_tally.tinggi',
                        'dt_isi_tally.lebar',
                        'dt_isi_tally.panjang',
                        'dt_isi_tally.pcs',
                        'dt_isi_tally.volume',
                        'mst_lokasi_kedatangan.lokasi_kedatangan',
                        'dt_isi_tally.created_date',
                        'mst_chamber.chamber_name',
                        DB::raw('"Non BS" status_data')
                    )
                    ->where('hd_lpb.status', '=', true)
                    ->where('tally_table.date_in_kd', '!=', NULL)
                    ->where('tally_table.date_out_kd', '=', NULL)
                    ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')

                    ->whereNotIn('dt_lpb.tally_id',  DB::table('dt_isi_bandsaw')
                        ->select('tally_id')
                        ->groupBy('tally_id'))
                    ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', 'hd_lpb.lokasi_kedatangan_id')
                    ->join('dt_lpb', 'dt_lpb.hd_lpb_id', '=', 'hd_lpb.hd_lpb_id')
                    ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
                    ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
                    ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
                    ->join('mst_chamber', 'mst_chamber.chamber_id', 'tally_table.chamber_id')
                    ->orderBy('hd_lpb.created_by');


                $query2 = DB::table('dt_isi_bandsaw')
                    ->select(
                        'tally_table.tally_no',
                        'tally_table.date_in_kd',
                        'mst_jenis_kayu.jenis_kayu',
                        'dt_isi_bandsaw.deskripsi_id',
                        'dt_isi_bandsaw.tinggi',
                        'dt_isi_bandsaw.lebar',
                        'dt_isi_bandsaw.panjang',
                        'dt_isi_bandsaw.pcs',
                        'dt_isi_bandsaw.volume',
                        'mst_lokasi_kedatangan.lokasi_kedatangan',
                        'dt_isi_bandsaw.created_date',
                        'mst_chamber.chamber_name',
                        DB::raw('"BS" status_data')
                    )
                    ->where('dt_isi_bandsaw.status', '=', true)
                    ->where('mst_jenis_kayu.jenis_kayu', '!=', 'Multiplex')
                    

                    // ->where('tally_table.date_in_kd', '=', null)
                    // ->where('dt_isi_bandsaw.tally_id', '=', $id)
                    ->whereIn('dt_isi_bandsaw.tally_id', DB::table('tally_table')
                        ->select('tally_table.tally_id')
                        ->where('tally_table.date_in_kd', '!=', NULL)
                        ->where('tally_table.date_out_kd', '=', NULL)
                        ->join('dt_isi_bandsaw', 'dt_isi_bandsaw.tally_id', '=', 'tally_table.tally_id')
                        ->groupBy('tally_id'))
                    ->join('dt_lpb', 'dt_lpb.tally_id', '=', 'dt_isi_bandsaw.tally_id')
                    ->join('hd_lpb', 'hd_lpb.hd_lpb_id', '=', 'dt_lpb.hd_lpb_id')
                    ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_bandsaw.tally_id')
                    ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=', 'hd_lpb.lokasi_kedatangan_id')
                    ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_bandsaw.jenis_kayu_id')
                    // ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_bandsaw.deskripsi_id')
                    ->join('mst_chamber', 'mst_chamber.chamber_id', 'tally_table.chamber_id')
                    ->groupBy('tally_table.tally_no', 'tally_table.date_in_kd','dt_isi_bandsaw.deskripsi_id', 'mst_jenis_kayu.jenis_kayu', 'dt_isi_bandsaw.tinggi', 'dt_isi_bandsaw.lebar', 'dt_isi_bandsaw.panjang', 'dt_isi_bandsaw.pcs', 'dt_isi_bandsaw.volume', 'mst_lokasi_kedatangan.lokasi_kedatangan', 'dt_isi_bandsaw.created_date', 'mst_chamber.chamber_name')
                    ->union($query)
                    ->orderBy('created_date')->get();


                    // dd($query2);

//test data 
                    // $q = DB::table('tally_table')
                    // ->select(
                    //     'tally_table.tally_no',
                    //     'tally_table.date_in_kd',
                    //     'tally_table.date_out_kd',
                    //     'tally_table.date_mutation_wip',
                    //     'dt_isi_tally.isi_tally_id',
                    //     'mst_jenis_kayu.jenis_kayu',
                    //     'dt_isi_tally.tinggi',
                    //     'dt_isi_tally.lebar',
                    //     'dt_isi_tally.panjang',
                    //     'dt_isi_tally.pcs',
                    //     'dt_isi_tally.volume'
                    // )
                   
                    // ->join('dt_isi_tally', 'dt_isi_tally.tally_id', '=', 'tally_table.tally_id')
                    // ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
                    // ->orderBy('tally_table.created_by')->get();
                    // dd($q);






                foreach ($query2 as $product) {
                    $data[] = array(
                     
                        $product->tally_no,
                        $product->jenis_kayu,
                        $product->date_in_kd,
                        $product->tinggi,
                        $product->lebar,
                        $product->panjang,
                        $product->pcs,
                        $product->volume,
                        $product->lokasi_kedatangan,
                        $product->chamber_name,
                        $product->status_data


                    );
                }

                $sheet->fromArray($data, null, 'A1', false, false);
                $headings = array(
                   'Nomor Tally','Jenis Kayu',  'Tanggal Masuk KD', 'Tinggi', 'Lebar', 'Panjang', 'Pcs', 'Volume', 'Lokasi Kedatangan', 'Chamber', 'Status'
                    
                );
                $sheet->prependRow(1, $headings);
            });
        })->export('xlsx');
    }


    public function save(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        $this->validate($request, [
            'date_in_kd'    => 'required|date',
            'chamber_id'    => 'required|integer',
            'tally_no'      => 'required',
        ]);

        DB::beginTransaction();
        try {

            // $now    = new \DateTime();

            $tally_split = str_replace("\r\n", ",", $request->get('tally_no'));
            $tally_split = explode(",", $tally_split);
            $query = DB::table('tally_table')
                ->whereIn('tally_no', $tally_split)
                ->where('date_in_kd', '=', NULL)
                ->where('date_out_kd', '=', NULL)
                ->update(['chamber_id' => $request->get('chamber_id'), 'date_in_kd' => $request->get('date_in_kd')]);

            //dd($query);

            //$query->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('fields.add-input-kd') . ' ' . ''])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('fields.add-input-kd') . ' ' . ''])
            );
        }

        return redirect(route('input-kd-index'));

        // $query = DB::table('hd_lpb')
        // ->select('hd_lpb.*', 'tally_table.tally_no')
        //         ->join('tally_table', 'tally_table.tally_id', '=', 'hd_lpb.tally_id')
        //      //   ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_lpb.supplier_id')
        // ->orderBy('hd_lpb.created_by');

        // return view('produksi/lpb/setup-lpb.add',[
        //     "model"         => $model,
        //     // "models"         => $query->paginate(10),
        //    // "tallyOptions"   => TallyService::getActiveTally(),
        //     "supplierOptions" => SupplierService::getActiveSupplier(),
        //     "arrivallocationOptions"    => ArrivalLocationService::getActiveArrivalLocation(),
        //     "url"           => self::URL,
        //     "resource"      => self::RESOURCE,
        // ]);

        // $b = htmlspecialchars_decode($request->get('tally_no'));
        // $a = $request->get('tally_no');
        // $text = str_replace(PHP_EOL, ',',$a);
        // $text = explode(",", $text);
        // // echo($text);
        // // $text = "[". $text . "]";
        // $query = DB::table('tally_table')
        //         ->whereIn('tally_no', $text)->get();

        // dd($query);
        // dd($text);
    }
}
