<?php 
// use App\Model\MasterContainer\ContainerInspect;
list($titleheader, $date) = explode("#", $title);
?>

<br/><br/>
<table id="header" width="100%">
    <tr>
        <td width="22%" rowspan="2">
            <img src="{{ asset('assets/images/logo_dark_pdf.png') }}" align="center">
        </td>
        <td colspan="2" width="60%" height="35" align="center" style="font-size: 14px; font-weight: bold;"><p style="font-size: 1px"></p><br>{{ strtoupper($titleheader) }}</td>
    </tr>
    <tr>
        <td colspan="2" width="78%" align="right" style="font-size: 8px;">{{ $date }}</td>
    </tr>
</table>
<hr/>