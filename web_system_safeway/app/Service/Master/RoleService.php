<?php

namespace App\Service\Master;

class RoleService
{
    public static function getActiveRole()
    {
        return \DB::table('mst_role')
            ->where('status', '=', true)
            ->orderBy('nama_role')
            ->get();
    }
}
