@extends('layouts.master')

@section('title', trans('menu.container-inspect'))

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">{{ trans('menu.container-inspect') }}</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="{{ route('dashboard') }}">{{ trans('menu.dashboard') }}</a>
                            </li>
                            <li>
                                <a>{{ trans('menu.division-container') }}</a>
                            </li>
                            <li class="active">
                                {{ trans('menu.container-inspect') }}
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-border panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ trans('fields.add-inspect') }}</h3>
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal" role="form" method="post" action="{{ route('container-inspect-save') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="hidden" name="id" class="form-control" value="{{ count($errors) > 0 ? old('id') : $model->containerinspect_id }}">
                                        <div class="form-group {{ $errors->has('container_number') ? 'has-error' : '' }}">
                                            <label class="col-md-4 control-label">{{ trans('fields.container_number') }}</label>
                                            <div class="col-md-6"> 
                                                <input type="text" name="container_number" class="form-control" value="{{ count($errors) > 0 ? old('container_number') : $model->container_number }}">
                                                @if($errors->has('container_number'))
                                                <span class="help-block">{{ $errors->first('container_number') }}</span>
                                                    @endif
                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('size') ? 'has-error' : '' }}">
                                            <label class="col-md-4 control-label">{{ trans('fields.size') }}</label>
                                            <div class="col-md-6">
                                                <select class="form-control" id="size" name="size">
                                                    <?php $sizeId = count($errors) > 0 ? old('size') : $model->size_id; ?>
                                                        <option value="">{{ trans('fields.please-select') }}</option>
                                                    @foreach($sizeOptions as $div)
                                                        <option value="{{ $div->size_id }}" {{ $div->size_id == $sizeId ? 'selected' : '' }}>{{ $div->size }}</option>
                                                    @endforeach
                                                </select>
                                                    @if($errors->has('size'))
                                                        <span class="help-block">{{ $errors->first('size') }}</span>
                                                    @endif
                                            </div>
                                        </div>



                                         <div class="form-group {{ $errors->has('manufacture_year') ? 'has-error' : '' }}">
                                            <label class="col-md-4 control-label">{{ trans('fields.manufacture_year') }}</label>
                                            <div class="col-md-6">
                                                <input type="text" name="manufacture_year" class="form-control" value="{{ count($errors) > 0 ? old('manufacture_year') : $model->manufacture_year }}">
                                                @if($errors->has('manufacture_year'))
                                                <span class="help-block">{{ $errors->first('manufacture_year') }}</span>
                                                    @endif
                                            </div>
                                        </div>
                                        <div class="form-group {{ $errors->has('previous_cargo') ? 'has-error' : '' }}">
                                            <label class="col-md-4 control-label">{{ trans('fields.previous_cargo') }}</label>
                                            <div class="col-md-6">
                                                <input type="text" name="previous_cargo" class="form-control" value="{{ count($errors) > 0 ? old('previous_cargo') : $model->previous_cargo }}">
                                                @if($errors->has('previous_cargo'))
                                                <span class="help-block">{{ $errors->first('previous_cargo') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group {{ $errors->has('packingin') ? 'has-error' : '' }}">
                                            <label class="col-md-4 control-label">{{ trans('fields.packingin') }}</label>
                                            <div class="col-md-6">
                                                <input type="text" name="packingin" class="form-control" value="{{ count($errors) > 0 ? old('packingin') : $model->packingin }}">
                                                @if($errors->has('packingin'))
                                                <span class="help-block">{{ $errors->first('packingin') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group {{ $errors->has('nama_agent') ? 'has-error' : '' }}">
                                            <label class="col-md-4 control-label">{{ trans('fields.nama_agent') }}</label>
                                            <div class="col-md-6">
                                                <select class="form-control" id="agent" name="agent">
                                                    <?php $agentId = count($errors) > 0 ? old('agent') : $model->size_id; ?>
                                                        <option value="">{{ trans('fields.please-select') }}</option>
                                                    @foreach($agentOptions as $div)
                                                        <option value="{{ $div->agent_id }}" {{ $div->agent_id == $agentId ? 'selected' : '' }}>{{ $div->nama_agent }}</option>
                                                    @endforeach
                                                </select>
                                                    @if($errors->has('agent'))
                                                        <span class="help-block">{{ $errors->first('nama_agent') }}</span>
                                                    @endif
                                            </div>
                                        </div>


                                     
                                        <div class="form-group {{ $errors->has('nama_depolocation') ? 'has-error' : '' }}">
                                            <label class="col-md-4 control-label">{{ trans('fields.nama_depolocation') }}</label>
                                            <div class="col-md-6">
                                                <select class="form-control" id="depolocation" name="depolocation">
                                                    <?php $depolocationId = count($errors) > 0 ? old('depolocation') : $model->depolocation_id; ?>
                                                        <option value="">{{ trans('fields.please-select') }}</option>
                                                    @foreach($depoOptions as $div)
                                                        <option value="{{ $div->depolocation_id }}" {{ $div->depolocation_id == $depolocationId ? 'selected' : '' }}>{{ $div->nama_depolocation }}</option>
                                                    @endforeach
                                                </select>
                                                    @if($errors->has('depolocation'))
                                                        <span class="help-block">{{ $errors->first('nama_depolocation') }}</span>
                                                    @endif
                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('nama_customer') ? 'has-error' : '' }}">
                                            <label class="col-md-4 control-label">{{ trans('fields.nama_customer') }}</label>
                                            <div class="col-md-6">
                                                <select class="form-control" id="customer" name="customer">
                                                    <?php $customerId = count($errors) > 0 ? old('customer') : $model->customer_id; ?>
                                                        <option value="">{{ trans('fields.please-select') }}</option>
                                                    @foreach($customerOptions as $div)
                                                        <option value="{{ $div->customer_id }}" {{ $div->customer_id == $customerId ? 'selected' : '' }}>{{ $div->nama_customer }}</option>
                                                    @endforeach
                                                </select>
                                                    @if($errors->has('customer'))
                                                        <span class="help-block">{{ $errors->first('nama_customer') }}</span>
                                                    @endif
                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('safeway_sn') ? 'has-error' : '' }}">
                                            <label class="col-md-4 control-label">{{ trans('fields.safeway_sn') }}</label>
                                            <div class="col-md-6">
                                                <input type="text" name="safeway_sn" class="form-control" value="{{ count($errors) > 0 ? old('safeway_sn') : $model->safeway_sn }}">
                                                @if($errors->has('safeway_sn'))
                                                <span class="help-block">{{ $errors->first('safeway_sn') }}</span>
                                                @endif
                                            </div>
                                        </div> 
                           

                                        <div class="form-group {{ $errors->has('nama_inspect') ? 'has-error' : '' }}">
                                            <label class="col-md-4 control-label">{{ trans('fields.nama_inspect') }}</label>
                                            <div class="col-md-6">
                                                <select class="form-control" id="inspect" name="inspect">
                                                    <?php $inspectId = count($errors) > 0 ? old('inspect') : $model->inspect_id; ?>
                                                        <option value="">{{ trans('fields.please-select') }}</option>
                                                    @foreach($inspectOptions as $div)
                                                        <option value="{{ $div->inspect_id }}" {{ $div->inspect_id == $inspectId ? 'selected' : '' }}>{{ $div->nama_inspect }}</option>
                                                    @endforeach
                                                </select>
                                                    @if($errors->has('inspect'))
                                                        <span class="help-block">{{ $errors->first('nama_inspect') }}</span>
                                                    @endif
                                            </div>
                                        </div>
                                    </div>


                                 <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group {{ $errors->has('doorclosed') ? 'has-error' : '' }}">
                                                    <label class="col-md-4 control-label">{{ trans('fields.doorclosed') }}</label>
                                                    <div class="col-md-6">
                                                        <input type="file" id="doorclosed" name="doorclosed" style="display:none">
                                                        <div class="btn btn-photo well text-center" style="padding: 5px; margin: 0px;">
                                                    @if(!empty($model->doorclosed))
                                                    <img height="150" width="150"src="{{ asset(Config::get('app.paths.foto-container').'/'.$model->doorclosed) }}"/><span></span>
                                                    @else
                                                    <img height="150" height="150" hidden/><span>{{ trans('fields.choose-file') }}</span>
                                                    @endif
                                                </div>
                                                @if($errors->has('doorclosed'))
                                                <span class="help-block">{{ $errors->first('doorclosed') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group {{ $errors->has('roof') ? 'has-error' : '' }}">
                                                    <label class="col-md-4 control-label">{{ trans('fields.roof') }}</label>
                                                    <div class="col-md-6">
                                                        <input type="file" id="roof" name="roof" style="display:none">
                                                        <div class="btn btn-photo well text-center" style="padding: 5px; margin: 0px;">
                                                    @if(!empty($model->roof))
                                                    <img height="150"  width="150" src="{{ asset(Config::get('app.paths.foto-container').'/'.$model->roof) }}"/><span></span>
                                                    @else
                                                    <img height="150" width="150" hidden/><span>{{ trans('fields.choose-file') }}</span>
                                                    @endif
                                                </div>
                                                @if($errors->has('roof'))
                                                <span class="help-block">{{ $errors->first('roof') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group {{ $errors->has('seal') ? 'has-error' : '' }}">
                                                    <label class="col-md-4 control-label">{{ trans('fields.seal') }}</label>
                                                    <div class="col-md-6">
                                                        <input type="file" id="seal" name="seal" style="display:none">
                                                        <div class="btn btn-photo well text-center" style="padding: 5px; margin: 0px;">
                                                    @if(!empty($model->seal))
                                                    <img height="150"  width="150" src="{{ asset(Config::get('app.paths.foto-container').'/'.$model->seal) }}"/><span></span>
                                                    @else
                                                    <img height="150" width="150" hidden/><span>{{ trans('fields.choose-file') }}</span>
                                                    @endif
                                                </div>
                                                @if($errors->has('seal'))
                                                <span class="help-block">{{ $errors->first('seal') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                       
                                        
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{ $errors->has('allinterior') ? 'has-error' : '' }}">
                                                    <label class="col-md-4 control-label">{{ trans('fields.allinterior') }}</label>
                                                    <div class="col-md-6">
                                                        <input type="file" id="allinterior" name="allinterior" style="display:none">
                                                        <div class="btn btn-photo well text-center" style="padding: 5px; margin: 0px;">
                                                    @if(!empty($model->allinterior))
                                                    <img height="150"  width="150" src="{{ asset(Config::get('app.paths.foto-container').'/'.$model->allinterior) }}"/><span></span>
                                                    @else
                                                    <img height="150" width="150" hidden/><span>{{ trans('fields.choose-file') }}</span>
                                                    @endif
                                                </div>
                                                @if($errors->has('allinterior'))
                                                <span class="help-block">{{ $errors->first('allinterior') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group {{ $errors->has('leftwall') ? 'has-error' : '' }}">
                                                    <label class="col-md-4 control-label">{{ trans('fields.leftwall') }}</label>
                                                    <div class="col-md-6">
                                                        <input type="file" id="leftwall" name="leftwall" style="display:none">
                                                        <div class="btn btn-photo well text-center" style="padding: 5px; margin: 0px;">
                                                    @if(!empty($model->leftwall))
                                                    <img height="150"  width="150" src="{{ asset(Config::get('app.paths.foto-container').'/'.$model->leftwall) }}"/><span></span>
                                                    @else
                                                    <img height="150" width="150" hidden/><span>{{ trans('fields.choose-file') }}</span>
                                                    @endif
                                                </div>
                                                @if($errors->has('leftwall'))
                                                <span class="help-block">{{ $errors->first('leftwall') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                         <div class="form-group {{ $errors->has('cscplate') ? 'has-error' : '' }}">
                                                    <label class="col-md-4 control-label">{{ trans('fields.cscplate') }}</label>
                                                    <div class="col-md-6">
                                                        <input type="file" id="cscplate" name="cscplate" style="display:none">
                                                        <div class="btn btn-photo well text-center" style="padding: 5px; margin: 0px;">
                                                    @if(!empty($model->cscplate))
                                                    <img height="150"  width="150" src="{{ asset(Config::get('app.paths.foto-container').'/'.$model->cscplate) }}"/><span></span>
                                                    @else
                                                    <img height="150" width="150" hidden/><span>{{ trans('fields.choose-file') }}</span>
                                                    @endif
                                                </div>
                                                @if($errors->has('cscplate'))
                                                <span class="help-block">{{ $errors->first('cscplate') }}</span>
                                                @endif

                                            </div>
                                        </div>
                                       
                                    </div>
                                    <div class="col-md-4">

                                        <div class="form-group {{ $errors->has('floor') ? 'has-error' : '' }}">
                                                    <label class="col-md-4 control-label">{{ trans('fields.floor') }}</label>
                                                    <div class="col-md-6">
                                                        <input type="file" id="floor" name="floor" style="display:none">
                                                        <div class="btn btn-photo well text-center" style="padding: 5px; margin: 0px;">
                                                    @if(!empty($model->floor))
                                                    <img height="150"  width="150" src="{{ asset(Config::get('app.paths.foto-container').'/'.$model->floor) }}"/><span></span>
                                                    @else
                                                    <img height="150" width="150" hidden/><span>{{ trans('fields.choose-file') }}</span>
                                                    @endif
                                                </div>
                                                @if($errors->has('floor'))
                                                <span class="help-block">{{ $errors->first('floor') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        
                                    
                                        <div class="form-group {{ $errors->has('rightwall') ? 'has-error' : '' }}">
                                                    <label class="col-md-4 control-label">{{ trans('fields.rightwall') }}</label>
                                                    <div class="col-md-6">
                                                        <input type="file" id="rightwall" name="rightwall" style="display:none">
                                                        <div class="btn btn-photo well text-center" style="padding: 5px; margin: 0px;">
                                                    @if(!empty($model->rightwall))
                                                    <img height="150"  width="150" src="{{ asset(Config::get('app.paths.foto-container').'/'.$model->rightwall) }}"/><span></span>
                                                    @else
                                                    <img height="150" width="150" hidden/><span>{{ trans('fields.choose-file') }}</span>
                                                    @endif
                                                </div>
                                                @if($errors->has('rightwall'))
                                                <span class="help-block">{{ $errors->first('rightwall') }}</span>
                                                @endif

                                            </div>
                                        </div>

                                  

                                        <div class="form-group {{ $errors->has('temperature') ? 'has-error' : '' }}">
                                                    <label class="col-md-4 control-label">{{ trans('fields.temperature') }}</label>
                                                    <div class="col-md-6">
                                                        <input type="file" id="temperature" name="temperature" style="display:none">
                                                        <div class="btn btn-photo well text-center" style="padding: 5px; margin: 0px;">
                                                    @if(!empty($model->temperature))
                                                    <img height="150"  width="150" src="{{ asset(Config::get('app.paths.foto-container').'/'.$model->temperature) }}"/><span></span>
                                                    @else
                                                    <img height="150" width="150" hidden/><span>{{ trans('fields.choose-file') }}</span>
                                                    @endif
                                                </div>
                                                @if($errors->has('temperature'))
                                                <span class="help-block">{{ $errors->first('temperature') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    <div class="col-sm-12 data-table-toolbar text-right">
                                    <div class="form-group">
                                        <a href="{{ url('division-container/container-inspect') }}" class="btn btn-sm btn-warning"><i class="fa fa-reply"></i> {{ trans('fields.cancel') }}</a>
                                        <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i> {{ trans('fields.save') }}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- end row -->
                    </div> <!-- end card-box -->
                </div><!-- end col -->
            </div>
       </div>
    </div>
</div>
@endsection


@section('script')
@parent
<script type="text/javascript">
$(document).on('ready', function() {
    $('.btn-photo').on('click', function(){
        $(this).parent().find('input[type="file"]').click();
    });

    $("#doorclosed").on('change', function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            var $img    = $(this).parent().find('img');
            var $span   = $(this).parent().find('span');
            reader.onload = function (e) {
                $img.attr('src', e.target.result);
                $img.show();
                $span.hide();
            }
            reader.readAsDataURL(this.files[0]);
        }
    });
    $("#allinterior").on('change', function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            var $img    = $(this).parent().find('img');
            var $span   = $(this).parent().find('span');
            reader.onload = function (e) {
                $img.attr('src', e.target.result);
                $img.show();
                $span.hide();
            }
            reader.readAsDataURL(this.files[0]);
        }
    });
    $("#floor").on('change', function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            var $img    = $(this).parent().find('img');
            var $span   = $(this).parent().find('span');
            reader.onload = function (e) {
                $img.attr('src', e.target.result);
                $img.show();
                $span.hide();
            }
            reader.readAsDataURL(this.files[0]);
        }
    });
    $("#roof").on('change', function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            var $img    = $(this).parent().find('img');
            var $span   = $(this).parent().find('span');
            reader.onload = function (e) {
                $img.attr('src', e.target.result);
                $img.show();
                $span.hide();
            }
            reader.readAsDataURL(this.files[0]);
        }
    });
    $("#leftwall").on('change', function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            var $img    = $(this).parent().find('img');
            var $span   = $(this).parent().find('span');
            reader.onload = function (e) {
                $img.attr('src', e.target.result);
                $img.show();
                $span.hide();
            }
            reader.readAsDataURL(this.files[0]);
        }
    });
    $("#rightwall").on('change', function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            var $img    = $(this).parent().find('img');
            var $span   = $(this).parent().find('span');
            reader.onload = function (e) {
                $img.attr('src', e.target.result);
                $img.show();
                $span.hide();
            }
            reader.readAsDataURL(this.files[0]);
        }
    });
    $("#seal").on('change', function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            var $img    = $(this).parent().find('img');
            var $span   = $(this).parent().find('span');
            reader.onload = function (e) {
                $img.attr('src', e.target.result);
                $img.show();
                $span.hide();
            }
            reader.readAsDataURL(this.files[0]);
        }
    });
    $("#cscplate").on('change', function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            var $img    = $(this).parent().find('img');
            var $span   = $(this).parent().find('span');
            reader.onload = function (e) {
                $img.attr('src', e.target.result);
                $img.show();
                $span.hide();
            }
            reader.readAsDataURL(this.files[0]);
        }
    });
    $("#temperature").on('change', function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            var $img    = $(this).parent().find('img');
            var $span   = $(this).parent().find('span');
            reader.onload = function (e) {
                $img.attr('src', e.target.result);
                $img.show();
                $span.hide();
            }
            reader.readAsDataURL(this.files[0]);
        }
    });

});
</script>
@endsection





