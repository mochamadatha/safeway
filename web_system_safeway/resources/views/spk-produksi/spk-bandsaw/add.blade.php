@extends('layouts.master')

@section('title', trans('menu.setup-spk-bandsaw'))

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">{{ trans('menu.setup-spk-bandsaw') }}</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="{{ route('dashboard') }}">{{ trans('menu.dashboard') }}</a>
                            </li>
                            <li>
                                <a>{{ trans('menu.produksi') }}</a>
                            </li>
                            <li class="active">
                                {{ trans('menu.setup-spk-bandsaw') }}
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-border panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ trans('fields.add-spk-bandsaw') }}</h3>
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal" role="form" method="post" action="{{ route('spk-bandsaw-save') }}">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="p-20">
                                            <input type="hidden" name="id" class="form-control" value="{{ count($errors) > 0 ? old('hd_spk_bandsaw_id') : $model->hd_spk_bandsaw_id }}">
                                            
                                             <div class="form-group {{ $errors->has('bandsaw_date') ? 'has-error' : '' }}">
                                                <label class="col-md-2 control-label">{{ trans('fields.bandsaw_date') }}</label>
                                                <div class="col-md-5">
                                                    <input type="date" name="bandsaw_date" class="form-control" value="{{ count($errors) > 0 ? old('bandsaw_date') : $model->bandsaw_date }}">
                                                    @if($errors->has('bandsaw_date'))
                                                        <span class="help-block">{{ $errors->first('bandsaw_date') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-sm-12 data-table-toolbar text-right" style="padding-top: 10px;">
                                        <div class="form-group">
                                            <a href="{{ url('spk-produksi/spk-bandsaw') }}" class="btn btn-sm btn-warning"><i class="fa fa-reply"></i> {{ trans('fields.cancel') }}</a>
                                            <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i> {{ trans('fields.save') }}</button>
                                        </div>
                                    </div> 
                                </div>
                            </form>
                        </div>
                    </div>
                        <!-- end row -->
                </div> <!-- end card-box -->
            </div><!-- end col -->
        </div>
    </div>
</div>
@endsection