<?php

namespace App\Http\Controllers\Produksi\SetupLPB;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Produksi\LPB;
use App\Model\Produksi\LPBDetail;
use App\Service\Master\TallyService;
use App\Model\MasterProduksi\Supplier;
use App\Service\Master\SupplierService;
use App\Model\Produksi\HdLpb;
use Elibyy\TCPDF\Facades\TCPDF;
use App\Model\Produksi\IsiAfkir;
use App\Service\Master\WoodService;
use App\Model\MasterProduksi\LokasiKedatangan;
use App\Service\Master\ArrivalLocationService;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class SetupLPBController extends Controller
{
    const URL       = 'produksi/setup-lpb';
    const RESOURCE  = 'setup-lpb';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'view'])) {
            abort('403');
        }

        if ($request->isMethod('post')) {
            $request->session()->put('filters', $request->all());
            return redirect(self::URL . '?page=1');
        } elseif (empty($request->get('page'))) {
            $request->session()->forget('filters');
        }
        $filters = $request->session()->get('filters');
        $query = \DB::table('hd_lpb')
            ->select('hd_lpb.*', 'mst_supplier.supplier_name', 'mst_lokasi_kedatangan.lokasi_kedatangan_id') //, 'tally_table.tally_no'
            ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_lpb.supplier_id')
            ->join('mst_lokasi_kedatangan', 'mst_lokasi_kedatangan.lokasi_kedatangan_id', '=', 'hd_lpb.lokasi_kedatangan_id')
            //   ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_lpb.supplier_id')
            ->orderBy('hd_lpb.tanggal_kedatangan', 'DESC');

        // dd($query);

        if (!empty($filters['nomor_lpb'])) {
            $query->where('hd_lpb.nomor_lpb', 'like', '%' . $filters['nomor_lpb'] . '%');
        }

        if (!empty($filters['nomor_sj'])) {
            $query->where('hd_lpb.nomor_sj', 'like', '%' . $filters['nomor_sj'] . '%');
        }



        return view('produksi/lpb/setup-lpb.index', [
            "models"        => $query->paginate(10),
            "filters"       => $filters,
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function add(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        $model           = new LPB();
        $model->status   = true;

        if ($model === null) {
            abort('404');
        }

        // $query = DB::table('hd_lpb')
        // ->select('hd_lpb.*', 'tally_table.tally_no')
        //         ->join('tally_table', 'tally_table.tally_id', '=', 'hd_lpb.tally_id')
        //      //   ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_lpb.supplier_id')
        // ->orderBy('hd_lpb.created_by');

        return view('produksi/lpb/setup-lpb.add', [
            "model"         => $model,
            // "models"         => $query->paginate(10),
            // "tallyOptions"   => TallyService::getActiveTally(),
            "supplierOptions" => SupplierService::getActiveSupplier(),
            "arrivallocationOptions"    => ArrivalLocationService::getActiveArrivalLocation(),
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function addtally(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }

        // $model = LPB::find($id);
        $model = DB::table('hd_lpb')
            ->select('hd_lpb.*', 'mst_supplier.supplier_name')
            ->where('hd_lpb.hd_lpb_id', '=', $id)
            ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_lpb.supplier_id')->first();

        $rekap = DB::table('dt_lpb')
            ->select('tally_table.tally_no', DB::raw('COUNT(tally_table.tally_no) as tally'))
            ->where('hd_lpb_id', '=', $id)
            ->where('dt_lpb.status', '=', true)


            ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
            ->groupBy('tally_table.tally_no')
            ->orderBy('tally_table.tally_no');


        $re = DB::table('dt_lpb')
            ->select(DB::raw('COUNT(tally_table.tally_no) as tally'))
            ->where('hd_lpb_id', '=', $id)
            ->where('dt_lpb.status', '=', true)

            ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
            ->groupBy('tally_table.tally_no')
            ->first();

        // dd($re);
        if ($model === null) {
            abort('404');
        }

        $query = DB::table('dt_lpb')
            ->select('dt_lpb.dt_lpb_id', 'mst_jenis_kayu.jenis_kayu', 'tally_table.tally_no', 'mst_deskripsi_tallysheet.nama_deskripsi', 'dt_lpb.tally_id', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 'dt_isi_tally.panjang', 'dt_isi_tally.pcs', 'dt_isi_tally.volume', 'mst_harga.harga', 'dt_lpb.total_harga', 'dt_lpb.harga_satuan', 'dt_lpb.note', 'dt_lpb.total_harga_edit')
            ->where('hd_lpb_id', '=', $id)
            ->where('dt_lpb.status', '=', true)
            ->where('dt_isi_tally.status', '=', true)
            ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
            ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_tally.deskripsi_id')
            ->join('mst_harga', 'mst_harga.harga_id', '=', 'dt_lpb.harga_id')
            ->orderBy('tally_table.tally_no');

        // dd($query);

        return view('produksi/lpb/setup-lpb.addtally', [
            "model"         => $model,
            "rekap"         => $rekap->paginate(10),
            "re"         => $re,
            "models"         => $query->paginate(10),
            "tallyOptions"   => TallyService::getActiveTally(),
            // "desOptions"   => DesService::getActiveDes(),
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }

    public function destroy(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }
//  dd($id);
        // $isi = LPBDetail::;

        $isi = DB::table('dt_lpb')
        ->select('dt_lpb.*')
        ->where('dt_lpb.tally_id', '=', $id);

        $direct= DB::table('dt_lpb')
        ->select('dt_lpb.hd_lpb_id')
        ->where('dt_lpb.tally_id', '=', $id)->first();
        // dd($dt);
        $dt = $direct->hd_lpb_id;
        // dd($dt);
// dd($isi->hd_lpb_id);
        if ($isi === null) {
            abort('404');
        }
        // dd($isi->);

        // $isi =  LPBDetail::where('dt_lpb_id',$id)->delete();



        DB::beginTransaction();
        try {

            $isi->delete();
            DB::commit();



            $request->session()->flash(
                'successMessage',
                trans('message.delete-message', ['variable' => trans('fields.notally') . ' ' . $id])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.faildel-message', ['variable' => trans('fields.notally') . ' ' . $id])
            );
        }

        return redirect(route('tally-lpb-add', ['id' => $dt]));
    }

    public function importexcel(Request $request)
    {

        //if ($request->Request('file')) {
        $model  = new HdLpb();
        $now    = new \DateTime();



        //$model->modified_date = $now;
        $path = $request->file('file')->getRealPath();
        $data = EXCEL::load($path, function ($reader) { })->get();

        if (!empty($data) && $data->count()) {
            foreach ($data as $key => $value) {
                $modal = new HdLpb();
                $modal->supplier_id = $value->supplier_id;
                $modal->lokasi_kedatangan_id = $value->lokasi_kedatangan_id;
                $modal->nomor_lpb = $value->nomor_lpb;
                $modal->nomor_po = $value->nomor_po;
                $modal->nomor_sj = $value->nomor_sj;
                $modal->tanggal_kedatangan = $value->tanggal_kedatangan;
                $modal->plat_mobil = $value->plat_mobil;
                $modal->created_by = $value->created_by;
                $modal->created_date    =  $value->created_date;
                $modal->save();
            }
        }

        return back();
    }

    public function importexcelisi(Request $request)
    {

        //if ($request->Request('file')) {
        $model  = new LPBDetail();
        $now    = new \DateTime();



        //$model->modified_date = $now;
        $path = $request->file('file')->getRealPath();
        $data = EXCEL::load($path, function ($reader) { })->get();

        if (!empty($data) && $data->count()) {
            foreach ($data as $key => $value) {
                $modal = new LPBDetail();
                $modal->hd_lpb_id = $value->hd_lpb_id;
                $modal->tally_id = $value->tally_id;
                $modal->isi_tally_id = $value->isi_tally_id;
                $modal->harga_id = $value->harga_id;
                $modal->harga_satuan = $value->harga_satuan;
                $modal->total_harga = $value->total_harga;
                $modal->status = $value->status;
                $modal->created_by = $value->created_by;
                $modal->created_date    =  $value->created_date;
                $modal->save();
            }
        }

        return back();
    }

    public function saveeditprice(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $id     = intval($request->get('dt_lpb_id', 0));

        $this->validate($request, [
            'harga'            => 'required|integer',
            'pembayaran'       => 'required',
            'note'             => 'required|string',

        ]);

        // dd($id);

        DB::beginTransaction();
        try {

            $data = DB::table('dt_lpb')
                ->select('dt_isi_tally.pcs', 'dt_isi_tally.volume')
                ->where('dt_lpb.dt_lpb_id', '=', $id)
                ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')->first();

            // dd($data);

            $now    = new \DateTime();
            if (empty($id)) {
                $model  = new LPBDetail();
                $model->created_by      = \Auth::user()->id;
                $model->created_date    = $now;
            } else {
                $model  = LPBDetail::find($id);
                $model->modified_by   = \Auth::user()->id;
                $model->modified_date = $now;
                $model->harga_satuan = $request->get('harga');
                $model->note = $request->get('note');
            }
            if ($request->get('pembayaran') == 0) {
                $model->total_harga        = $data->volume * $model->harga_satuan;
            } elseif ($request->get('pembayaran') == 1) {
                $model->total_harga     = $data->pcs * $model->harga_satuan;
            }
            // dd ($model);

            // $model->total_harga   = $request->get('total_harga');

            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => 'Data'])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => 'Data'])
            );
        }

        return redirect(route('tally-lpb-add', ['id' => $model->hd_lpb_id]));
    }



    public function saveaddtally(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add']) && !\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $this->validate($request, [
            'tally_id'            => 'required|integer',
            'hd_lpb_id'           => 'required|integer',
            'supplier_id'         => 'required|integer',
        ]);

        // dd($request->get('pembayaran'));

        $tally_id       = $request->get('tally_id');
        $hd_lpb_id      = $request->get('hd_lpb_id');
        $supplier_id    = $request->get('supplier_id');

        DB::beginTransaction();
        try {

            $query = DB::table('dt_isi_tally')
                ->select('tally_id', 'jenis_kayu_id', 'deskripsi_id', 'isi_tally_id', 'volume', 'pcs')
                ->where('tally_id', '=', $tally_id)
                ->orderBy('created_by')->get();

            // dd($query);

            foreach ($query as $key) {
                $price = DB::table('mst_harga')
                    ->select('harga_id', 'harga')
                    ->where('supplier_id', '=', $supplier_id)
                    ->where('jenis_kayu_id', '=', $key->jenis_kayu_id)
                    ->where('deskripsi_id', '=', $key->deskripsi_id)->first();
                // echo $hd_lpb_id." tally id ". $key->tally_id ." isi ".$key->isi_tally_id ." price ".$price->harga_id."<br>";}
                $now    = new \DateTime();
                // if(empty($id)){
                $model  = new LPBDetail();
                $model->created_by      = \Auth::user()->id;
                $model->created_date    = $now;
                // }else{
                //     $model  = LPBDetail::find($id);
                //     $model->modified_by   = \Auth::user()->id;
                //     $model->modified_date = $now;
                // }

                $model->hd_lpb_id       = $hd_lpb_id;
                $model->tally_id        = $tally_id;
                $model->isi_tally_id    = $key->isi_tally_id;
                $model->harga_id        = $price->harga_id;
                // $model->total_harga        = $key->volume*$price->harga;
                if ($request->get('pembayaran') == 0) {
                    $model->total_harga        = $key->volume * $price->harga;
                } elseif ($request->get('pembayaran') == 1) {
                    $model->total_harga        = $key->pcs * $price->harga;
                }
                // dd($key->volume." ".$key->pcs." ".$price->harga);

                $model->status          = TRUE;

                // Save untuk add tally


                $model->save();
            }



            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('fields.add-tally-sheet') . ' ' . $tally_id])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('fields.add-tally-sheet') . ' ' . $tally_id])
            );
        }

        return redirect(route('tally-lpb-add', ['id' => $hd_lpb_id]));
    }

    public function reportlpbexcel(Request $request, $id)
    {

        $this->id = $id;
        ob_end_clean();
        ob_start();
        Excel::create('Export data', function ($excel) {

            $excel->sheet('Sheet 1', function ($sheet) {

                $model = \DB::table('hd_lpb')
                    // ->select('hd_lpb.*', 'mst_supplier.supplier_name', 'mst_supplier.pic')
                    ->select('hd_lpb.hd_lpb_id')
                    ->where('hd_lpb.hd_lpb_id', '=', $this->id)
                    ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_lpb.supplier_id')
                    ->orderBy('hd_lpb.hd_lpb_id')->get();

                // dd($model);

                // $model = \DB::table('hd_lpb')
                //     ->select('hd_lpb.*', 'mst_supplier.supplier_name', 'mst_supplier.pic')
                //     ->where('hd_lpb.hd_lpb_id', '=', $this->id)
                //     ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_lpb.supplier_id')
                //     ->orderBy('hd_lpb.hd_lpb_id')->first();

                $isi = DB::table('dt_lpb')
                    ->select(
                        'dt_lpb.dt_lpb_id',
                        'mst_jenis_kayu.jenis_kayu',
                        'tally_table.tally_no',
                        'mst_deskripsi_tallysheet.nama_deskripsi',
                        'dt_lpb.tally_id',
                        'dt_isi_tally.tinggi',
                        'dt_isi_tally.lebar',
                        'dt_isi_tally.panjang',
                        'dt_isi_tally.pcs',
                        'dt_isi_tally.volume',
                        'mst_harga.harga',
                        'dt_lpb.total_harga',
                        'dt_lpb.harga_satuan',
                        'dt_lpb.note',
                        'dt_lpb.total_harga_edit'
                    )
                    ->where('hd_lpb_id', '=', $this->id)
                    ->where('dt_lpb.status', '=', true)
                    ->where('dt_isi_tally.status', '=', true)
                    ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
                    ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
                    ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
                    ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_tally.deskripsi_id')
                    ->join('mst_harga', 'mst_harga.harga_id', '=', 'dt_lpb.harga_id')
                    ->orderBy('tally_table.tally_no')->get();

                foreach ($model as $product) {
                    foreach ($isi as $isi) {


                        $data[] = array(
                            // $product->,
                            $isi->jenis_kayu,
                            $isi->nama_deskripsi,
                            $isi->tally_no,
                            $isi->tinggi,
                            $isi->lebar,
                            $isi->panjang,
                            $isi->pcs,
                            $isi->volume,
                            $isi->harga,

                        );
                    }
                }
                $sheet->fromArray($data, null, 'A1', false, false);
                $headings = array('jenis_kayu', 'nama_deskripsi', 'tally_no', 'tinggi', 'lebar', 'panjang', 'pcs', 'volume', 'Satuan Harga');
                $sheet->prependRow(1, $headings);
            });


            $query = DB::table('dt_isi_afkir')
                ->select('dt_isi_afkir.*', 'mst_jenis_kayu.jenis_kayu')
                ->where('hd_lpb_id', '=', $this->id)
                ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_afkir.jenis_kayu_id')
                ->orderBy('dt_isi_afkir.created_by')->get();




            $excel->sheet('Data Afkir LPB', function ($sheet) {

                $model = \DB::table('hd_lpb')
                    // ->select('hd_lpb.*', 'mst_supplier.supplier_name', 'mst_supplier.pic')
                    ->select('hd_lpb.hd_lpb_id')
                    ->where('hd_lpb.hd_lpb_id', '=', $this->id)
                    ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_lpb.supplier_id')
                    ->orderBy('hd_lpb.hd_lpb_id')->get();

                // dd($model);

                // $model = \DB::table('hd_lpb')
                //     ->select('hd_lpb.*', 'mst_supplier.supplier_name', 'mst_supplier.pic')
                //     ->where('hd_lpb.hd_lpb_id', '=', $this->id)
                //     ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_lpb.supplier_id')
                //     ->orderBy('hd_lpb.hd_lpb_id')->first();



                $query = DB::table('dt_isi_afkir')
                    ->select('dt_isi_afkir.*', 'mst_jenis_kayu.jenis_kayu')
                    ->where('hd_lpb_id', '=', $this->id)
                    ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_afkir.jenis_kayu_id')
                    ->orderBy('dt_isi_afkir.created_by')->get();

                foreach ($model as $product) {
                    foreach ($query as $isi) {
                        $data[] = array(
                            // $product->,
                            $isi->jenis_kayu,
                            $isi->tinggi,
                            $isi->lebar,
                            $isi->panjang,
                            $isi->pcs,
                            $isi->volume,





                        );
                    }
                }




                $sheet->fromArray($data, null, 'A1', false, false);
                $headings = array('jenis_kayu', 'tinggi', 'lebar', 'panjang', 'pcs', 'volume');
                $sheet->prependRow(1, $headings);
            });
        })->export('xlsx');
    }




    public function reportlpbpdf(Request $request, $id)
    {

        $model = \DB::table('hd_lpb')
            ->select('hd_lpb.*', 'mst_supplier.supplier_name', 'mst_supplier.pic')
            ->where('hd_lpb.hd_lpb_id', '=', $id)
            ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_lpb.supplier_id')
            ->orderBy('hd_lpb.hd_lpb_id')->first();



        $isi = DB::table('dt_lpb')
            ->select('dt_lpb.dt_lpb_id', 'mst_jenis_kayu.jenis_kayu', 'tally_table.tally_no', 'mst_deskripsi_tallysheet.nama_deskripsi', 'dt_lpb.tally_id', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 'dt_isi_tally.panjang', 'dt_isi_tally.pcs', 'dt_isi_tally.volume', 'mst_harga.harga', 'dt_lpb.total_harga', 'dt_lpb.harga_satuan', 'dt_lpb.note', 'dt_lpb.total_harga_edit')
            ->where('hd_lpb_id', '=', $id)
            ->where('dt_lpb.status', '=', true)
            ->where('dt_isi_tally.status', '=', true)
            ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
            ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_tally.deskripsi_id')
            ->join('mst_harga', 'mst_harga.harga_id', '=', 'dt_lpb.harga_id')
            ->orderBy('dt_isi_tally.tinggi')->get();

// dd($isi);
    

        $jumlah = DB::table('dt_lpb')
            ->select( 'mst_jenis_kayu.jenis_kayu',  'mst_deskripsi_tallysheet.nama_deskripsi', DB::raw('sum(dt_isi_tally.pcs) as pcs, sum(dt_isi_tally.volume) as volume, sum(dt_lpb.total_harga) as total_harga'))
            ->where('hd_lpb_id', '=', $id)
            ->where('dt_lpb.status', '=', true)
            ->where('dt_isi_tally.status', '=', true)
            ->groupBy('mst_jenis_kayu.jenis_kayu', 'mst_deskripsi_tallysheet.nama_deskripsi')
            ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
            ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_tally.deskripsi_id')
            // ->join('mst_harga', 'mst_harga.harga_id', '=', 'dt_lpb.harga_id')
            ->orderBy('dt_lpb.created_by')->get();


    //    dd($jumlah);

        $afkir = DB::table('dt_isi_afkir')
            ->select('dt_isi_afkir.*', 'mst_jenis_kayu.jenis_kayu')
            ->where('hd_lpb_id', '=', $id)
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_afkir.jenis_kayu_id')
            ->orderBy('dt_isi_afkir.created_by')->get();


        // dd($model);

        $now    = new \DateTime();
        $now = date_format($now, 'd/m/Y h:i:s');
        // $model =  HdLpb::find($id);


        $header = view('produksi/lpb/setup-lpb.header-pdf', [
            'title' => trans('menu.lpb-pdf-report') . "#" . $now,
            "model"         => $model,
            // "supplierOptions" => SupplierService::getActiveSupplier(),
        ])->render();
        \PDF::setHeaderCallback(function ($pdf) use ($header) {
            $pdf->writeHTML($header);
        });


        // $footer = view('produksi/lpb/setup-lpb.footer-pdf', [
        //     'title' => trans('menu.lpb-pdf-report')."#".$now,
        //     "model"         => $model,
        //     // "supplierOptions" => SupplierService::getActiveSupplier(),
        // ])->render();
        // \PDF::setFooterCallback(function($pdf) use ($footer) {
        //     $pdf->writeHTML($footer);
        // });

        // Custom Footer
        \PDF::setFooterCallback(function ($pdf) {

            // Position at 15 mm from bottom
            $pdf->SetY(-15);
            // Set font
            $pdf->SetFont('helvetica', 'I', 8);
            // Page number
            $pdf->Cell(0, 5, 'FR/09/09/SUB - 25/08/2016 - 00 -  ' . $pdf->getAliasNumPage() . '/' . $pdf->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
        });

        $html = view('produksi/lpb/setup-lpb.lpb-pdf-report', [
            "isi"         => $isi,
            // "total"         => $total,
            "afkir"       => $afkir,
            "jumlah"        => $jumlah,
            "supplierOptions" => SupplierService::getActiveSupplier(),
            "woodOptions"   => WoodService::getActiveWood(),


        ])->render();

        $pdf = new TCPDF();
        $pdf::SetTitle('LAPORAN PENERIMAAN BAHAN SAWN TIMBER-' . $model->nomor_lpb . '-' . $now);
        ob_end_clean();

        $pdf::SetTopMargin(41);

        $pdf::SetTopMargin(41);

        $pdf::AddPage();
        $pdf::writeHTML($html, true, false, true, false, '');
        $pdf::Output('LAPORAN PENERIMAAN BAHAN SAWN TIMBER-' . $model->nomor_lpb . '-' . $now . '.pdf');
        // $pdf::Output($query->container_number.' '.$query->created_date.'.pdf');
        // $pdf->SetFont('Helvetica', '', 9);



    }


    public function reportlpb2pdf(Request $request, $id)
    {

        $model = \DB::table('hd_lpb')
            ->select('hd_lpb.*', 'mst_supplier.supplier_name', 'mst_supplier.pic')
            ->where('hd_lpb.hd_lpb_id', '=', $id)

            ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_lpb.supplier_id')
            ->orderBy('hd_lpb.hd_lpb_id')->first();



            $isi = DB::table('dt_lpb')
            ->select('dt_lpb.dt_lpb_id', 'mst_jenis_kayu.jenis_kayu', 'tally_table.tally_no', 'mst_deskripsi_tallysheet.nama_deskripsi', 'dt_lpb.tally_id', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 'dt_isi_tally.panjang', 'dt_isi_tally.pcs', 'dt_isi_tally.volume', 'mst_harga.harga', 'dt_lpb.total_harga', 'dt_lpb.harga_satuan', 'dt_lpb.note', 'dt_lpb.total_harga_edit')
            ->where('hd_lpb_id', '=', $id)
            ->where('dt_lpb.status', '=', true)
            ->where('dt_isi_tally.status', '=', true)
            ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
            ->join('tally_table', 'tally_table.tally_id', '=', 'dt_isi_tally.tally_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
            ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_tally.deskripsi_id')
            ->join('mst_harga', 'mst_harga.harga_id', '=', 'dt_lpb.harga_id')
            ->orderBy('tally_table.tally_no')->get();

// dd($isi);
    

        $jumlah = DB::table('dt_lpb')
            ->select( 'mst_jenis_kayu.jenis_kayu',  'mst_deskripsi_tallysheet.nama_deskripsi', DB::raw('sum(dt_isi_tally.pcs) as pcs, sum(dt_isi_tally.volume) as volume, sum(dt_lpb.total_harga) as total_harga'))
            ->where('hd_lpb_id', '=', $id)
            ->where('dt_lpb.status', '=', true)
            ->where('dt_isi_tally.status', '=', true)
            ->groupBy('mst_jenis_kayu.jenis_kayu', 'mst_deskripsi_tallysheet.nama_deskripsi')
            ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
            ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_tally.deskripsi_id')
            // ->join('mst_harga', 'mst_harga.harga_id', '=', 'dt_lpb.harga_id')
            ->orderBy('dt_lpb.created_by')->get();


        $total = DB::table('dt_lpb')
            ->select('mst_jenis_kayu.jenis_kayu', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 'dt_isi_tally.panjang', DB::raw('sum(dt_isi_tally.pcs) as pcs, sum(dt_isi_tally.volume) as volume')) //, 'dt_isi_tally.volume'
            ->where('hd_lpb_id', '=', $id)
            ->where('dt_lpb.status', '=', true)
            ->where('dt_isi_tally.status', '=', true)
            ->groupBy('mst_jenis_kayu.jenis_kayu', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 'dt_isi_tally.panjang')
            ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
            // ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_tally.deskripsi_id')
            ->join('mst_harga', 'mst_harga.harga_id', '=', 'dt_lpb.harga_id')
            ->orderBy('dt_lpb.created_by')->get();

        // $jumlah = DB::table('dt_lpb')
        //     ->select('mst_jenis_kayu.jenis_kayu', 'mst_deskripsi_tallysheet.nama_deskripsi', DB::raw('sum(dt_isi_tally.pcs) as pcs, sum(dt_isi_tally.volume) as volume'))
        //     ->where('hd_lpb_id', '=', $id)
        //     ->where('dt_lpb.status', '=', true)
        //     ->where('dt_isi_tally.status', '=', true)
        //     ->groupBy('mst_jenis_kayu.jenis_kayu', 'mst_deskripsi_tallysheet.nama_deskripsi')
        //     ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
        //     ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
        //     ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_tally.deskripsi_id')
        //     ->orderBy('dt_lpb.created_by')->get();


        // dd($jumlah);

        $afkir = DB::table('dt_isi_afkir')
            ->select('dt_isi_afkir.*', 'mst_jenis_kayu.jenis_kayu')
            ->where('hd_lpb_id', '=', $id)
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_afkir.jenis_kayu_id')
            ->orderBy('dt_isi_afkir.created_by')->get();


        // dd($model);

        $now    = new \DateTime();
        $now = date_format($now, 'd/m/Y h:i:s');
        // $model =  HdLpb::find($id);


        $header = view('produksi/lpb/setup-lpb.header-pdf', [
            'title' => trans('menu.lpb-pdf-report') . "#" . $now,
            "model"         => $model,
            // "supplierOptions" => SupplierService::getActiveSupplier(),
        ])->render();
        \PDF::setHeaderCallback(function ($pdf) use ($header) {
            $pdf->writeHTML($header);
        });


        // $footer = view('produksi/lpb/setup-lpb.footer-pdf', [
        //     'title' => trans('menu.lpb-pdf-report')."#".$now,
        //     "model"         => $model,
        //     // "supplierOptions" => SupplierService::getActiveSupplier(),
        // ])->render();
        // \PDF::setFooterCallback(function($pdf) use ($footer) {
        //     $pdf->writeHTML($footer);
        // });

        // Custom Footer
        \PDF::setFooterCallback(function ($pdf) {

            // Position at 15 mm from bottom
            $pdf->SetY(-15);
            // Set font
            $pdf->SetFont('helvetica', 'I', 8);
            // Page number
            $pdf->Cell(0, 5, 'FR/09/09/SUB - 25/08/2016 - 00 -  ' . $pdf->getAliasNumPage() . '/' . $pdf->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
        });

        $html = view('produksi/lpb/setup-lpb.lpb-pdf-report', [
            "isi"         => $isi,
            "total"         => $total,
            "afkir"       => $afkir,
            "jumlah"        => $jumlah,
            "supplierOptions" => SupplierService::getActiveSupplier(),
            "woodOptions"   => WoodService::getActiveWood(),


        ])->render();

        $pdf = new TCPDF();
        $pdf::SetTitle('LAPORAN PENERIMAAN BAHAN SAWN TIMBER-' . $model->nomor_lpb . '-' . $now);
        ob_end_clean();

        $pdf::SetTopMargin(41);

        $pdf::SetTopMargin(41);

        $pdf::AddPage();
        $pdf::writeHTML($html, true, false, true, false, '');
        $pdf::Output('LAPORAN PENERIMAAN BAHAN SAWN TIMBER-' . $model->nomor_lpb . '-' . $now . '.pdf');
        // $pdf::Output($query->container_number.' '.$query->created_date.'.pdf');
        // $pdf->SetFont('Helvetica', '', 9);



    }

    public function reportlpbpdfaccurate(Request $request, $id)
    {

        $model = \DB::table('hd_lpb')
            ->select('hd_lpb.*', 'mst_supplier.supplier_name', 'mst_supplier.pic')
            ->where('hd_lpb.hd_lpb_id', '=', $id)
            ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_lpb.supplier_id')
            ->orderBy('hd_lpb.hd_lpb_id')->first();



        $total = DB::table('dt_lpb')
            ->select('mst_jenis_kayu.jenis_kayu', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 'dt_isi_tally.panjang', 'dt_lpb.harga_satuan', 'mst_harga.harga', DB::raw('sum(dt_isi_tally.pcs) as pcs, sum(dt_isi_tally.volume) as volume, sum(dt_lpb.total_harga) as total_harga')) //, 'dt_isi_tally.volume'
            ->where('hd_lpb_id', '=', $id)
            ->where('dt_lpb.status', '=', true)
            ->where('dt_isi_tally.status', '=', true)
            ->groupBy('mst_jenis_kayu.jenis_kayu', 'dt_isi_tally.tinggi', 'dt_isi_tally.lebar', 'dt_isi_tally.panjang', 'dt_lpb.harga_satuan', 'mst_harga.harga')
            ->join('dt_isi_tally', 'dt_isi_tally.isi_tally_id', '=', 'dt_lpb.isi_tally_id')
            ->join('mst_jenis_kayu', 'mst_jenis_kayu.jenis_kayu_id', '=', 'dt_isi_tally.jenis_kayu_id')
            // ->join('mst_deskripsi_tallysheet', 'mst_deskripsi_tallysheet.deskripsi_id', '=', 'dt_isi_tally.deskripsi_id')
            ->join('mst_harga', 'mst_harga.harga_id', '=', 'dt_lpb.harga_id')
            ->orderBy('dt_lpb.created_date')->get();

        
        // dd($total);



        //dd($total);

        $now    = new \DateTime();
        $now = date_format($now, 'd/m/Y h:i:s');
        // $model =  HdLpb::find($id);


        $header = view('produksi/lpb/setup-lpb.header-pdf', [
            'title' => trans('menu.lpb-pdf-report') . "#" . $now,
            "model"         => $model,
            // "supplierOptions" => SupplierService::getActiveSupplier(),
        ])->render();
        \PDF::setHeaderCallback(function ($pdf) use ($header) {
            $pdf->writeHTML($header);
        });

        $html = view('produksi/lpb/setup-lpb.lpb-pdf-report-accurate', [

            "total"         => $total,

            "supplierOptions" => SupplierService::getActiveSupplier(),
            "woodOptions"   => WoodService::getActiveWood(),


        ])->render();

        $pdf = new TCPDF();
        $pdf::SetTitle('LAPORAN PENERIMAAN BAHAN SAWN TIMBER-' . $model->nomor_lpb . '-' . $now);
        ob_end_clean();

        $pdf::AddPage();
        $pdf::writeHTML($html, true, false, true, false, '');
        $pdf::Output('LAPORAN PENERIMAAN BAHAN SAWN TIMBER-' . $model->nomor_lpb . '-' . $now . '.pdf');

        // $pdf::Output($query->container_number.' '.$query->created_date.'.pdf');
        // $pdf->SetFont('Helvetica', '', 9);



    }



    public function Status(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $model = Status::find($id);

        if ($model === null) {
            abort('404');
        }

        return view('produksi/lpb/setup-lpb.add', [
            "model"         => $model,
            "resources"     => config('app.resources'),
        ]);
    }

    public function edit(Request $request, $id)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add'])) {
            abort('403');
        }


        $model = LPB::find($id);

        if ($model === null) {
            abort('404');
        }

        // $query = DB::table('hd_lpb')
        // ->select('hd_lpb.*', 'tally_table.tally_no')
        //         ->join('tally_table', 'tally_table.tally_id', '=', 'hd_lpb.tally_id')
        //      //   ->join('mst_supplier', 'mst_supplier.supplier_id', '=', 'hd_lpb.supplier_id')
        // ->orderBy('hd_lpb.created_by');

        return view('produksi/lpb/setup-lpb.add', [
            "model"         => $model,
            // "models"         => $query->paginate(10),
            // "tallyOptions"   => TallyService::getActiveTally(),
            "supplierOptions" => SupplierService::getActiveSupplier(),
            "arrivallocationOptions"    => ArrivalLocationService::getActiveArrivalLocation(),
            "url"           => self::URL,
            "resource"      => self::RESOURCE,
        ]);
    }



    public function save(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add']) && !\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $id     = intval($request->get('id', 0));

        $this->validate($request, [
            'supplier_id'            => 'required|integer',
            'lokasi_kedatangan_id'            => 'required|integer',
            'nomor_lpb' => 'required|max:255|unique:hd_lpb,nomor_lpb,' . $id . ',hd_lpb_id',
            'tanggal_kedatangan' => 'required|date',
            'nomor_po' => 'required|max:255',
            'nomor_sj' => 'required|max:255',
            'plat_mobil' => 'required|max:255',



        ]);

        DB::beginTransaction();


        try {

            $now    = new \DateTime();
            if (empty($id)) {
                $model  = new LPB();
                $model->created_by      = \Auth::user()->id;
                $model->created_date    = $now;
            } else {
                $model  = LPB::find($id);
                $model->modified_by   = \Auth::user()->id;
                $model->modified_date = $now;
            }


            $model->nomor_lpb   = $request->get('nomor_lpb');
            $model->tanggal_kedatangan   = $request->get('tanggal_kedatangan');
            $model->supplier_id   = $request->get('supplier_id');
            $model->lokasi_kedatangan_id   = $request->get('lokasi_kedatangan_id');
            $model->nomor_po   = $request->get('nomor_po');
            $model->nomor_sj   = $request->get('nomor_sj');
            $model->plat_mobil   = $request->get('plat_mobil');
            $model->tanggal_afkir   = $request->get('tanggal_afkir');
            $model->plat_afkir   = $request->get('plat_afkir');

            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('fields.add-lpb') . ' ' . ''])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('fields.add-lpb') . ' ' . ''])
            );
        }

        return redirect(route('setup-lpb-index'));
    }

    public function savedetail(Request $request)
    {
        if (!\Gate::allows('access', [self::RESOURCE, 'add']) && !\Gate::allows('access', [self::RESOURCE, 'update'])) {
            abort('403');
        }

        $id     = intval($request->get('hd_lpb_id', 0));

        $this->validate($request, [
            'hd_lpb_id'            => 'required|integer',
            'tally_id'            => 'required|integer',
        ]);

        DB::beginTransaction();
        try {

            $now    = new \DateTime();
            if (empty($id)) {
                $model  = new LPBDetail();
                $model->created_by      = \Auth::user()->id;
                $model->created_date    = $now;
            } else {
                $model  = LPBDetail::find($id);
                $model->modified_by   = \Auth::user()->id;
                $model->modified_date = $now;
            }


            $model->nomor_lpb               = $request->get('nomor_lpb');
            $model->tanggal_kedatangan      = $request->get('tanggal_kedatangan');
            $model->supplier_id             = $request->get('supplier_id');
            $model->nomor_po                = $request->get('nomor_po');
            $model->nomor_sj                = $request->get('nomor_sj');
            $model->plat_mobil              = $request->get('plat_mobil');


            $model->save();
            DB::commit();

            $request->session()->flash(
                'successMessage',
                trans('message.saved-message', ['variable' => trans('fields.add-lpb') . ' ' . ''])
            );
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash(
                'errorMessage',
                trans('message.failed-message', ['variable' => trans('fields.add-lpb') . ' ' . ''])
            );
        }

        return redirect(route('setup-lpb-index'));
    }
}
