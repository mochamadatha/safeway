<?php

namespace App\Service\Master;

class LocationOutputKDService
{
    public static function getActiveLocationOutputKD()
    {
        return \DB::table('mst_lokasi_outputkd')
            ->where('status', '=', true)
            ->orderBy('lokasi_outputkd')
            ->get();
    }
}
