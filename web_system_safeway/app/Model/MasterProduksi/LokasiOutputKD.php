<?php

namespace App\Model\MasterProduksi;

use Illuminate\Database\Eloquent\Model;

class LokasiOutputKD extends Model
{
    protected $table        = 'mst_lokasi_outputkd';
    protected $primaryKey   = 'lokasi_outputkd_id';
    public $timestamps      = false;
}
